#!/usr/bin/env python

import sys, os 
from waflib import Utils

def options(opt):
	opt.load('compiler_cxx')
	opt.load('compiler_c')

def configure(conf):
	conf.load('compiler_cxx')
	conf.load('compiler_c')

	#conf.check_cxx(lib="glfw",
	#            errmsg="Please install the glfw library",
	#            mandatory=True)
	#conf.check_cxx(lib="glm",
	#            errmsg="Please install the glm library",
	#            mandatory=True)
	#conf.check_cxx(lib="openni",
	#            errmsg="Please install the openni2 library",
	#            mandatory=True)
	#conf.check_cxx(lib="fltk",
	#            errmsg="Please install the fltk library",
	#            mandatory=True)
	#conf.check_cxx(lib="opencv2",
	#            errmsg="Please install the opencv library",
	#            mandatory=True)


	#platform specific
	if sys.platform == 'darwin': 
		conf.env.INCLUDES_OS = ['/opt/local/include',
				'/opt/local/include/libxml2',
				'/opt/local/include/opencv4',
				'/opt/local/include/opencv3',
				'/opt/local/include/openni']
		conf.env.LIBPATH_OS = ['/opt/local/lib','/opt/local/lib/opencv3']
		conf.env.LIB_OS = ['m', 'xml2', 'fltk', 'fltk_images',
				'glfw', 'glew','OpenNI2',
				'opencv_core', 'opencv_highgui', 
				'opencv_calib3d', 'opencv_imgproc', 
				'opencv_imgcodecs', 'opencv_videoio', 
				'opencv_aruco', 'opencv_features2d',
				'assimp']
		conf.env.FRAMEWORK_OS = ['Cocoa','OpenGL', 'AGL', 'Carbon', 
				'Accelerate', 'IOKit','System', 'AppKit',
				'CoreFoundation']
		conf.env.DEFINES_OS  = ['OSX=1', 'POSIX=1']
	elif sys.platform == 'win32' or sys.platform == 'cygwin':
		conf.env.INCLUDES_OS = ['os/win/include/', 'C:\MinGW\include']
		conf.env.LIBPATH_OS = [os.path.join(os.getcwd(), 'os/win/lib/')]
		conf.env.LIB_OS = ['m', 'pthreadGC1',
				'ws2_32', 'xml2', 'GLU', 'GL',
				'OpenNI2', 'fltk', 'fltk_gl', 
				'opencv_core', 'opencv_highgui', 
				'opencv_calib3d', 'opencv_imgproc']
	else : #linux
		conf.env.INCLUDES_OS = ['/usr/include', '/usr/local/include',
				'/usr/include/libxml2', 
				'/usr/include/opencv4',
				'/usr/local/include/opencv4',
				'/usr/local/include/openni2',
				'/usr/include/openni2',
				'build/']
		conf.env.LIB_OS = ['jpeg', 'X11', 'm',
				'xml2', 'GLU', 'GL', 'pthread',
				'glfw', 'GLEW', 
				'OpenNI2', 'fltk', 'fltk_images',
				'opencv_core', 'opencv_highgui', 
				'opencv_calib3d', 'opencv_imgproc', 
				'opencv_imgcodecs', 'opencv_videoio', 
				'opencv_aruco', 'opencv_features2d',
				'assimp', 'rtaudio']
		conf.env.LIBPATH_OS = ['/usr/local/lib/']
		conf.env.DEFINES_OS  = ['POSIX=1','GL43=1', 'LINUX=1']

	#release specific
	conf.env.CXXFLAGS = ['-O3', '-w', '-std=c++2a'] 
	conf.env.DEFINES  = ['DEBUG(x)=//x']

	#debug specific
	conf.setenv('debug', env=conf.env.derive())
	conf.env.CXXFLAGS = ['-g', '-Wall', '-std=c++2a']
	conf.env.DEFINES  = ['DEBUG(x)=std::cout<< x <<std::endl;']

def build(bld):
	macApp = False
	if sys.platform == 'win32' or sys.platform == 'msys' :
		bld.objects(
				source  = bld.path.ant_glob('src/osc/oscPack/ip/win32/*.cpp')
				+bld.path.ant_glob('src/osc/oscPack/ip/*.cpp')
				+bld.path.ant_glob('src/osc/oscPack/osc/*.cpp'),
				use     = ['OS'],
				target  = 'oscpack')
	else : 
		bld.objects(
				source  = bld.path.ant_glob('src/osc/oscPack/ip/posix/*.cpp')
				+bld.path.ant_glob('src/osc/oscPack/ip/*.cpp')
				+bld.path.ant_glob('src/osc/oscPack/osc/*.cpp'),
				use     = ['OS'],
				target  = 'oscpack')

	if sys.platform == 'darwin': 
		installPath = '/opt/bin/'
		macApp = 'True'

	#Create shaders.h.in file to integrate shaders in binary
	shaders = ""
	for s in ["render", "post", "select", "slice", "calib"] :
		for v in ["43", "30"] :
			shaders += 'std::string '+s+v+'FS = R"('\
				+ Utils.readf('src/shaders/'+s+v+'.fs')\
				+ ')";\n\n'
			shaders += 'std::string '+s+v+'VS = R"('\
				+ Utils.readf('src/shaders/'+s+v+'.vs')\
				+ ')";\n\n'

	Utils.writef('build/shaders.h.in', shaders)
	bld(rule='touch shaders.h.in', 
			source=bld.path.ant_glob('src/shaders/*'),
			target="shaders.h.in")

	bld.program(
			source       = bld.path.ant_glob('src/gui/*.cpp')
			+ bld.path.ant_glob('src/modules/*.cpp')
			+ bld.path.ant_glob('src/geoms/*.cpp')
			+ bld.path.ant_glob('src/osc/*.cpp')
			+ bld.path.ant_glob('src/audio/*.cpp')
			+ bld.path.ant_glob('src/*.cpp'),
			use          = ['OS','ringbuf','oscpack', 'shaders.h.in'],
			target       = 'rivill'+bld.variant,
			vnum         = '0.0.1',
			mac_app      = macApp,
			mac_plist     = 'data/Info.plist',
			mac_resources = 'data/rivill.icns',
			)

	bld.program(
			source="src/utils/create_marker.cpp",
			use="OS",
			target="rivill_create_marker")

	bld.install_files('${PREFIX}/share/applications', 
			['data/rivill.desktop'])
	bld.install_files('${PREFIX}/share/mime/packages', 
			['data/rivill.xml'])
	bld.install_files('${PREFIX}/share/icons', 
			['data/rivill.png'])

from waflib.Build import BuildContext, CleanContext
class debug(BuildContext): 
	cmd = 'debug'
	variant = 'debug' 

