/***************************************************************************
 *  OscListener.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef OscListener_h
#define OscListener_h

#include "../modules/Listener.hpp"

class OscListener: public Listener {
    public:
        OscListener();
        OscListener(const std::string& adPort);
        virtual ~OscListener(){}
        virtual void setAttribute(Attribute* att);
        virtual void update(std::vector<float>& floatValues, 
                            std::vector<int>& intValues,
                            std::vector<bool>& boolValues,
                            std::vector<std::string>& stringValues);
        void updateNet();
        virtual void setParameters(const std::vector<std::string>& par);

    protected:
        std::string m_adress;
        unsigned int m_port;
        std::string m_url;
        bool m_grouped;
};

#endif
