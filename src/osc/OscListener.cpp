/***************************************************************************
 *  OscListener.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "OscListener.hpp"
#include <iostream>
#include <sstream>
#include "OscManager.hpp"
#include "../modules/Attribute.hpp"

using namespace std;

OscListener::OscListener(const std::string& adPort):Listener() {
    m_listenerType="OscListener";
    m_parameters.push_back("");
    m_parametersValues.push_back(vector<string>());
    m_parameters.push_back(adPort);
    m_parametersValues.push_back(vector<string>());

    vector<string> vals;
    vals.push_back("grouped");
    vals.push_back("separated");
    m_parameters.push_back(vals[0]);
    m_parametersValues.push_back(vals);
    m_attribute=NULL;
    m_grouped=true;
    updateNet();
}

OscListener::OscListener():Listener() {
    m_listenerType="OscListener";
    m_parameters.push_back("");
    m_parametersValues.push_back(vector<string>());
    m_parameters.push_back("127.0.0.1:7000");
    m_parametersValues.push_back(vector<string>());

    vector<string> vals;
    vals.push_back("grouped");
    vals.push_back("separated");
    m_parameters.push_back(vals[0]);
    m_parametersValues.push_back(vals);
    m_attribute=NULL;
    m_grouped=true;
    updateNet();

	//FIXME, define listener attributes with different types
}

void OscListener::setAttribute(Attribute* att) { 
    m_attribute=att;
    updateNet();
}

void OscListener::setParameters(const std::vector<std::string>& params) {
    Listener::setParameters(params);
    updateNet();
}

void OscListener::updateNet() {
    if(m_parameters[0].size()==0) {
        if(m_attribute!=NULL) {
            m_parameters[0]=m_attribute->getFullName();
        }
    }
    m_url = m_parameters[0];
    size_t sep = m_parameters[1].find(':');
    if(sep!=string::npos) {
        istringstream iss;
        iss.str(m_parameters[1].substr(sep+1));
        iss >> m_port;
        m_adress=m_parameters[1].substr(0, sep);
    }
    if(m_parameters[2].compare("grouped")==0) {
        m_grouped=true;
    }
    else {
        m_grouped=false;
    }
}

void OscListener::update(vector<float>& floatValues, 
                         vector<int>& intValues,
                         vector<bool>& boolValues,
                         vector<string>& stringValues) {
	//FIXME get OutputManager -> selected output
    OscManager* osc = OscManager::getInstance();
    if(m_grouped) {
		//if(floatValues.size()<10) {
			osc->sendMessage(m_parameters[1], 
							 m_adress, 
							 m_port, 
							 m_url, 
							 floatValues, 
							 intValues,
							 boolValues,
							 stringValues);
		//}
		/*else { //FIXME
			osc->sendBlobMessage(m_parameters[1],
								 m_adress,
								 m_port,
								 m_url,
								 floatValues);
		}*/
    }
    else {
        int vi = 1;
        for(float& v : floatValues) {
            ostringstream oss;
            oss << vi;
            osc->sendMessage(m_parameters[1], 
                             m_adress, 
                             m_port, 
                             m_url+"/"+oss.str(), 
                             vector<float>(1, v), 
                             intValues,
                             boolValues,
                             stringValues);
            vi++;
        }

        vi=1;
        for(int& i : intValues) {
            ostringstream oss;
            oss << vi;
            osc->sendMessage(m_parameters[1], 
                             m_adress, 
                             m_port, 
                             m_url+"/"+oss.str(), 
                             floatValues,
                             vector<int>(1, i), 
                             boolValues,
                             stringValues);
            vi++;
        }

        vi=1;
        for(bool b : boolValues) {
            ostringstream oss;
            oss << vi;
            osc->sendMessage(m_parameters[1], 
                             m_adress, 
                             m_port, 
                             m_url+"/"+oss.str(), 
                             floatValues,
                             intValues,
                             vector<bool>(1, b), 
                             stringValues);
            vi++;
        }

        vi=1;
        for(string& s : stringValues) {
            ostringstream oss;
            oss << vi;
            osc->sendMessage(m_parameters[1], 
                             m_adress, 
                             m_port, 
                             m_url+"/"+oss.str(), 
                             floatValues,
                             intValues,
                             boolValues,
                             vector<string>(1, s));
            vi++;
        }
    }
}



