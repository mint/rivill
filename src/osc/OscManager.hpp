/***************************************************************************
 *	OscManager.hpp
 *	Part of OscManager
 *	2015-  Florent Berthaut
 *	hitmuri.net
 ****************************************************************************/
/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef OscManager_h
#define OscManager_h

#include <string>
#include <vector>
#include <deque>
#include <map>
#include <thread>
#include <mutex>
#include "oscPack/ip/IpEndpointName.h"
#include "oscPack/osc/OscReceivedElements.h"
#include "oscPack/osc/OscOutboundPacketStream.h"
#include "oscPack/ip/UdpSocket.h"
#include "oscPack/osc/OscPacketListener.h"

#define IP_MTU_SIZE 10000

struct OscMessage {
	std::string m_name;
	std::vector<float> m_floatValues;
	std::vector<int> m_intValues;
	std::vector<bool> m_boolValues;
	std::vector<std::string> m_strValues;
};

struct OscStream {
	std::string m_id;
	std::string m_adress;
	int m_port;
	char m_buffer[IP_MTU_SIZE];
	osc::OutboundPacketStream* m_stream;
	bool m_bundleStarted;
};

class OscManager :	public osc::OscPacketListener {
	public:
		static OscManager* getInstance();
		~OscManager();

		void init();
		int run();
		void update();
		void stop();
		virtual void ProcessMessage(const osc::ReceivedMessage& m,
									const IpEndpointName& remoteEndpoint);
		
		void sendMessage(const std::string& id,
						 const std::string& adress,
						 const int& port,
						 const std::string& name, 
						 const std::vector<float>& fValues,
						 const std::vector<int>& iValues,
						 const std::vector<bool>& bValues,
						 const std::vector<std::string>& sValues);

	public:
		UdpListeningReceiveSocket* m_socket;

	private:
		OscManager();

	private:
		size_t m_messageSize;
		char m_buffer[IP_MTU_SIZE];
		std::deque<OscMessage> m_messages;
		std::map<std::string, OscStream*> m_streamsMap;
		std::vector<OscStream*> m_streamsVec;
		std::thread* m_thread;
		std::mutex m_msgLock;

};

#endif

