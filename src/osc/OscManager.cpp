/***************************************************************************
 *  OscManager.cpp
 *  Part of OscManager
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "OscManager.hpp" 

#include <iostream>
#include <pthread.h>

#include "../Reveal.hpp"
#include "OscListener.hpp"

using namespace std;

OscManager* OscManager::getInstance() {
    static OscManager rev;
    return &rev;
}

OscManager::OscManager(): osc::OscPacketListener() {}
OscManager::~OscManager() {}

void* oscThreadFunction(void* pvParam) {
    OscManager *pThis=(OscManager*)pvParam;
    pThis->m_socket->Run();
    return 0;
}

void OscManager::init() {
    //initialize osc input 
    try{
        m_socket = new UdpListeningReceiveSocket(
                            IpEndpointName(IpEndpointName::ANY_ADDRESS, 8327),
                            (osc::OscPacketListener*)this);
	m_thread = new std::thread(oscThreadFunction, this);
    }
    catch(const exception& e) {
        DEBUG("Error in the OscManager "<<e.what());
    }
}

void OscManager::update() {
    //retrieve osc messages
	if(m_msgLock.try_lock()) {
		while(m_messages.size()>0) {
			OscMessage firstMess = m_messages.front();
			m_messages.pop_front();
			OscMessage *mess = &firstMess;

			std::string name = mess->m_name;
			if(Reveal::getInstance()->getDebugOSC()) {
				cout<<"OSC: "<<name<<endl;
			}
			//try attributes first then modules 
			Attribute *att = Reveal::getInstance()->findAttribute(name);
			if(att) {
				if(att->getAccessibility()==Attribute::GLOBAL 
						|| att->getAccessibility()==Attribute::OUTPUT) {

					//test first register / unregister listeners 
					if(mess->m_strValues.size()>0 
							&& mess->m_strValues[0].compare("register")==0) {
						att->addListener(new OscListener(mess->m_strValues[1]));
					}
					else if(mess->m_strValues.size()>0 
							&& mess->m_strValues[0].compare("unregister")==0) {
						//FIXME
					}
					else  //test set attributes
					if(att->getType()==Attribute::FLOAT_ATTRIBUTE 
						 || att->getType()==Attribute::ACTION_FLOAT_ATTRIBUTE) {
						if(mess->m_floatValues.size()>0) {
							att->setFloats(mess->m_floatValues);
						}
						else if(mess->m_intValues.size()>0) {
							att->setInts(mess->m_intValues);
						}
					}
					else if((att->getType()==Attribute::INT_ATTRIBUTE 
							 || att->getType()==Attribute::ACTION_INT_ATTRIBUTE)
							&& mess->m_intValues.size()>0) {
						att->setInts(mess->m_intValues);
					}
					else if(att->getType()==Attribute::BOOL_ATTRIBUTE 
								&& mess->m_boolValues.size()>0) {
						att->setBools(mess->m_boolValues);
					}
					else if((att->getType()==Attribute::STRING_ATTRIBUTE 
						  || att->getType()==Attribute::ACTION_STRING_ATTRIBUTE)
							&& mess->m_strValues.size()>0) {
						att->setStrings(mess->m_strValues);
					}
					else if((att->getType()==Attribute::FILE_OPEN_ATTRIBUTE 
						   || att->getType()==Attribute::FOLDER_OPEN_ATTRIBUTE)
							&& mess->m_strValues.size()>0) {
						att->setFile(mess->m_strValues[0]);
					}
					else if(att->getType()==Attribute::ACTION_ATTRIBUTE){
						att->setAction();
					}
				}
			}
		}
		m_msgLock.unlock();
    }

    //send created bundles 
	/*
    vector<OscStream*>::iterator itStream = m_streamsVec.begin();
    for(; itStream!=m_streamsVec.end(); ++itStream) {
        if((*itStream)->m_bundleStarted) {
            try {
                *((*itStream)->m_stream) << osc::EndBundle;   
                UdpSocket socket;
                socket.SetEnableBroadcast(true);
                socket.SendTo(IpEndpointName((*itStream)->m_adress.c_str(), 
                                             (*itStream)->m_port),
                              (*itStream)->m_stream->Data(), 
                              (*itStream)->m_stream->Size());
            }
            catch(const exception& e) {
                cout<<"Error in the osc output "<<e.what()<<endl;
            }
        }
        (*itStream)->m_bundleStarted=false;
    }*/
}

void OscManager::stop() {

}

void OscManager::ProcessMessage(const osc::ReceivedMessage& mess,
                                const IpEndpointName& remoteEndpoint) {
    //OscMessage *newMessage = new OscMessage();
    OscMessage newMessage;
    try {
        newMessage.m_name=std::string(mess.AddressPattern());
        osc::ReceivedMessage::const_iterator itArg = mess.ArgumentsBegin();
        for(; itArg!=mess.ArgumentsEnd(); ++itArg) {
            if(itArg->IsFloat()) {
                newMessage.m_floatValues.push_back(itArg->AsFloatUnchecked());
                newMessage.m_boolValues.push_back((itArg->AsFloatUnchecked()));
            }
            else if( itArg->IsInt32() ) {
              newMessage.m_intValues.push_back(
                (float)(itArg->AsInt32Unchecked()));
              newMessage.m_boolValues.push_back((itArg->AsInt32Unchecked()));
            }
            else if( itArg->IsString() ) {
                newMessage.m_strValues.push_back(
                                            std::string(itArg->AsString()));
            }
        }
    } 
    catch(const exception& e) {
        cout<<"Exception when receiving osc message "<<e.what()<<endl;
    }

	m_msgLock.lock();
		m_messages.push_back(newMessage);
	m_msgLock.unlock();
	/*
    if(jack_ringbuffer_write_space(m_messagesRingBuffer)>=m_messageSize 
            && received) {
        jack_ringbuffer_write(m_messagesRingBuffer, 
                                (char*)(&newMessage),
                                    m_messageSize);    
    }
    else {
        delete newMessage;
    }*/
}

void OscManager::sendMessage(const string& id,
                             const string& adress,
                             const int& port,
                             const string& name, 
                             const vector<float>& fValues,
                             const vector<int>& iValues,
                             const vector<bool>& bValues,
                             const vector<string>& sValues) {
    //create the stream to the destination if it does not exist
    if(m_streamsMap.find(id)==m_streamsMap.end()) {
        OscStream* newStream = new OscStream();
        newStream->m_id=id;
        newStream->m_adress=adress;
        newStream->m_port=port;
        newStream->m_stream=new osc::OutboundPacketStream(newStream->m_buffer, 
                                                          IP_MTU_SIZE);
        newStream->m_bundleStarted=false;
        m_streamsMap[id]=newStream;
        m_streamsVec.push_back(newStream);
    }

    OscStream* stream = m_streamsMap[id];

    //start a bundle if it is not started
    if(!stream->m_bundleStarted) {
        stream->m_stream->Clear();
        *(stream->m_stream) << osc::BeginBundleImmediate;
        stream->m_bundleStarted=true;
    }

    //add a message 
    try {
        *(stream->m_stream) << osc::BeginMessage(name.c_str());
        vector<string>::const_iterator itS;    
        for(itS=sValues.begin(); itS!=sValues.end(); ++itS) {
            *(stream->m_stream)<<(*itS).c_str();
        }
        vector<float>::const_iterator itF;    
        for(itF=fValues.begin(); itF!=fValues.end(); ++itF) {
            *(stream->m_stream)<<(float)(*itF);
        }
        vector<int>::const_iterator itI;    
        for(itI=iValues.begin(); itI!=iValues.end(); ++itI) {
            *(stream->m_stream)<<(int)(*itI);
        }
        vector<bool>::const_iterator itB;    
        for(itB=bValues.begin(); itB!=bValues.end(); ++itB) {
            *(stream->m_stream)<<(bool)(*itB);
        }
        *(stream->m_stream) << osc::EndMessage;   
    }
    catch(const exception& e) {
        DEBUG("Error in the osc output "<<e.what());
    }
	//FIXME :not efficient, but bundles become too big because of voxels/histo
	//FIXME: change voxels/histo to blobs or test bundle size
	try {
		*(stream->m_stream) << osc::EndBundle;   
		UdpSocket socket;
		socket.SetEnableBroadcast(true);
		socket.SendTo(IpEndpointName(stream->m_adress.c_str(), 
									 stream->m_port),
					  stream->m_stream->Data(), 
					  stream->m_stream->Size());
        stream->m_bundleStarted=false;
	}
	catch(const exception& e) {
		cout<<"Error in the osc output "<<e.what()<<endl;
	}
}

