#version 130 

#define s2(a, b) temp = a; a = min(a, b); b = max(temp, b);
#define mn3(a, b, c)  s2(a, b); s2(a, c);
#define mx3(a, b, c)  s2(b, c); s2(a, c);
#define mnmx3(a, b, c) mx3(a, b, c); s2(a, b);
#define mnmx4(a, b, c, d) s2(a, b); s2(c, d); s2(a, c); s2(b, d);
#define mnmx5(a, b, c, d, e) s2(a,b); s2(c,d);mn3(a,c,e);mx3(b,d,e);
#define mnmx6(a,b,c,d,e,f) s2(a,d);s2(b,e);s2(c,f);mn3(a,b,c);mx3(d,e,f);
in vec3 vertex; 

uniform sampler2D depthCamTex;
uniform sampler2D depthCamTexFil;
uniform sampler2D markersTex;
uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 viewProjMat;
uniform float camXReso;
uniform float camYReso;
uniform float camXZFactor;
uniform float camYZFactor;
uniform int camFilter;
uniform int background;
uniform float camContThresh;
uniform float camMarkers;
uniform int depthType;
uniform int depthID;
uniform float outputCurs;
uniform float mirrored;

out vec3 viewRay;
out float kept;
flat out int outDepthID;

void main(void) {
	vec4 depthPos = vec4(0, 0, 0, 1);
	vec4 posWS = vec4(0, 0, 0, 1);
	//depth camera
	if(depthType==0) {
		vec2 coords = vertex.xy;
		//get marker if any
		if(camMarkers>0) {
			outDepthID = int(float(texture(markersTex,
							vec2(coords.x/camXReso,
								coords.y/camYReso)).r)
					* 255.0);
		}
		else {
			outDepthID=depthID+1;
		}
		//get depth pixel in camera texture
		float depth = 
			float(texture(depthCamTex,
						vec2(coords.x/camXReso,
							coords.y/camYReso)).r)
			* 65535.0;
		kept=1.0;
		float backgroundDepth=0;
		if(background>0) {
			float backDepth = float(texture(depthCamTex,
						vec2(coords.x/camXReso,
							coords.y/camYReso)).r)
				* 65535.0;
			if(backDepth<depth) {
				depth=backDepth;
				backgroundDepth=1;
			}
		}
		if(depth<=500.0 || depth>8000.0) {
			kept=0.0;
		}
		else if(backgroundDepth==0) {
			//filter out contours
			float maxD=camContThresh; 
			bool contour=false;
			for(int dX = -1; dX <= 1; ++dX) {
				for(int dY = -1; dY <= 1; ++dY) {
					if(abs(float(texture(depthCamTex,
										vec2((coords.x+dX)/camXReso,
											(coords.y+dY)/camYReso)).r)
								* 65535.0
								- depth) > maxD) {
						contour=true;
					}
				}
			}
			if(contour) {
				kept=0.0;
			}
			//see if needs filtering
			if(camFilter>0) {
				float filtDepth = float(texture(depthCamTexFil,
							vec2(coords.x/camXReso,
								coords.y/camYReso)).r)
					* 65535.0;
				//select either filtered version or current one
				//depending on movement 
				if(abs(filtDepth-depth)<camContThresh) {
					float filRatio = abs(filtDepth-depth)/camContThresh;
					depth=(1-filRatio)*filtDepth+(filRatio)*depth;
				}
			}
		}
		//test distance again
		if(depth<=500.0 || depth>8000.0) {
			kept=0.0;
		}
		//compute position in world units
		depthPos = vec4((vertex.x/camXReso-0.5)
				*depth*camXZFactor,
				(0.5-(vertex.y/camYReso))
				*depth*camYZFactor,
				depth,
				1.0);
		//get world pos
		posWS = modelMat * depthPos;
	}
	//other slicing shapes
	else {
		outDepthID=depthID+1;
		kept=1.0;
		depthPos=vec4(vertex, 1.0);
		posWS = modelMat * depthPos;
		if(mirrored<1) {
			posWS.x = -posWS.x;
		}
	}
	//keep/interpolate ray from view to each vertex
	viewRay = (posWS.xyz - vec3(viewMat[3][0],
				viewMat[3][1],
				viewMat[3][2]));
	gl_Position = viewProjMat * posWS;
};

