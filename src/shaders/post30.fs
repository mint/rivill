#version 130

uniform float viewportWidth;
uniform float viewportHeight;
uniform float postFilter;
uniform sampler2D renderTex;

out vec4 color;

void main(void) {
	float coordX = gl_FragCoord.x;
	float coordY = gl_FragCoord.y;
	ivec2 patSize = textureSize(renderTex, 0);
	vec3 sumCol = vec3(0,0,0);
	float cnt=0;
	for(float dX = coordX-postFilter; dX <= coordX+postFilter; ++dX) {
		for(float dY = coordY-postFilter; dY <= coordY+postFilter; ++dY) {
			if(dX>=0 && dX<viewportWidth 
					&& dY>=0 && dY<viewportHeight) {
				sumCol+=texelFetch(renderTex, 
						ivec2((dX-0.5)/viewportWidth*patSize.x,
							(dY-0.5)/viewportHeight*patSize.y),
						0).xyz; 
				cnt+=1;
			}
		}
	}
	color=vec4(sumCol.xyz/cnt, 1.0);
}
