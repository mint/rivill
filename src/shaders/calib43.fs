#version 430 

uniform float viewportWidth;
uniform float viewportHeight;
uniform float patW;
uniform float patPosX;
uniform float patPosY;
uniform float patNbX;
uniform float patNbY;
uniform float patBright;
uniform vec2 patOffset;
uniform vec2 patRange;

layout(binding=0) uniform sampler2D patternTex;

out vec4 color;

void main(void) {
	/*
	   float coordX = (gl_FragCoord.x-0.5)/viewportWidth;
	   float coordY = (gl_FragCoord.y-0.5)/viewportHeight;
	   ivec2 patSize = textureSize(patternTex, 0);
	   float pat = texelFetch(patternTex,
	   ivec2(coordX*float(patSize.x),
	   (1.0-coordY)*float(patSize.y)),
	   0).r;
	   color=vec4(pat, pat, pat, 1.0);
	//   color=vec4(1.0,1.0,1.0,1.0);
	 */
	//pixel is white by default
	color = vec4(patBright);
	float offX = gl_FragCoord.x - patPosX;
	float offY = (viewportHeight-gl_FragCoord.y) - patPosY;
	//black squares
	if(offX>=0 && offX<patW*(patNbX+1)
			&& offY>=0 && offY<patW*(patNbY+1)) {
		if((mod(offX,patW*2)<patW && mod(offY,patW*2)<patW) 
				|| (mod(offX,patW*2)>patW && mod(offY,patW*2)>patW)) {
			color = vec4(0, 0, 0, 1);
		}
	}
	//color in red the pattern range border
	float thick=2;
	float coordY = viewportHeight-gl_FragCoord.y;
	if(((abs(gl_FragCoord.x-patOffset.x)<thick
					|| abs(gl_FragCoord.x-(patOffset.x+patRange.x))<thick)
				&& coordY>patOffset.y 
				&& coordY<(patOffset.y+patRange.y))
			||((abs(coordY-patOffset.y)<thick
					|| abs(coordY-(patOffset.y+patRange.y))<thick)
				&& gl_FragCoord.x>patOffset.x 
				&& gl_FragCoord.x<(patOffset.x+patRange.x))) {
		color=vec4(1,0,0,1);
	}
};

