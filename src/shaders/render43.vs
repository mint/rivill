#version 430 

in vec3 vertex; 
in vec2 texCoords;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 viewProjMat;
uniform float mirrored;

out vec3 viewRay;
out vec2 uvCoords;

void main(void) {
	vec4 posWS = modelMat * vec4(vertex, 1.0);
	if(mirrored<1) {
		posWS.x = -posWS.x;
	}
	gl_Position = viewProjMat * posWS;
	viewRay = posWS.xyz - vec3(viewMat[3][0],
			viewMat[3][1],
			viewMat[3][2]);
	uvCoords=texCoords;
};

