#version 130

uniform sampler2D sliceTex;
uniform float viewportWidth;
uniform float viewportHeight;
uniform float shapeID;
uniform float shapeIDBit;
uniform float mirrored;
uniform float thickness;
uniform int surface;

out vec4 color;

void main(void) {
	float coordX = (gl_FragCoord.x-0.5)/viewportWidth;
	float coordY = (gl_FragCoord.y-0.5)/viewportHeight;
	ivec2 sliSize = textureSize(sliceTex, 0);
	vec4 sli = texelFetch(sliceTex,
			ivec2(coordX*float(sliSize.x),
				coordY*float(sliSize.y)),
			0);
	float pixZ=abs(gl_FragCoord.z);
	float cutZ=abs(sli.z);
	//  keep all ids of back surfaces after depth map
	if(pixZ>cutZ && cutZ>0) {
		if((mirrored<1 && !gl_FrontFacing)
				|| (mirrored>0 && gl_FrontFacing)) {
			color = vec4(shapeID, 0, 
					(abs(pixZ-cutZ)<thickness/1000.0 && surface>0)?1:0, 
					shapeIDBit);
		}
		else { //occlude back when front face also behind
			color = vec4(-shapeID, 0, 0, -shapeIDBit);
		}
	}
	else {
		discard;
	}
};

