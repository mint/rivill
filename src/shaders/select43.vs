#version 430

in vec3 vertex;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 viewProjMat;
uniform float mirrored;

void main(void) {
	vec4 posWS = modelMat * vec4(vertex, 1.0);
	if(mirrored<1) {
		posWS.x = -posWS.x;
	}
	gl_Position = viewProjMat * posWS;
}
