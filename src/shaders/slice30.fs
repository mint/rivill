#version 130 
 
in float kept;
in vec3 viewRay;
flat in int outDepthID;
 
out vec4 color;
 
void main(void) {
   float thresh=0.8;
   if(kept>=thresh) {
       color = vec4((kept-thresh)/(1.0-thresh),
                     float(outDepthID),
                     gl_FragCoord.z,
                     length(viewRay));
   }
   else {
       discard;
   }
//       color = vec4(1.0, 1.0, 0.0, 1.0);
};
