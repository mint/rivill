/***************************************************************************
 *  ModuleWidget.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "ModuleWidget.hpp"
#include "AttributeWidget.hpp"
#include <vector>

using namespace std;

ModuleWidget::ModuleWidget(int x, int y, 
                           int w, int h, 
                           Module* module): Fl_Toggle_Button(x,y,w,h,""),
                                            m_module(module) {
    m_label = "";
    for(int d=0; d<int(m_module->getDepth())-1; ++d) {
        m_label+="  ";
    }
    if(m_module->getDepth()>0) {
        m_label+="|-";
    }
    m_label+=m_module->getName();
    label(m_label.c_str());
    align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    box(FL_BORDER_BOX);
    color(FL_BACKGROUND2_COLOR, FL_SELECTION_COLOR);
}

void ModuleWidget::fillGroupWithAttributes(Fl_Pack* group) {
    vector<Attribute*>& attributes = m_module->editAttributes();
    vector<Attribute*>::iterator itAtt = attributes.begin();
    for(; itAtt!=attributes.end(); ++itAtt) {
       if((*itAtt)->getAccessibility()!=Attribute::HIDDEN) {
            group->add(new AttributeWidget(0, 0, group->w(), 30, *itAtt));
       }
    }
}


