/***************************************************************************
 *  AttributeWidget.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef AttributeWidget_h 
#define AttributeWidget_h

#include "../modules/Attribute.hpp"
#include <FL/Fl_Group.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Toggle_Button.H>
#include "GuiListener.hpp"

class AttributeWidget : public Fl_Group {
    public:
        AttributeWidget(int x, int y, int w, int h, Attribute* attribute);
        virtual ~AttributeWidget();

        static void statFolderSave(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbFolderSave(w);
        }    
        void cbFolderSave(Fl_Widget*);

        static void statFolderOpen(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbFolderOpen(w);
        }    
        void cbFolderOpen(Fl_Widget*);

        static void statFileOpen(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbFileOpen(w);
        }    
        void cbFileOpen(Fl_Widget*);

        static void statFileSave(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbFileSave(w);
        }    
        void cbFileSave(Fl_Widget*);

        static void statAction(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbAction(w);
        }    
        void cbAction(Fl_Widget*);

        static void statFloats(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbFloats(w);
        }    
        void cbFloats(Fl_Widget*);

        static void statInts(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbInts(w);
        }    
        void cbInts(Fl_Widget*);

        static void statStrings(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbStrings(w);
        }    
        void cbStrings(Fl_Widget*);
        static void statChoices(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbChoices(w);
        }    
        void cbChoices(Fl_Widget*);
        static void statChecks(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbChecks(w);
        }    
        void cbChecks(Fl_Widget*);

        static void statShowListeners(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbShowListeners(w);
        }    
        void cbShowListeners(Fl_Widget*);

        static void statAddListener(Fl_Widget* w, void* f){ 
            AttributeWidget *tmpf = static_cast<AttributeWidget*>(f);
            tmpf->cbAddListener(w);
        }    
        void cbAddListener(Fl_Widget*);

        void updateAttributeValues(std::vector<float>& floatValues, 
                                   std::vector<int>& intValues,
                                   std::vector<bool>& boolValues,
                                   std::vector<std::string>& stringValues);

    private:
        Attribute* m_attribute;
        std::string m_name;
        std::string m_fullName;
        unsigned int m_nbValues;
        std::vector<bool> m_stringIsChoice;
        Fl_Toggle_Button* m_listenersBut;
        std::vector<Fl_Value_Input*> m_valueInputs;
        std::vector<Fl_Input*> m_inputs;
        std::vector<Fl_Choice*> m_choices;
        std::vector<Fl_Toggle_Button*> m_checks;
        Fl_Button* m_button;
        Fl_Output* m_output;
        Fl_Button* m_addListenerBut;
        GuiListener* m_guiListener;
};

#endif

