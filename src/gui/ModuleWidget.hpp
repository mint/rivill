/***************************************************************************
 *  ModuleWidget.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef ModuleWidget_h 
#define ModuleWidget_h

#include "../modules/Module.hpp"
#include <FL/Fl_Toggle_Button.H>
#include <FL/Fl_Pack.H>

class ModuleWidget : public Fl_Toggle_Button {
    public:
        ModuleWidget(int x, int y, int w, int h, Module* module);
        virtual ~ModuleWidget() {}
        void fillGroupWithAttributes(Fl_Pack*);
        inline Module* getModule(){return m_module;}

    private:
        Module* m_module;
        std::string m_label;

};

#endif

