/***************************************************************************
 *  MainWindow.hpp
 *  Part of Rouages
 *  2013  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef MainWindow_h
#define MainWindow_h

#include <string>
#include <map>

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Toggle_Button.H>
#include <FL/Fl_Shared_Image.H>
#include <FL/Fl.H>
#include <FL/fl_ask.H>

#include "ModuleWidget.hpp"

class MainWindow: public Fl_Double_Window {
    public:
        static MainWindow* getInstance();
        ~MainWindow();
        void init();
        void refreshModules(bool clearAttributes=false, 
                            bool clearListeners=false);
        void displayAttributes(ModuleWidget*);

        static void statTree(Fl_Widget* w,void* f){ 
            MainWindow *tmpf = static_cast<MainWindow *>(f);
            tmpf->cbTree(w);
        }    
        void cbTree(Fl_Widget*);

        void cbOpen();
        void cbSave();
        void cbSaveAs();

        static void statQuit(Fl_Widget* w,void* f){ 
            MainWindow *tmpf = static_cast<MainWindow *>(f);
            tmpf->cbQuit();
        }    
        void cbQuit();
        int handle(int event);

        inline Fl_Pack* getListenersGroup(){return m_listeners;}
        void showListeners();
        void hideListeners();

    private:
        MainWindow();

    private:
        int m_treeWidth;
        int m_attributesWidth;
        Fl_Scroll* m_scrollModules;
        Fl_Pack* m_modules;
        Fl_Scroll* m_scrollAttributes;
        Fl_Pack* m_attributes;
        Fl_Scroll* m_scrollListeners;
        Fl_Pack* m_listeners;

        std::string m_fileName;
        std::string m_selectedModulePath;
        std::map<std::string, ModuleWidget*> m_moduleButtonsMap;
};

#endif

