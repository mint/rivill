/***************************************************************************
 *  AttributeWidget.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "AttributeWidget.hpp"
#include <iostream>
#include <FL/fl_draw.H>
#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Native_File_Chooser.H>
#include <FL/filename.H>

#include "../Reveal.hpp"
#include "MainWindow.hpp"
#include "ListenerWidget.hpp"
#include "GuiListener.hpp"

using namespace std;

AttributeWidget::AttributeWidget(int x, int y, 
                                 int w, int h, 
                                 Attribute* attribute):Fl_Group(x,y,w,h,""),
                                                       m_attribute(attribute) {
    m_name = m_attribute->getName();
    m_fullName = m_attribute->getFullName();
    m_nbValues = m_attribute->getNbValues();
    label(m_name.c_str());
    align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    color(FL_SELECTION_COLOR);
    box(FL_BORDER_BOX);
    end();


    int margin=5;
    //add widgets depending on type
	if(m_nbValues<=4) {
		switch(m_attribute->getType()) {
        case Attribute::FLOAT_ATTRIBUTE :
        case Attribute::ACTION_FLOAT_ATTRIBUTE : {
            int inpWidth=50;
            int pos=fl_width(m_name.c_str())+margin*2;
            if(m_attribute->getType()==Attribute::ACTION_FLOAT_ATTRIBUTE) {
                label("");
                m_button = new Fl_Button(x+margin*2, y+margin, 
                                         pos, h-2*margin, 
                                         m_name.c_str());
                pos+=margin*4;
                m_button->callback(statAction, this);
                m_button->box(FL_FLAT_BOX);
                if(m_attribute->getAccessibility()==Attribute::OUTPUT) {
                    m_button->deactivate();
                }
                add(m_button);
            }
            for(unsigned int v=0; v<m_nbValues; ++v) {
                Fl_Value_Input* adj = new Fl_Value_Input(
                                                x+pos+(inpWidth+margin)*v,
                                                y+margin, inpWidth, h-2*margin, 
                                                "");
                adj->step(0.1);
                adj->bounds(-10000, 10000);
                if(m_attribute->getType()==Attribute::FLOAT_ATTRIBUTE) {
                    adj->callback(statFloats, this);
                }
                adj->value(m_attribute->getFloat(v));
                adj->box(FL_FLAT_BOX);
                adj->color(FL_BACKGROUND_COLOR, FL_SELECTION_COLOR);
                if(m_attribute->getAccessibility()==Attribute::OUTPUT) {
                    adj->deactivate();
                }
                add(adj);
                m_valueInputs.push_back(adj);
            }
        }break;
        case Attribute::INT_ATTRIBUTE : 
        case Attribute::ACTION_INT_ATTRIBUTE : {
            int inpWidth=50;
            int pos=fl_width(m_name.c_str())+margin*2;
            if(m_attribute->getType()==Attribute::ACTION_INT_ATTRIBUTE) {
                label("");
                m_button = new Fl_Button(x+margin*2, y+margin, 
                                         pos, h-2*margin, 
                                         m_name.c_str());
                pos+=margin*4;
                m_button->callback(statAction, this);
                m_button->box(FL_FLAT_BOX);
                if(m_attribute->getAccessibility()==Attribute::OUTPUT) {
                    m_button->deactivate();
                }
                add(m_button);
            }
            for(unsigned int v=0; v<m_nbValues; ++v) {
                Fl_Value_Input*  adj = new Fl_Value_Input(
                                                x+pos+(inpWidth+margin)*v,
                                                y+margin, inpWidth, h-2*margin,
                                                "");
                adj->step(1);
                adj->bounds(-10000, 10000);
                if(m_attribute->getType()==Attribute::INT_ATTRIBUTE) {
                    adj->callback(statInts, this);
                }
                adj->value(m_attribute->getInt(v));
                adj->box(FL_FLAT_BOX);
                adj->color(FL_BACKGROUND_COLOR, FL_SELECTION_COLOR);
                if(m_attribute->getAccessibility()==Attribute::OUTPUT) {
                    adj->deactivate();
                }
                add(adj);
                m_valueInputs.push_back(adj);
            }
        }break;
        case Attribute::BOOL_ATTRIBUTE : {
            int pos=fl_width(m_name.c_str())+10;
            for(unsigned int v=0; v<m_nbValues; ++v) {
                vector<string>& labels = m_attribute->editValueLabels();
                string lab=" X ";
                if(labels.size()>v) {
                    lab=labels[v].c_str();
                }
                Fl_Toggle_Button* adj = new Fl_Toggle_Button(
                                                pos,y+margin, 
                                                fl_width(lab.c_str()), 
                                                h-2*margin,
                                                lab.c_str());
                adj->copy_label(lab.c_str());
                pos+=fl_width(lab.c_str())+margin;
                adj->callback(statChecks, this);
                adj->value(m_attribute->getBool(v));
                adj->box(FL_FLAT_BOX);
                adj->down_box(FL_DOWN_BOX);
                adj->clear_visible_focus();
                adj->color(FL_BACKGROUND2_COLOR, FL_SELECTION_COLOR);
                if(m_attribute->getAccessibility()==Attribute::OUTPUT) {
                    adj->deactivate();
                }
                add(adj);
                m_checks.push_back(adj);
            }
        }break;
        case Attribute::STRING_ATTRIBUTE: 
        case Attribute::ACTION_STRING_ATTRIBUTE: {
            int inpWidth=50;
            int pos=fl_width(m_name.c_str())+margin*2;
            if(m_attribute->getType()==Attribute::ACTION_STRING_ATTRIBUTE) {
                label("");
                m_button = new Fl_Button(x+margin*2, y+margin, 
                                         pos, h-2*margin, 
                                         m_name.c_str());
                pos+=margin*4;
                m_button->callback(statAction, this);
                m_button->box(FL_FLAT_BOX);
                if(m_attribute->getAccessibility()==Attribute::OUTPUT) {
                    m_button->deactivate();
                }
                add(m_button);
            }

            for(unsigned int v=0; v<m_nbValues; ++v) {
                Fl_Widget* adj;
                if(m_attribute->getStringValuesChoices().size()>v) {
                    Fl_Choice* ch = new Fl_Choice(x+pos+(inpWidth+margin)*v, 
                                                 y+margin, inpWidth, h-2*margin,
                                                 "");
                    vector<string>& choices 
                        = m_attribute->editStringValuesChoices()[v];
                    vector<string>::iterator itChoice = choices.begin();
                    int c=0;
                    for(; itChoice!=choices.end(); ++itChoice, ++c) {
                        if(m_attribute->getType()==Attribute::STRING_ATTRIBUTE){
                            ch->add("", 0, statChoices, this);
                            ch->replace(c, (*itChoice).c_str());
                        }
                        else {
                            ch->add("", 0, NULL, this);
                            ch->replace(c, (*itChoice).c_str());
                        }
                        if(fl_width((*itChoice).c_str())+30>inpWidth) {
                            inpWidth=fl_width((*itChoice).c_str())+30;
                        }
                        if(m_attribute->getString(v).compare((*itChoice))==0 ) {
                            ch->value(c);
                        }
                    }
                    ch->size(min(inpWidth,w/2),ch->h());
                    m_choices.push_back(ch);
                    adj=ch;
                }
                else {
                    Fl_Input* inp = new Fl_Input(x+pos+(inpWidth+margin)*v, 
                                                 y+margin, inpWidth, h-2*margin,
                                                 "");
                    inp->value(m_attribute->getString(v).c_str());
                    if(fl_width(inp->value())>inpWidth) {
                        inp->size(fl_width(inp->value()), inp->h());
                    }
                    if(m_attribute->getType()==Attribute::STRING_ATTRIBUTE) {
                        inp->callback(statStrings, this);
                    }
                    m_inputs.push_back(inp);
                    adj=inp;
                }
                adj->when(FL_WHEN_CHANGED);
                adj->box(FL_FLAT_BOX);
                adj->color(FL_BACKGROUND_COLOR, FL_SELECTION_COLOR);
                if(m_attribute->getAccessibility()==Attribute::OUTPUT) {
                    adj->deactivate();
                }
                add(adj);
            }
        }break;
        case Attribute::FILE_OPEN_ATTRIBUTE :
        case Attribute::FILE_SAVE_ATTRIBUTE :
        case Attribute::FOLDER_OPEN_ATTRIBUTE : 
        case Attribute::FOLDER_SAVE_ATTRIBUTE : {
            label("");
            m_button = new Fl_Button(x+margin, y+margin, 
                                     fl_width(m_name.c_str())+10, h-2*margin, 
                                     m_name.c_str());
            if(m_attribute->getType()==Attribute::FILE_OPEN_ATTRIBUTE) {
                m_button->callback(statFileOpen, this);
            }
            else if(m_attribute->getType()==Attribute::FOLDER_OPEN_ATTRIBUTE) {
                m_button->callback(statFolderOpen, this);
            }
            else if(m_attribute->getType()==Attribute::FOLDER_SAVE_ATTRIBUTE) {
                m_button->callback(statFolderSave, this);
            }
            else {
                m_button->callback(statFileSave, this);
            }
            m_button->box(FL_FLAT_BOX);
            if(m_attribute->getAccessibility()==Attribute::OUTPUT) {
                m_button->deactivate();
            }
            add(m_button);
            m_output = new Fl_Output(x+m_button->w()+margin*2, 
                                     y+margin, 
                                     60, h-2*margin, "");
            m_output->value(fl_filename_name(m_attribute->getFile().c_str()));
            m_output->deactivate();
            m_output->size(fl_width(m_output->value())+10,m_output->h());
            m_output->box(FL_NO_BOX);
            add(m_output);
        }break;
        case Attribute::ACTION_ATTRIBUTE : {
            label("");
            m_button = new Fl_Button(x+margin, y+margin, 
                                              fl_width(m_name.c_str())+10, 
                                              h-2*margin, 
                                              m_name.c_str());
            m_button->callback(statAction, this);
            m_button->box(FL_FLAT_BOX);
            if(m_attribute->getAccessibility()==Attribute::OUTPUT) {
                m_button->deactivate();
            }
            add(m_button);
        }break;
        default:break;
    	}
	}

    //add listeners button 
    m_listenersBut = new Fl_Toggle_Button(x+w-(40+margin), y+margin, 
                                               20, h-2*margin,"L");
    m_listenersBut->callback(statShowListeners, this);
    m_listenersBut->box(FL_FLAT_BOX);
    m_listenersBut->color(FL_BACKGROUND2_COLOR, FL_SELECTION_COLOR);
    add(m_listenersBut);

    //register us as guilistener of the attribute
	if(m_nbValues<=4) {
		m_guiListener = new GuiListener(this);
		m_attribute->addListener(m_guiListener);
	}
	else {
		m_guiListener=NULL;
	}
}

AttributeWidget::~AttributeWidget() {
	if(m_guiListener) {
		m_attribute->removeListener(m_guiListener);
		delete m_guiListener;
	}
}

void AttributeWidget::cbFolderSave(Fl_Widget*) {
    Fl_Native_File_Chooser fnfc;
    fnfc.title(m_name.c_str());
    fnfc.type(Fl_Native_File_Chooser::BROWSE_SAVE_DIRECTORY);
    if(fnfc.show()==0) {
        m_attribute->setFile(string(fnfc.filename()), m_guiListener->getID());
        m_output->value(fl_filename_name(m_attribute->getFile().c_str()));
        m_output->size(fl_width(m_output->value())+10, m_output->h());
    }
}

void AttributeWidget::cbFolderOpen(Fl_Widget*) {
    Fl_Native_File_Chooser fnfc;
    fnfc.title(m_name.c_str());
    fnfc.type(Fl_Native_File_Chooser::BROWSE_DIRECTORY);
    if(fnfc.show()==0) {
        m_attribute->setFile(string(fnfc.filename()), m_guiListener->getID());
        m_output->value(fl_filename_name(m_attribute->getFile().c_str()));
        m_output->size(fl_width(m_output->value())+10, m_output->h());
    }
}

void AttributeWidget::cbFileOpen(Fl_Widget*) {
    Fl_Native_File_Chooser fnfc;
    fnfc.title(m_name.c_str());
    fnfc.type(Fl_Native_File_Chooser::BROWSE_FILE);
    if(fnfc.show()==0) {
        m_attribute->setFile(string(fnfc.filename()), m_guiListener->getID());
        m_output->value(fl_filename_name(m_attribute->getFile().c_str()));
        m_output->size(fl_width(m_output->value())+10, m_output->h());
    }
}

void AttributeWidget::cbFileSave(Fl_Widget*) {
    Fl_Native_File_Chooser fnfc;
    fnfc.title(m_name.c_str());
    fnfc.type(Fl_Native_File_Chooser::BROWSE_SAVE_FILE);
    if(fnfc.show()==0) {
        m_attribute->setFile(string(fnfc.filename()), m_guiListener->getID());
        m_output->value(fl_filename_name(m_attribute->getFile().c_str()));
        m_output->size(fl_width(m_output->value())+10, m_output->h());
    }
}

void AttributeWidget::cbAction(Fl_Widget*) {
    if(m_attribute->getType()==Attribute::ACTION_STRING_ATTRIBUTE) {
        vector<string> vals;
        if(m_attribute->getStringValuesChoices().size()>0) {
            vector<Fl_Choice*>::iterator itCh = m_choices.begin();
            for(; itCh!=m_choices.end(); ++itCh) {
                vals.push_back((*itCh)->text());
            }
        }
        else {
            vector<Fl_Input*>::iterator itInp = m_inputs.begin();
            for(; itInp!=m_inputs.end(); ++itInp) {
                vals.push_back((*itInp)->value());
            }
        }
        m_attribute->setStrings(vals, m_guiListener->getID());
    }
    else if(m_attribute->getType()==Attribute::ACTION_FLOAT_ATTRIBUTE) {
        vector<float> vals;
        vector<Fl_Value_Input*>::iterator itInp = m_valueInputs.begin();
        for(; itInp!=m_valueInputs.end(); ++itInp) {
            vals.push_back((*itInp)->value());
        }
        m_attribute->setFloats(vals, m_guiListener->getID());
    }
    else if(m_attribute->getType()==Attribute::ACTION_INT_ATTRIBUTE) {
        vector<int> vals;
        vector<Fl_Value_Input*>::iterator itInp = m_valueInputs.begin();
        for(; itInp!=m_valueInputs.end(); ++itInp) {
            vals.push_back((*itInp)->value());
        }
        m_attribute->setInts(vals, m_guiListener->getID());
    }
    else {
        m_attribute->setAction();
    }
}

void AttributeWidget::cbFloats(Fl_Widget*) {
    vector<float> vals;
    vector<Fl_Value_Input*>::iterator itInp = m_valueInputs.begin();
    for(; itInp!=m_valueInputs.end(); ++itInp) {
        vals.push_back((*itInp)->value());
    }
    m_attribute->setFloats(vals, m_guiListener->getID());
}

void AttributeWidget::cbInts(Fl_Widget*) {
    vector<int> vals;
    vector<Fl_Value_Input*>::iterator itInp = m_valueInputs.begin();
    for(; itInp!=m_valueInputs.end(); ++itInp) {
        vals.push_back((*itInp)->value());
    }
    m_attribute->setInts(vals, m_guiListener->getID());
}

void AttributeWidget::cbStrings(Fl_Widget*) {
    vector<string> vals;
    vector<Fl_Input*>::iterator itInp = m_inputs.begin();
    for(; itInp!=m_inputs.end(); ++itInp) {
        vals.push_back((*itInp)->value());
    }
    m_attribute->setStrings(vals, m_guiListener->getID());
}

void AttributeWidget::cbChoices(Fl_Widget*) {
    vector<string> vals;
    vector<Fl_Choice*>::iterator itCh = m_choices.begin();
    for(; itCh!=m_choices.end(); ++itCh) {
        vals.push_back((*itCh)->text());
    }
    m_attribute->setStrings(vals, m_guiListener->getID());
}

void AttributeWidget::cbChecks(Fl_Widget*) {
    vector<bool> vals;
    vector<Fl_Toggle_Button*>::iterator itCh = m_checks.begin();
    for(; itCh!=m_checks.end(); ++itCh) {
        vals.push_back((*itCh)->value());
    }
    m_attribute->setBools(vals, m_guiListener->getID());
}

void AttributeWidget::cbShowListeners(Fl_Widget*) {
    if(m_listenersBut->value()) {
        MainWindow::getInstance()->showListeners();

        //FIXME unselect other listenersbuttons in the other attribute widgets
        
        Fl_Pack* group = MainWindow::getInstance()->getListenersGroup();
        group->clear();
        //add one widget per listener of type osclistener
        vector<Listener*>& listeners = m_attribute->editListeners();
        vector<Listener*>::iterator itLis = listeners.begin();
        for(; itLis!=listeners.end(); ++itLis) {
            if((*itLis)->getListenerType().compare("OscListener")==0) {
                group->add(new ListenerWidget(0, 0, group->w(), 30, *itLis));
            }
        }
        //add the add button
        m_addListenerBut = new Fl_Button(0, 0, group->w(), 30, "Add");
        m_addListenerBut->box(FL_FLAT_BOX);
        m_addListenerBut->callback(statAddListener, this);
        group->add(m_addListenerBut);
        group->redraw();
    }
    else {
        MainWindow::getInstance()->hideListeners();
    }
    MainWindow::getInstance()->redraw();
}

void AttributeWidget::cbAddListener(Fl_Widget*) {
    Listener* lis = Reveal::getInstance()->createListener("OscListener");
    if(lis!=NULL) {
        m_attribute->addListener(lis);
        cbShowListeners(this);
    }
}

void AttributeWidget::updateAttributeValues(vector<float>& floatValues, 
                                            vector<int>& intValues,
                                            vector<bool>& boolValues,
                                            vector<string>& stringValues) {
    switch(m_attribute->getType()) {
        case Attribute::FLOAT_ATTRIBUTE : {
            unsigned int w=0;
            for(;w<floatValues.size() && w<m_valueInputs.size(); ++w) {
                m_valueInputs[w]->value(floatValues[w]);
            }
        }break;
        case Attribute::INT_ATTRIBUTE : {
            unsigned int w=0;
            for(;w<intValues.size() && w<m_valueInputs.size(); ++w) {
                m_valueInputs[w]->value(intValues[w]);
            }
        }break;
        case Attribute::BOOL_ATTRIBUTE : {
            unsigned int w=0;
            for(;w<boolValues.size() && w<m_checks.size(); ++w) {
                m_checks[w]->value(boolValues[w]);
            }
        }break;
        case Attribute::FILE_OPEN_ATTRIBUTE:
        case Attribute::FILE_SAVE_ATTRIBUTE:
        case Attribute::FOLDER_SAVE_ATTRIBUTE:
        case Attribute::FOLDER_OPEN_ATTRIBUTE: {
            m_output->value(stringValues[0].c_str());
            m_output->size(fl_width(m_output->value())+10, m_output->h());
        }break;
        case Attribute::STRING_ATTRIBUTE : {
            unsigned int w=0;
            for(;w<stringValues.size() && w<m_inputs.size(); ++w) {
                m_inputs[w]->value(stringValues[w].c_str());
            }
            w=0;
            for(;w<stringValues.size() && w<m_choices.size(); ++w) {
                m_choices[w]->value(
                                m_choices[w]->find_index(
                                    stringValues[w].c_str()));
            }
        }break;
        default:break;
    }
}

