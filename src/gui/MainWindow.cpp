/***************************************************************************
 *  MainWindow.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "MainWindow.hpp"

#include <iostream>
#include <FL/Fl.H>                          
#include <FL/Fl_Native_File_Chooser.H>
#include <FL/fl_draw.H>

#include "../Reveal.hpp"

using namespace std;


void MyDraw(const Fl_Label *o, int X, int Y, int W, int H, Fl_Align a) {
    fl_font(o->font, o->size);
    fl_color((Fl_Color)o->color);
    fl_draw(o->value, X, Y, W, H, a, o->image, 0);
}

void MyMeasure(const Fl_Label *o, int &W, int &H) {
    fl_font(o->font, o->size);
    fl_measure(o->value, W, H, 0);
}

MainWindow::MainWindow(): Fl_Double_Window(600, 327, "Rivill - Configuration"),
                          m_treeWidth(200),
                          m_attributesWidth(400) {
    //resizable(this);
    Fl::option(Fl::OPTION_FNFC_USES_GTK, false);
    Fl::option(Fl::OPTION_VISIBLE_FOCUS, false);
    Fl::scheme("gtk+");
    Fl::set_color(FL_BACKGROUND_COLOR, 200, 200, 200);
    Fl::set_color(FL_BACKGROUND2_COLOR, 150, 150, 150);
    Fl::set_color(FL_SELECTION_COLOR, 250, 250, 250);
    Fl::set_labeltype(FL_NORMAL_LABEL, MyDraw, MyMeasure);
    end();
}

void MainWindow::init() {
    
    this->color(FL_BLACK);

    m_scrollModules = new Fl_Scroll(0,0,m_treeWidth,h(),"");
        m_modules= new Fl_Pack(0, 
                               0, 
                               m_treeWidth, 
                               h(), "Modules");
        m_modules->color(FL_SELECTION_COLOR);
        m_modules->resizable(NULL);
    m_scrollModules->end();
    m_scrollModules->color(FL_BLACK, FL_BLACK);
    m_scrollModules->align(FL_ALIGN_LEFT);

    add(m_scrollModules);

    m_scrollAttributes = new Fl_Scroll(m_treeWidth,0,m_attributesWidth,h(),"");
        m_attributes= new Fl_Pack(m_treeWidth, 
                                  0, 
                                  m_attributesWidth, 
                                  h(), "Attributes");
        m_attributes->color(FL_SELECTION_COLOR);
    m_scrollAttributes->end();
    m_scrollAttributes->color(FL_BLACK);
    add(m_scrollAttributes);

    m_scrollListeners = new Fl_Scroll(m_treeWidth+m_attributesWidth, 0, 
                                      m_treeWidth*2, h(),
                                      "Listeners");
        m_listeners= new Fl_Pack(m_treeWidth+m_attributesWidth, 0, 
                                 m_treeWidth*2, h(), 
                                 "");
        m_listeners->color(FL_SELECTION_COLOR);
    m_scrollListeners->end();
    m_scrollListeners->color(FL_BLACK);
    add(m_scrollListeners);

    callback(statQuit);

    redraw();
}

MainWindow::~MainWindow() {
}

MainWindow* MainWindow::getInstance() {
    static MainWindow instance;
    return &instance;
}

void MainWindow::cbQuit() {
    int res = fl_choice("Save before quit ?", "Don't quit", 
                        "Save first", "Quit without saving");
    switch(res) {
        case 0: break;
        case 1: {
            cbSave();
            Reveal::getInstance()->quit();
        }
        case 2: {
            Reveal::getInstance()->quit();
        }
    }
}

void MainWindow::refreshModules(bool clearAttributes, bool clearListeners) {
    if(clearAttributes) {
        m_attributes->clear();
    }
    if(clearListeners) {
        m_listeners->clear();
    }
    //refresh modules
    m_modules->clear();
    m_moduleButtonsMap.clear();
    vector<Module*> modules;
    Reveal::getInstance()->getSubTree(modules);
    vector<Module*>::iterator itMod = modules.begin();
    for(; itMod!=modules.end(); ++itMod) {
        ModuleWidget* newMod = new ModuleWidget(m_modules->x(), 10, 
                                                m_modules->w(), 30, 
                                                *itMod);
        m_moduleButtonsMap[(*itMod)->getFullName()]=newMod;
        newMod->callback(statTree, this);
        m_modules->add(newMod);
    }

    //select previously selected button if possible
    if(m_moduleButtonsMap.find(m_selectedModulePath)!=m_moduleButtonsMap.end()){
        m_moduleButtonsMap[m_selectedModulePath]->value(1);
        displayAttributes(m_moduleButtonsMap[m_selectedModulePath]);
    }

    //redraw all
    this->redraw();
}

void MainWindow::displayAttributes(ModuleWidget* mod) {
    m_attributes->clear();
    m_scrollAttributes->scroll_to(0,0);
    mod->fillGroupWithAttributes(m_attributes);
    m_listeners->clear();
}

void MainWindow::cbTree(Fl_Widget* w) {
    m_selectedModulePath="";
    int childID = m_modules->find(w);
    for(int c=0; c<m_modules->children(); ++c) {
        if(c==childID) {
            ModuleWidget* selWid 
                = static_cast<ModuleWidget*>(m_modules->child(c));
            selWid->value(1);
            m_selectedModulePath = selWid->getModule()->getFullName();
            displayAttributes(selWid);
            redraw();
        }
        else {
            static_cast<Fl_Toggle_Button*>(m_modules->child(c))->value(0);
        }
    }
}

int MainWindow::handle(int event) {
    switch(event) {
        case FL_SHORTCUT:
        case FL_KEYDOWN : {
            if(Fl::event_command()) {
                switch(Fl::event_key()) {
                    case 'o': cbOpen(); return 1;
                    case 's': cbSave(); return 1;
                    case 'q': cbQuit(); return 1;
                }
            }
            else if(Fl::event_key()==FL_Escape) {
                cbQuit(); 
                return 1;
            }
        }break;
        case FL_CLOSE: {
            cbQuit();
            return 1;
        }break;
        default: break;
    }
    return Fl_Group::handle(event);
}

void MainWindow::cbOpen() {
    Fl_Native_File_Chooser fnfc;
    fnfc.title("Open .rev file");
    fnfc.type(Fl_Native_File_Chooser::BROWSE_FILE);
    if(fnfc.show()==0) {
        Reveal::getInstance()->open(fnfc.filename());
    }
}

void MainWindow::cbSave() {
    const string& fileStr = Reveal::getInstance()
                                    ->getAttribute("save")->getFile();
    if(fileStr.compare("save")!=0 && fileStr.compare("")!=0) {
        Reveal::getInstance()->save(fileStr);
    }
    else {
        cbSaveAs();
    }
}

void MainWindow::cbSaveAs() {
    Fl_Native_File_Chooser fnfc;
    fnfc.title("Save to .rev file");
    fnfc.type(Fl_Native_File_Chooser::BROWSE_SAVE_FILE);
    if(fnfc.show()==0) {
        Reveal::getInstance()->getAttribute("save")->setFile(fnfc.filename());
    }
}

void MainWindow::showListeners() {
    m_scrollListeners->show();
    m_scrollListeners->resize(m_treeWidth+m_attributesWidth,0, 
                              m_treeWidth*2,h());
    size(m_treeWidth+m_attributesWidth+m_treeWidth*2, h()); 
    redraw();
}

void MainWindow::hideListeners() {
    m_scrollListeners->hide();
    size(m_treeWidth+m_attributesWidth,h()); 
    redraw();
}

