/***************************************************************************
 *  ListenerWidget.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef ListenerWidget_h 
#define ListenerWidget_h

#include "../modules/Listener.hpp"
#include <FL/Fl_Group.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Button.H>

class ListenerWidget : public Fl_Group {
    public:
        ListenerWidget(int x, int y, int w, int h, Listener* listener);
        virtual ~ListenerWidget() {}

        static void statParams(Fl_Widget* w, void* f){ 
            ListenerWidget *tmpf = static_cast<ListenerWidget*>(f);
            tmpf->cbParams(w);
        }    
        void cbParams(Fl_Widget*);
        static void statRemove(Fl_Widget* w, void* f){ 
            ListenerWidget *tmpf = static_cast<ListenerWidget*>(f);
            tmpf->cbRemove(w);
        }    
        void cbRemove(Fl_Widget*);

    private:
        Listener* m_listener;
        std::vector<Fl_Input*> m_parameters;
        std::vector<Fl_Choice*> m_choices;
        Fl_Button* m_removeBut;
};

#endif

