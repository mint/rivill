/***************************************************************************
 *  ListenerWidget.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "ListenerWidget.hpp"
#include <iostream>
#include <FL/fl_draw.H>
#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Input_Choice.H>
#include <FL/Fl_Native_File_Chooser.H>
#include <FL/filename.H>

using namespace std;

ListenerWidget::ListenerWidget(int x, int y, 
                               int w, int h, 
                               Listener* listener): 
                                                  Fl_Group(x,y,w,h,""),
                                                  m_listener(listener) {
    align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE);
    color(FL_SELECTION_COLOR);
    box(FL_BORDER_BOX);
    end();

    int margin=5;
    int pos=x+margin;
    vector<string>& lisParams = m_listener->editParameters();
    vector<vector<string> >& lisParVals = m_listener->editParametersValues();
    vector<string>::iterator itPar = lisParams.begin();
    vector<vector<string> >::iterator itVal = lisParVals.begin();
    for(; itPar!=lisParams.end(); ++itPar, ++itVal) {
        if((*itVal).size()>0) {
            Fl_Choice* inp = new Fl_Choice(pos, y+margin, 
                                           (w-40)/lisParams.size(), h-2*margin,
                                           "");
            for(string &value : (*itVal)) {
                inp->add(value.c_str());
            }

            pos+=inp->w()+margin;
            inp->value(0);
            inp->callback(statParams, this);
            inp->when(FL_WHEN_CHANGED);
            inp->box(FL_FLAT_BOX);
            inp->color(FL_BACKGROUND_COLOR, FL_SELECTION_COLOR);
            m_choices.push_back(inp);
            add(inp);
        }
        else {
            Fl_Input* inp = new Fl_Input(pos, y+margin, 
                                         (w-40)/lisParams.size(), h-2*margin,
                                         "");
            pos+=inp->w()+margin;
            inp->value((*itPar).c_str());
            inp->callback(statParams, this);
            inp->when(FL_WHEN_CHANGED);
            inp->box(FL_FLAT_BOX);
            inp->color(FL_BACKGROUND_COLOR, FL_SELECTION_COLOR);
            add(inp);
            m_parameters.push_back(inp);
        }
    }

    m_removeBut = new Fl_Button(pos, y+margin, 
                                20, h-2*margin, "X");
    m_removeBut->callback(statRemove, this);
    m_removeBut->box(FL_FLAT_BOX);
    m_removeBut->color(FL_BACKGROUND_COLOR, FL_SELECTION_COLOR);
    add(m_removeBut);
}

void ListenerWidget::cbParams(Fl_Widget*) {
    vector<string> params;
    for(Fl_Input* inp : m_parameters) {
        params.push_back(inp->value());
    }
    for(Fl_Choice* inp : m_choices) {
        params.push_back(inp->text(inp->value()));
    }
    m_listener->setParameters(params);
}

void ListenerWidget::cbRemove(Fl_Widget*) {
    m_listener->deleteListener();
}


