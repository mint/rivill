/***************************************************************************
 *  Listener.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "Listener.hpp"
#include <iostream>
#include <sstream>
#include "Attribute.hpp"
#include "../Reveal.hpp"

using namespace std;

Listener::Listener():m_saved(true){}

void Listener::deleteListener() {
    m_attribute->removeListener(this);
    Reveal::getInstance()->deleteListener(this);
}

void Listener::setParameters(const vector<string>& params) { 
    if(params.size()==m_parameters.size()) {
        m_parameters=params;
    }
}

void Listener::load(xmlNodePtr lisNode) {
    vector<string> params;
    xmlNodePtr paramNode;
    for(paramNode=lisNode->children; paramNode; paramNode=paramNode->next){
        if(paramNode->type == XML_ELEMENT_NODE) {
            if(!xmlStrcmp(paramNode->name, (const xmlChar *)"Parameter")) {
                char* value = (char*)xmlGetProp(paramNode,(xmlChar*)"value");
                if(value!=NULL) {
                    params.push_back(value);
                }
            }
        }
    }
    setParameters(params);
}

void Listener::save(xmlNodePtr parentNode) {
    if(m_saved) {
        xmlNodePtr lisNode = xmlNewChild(parentNode, NULL, 
                                          BAD_CAST m_listenerType.c_str(), 
                                          NULL);
        vector<string>::iterator itParam = m_parameters.begin();
        for(; itParam!=m_parameters.end(); ++itParam) {
            xmlNodePtr parNode = xmlNewChild(lisNode, NULL, 
                                             BAD_CAST "Parameter", NULL);
            xmlNewProp(parNode,BAD_CAST"value",BAD_CAST (*itParam).c_str());
        }
    }
}

