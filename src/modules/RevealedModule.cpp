/***************************************************************************
 *  RevealedModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "RevealedModule.hpp" 

#include <iostream>
#include <fstream>
#include <limits>
#include <unistd.h>

#include <glm/gtx/norm.hpp>

#include <FL/filename.H>
#include <opencv2/opencv.hpp>

#include "../geoms/Geometry.hpp"

using namespace std;

RevealedModule::RevealedModule(): GeomModule(), 
                                  m_volumeTextureWidth(300), 
                                  m_volumeTextureHeight(300),
                                  m_outputActive(true) {

    Reveal::getInstance()->registerRevealed(this);

    //Material
    m_surface=1;
    m_reactivityType=0;
    m_surfaceThickness=5.0;
	m_shapeGeom=Reveal::GEOM_SPHERE;
    m_layersData=NULL;
    m_insideImage=NULL;
    m_surfaceImage=NULL;
    m_gradientImage=NULL;
	m_textureGrayscale=false;
	m_textureCanDelete=true;
    m_layersNb=0;
    for(int d=0; d<3; d++) {
        m_textureOffset[d]=0;
        m_textureScale[d]=1;
    }

	m_revSize=0;
	m_revSurf=0;
	m_revIns=0;
	m_revCent=0;
	m_revCol=0;
	m_revHis=0;
	m_revVox=0;
	m_voxDim=5;
	m_voxSize = m_voxDim*m_voxDim*m_voxDim;

	m_outHistoSize=m_voxDim*m_voxDim;
	m_outVoxelSize=m_voxDim*m_voxDim;

	m_reactThread=NULL;
	m_reactTexInput = new float[m_voxSize];
	m_reactTexInternal = new float[m_voxSize];
	m_reactTexOutput = new char[m_voxSize*3];
	for(int v=0; v<m_voxSize; v++) {
		m_reactTexInput[v]=0;
		m_reactTexInternal[v]=0;
		m_reactTexOutput[v*3]=0;
		m_reactTexOutput[v*3+1]=0;
		m_reactTexOutput[v*3+2]=0;
	}
}

RevealedModule::~RevealedModule() {}

void RevealedModule::addRevealedAttributes() {
    addAttribute(new Attribute("color", 
                               Attribute::FLOAT_ATTRIBUTE,
                               colorCallback, this, 3));
    addAttribute(new Attribute("surface", 
                               Attribute::STRING_ATTRIBUTE,
                               surfaceCallback, this, 1));
    m_surfaces.push_back("none");
    m_surfaces.push_back("color");
    m_surfaces.push_back("texture");
    m_attributesMap["surface"]->editStringValuesChoices().assign(1,m_surfaces);
    m_attributesMap["surface"]->initStrings(vector<string>(1,m_surfaces[1]));
    addAttribute(new Attribute("surface_color", 
                               Attribute::FLOAT_ATTRIBUTE,
                               surfaceColorCallback, this, 3));
    addAttribute(new Attribute("surface_thickness", 
                               Attribute::FLOAT_ATTRIBUTE,
                               surfaceThicknessCallback, this, 1));
    m_attributesMap["surface_thickness"]
						->initFloats(vector<float>(1,m_surfaceThickness));
    addAttribute(new Attribute("surface_texture", 
                               Attribute::FILE_OPEN_ATTRIBUTE, 
                               surfaceTextureCallback, this));

    vector<float> initCol(3,0);
    initCol[0]=360;
    initCol[2]=100;
    m_color.resize(3, 0);
    m_surfaceColor.resize(3, 0);
    hsv2rgb(initCol, m_color);
    hsv2rgb(initCol, m_surfaceColor);
    m_attributesMap["color"]->setFloats(initCol);
    m_attributesMap["surface_color"]->setFloats(initCol);

}

void RevealedModule::addRevealedInsideAttributes() {
	addRevealedInsideStructureAttributes();
	addRevealedInsideGradientAttributes();
	addRevealedInsideDensityAttributes();
	addRevealedInsideTextureAttributes();
}

void RevealedModule::addRevealedInsideStructureAttributes() {
	//Visible
    addAttribute(new Attribute("inside_visible", 
                               Attribute::BOOL_ATTRIBUTE,
                               insideVisibleCallback, this, 1));
    m_attributesMap["inside_visible"]->initBools(vector<bool>(1, true));
	m_insideVisible=true;
  
	//Axes and structure
    addAttribute(new Attribute("inside_axes", 
                               Attribute::STRING_ATTRIBUTE,
                               insideAxesCallback, this, 1));
    m_insideAxes.push_back("local");
    m_insideAxes.push_back("global");
    m_attributesMap["inside_axes"]
            ->editStringValuesChoices().assign(1, m_insideAxes);
    m_attributesMap["inside_axes"]
            ->initStrings(vector<string>(1, m_insideAxes[0]));

    addAttribute(new Attribute("inside_structure", 
                               Attribute::STRING_ATTRIBUTE,
                               insideStructureCallback, this, 1));
    m_insideStructures.push_back("from_center");
    m_insideStructures.push_back("along_x");
    m_insideStructures.push_back("along_y");
    m_insideStructures.push_back("along_z");
    m_insideStructures.push_back("from_surface");
    m_attributesMap["inside_structure"]
            ->editStringValuesChoices().assign(1, m_insideStructures);
    m_attributesMap["inside_structure"]
            ->setStrings(vector<string>(1,m_insideStructures[4]));

	m_structureRatio=0;
    addAttribute(new Attribute("structure_ratio", 
                               Attribute::FLOAT_ATTRIBUTE,
                               structureRatioCallback, this, 1));
}

void RevealedModule::addRevealedInsideGradientAttributes() {
	//Gradient
    addAttribute(new Attribute("gradient_alpha", 
                               Attribute::FLOAT_ATTRIBUTE,
                               gradientAlphaCallback, this, 1));
    m_attributesMap["gradient_alpha"]->setFloats(vector<float>(1, 1));
    addAttribute(new Attribute("gradient_type", 
                               Attribute::STRING_ATTRIBUTE,
                               gradientTypeCallback, this, 1));
    m_gradientTypes.push_back("grayscale");
    m_gradientTypes.push_back("texture");
    m_attributesMap["gradient_type"]
            ->editStringValuesChoices().assign(1, m_gradientTypes);
    m_attributesMap["gradient_type"]
            ->setStrings(vector<string>(1, m_gradientTypes[0]));
    addAttribute(new Attribute("gradient_steps", 
                               Attribute::INT_ATTRIBUTE,
                               gradientStepsCallback, this, 1));
    addAttribute(new Attribute("gradient_curve_ratio", 
                               Attribute::FLOAT_ATTRIBUTE,
                               gradientCurveRatioCallback, this, 1));
    m_attributesMap["gradient_curve_ratio"]->setFloats(vector<float>(1, 1.0));

    addAttribute(new Attribute("gradient_texture", 
                               Attribute::FILE_OPEN_ATTRIBUTE,
                               gradientTextureCallback, this));
}

void RevealedModule::addRevealedInsideDensityAttributes() {
	//Density
    addAttribute(new Attribute("density_alpha", 
                               Attribute::FLOAT_ATTRIBUTE,
                               densityAlphaCallback, this, 1));
    m_attributesMap["density_alpha"]->setFloats(vector<float>(1, 0.5));
    addAttribute(new Attribute("density_type", 
                               Attribute::STRING_ATTRIBUTE,
                               densityTypeCallback, this, 1));
    m_insideDensityTypes.push_back("layers");
    m_insideDensityTypes.push_back("grid");
    m_insideDensityTypes.push_back("pointcloud");
    m_attributesMap["density_type"]
            ->editStringValuesChoices().assign(1, m_insideDensityTypes);
    m_attributesMap["density_type"]
            ->setStrings(vector<string>(1,m_insideDensityTypes[0]));
    addAttribute(new Attribute("density_ratio", 
                               Attribute::FLOAT_ATTRIBUTE,
                               densityRatioCallback, this, 1));
    m_attributesMap["density_ratio"]->setFloats(vector<float>(1,0.3));
    addAttribute(new Attribute("density_size", 
                               Attribute::FLOAT_ATTRIBUTE,
                               densitySizeCallback, this, 1));
    m_attributesMap["density_size"]->setFloats(vector<float>(1,0.1));
    addAttribute(new Attribute("density_curve_ratio", 
                               Attribute::FLOAT_ATTRIBUTE,
                               densityCurveRatioCallback, this, 1));
    m_attributesMap["density_curve_ratio"]->setFloats(vector<float>(1,1));
}

void RevealedModule::addRevealedInsideTextureAttributes() {
	//Textures
    addAttribute(new Attribute("texture_alpha", 
                               Attribute::FLOAT_ATTRIBUTE,
                               textureAlphaCallback, this, 1));
    addAttribute(new Attribute("texture", 
                               Attribute::FILE_OPEN_ATTRIBUTE, 
                               textureCallback, this));
    addAttribute(new Attribute("texture_layers_folder", 
                               Attribute::FOLDER_OPEN_ATTRIBUTE, 
                               textureLayersFolderCallback, this));
    addAttribute(new Attribute("texture_grayscale", 
                               Attribute::BOOL_ATTRIBUTE, 
                               textureGrayscaleCallback, this, 1));
    addAttribute(new Attribute("texture_blend", 
                               Attribute::BOOL_ATTRIBUTE,
                               textureBlendCallback, this, 1));
    m_attributesMap["texture_blend"]->setBools(vector<bool>(1, true));
    addAttribute(new Attribute("texture_offset", 
                               Attribute::FLOAT_ATTRIBUTE, 
                               textureOffsetCallback, this, 3));
    addAttribute(new Attribute("texture_scale", 
                               Attribute::FLOAT_ATTRIBUTE, 
                               textureScaleCallback, this, 3));
    m_attributesMap["texture_scale"]->initFloats(vector<float>(3, 1));

    addAttribute(new Attribute("texture_reactivity", 
                               Attribute::STRING_ATTRIBUTE,
                               textureReactCallback, this, 1));
    m_reactivityTypes.push_back("none");
    m_reactivityTypes.push_back("thermal");
    m_attributesMap["texture_reactivity"]
            ->editStringValuesChoices().assign(1, m_reactivityTypes);
    m_attributesMap["texture_reactivity"]
            ->initStrings(vector<string>(1,m_reactivityTypes[0]));

    addAttribute(new Attribute("layer_anim_position", 
                               Attribute::FLOAT_ATTRIBUTE, 
                               animPosCallback, this, 1));
    addAttribute(new Attribute("layer_anim_play", 
                               Attribute::BOOL_ATTRIBUTE, 
                               animPlayCallback, this, 1));
    m_attributesMap["layer_anim_play"]->initBools(vector<bool>(1,false));
    addAttribute(new Attribute("layer_anim_speed", 
                               Attribute::FLOAT_ATTRIBUTE, 
                               animSpeedCallback, this, 1));
    m_attributesMap["layer_anim_speed"]->initFloats(vector<float>(1,1));
}

void RevealedModule::addRevealedOutputAttributes() {
    addAttribute(new Attribute("revealed_name", 
                               Attribute::STRING_ATTRIBUTE,
                               revnamCallback, this, 1, Attribute::OUTPUT));
    m_attributesMap["revealed_name"]->initStrings(vector<string>(1,"name"));
    m_revAttributes.push_back(m_attributesMap["revealed_name"]);
    addAttribute(new Attribute("revealed_surface", 
                               Attribute::FLOAT_ATTRIBUTE,
                               revsurCallback, this, 1, Attribute::OUTPUT));
    m_revAttributes.push_back(m_attributesMap["revealed_surface"]);
	m_attributesMap["revealed_surface"]->setChangeCount(30);
    addAttribute(new Attribute("revealed_inside", 
                               Attribute::FLOAT_ATTRIBUTE,
                               revinsCallback, this, 1, Attribute::OUTPUT));
	m_attributesMap["revealed_inside"]->setChangeCount(30);
    m_revAttributes.push_back(m_attributesMap["revealed_inside"]);
    addAttribute(new Attribute("revealed_center", 
                               Attribute::FLOAT_ATTRIBUTE,
                               revcentCallback, this, 3, Attribute::OUTPUT));
    m_revAttributes.push_back(m_attributesMap["revealed_center"]);
    addAttribute(new Attribute("revealed_extent", 
                               Attribute::FLOAT_ATTRIBUTE,
                               revextCallback, this, 4, Attribute::OUTPUT));
    m_revAttributes.push_back(m_attributesMap["revealed_extent"]);
    addAttribute(new Attribute("revealed_color", 
                               Attribute::FLOAT_ATTRIBUTE,
                               revcolCallback, this, 3, Attribute::OUTPUT));
    m_revAttributes.push_back(m_attributesMap["revealed_color"]);
    addAttribute(new Attribute("revealed_histo", 
                               Attribute::FLOAT_ATTRIBUTE,
                               revhistoCallback, this, m_outHistoSize, 
							   Attribute::OUTPUT));
    m_revAttributes.push_back(m_attributesMap["revealed_histo"]);
    addAttribute(new Attribute("revealed_voxels", 
                               Attribute::FLOAT_ATTRIBUTE,
                               revvoxelCallback, this, 
							   m_outVoxelSize*4, 
							   Attribute::OUTPUT));
    m_revAttributes.push_back(m_attributesMap["revealed_voxels"]);
}

void RevealedModule::deleteModule() {
    Reveal::getInstance()->unregisterRevealed(this);
    GeomModule::deleteModule();
}

void RevealedModule::draw(const int& contextID,
                          const Reveal::REVIL_PROGRAM& prog, 
                          map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                          const unsigned int& comp){

    GeomModule::draw(contextID, prog, uniforms, comp);
    glUniform3fv(uniforms[Reveal::SHAPECOLOR], 1, &m_color[0]);

    glUniform1i(uniforms[Reveal::SURFACE], m_surface);
    glUniform3fv(uniforms[Reveal::SURFACECOLOR], 1, &m_surfaceColor[0]);
    glUniform1f(uniforms[Reveal::SURFACETHICKNESS], m_surfaceThickness);

    glUniform1f(uniforms[Reveal::SHAPEID], m_revID);
    glUniform1f(uniforms[Reveal::SHAPEIDBIT], m_revIDBit);
	glUniform1i(uniforms[Reveal::SHAPEGEOM], m_shapeGeom);
    if(prog==Reveal::RENDERPROG) {
        if(m_insideIDMap.find(contextID)==m_insideIDMap.end()) {
            GLuint texID;
            glGenTextures(1, &texID);
            m_insideIDMap[contextID]=texID;
            m_insideUpMap[contextID]=true;

            glGenTextures(1, &texID);
            m_coordsIDMap[contextID]=texID;
            m_coordsUpMap[contextID]=true;

            glGenTextures(1, &texID);
            m_surfaceIDMap[contextID]=texID;
            m_surfaceUpMap[contextID]=true;

            glGenTextures(1, &texID);
            m_gradientIDMap[contextID]=texID;
            m_gradientUpMap[contextID]=true;

			m_boundingRect[contextID]=glm::vec4(0.0);
			m_boundingRectUpdate[contextID]=true;

            glGenTextures(1, &texID);
            m_reactIDMap[contextID]=texID;
            m_reactUpMap[contextID]=true;
        }

		//test if bounding rect needs updating
		if(m_boundingRectUpdate[contextID]) {
			//get viewproj matrix of current projector
			ContextHandler* proj = Reveal::getInstance()
									->getContextHandler(contextID);
			glm::mat4& mat = proj->getViewProjMat();

			m_boundingRect[contextID][0]=numeric_limits<float>::max();
			m_boundingRect[contextID][1]=numeric_limits<float>::max();
			m_boundingRect[contextID][2]=-numeric_limits<float>::max();
			m_boundingRect[contextID][3]=-numeric_limits<float>::max();
			for(int p=0; p<8; ++p) {
				glm::vec4 pnt;
				pnt[0] = m_bbPnts[p%2][0];
				pnt[1] = m_bbPnts[(p/2)%2][1];
				pnt[2] = m_bbPnts[(p/4)%2][2];
				pnt[3] = 1.0;

				//transform the point with viewproj matrix
				pnt = mat * pnt;

				//find the bounding rect
				m_boundingRect[contextID][0] 
					= min<float>(pnt[0]/pnt[3], m_boundingRect[contextID][0]);
				m_boundingRect[contextID][1] 
					= min<float>(pnt[1]/pnt[3], m_boundingRect[contextID][1]);
				m_boundingRect[contextID][2] 
					= max<float>(pnt[0]/pnt[3], m_boundingRect[contextID][2]);
				m_boundingRect[contextID][3] 
					= max<float>(pnt[1]/pnt[3], m_boundingRect[contextID][3]);
			}

			//put coordinates between 0 and 1
			for(int p=0; p<4; ++p) {
				m_boundingRect[contextID][p]
					= (m_boundingRect[contextID][p]+1.0)/2.0;
			}

			m_boundingRect[contextID][0]=1.0-m_boundingRect[contextID][0];
			m_boundingRect[contextID][2]=1.0-m_boundingRect[contextID][2];

			m_boundingRectUpdate[contextID]=false;
		}

		glUniform1i(uniforms[Reveal::REVSIZE], m_revSize);
		glUniform1i(uniforms[Reveal::REVSURFACE], m_revSurf);
		glUniform1i(uniforms[Reveal::REVINSIDE], m_revIns);
		glUniform1i(uniforms[Reveal::REVCENTER], m_revCent);
		glUniform1i(uniforms[Reveal::REVCOLOR], m_revCol);
		glUniform1i(uniforms[Reveal::REVHISTO], m_revHis);
		glUniform1i(uniforms[Reveal::REVVOXELS], m_revVox);
		glUniform1i(uniforms[Reveal::OUTHISTOSIZE], m_outHistoSize);
		glUniform1i(uniforms[Reveal::OUTVOXELSIZE], m_voxDim);
		glUniform4fv(uniforms[Reveal::BRECT], 1, &m_boundingRect[contextID][0]);
        glUniform1i(uniforms[Reveal::REVEALEDBY], m_revealedBy);
        glUniform1i(uniforms[Reveal::REACTIVITY], m_reactivityType);

        glUniform1i(uniforms[Reveal::INSIDEVISIBLE], m_insideVisible);
        glUniform1i(uniforms[Reveal::INSIDESTRUCT], m_insideStructure);
        glUniform1f(uniforms[Reveal::STRUCTRATIO], m_structureRatio);

        glUniform1f(uniforms[Reveal::GRADIENTALPHA], m_insideGradientAlpha);
        glUniform1i(uniforms[Reveal::GRADIENTTYPE], m_gradientType);
        glUniform1i(uniforms[Reveal::GRADIENTSTEPS], m_insideGradientSteps);
        glUniform1f(uniforms[Reveal::GRADIENTCURVERATIO], 
					m_insideGradientCurveRatio);

        glUniform1f(uniforms[Reveal::DENSITYALPHA], m_insideDensityAlpha);
        glUniform1i(uniforms[Reveal::DENSITYTYPE], m_insideDensityType);
        glUniform1f(uniforms[Reveal::DENSITYRATIO], m_insideDensityRatio);
        glUniform1f(uniforms[Reveal::DENSITYSIZE], m_insideDensitySize);
        glUniform1f(uniforms[Reveal::DENSITYCURVERATIO], 
					m_insideDensityCurveRatio);

        glUniform1f(uniforms[Reveal::TEXALPHA], m_insideTextureAlpha);
        glUniform1i(uniforms[Reveal::TEXBLEND], m_insideTextureBlend);
        glUniform3fv(uniforms[Reveal::TEXOFFSET], 1, &m_textureOffset[0]);
        glUniform3fv(uniforms[Reveal::TEXSCALE], 1, &m_textureScale[0]);
        glUniform1i(uniforms[Reveal::TEXGRAY], m_textureGrayscale);

        glUniform1i(uniforms[Reveal::INSIDETEX], 2);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_3D, m_insideIDMap[contextID]);

        //test layers update
        int d=0;
        vector<RevealedLayer*>::iterator itLa = m_layers.begin();
        for(; itLa!=m_layers.end(); ++itLa, ++d) {
            if((*itLa)->update(m_timeDiffMs, d, m_layersData, 
                               m_texWidth, m_texHeight)) {
                m_insideUpMap[contextID]=true;
            }
        }

        if(m_insideUpMap[contextID]) { 
			if(m_insideTextureBlend) {
                glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, 
                                GL_LINEAR);
                glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, 
                                GL_LINEAR);
            }
            else {
                glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, 
                                GL_NEAREST);
                glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, 
                                GL_NEAREST);
            }
			if(m_layersNb>0) { 
				glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB8, 
							 m_texWidth, m_texHeight, m_layersNb, 0,
							 GL_RGB, GL_UNSIGNED_BYTE,
							 m_layersData);
			}
			else if(m_insideImage) {
				glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB8, 
							 m_texWidth, m_texHeight, 1, 0,
							 (m_insideImage->d()>3)?GL_RGBA:GL_RGB, 
							 GL_UNSIGNED_BYTE,
							 m_insideImage->data()[0]);
			}
            m_insideUpMap[contextID]=false;
        }

        glUniform1i(uniforms[Reveal::GRADIENTTEXTURE], 6);
        glActiveTexture(GL_TEXTURE6);
        glBindTexture(GL_TEXTURE_2D, m_gradientIDMap[contextID]);
        if(m_gradientUpMap[contextID] && m_gradientImage) {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_BORDER);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_BORDER);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 
                         m_texWidth, m_texHeight, 0,
                         GL_RGB, GL_UNSIGNED_BYTE,
                         m_gradientImage->data()[0]);
            m_gradientUpMap[contextID]=false;
        }

        glUniform1i(uniforms[Reveal::SURFACETEX], 3);
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, m_surfaceIDMap[contextID]);
        if(m_surfaceUpMap[contextID] && m_surfaceImage) {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_BORDER);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_BORDER);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 
                         m_texWidth, m_texHeight, 0,
                         GL_RGB, GL_UNSIGNED_BYTE,
                         m_surfaceImage->data()[0]);
            m_surfaceUpMap[contextID]=false;
        }

        glUniform1i(uniforms[Reveal::REACTTEX], 4);
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_3D, m_reactIDMap[contextID]);
        if(m_reactUpMap[contextID] && m_reactLock.try_lock()) {
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, 
							GL_LINEAR);
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, 
							GL_LINEAR);
			glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB8, 
						 m_voxDim, m_voxDim, m_voxDim, 0,
						 GL_RGB, GL_UNSIGNED_BYTE,
						 m_reactTexOutput);
            m_reactUpMap[contextID]=false;
			m_reactLock.unlock();
        }
    }
}

void RevealedModule::update(const int& timeDiffMs) {
    m_timeDiffMs=timeDiffMs;
}

void RevealedModule::setColor(const vector<float>& hsv) {
    hsv2rgb(hsv, m_color);
}

/*
void RevealedModule::setInside(const std::string& inside) {
    int ins=0;
    vector<string>::iterator itIns=m_insides.begin();
    for(; itIns!=m_insides.end(); ++itIns, ++ins) {
        if((*itIns).compare(inside)==0) {
            m_inside=ins;
        }
    }
    map<int, bool>::iterator itUp = m_insideUpMap.begin();
    for(; itUp!=m_insideUpMap.end(); ++itUp) {
        itUp->second=true;
    }
}*/

void RevealedModule::setInsideAxes(const std::string& axes) {
    if(axes.compare("local")==0) {
        m_localInsideAxes = true;
    }
    else {
        m_localInsideAxes = false;
    }
    updateModelMatrix();
}

void RevealedModule::setInsideStructure(const std::string& stru) {
    int t=0;
    vector<string>::iterator itStruct=m_insideStructures.begin();
    for(; itStruct!=m_insideStructures.end(); ++itStruct, ++t) {
        if((*itStruct).compare(stru)==0) {
            m_insideStructure = t;
        }
    }
}

void RevealedModule::setTextureBlend(bool b){
	m_insideTextureBlend=b;
    map<int, bool>::iterator itUp = m_insideUpMap.begin();
    for(; itUp!=m_insideUpMap.end(); ++itUp) {
        itUp->second=true;
    }
}

void RevealedModule::generateSurfaceDistanceTexture(const std::string& file) {
    ostringstream os;
    //int maxDigits = os.str().length();

    vector<GLfloat>& vertices = m_currentGeom->editVertexBufferData();
    vector<GLuint>& indices = m_currentGeom->editIndexBufferData();

    cout<<"Building volumetric texture from "<<indices.size()/3<<" faces"
        <<" and saving to file "<<file<<endl;

	//m_surfDistDims = glm::vec3(64,64,64);
	m_surfDistDims = glm::ivec3(128,128,128);

	//voxels
	std::vector<float> voxels(m_surfDistDims.x
							  *m_surfDistDims.y
							  *m_surfDistDims.z, 0);
    
    //for each pixel in the volumetric texture
    for(int v = 0; v < int(voxels.size()); ++v) {
		cout<<"Computing surface distance "
			<<to_string(float(v)/float(voxels.size())*100.0)<<"%"<<endl;

		//get 3D coord
		int i=v%m_surfDistDims.x;
		int j=(v/m_surfDistDims.x)%m_surfDistDims.y;
		int k=v/(m_surfDistDims.x*m_surfDistDims.y);

		//get 3D pos of voxel
		glm::vec3 pos;
		glm::vec3 pixCol(0,0,0);

		pos[0] = (1.0-float(i)/float(m_surfDistDims.x))-0.5;
		pos[1] = (1.0-float(j)/float(m_surfDistDims.y))-0.5;
		pos[2] = (float(k)/float(m_surfDistDims.z))-0.5;

		//find closest face
		float minDist = std::numeric_limits<float>::max();
		glm::vec3 nearestPnt;

		for(unsigned int f=0; f<indices.size()/3; f++) {

			glm::vec3 v0(vertices[indices[f*3+0]*5], 
						 vertices[indices[f*3+0]*5+1], 
						 vertices[indices[f*3+0]*5+2]);
			glm::vec3 v1(vertices[indices[f*3+1]*5], 
						 vertices[indices[f*3+1]*5+1], 
						 vertices[indices[f*3+1]*5+2]);
			glm::vec3 v2(vertices[indices[f*3+2]*5], 
						 vertices[indices[f*3+2]*5+1], 
						 vertices[indices[f*3+2]*5+2]);

			auto diff = v0 - pos;
			auto edge0 = v1 - v0;
			auto edge1 = v2 - v0;
			auto a00 = glm::dot(edge0, edge0);
			auto a01 = glm::dot(edge0, edge1);
			auto a11 = glm::dot(edge1, edge1);
			auto b0 = glm::dot(diff,edge0);
			auto b1 = glm::dot(diff,edge1);
			auto det = std::abs(a00 * a11 - a01 * a01);
			auto s = a01 * b1 - a11 * b0;
			auto t = a01 * b0 - a00 * b1;

			//NearestTriEntity ne;
			if (s + t <= det) {
				if (s < 0) {
					if (t < 0) { // region 4
						if (b0 < 0) {
							t = 0;
							if (-b0 >= a00) {
								// VN1
								//ne = NearestTriEntity::Vert1;
								s = 1;
							}
							else {
								// EN0
								//ne = NearestTriEntity::Edge0;
								s = -b0 / a00;
							}
						}
						else {
							s = 0;

							if (b1 >= 0) {
								// VN0
								//ne = NearestTriEntity::Vert0;
								t = 0;
							}
							else if (-b1 >= a11) {
								// VN2
								//ne = NearestTriEntity::Vert2;
								t = 1;
							}
							else {
								// EN2
								//ne = NearestTriEntity::Edge2;
								t = -b1 / a11;
							}
						}
					}
					else { // region 3
						s = 0;

						if (b1 >= 0) {
							// VN0
							//ne = NearestTriEntity::Vert0;
							t = 0;
						}
						else if (-b1 >= a11) {
							// VN2
							//ne = NearestTriEntity::Vert2;
							t = 1;
						}
						else {
							// EN2
							//ne = NearestTriEntity::Edge2;
							t = -b1 / a11;
						}
					}
				}
				else if (t < 0) { // region 5
					t = 0;

					if (b0 >= 0) {
						// VN0
						//ne = NearestTriEntity::Vert0;
						s = 0;
					}
					else if (-b0 >= a00) {
						// VN1
						//ne = NearestTriEntity::Vert1;
						s = 1;
					}
					else {
						// EN0
						//ne = NearestTriEntity::Edge0;
						s = -b0 / a00;
					}
				}
				else { // region 0
					// FN
					//ne = NearestTriEntity::Face;
					// minimum at interior point
					auto invDet = (1) / det;
					s *= invDet;
					t *= invDet;
				}
			}
			else {
				double tmp0, tmp1, numer, denom;

				if (s < 0) { // region 2
					tmp0 = a01 + b0;
					tmp1 = a11 + b1;

					if (tmp1 > tmp0) {
						numer = tmp1 - tmp0;
						denom = a00 - (2) * a01 + a11;

						if (numer >= denom) {
							// VN1
							//ne = NearestTriEntity::Vert1;
							s = 1;
							t = 0;
						}
						else {
							// EN1
							//ne = NearestTriEntity::Edge1;
							s = numer / denom;
							t = 1 - s;
						}
					}
					else {
						s = 0;

						if (tmp1 <= 0) {
							// VN2
							//ne = NearestTriEntity::Vert2;
							t = 1;
						}
						else if (b1 >= 0) {
							// VN0
							//ne = NearestTriEntity::Vert0;
							t = 0;
						}
						else {
							// EN2
							//ne = NearestTriEntity::Edge2;
							t = -b1 / a11;
						}
					}
				}
				else if (t < 0) { // region 6
					tmp0 = a01 + b1;
					tmp1 = a00 + b0;

					if (tmp1 > tmp0) {
						numer = tmp1 - tmp0;
						denom = a00 - 2 * a01 + a11;

						if (numer >= denom) {
							// VN2
							//ne = NearestTriEntity::Vert2;
							t = 1;
							s = 0;
						}
						else {
							// EN1
							//ne = NearestTriEntity::Edge1;
							t = numer / denom;
							s = 1 - t;
						}
					}
					else {
						t = 0;

						if (tmp1 <= 0) {
							// VN1
							//ne = NearestTriEntity::Vert1;
							s = 1;
						}
						else if (b0 >= 0) {
							// VN0
							//ne = NearestTriEntity::Vert0;
							s = 0;
						}
						else {
							// EN0
							//ne = NearestTriEntity::Edge0;
							s = -b0 / a00;
						}
					}
				}
				else { // region 1 
					numer = a11 + b1 - a01 - b0;

					if (numer <= 0) {
						// VN2
						//ne = NearestTriEntity::Vert2;
						s = 0;
						t = 1;
					}
					else {
						denom = a00 - (2) * a01 + a11;
						if (numer >= denom) {
							// VN1
							//ne = NearestTriEntity::Vert1;
							s = 1;
							t = 0;
						}
						else {
							// EN1
							//ne = NearestTriEntity::Edge1;
							s = numer / denom;
							t = 1 - s;
						}
					}
				}
			}

			glm::vec3 np = v0 + edge0 * s + edge1 * t;
			float dist = glm::distance(pos, np);
			if(dist<minDist) {
				nearestPnt = np;
				minDist = dist;
			}
		}

		float val = minDist;
		voxels[v] = std::max<float>(0.0f, val);	
	}

	ofstream outfile;
	outfile.open(file, ios::out | ios::trunc);
	outfile<<"PVM"<<endl;
	//write dimensions
	outfile<<to_string(m_surfDistDims.x)<<" "
			<<to_string(m_surfDistDims.y)<<" "
			<<to_string(m_surfDistDims.z)<<endl;
	//format
	outfile<<"8 1"<<endl;
	//data
	int c=0;
	m_surfDistData.resize(m_surfDistDims.x*m_surfDistDims.y*m_surfDistDims.z);
	for(int k = 0 ; k < m_surfDistDims.x; k++) {
		for(int j = 0; j < m_surfDistDims.y; j++) {
			for(int i = 0; i < m_surfDistDims.z; i++) {
				float val = voxels[k*(m_surfDistDims.x*m_surfDistDims.y)
										+ j*(m_surfDistDims.x)+i];
				outfile<<(unsigned char)(max<float>(0.0,val)*float(UCHAR_MAX));
				m_surfDistData[c]=val;
				c++;
			}
		}
	}
	outfile.close();	
	map<int, bool>::iterator itUp = m_coordsUpMap.begin();
	for(; itUp!=m_coordsUpMap.end(); ++itUp) {
		itUp->second=true;
	}

	//set the chosen file as the texture layer
    //setTextureLayersFolder(folder);
    //m_attributesMap["texture_layers_folder"]
    //    ->initStrings(vector<string>(1, folder));
}

void RevealedModule::openSurfaceDistanceTexture(const string& file) {
	parsePvm(file, m_surfDistDims, m_surfDistData);

	//ask for reinitializing texture uniform
	map<int, bool>::iterator itUp = m_coordsUpMap.begin();
	for(; itUp!=m_coordsUpMap.end(); ++itUp) {
		itUp->second=true;
	}
}

void RevealedModule::parsePvm(const std::string& file, 
							  glm::ivec3& dimensions, 
							  std::vector<float> &data) {
	ifstream inpfile;
	inpfile.open(file, ios::in);
	cout<<"Loading pvm file "<<endl;

	//read comment line
	string type;
	getline(inpfile, type);
	if(type.compare("PVM")==0) {
		//read dimensions line
		string dims;
		getline(inpfile, dims);
		istringstream dimsIss(dims);
		string dim;
		getline(dimsIss, dim, ' ');
		dimensions.x = stoi(dim);
		getline(dimsIss, dim, ' ');
		dimensions.y = stoi(dim);
		getline(dimsIss, dim, ' ');
		dimensions.z = stoi(dim);

		//recreate data
		data.resize(dimensions.x*dimensions.y*dimensions.z);

		//read data type line
		string type;
		getline(inpfile, type);
		istringstream typeIss(type);
		string typ;
		getline(typeIss, typ, ' ');

		//read data depending on type
		if(typ.compare("8")==0) {
			for(int c=0; c<int(data.size()); ++c) {
				unsigned char val = inpfile.get();
				data[c] = float(val)/float(UCHAR_MAX);
			}
		}
	}

	cout<<"Done loading"<<endl;
	inpfile.close();

}

//Gradient
void RevealedModule::setGradientType(const std::string& type) {
    int t=0;
    vector<string>::iterator itType=m_gradientTypes.begin();
    for(; itType!=m_gradientTypes.end(); ++itType, ++t) {
        if((*itType).compare(type)==0) {
            m_gradientType = t;
        }
    }
}

void RevealedModule::setGradientTexture(const std::string& f) {
    Fl_Shared_Image* newImage = Fl_Shared_Image::get(f.c_str(), 
                                                     m_texWidth, 
                                                     m_texHeight);
    if(newImage) {
        m_gradientImage=newImage;
        map<int, bool>::iterator itUp = m_gradientUpMap.begin();
        for(; itUp!=m_gradientUpMap.end(); ++itUp) {
            itUp->second=true;
        }
	}
}

//Density
void RevealedModule::setDensityType(const std::string& type) {
    int t=0;
    vector<string>::iterator itType=m_insideDensityTypes.begin();
    for(; itType!=m_insideDensityTypes.end(); ++itType, ++t) {
        if((*itType).compare(type)==0) {
            m_insideDensityType = t;
        }
    }
}


//Texture
void RevealedModule::setTexture(const std::string& f) {
    Fl_Shared_Image* newImage = Fl_Shared_Image::get(f.c_str(), 
                                                     m_texWidth, 
                                                     m_texHeight);
    if(newImage) {
        m_insideImage=newImage;
        //ask for reinitializing texture uniform
        map<int, bool>::iterator itUp = m_insideUpMap.begin();
        for(; itUp!=m_insideUpMap.end(); ++itUp) {
            itUp->second=true;
        }
    }
}

void RevealedModule::setTextureOffset(const float& x, 
                                      const float& y,
                                      const float& z) {
	m_textureOffset[0]=x;
    m_textureOffset[1]=y;
    m_textureOffset[2]=z;
}

void RevealedModule::setTextureScale(const float& x, 
                                     const float& y,
                                     const float& z) {
    m_textureScale[0]=(x!=0)?1.0/x:0;
    m_textureScale[1]=(y!=0)?1.0/y:0;
    m_textureScale[2]=(z!=0)?1.0/z:0;
}

void RevealedModule::setTextureLayersFolder(const std::string& folder) {
    dirent** filesList;
    int nbFiles = fl_filename_list(folder.c_str(), &filesList, fl_alphasort);
    m_layers.clear();
    for(int f=0; f<nbFiles; ++f) {
        m_texWidth=640;
        m_texHeight=480;
        string fullName = folder+"/"+string(filesList[f]->d_name);
        string extStr = string(fl_filename_ext(fullName.c_str()));
        if(extStr.compare(".png")==0 || extStr.compare(".jpg")==0 
				|| extStr.compare(".pgm")==0 ) {
            Fl_Shared_Image* newImage = Fl_Shared_Image::get(fullName.c_str(), 
                                                             m_texWidth, 
                                                             m_texHeight);
            if(newImage) {
                if(newImage->refcount()>0) {
                    newImage->reload();
                }
                RevealedLayerImage* layIm = new RevealedLayerImage();
                layIm->m_image = newImage;
                m_layers.push_back(layIm);
            }
        }
        else if(extStr.compare(".ogg")==0 
                || extStr.compare(".mp4")==0
                || extStr.compare(".mov")==0) {
            cv::VideoCapture movie; 
            if(movie.open(fullName.c_str())) {
                RevealedLayerVideo* layVi = new RevealedLayerVideo(m_texWidth, 
                                                                    m_texHeight);
                layVi->setVideo(movie);
                m_layers.push_back(layVi);
				m_textureCanDelete=false;
            }
        }
        else if(fl_filename_isdir(fullName.c_str()) && 
                    string(filesList[f]->d_name)[0]!='.'){ //animation folder
            dirent** anList;
            int nbAn=fl_filename_list(fullName.c_str(), &anList, fl_alphasort);
            vector<Fl_Shared_Image*> ans;
            for(int f=0; f<nbAn; ++f) {
                string anName = fullName+"/"+string(anList[f]->d_name);
                string ext2Str = string(fl_filename_ext(anName.c_str()));
                if(ext2Str.compare(".png")==0 || ext2Str.compare(".jpg")==0) {
                    Fl_Shared_Image* newAn=Fl_Shared_Image::get(anName.c_str(), 
                                                                  m_texWidth, 
                                                                  m_texHeight);
                    if(newAn) {
                        ans.push_back(newAn);
                    }
                }
            }
            if(ans.size()>0) {
                RevealedLayerAnim* layAn = new RevealedLayerAnim();
                layAn->m_images = ans;
                m_layers.push_back(layAn);
				m_textureCanDelete=false;
            }
        }
    }

    if(m_layers.size()>0) {
        //create data buffer of corresponding depth, width and height
        if(m_layersData) {
            delete [] m_layersData;
        }
        m_layersNb=m_layers.size();
        int nbBytes = m_texWidth*m_texHeight*m_layersNb*3;
        m_layersData = new char[nbBytes];

        //first texture update
        int d=0;
        vector<RevealedLayer*>::iterator itLa = m_layers.begin();
        for(; itLa!=m_layers.end(); ++itLa, ++d) {
            (*itLa)->update(0, d, m_layersData, m_texWidth, m_texHeight);
			if(m_textureCanDelete) {
				(*itLa)->releaseData();
			}
        }

        //ask for reinitializing texture uniform
        map<int, bool>::iterator itUp = m_insideUpMap.begin();
        for(; itUp!=m_insideUpMap.end(); ++itUp) {
            itUp->second=true;
        }
    }
}

void RevealedModule::setAnimPos(const float& pos) {
    vector<RevealedLayer*>::iterator itLa = m_layers.begin();
    for(; itLa!=m_layers.end(); ++itLa) {
        (*itLa)->setAnimPos(pos/100.0);
    }
}

void RevealedModule::setAnimPlay(const bool& play) {
    vector<RevealedLayer*>::iterator itLa = m_layers.begin();
    for(; itLa!=m_layers.end(); ++itLa) {
        (*itLa)->setAnimPlay(play);
    }
    if(play) {
        Reveal::getInstance()->addUpdateObserver(this);
    }
    else {
        Reveal::getInstance()->removeUpdateObserver(this);
    }
}

void RevealedModule::setAnimSpeed(const float& speed) {
    vector<RevealedLayer*>::iterator itLa = m_layers.begin();
    for(; itLa!=m_layers.end(); ++itLa) {
        (*itLa)->setAnimSpeed(speed);
    }
}

//Surface
void RevealedModule::setSurface(const std::string& surface) {
    int sur=0;
    vector<string>::iterator itSur=m_surfaces.begin();
    for(; itSur!=m_surfaces.end(); ++itSur, ++sur) {
        if((*itSur).compare(surface)==0) {
            m_surface=sur;
        }
    }
    map<int, bool>::iterator itUp = m_surfaceUpMap.begin();
    for(; itUp!=m_surfaceUpMap.end(); ++itUp) {
        itUp->second=true;
    }
}

void RevealedModule::setSurfaceColor(const vector<float>& hsv) {
    hsv2rgb(hsv, m_surfaceColor);
}

void RevealedModule::setSurfaceTexture(const std::string& f) {
    Fl_Shared_Image* newImage = Fl_Shared_Image::get(f.c_str(), 
                                                     m_texWidth, 
                                                     m_texHeight);
    if(newImage) {
        m_surfaceImage = newImage;
        //ask for reinitializing texture uniform
        map<int, bool>::iterator itUp = m_surfaceUpMap.begin();
        for(; itUp!=m_surfaceUpMap.end(); ++itUp) {
            itUp->second=true;
        }
    }
}

void RevealedModule::setSurfaceThickness(const float& surface) {
    m_surfaceThickness=surface;
}

void RevealedModule::gotAttributeListenersUpdate(Attribute* att) {
    bool active=false;
    vector<Attribute*>::iterator itAtt = m_revAttributes.begin();
    for(; itAtt!=m_revAttributes.end(); ++itAtt) {
        if((*itAtt)->editListeners().size()>0) {
            active=true;
        }
    } 
    if(active) {
        Reveal::getInstance()->registerOutputRevealed(this);
    }
    else {
        Reveal::getInstance()->unregisterOutputRevealed(this);

		//reset offsets
		unsigned int off=0;
		updateRevOffset(off);
		m_revSize=0;
    }
}


void RevealedModule::updateRevOffset(unsigned int& offset) {
	//Test all reveal output attributes
	m_revSize=1;
	//inside is always active so that we can check for other attributes
	m_revIns=0;
	if(m_attributesMap["revealed_inside"]->editListeners().size()>0) {
		m_revIns=m_revSize;
	}
	m_revSize+=1;

	m_revSurf=0;
	if(m_attributesMap["revealed_surface"]->editListeners().size()>0) {
		m_revSurf=m_revSize;
	}
	m_revSize+=1;

	m_revCent=0;
	if(m_attributesMap["revealed_center"]->editListeners().size()>0
			|| m_attributesMap["revealed_extent"]->editListeners().size()>0) {
		m_revCent=m_revSize;
	}
	m_revSize+=6;

	m_revCol=0;
	if(m_attributesMap["revealed_color"]->editListeners().size()>0) {
		m_revCol=m_revSize;
	}
	m_revSize+=4;

	m_revHis=0;
	if(m_attributesMap["revealed_histo"]->editListeners().size()>0) {
		m_revHis=m_revSize;
	}
	m_revSize+=(m_outHistoSize+1);

	m_revVox=0;
	if(m_attributesMap["revealed_voxels"]->editListeners().size()>0) {
		m_revVox=m_revSize;
	}
	m_revSize+=(m_outVoxelSize*5);
}

void RevealedModule::processReveal(const unsigned int& depthID,
								   const string& depthName,
								   const unsigned int& depthNb,
								   unsigned int* outputImg) {

	int offset = ((m_revID-1)*depthNb+(depthID-1))*m_revSize;

	//test if shape revealed
	int revealed = outputImg[offset];
	bool send=false;
    if(m_depthModsMap.find(depthID)==m_depthModsMap.end()) {
        if(revealed) {
            send=true;
            m_depthModsMap[depthID]=true;
        }
    }
    else {
        send=true;
        if(!revealed) {
            m_depthModsMap.erase(depthID);
        }
    }

	if(send) {
		vector<string> names(1, depthName);
		m_attributesMap["revealed_name"]->setStringsIfChanged(names);

		if(m_revIns>0) {
			vector<float> inside(1, outputImg[offset+m_revIns]);
			m_attributesMap["revealed_inside"]->setFloatsIfChangedOrCount(inside);
		}

		if(m_revSurf>0) {
			vector<float> vals(1, outputImg[offset+m_revSurf]);
			m_attributesMap["revealed_surface"]->setFloatsIfChangedOrCount(vals);
		}

		if(m_revCent>0 && revealed&1) {
			float fMinX = (1000.0-float(outputImg[offset+m_revCent]))/1000.0;
			float fMaxX = float(outputImg[offset+m_revCent+1])/1000.0;
			float fMinY = (1000.0-float(outputImg[offset+m_revCent+2]))/1000.0;
			float fMaxY = float(outputImg[offset+m_revCent+3])/1000.0;
			float fMinZ = (1000.0-float(outputImg[offset+m_revCent+4]))/1000.0;
			float fMaxZ = float(outputImg[offset+m_revCent+5])/1000.0;

			vector<float> center = {(fMaxX+fMinX)/2.0f,
									(fMaxY+fMinY)/2.0f,
									(fMaxZ+fMinZ)/2.0f};
			m_attributesMap["revealed_center"]->setFloatsIfChanged(center);
			vector<float> extent = {fabs(fMaxX-fMinX),
									fabs(fMaxY-fMinY),
									fabs(fMaxZ-fMinZ),
									(fabs(fMaxX-fMinX) +
										fabs(fMaxY-fMinY) + 
										fabs(fMaxZ-fMinZ))/3.0f};
			m_attributesMap["revealed_extent"]->setFloatsIfChanged(extent);
		}

		if(m_revCol>0 && revealed&1) {
			float nbPoints = float(outputImg[offset+m_revCol]);
			if(nbPoints>0) {
				vector<float> color = 
				 {(float(outputImg[offset+m_revCol+1])/1000.0f)/float(nbPoints),
				 (float(outputImg[offset+m_revCol+2])/1000.0f)/float(nbPoints),
				 (float(outputImg[offset+m_revCol+3])/1000.0f)/float(nbPoints)};
				m_attributesMap["revealed_color"]->setFloatsIfChanged(color);
			}
		}

		if(m_revHis>0 && revealed&1) {
			float nbPoints = float(outputImg[offset+m_revHis]);
			if(nbPoints>0) {
				vector<float> histo;
				for(int i=1; i<m_outHistoSize+1; i++) {
				  histo.push_back(float(outputImg[offset+m_revHis+i])/nbPoints);
				}
				m_attributesMap["revealed_histo"]->setFloatsIfChanged(histo);
			}
		}

		if(m_revVox>0 && revealed&1) {
			vector<float> voxels;
			for(int i=0; i<m_outVoxelSize; i++) {
				float nbPoints = float(outputImg[offset+m_revVox+i*5]);
				if(nbPoints>0) {
					//coords average
					voxels.push_back((float(outputImg[offset+m_revVox+i*5+1])/1000.0)
										/ nbPoints);
					voxels.push_back((float(outputImg[offset+m_revVox+i*5+2])/1000.0)
										/ nbPoints);
					voxels.push_back((float(outputImg[offset+m_revVox+i*5+3])/1000.0)
										/ nbPoints);
					//luminance average
					voxels.push_back((float(outputImg[offset+m_revVox+i*5+4])/1000.0)
										/ nbPoints);
				}
				else {
					voxels.push_back(0);
					voxels.push_back(0);
					voxels.push_back(0);
					voxels.push_back(0);
				}
			}
			m_attributesMap["revealed_voxels"]->setFloatsIfChanged(voxels);
		}
	}
}

void RevealedModule::hsv2rgb(const vector<float>& hsv, 
                             vector<float>& rgb) {
    float hh, p, q, t, ff;
    long   i;
    if(hsv[1] <= 0.0) {
        rgb[0] = hsv[2]/100.0;
        rgb[1] = hsv[2]/100.0;
        rgb[2] = hsv[2]/100.0;
        return;
    }
    hh = hsv[0];
    if(hh >= 360.0) { 
        hh = 0.0;
    }
    hh /= 60.0;
    i = (long)hh;
    ff = hh - i;
    p = hsv[2]/100.0 * (1.0 - hsv[1]/100.0);
    q = hsv[2]/100.0 * (1.0 - (hsv[1]/100.0 * ff));
    t = hsv[2]/100.0 * (1.0 - (hsv[1]/100.0 * (1.0 - ff)));
    switch(i) {
        case 0:
            rgb[0] = hsv[2]/100.0;
            rgb[1] = t;
            rgb[2] = p;
            break;
        case 1:
            rgb[0] = q;
            rgb[1] = hsv[2]/100.0;
            rgb[2] = p;
            break;
        case 2:
            rgb[0] = p;
            rgb[1] = hsv[2]/100.0;
            rgb[2] = t;
            break;
        case 3:
            rgb[0] = p;
            rgb[1] = q;
            rgb[2] = hsv[2]/100.0;
            break;
        case 4:
            rgb[0] = t;
            rgb[1] = p;
            rgb[2] = hsv[2]/100.0;
            break;
        case 5:
        default:
            rgb[0] = hsv[2]/100.0;
            rgb[1] = p;
            rgb[2] = q;
            break;
    }
}                          


//----------------------Simulation--------------------------
void* reactThreadFunction(void* pvParam) {
    ((RevealedModule*)pvParam)->textureReact();
    return NULL;
}

void RevealedModule::setTextureReact(const std::string& type) {
    int t=0;
    vector<string>::iterator itType=m_reactivityTypes.begin();
    for(; itType!=m_reactivityTypes.end(); ++itType, ++t) {
        if((*itType).compare(type)==0) {
            m_reactivityType = t;
        }
    }
	//stop reactive thread
	if(m_reactThread) {
		m_reactActive=false;
		m_reactThread->join();
		delete m_reactThread;
		m_reactThread=NULL;
	}
	
	//start it again if reactive
	if(m_reactivityType>0) {
		m_reactActive=true;
		m_reactThread = new std::thread(reactThreadFunction, this);
	}
}

void RevealedModule::textureReact() {
	while(m_reactActive) {
		//get voxel output from attribute
		m_reactLock.lock();
		vector<float> voxels(m_attributesMap["revealed_voxels"]->getFloats());
		int inside = m_attributesMap["revealed_inside"]->getFloats()[0];
		m_reactLock.unlock();

		//reset input texture
		for(int v=0; v<m_voxSize; ++v) {
			m_reactTexInput[v]=0;
		}

		//store revealed in input texture
		if(inside>0) {
			for(int v=0; v<m_outVoxelSize; ++v) {
				if(voxels[v*4]>0) { //if coord active
					int coord = int((voxels[v*4])*float(m_voxDim)) //x
						+ int((voxels[v*4+1])*float(m_voxDim))*m_voxDim //y
						+ int((voxels[v*4+2])*float(m_voxDim))*m_voxDim*m_voxDim;
					m_reactTexInput[coord] = 1.0;
				}
			}
		}
		
		//update voxels
		float step=1;
		for(int v=0; v<m_voxSize; ++v) {
			if(m_reactTexInput[v]>0) {
				m_reactTexInternal[v]=min<float>(m_reactTexInternal[v]+step, 
												 250.0);
			}
			else {
				m_reactTexInternal[v]=max<float>(m_reactTexInternal[v]-step, 
												0.0);
			}
		}

		//update react texture and update map
		m_reactLock.lock();
			for(int v=0; v<m_voxSize; ++v) {
				m_reactTexOutput[v*3]=char(10*sqrt(m_reactTexInternal[v]));
				m_reactTexOutput[v*3+1]=char(10*sqrt(m_reactTexInternal[v]));
				m_reactTexOutput[v*3+2]=char(10*sqrt(m_reactTexInternal[v]));
			}
			for(auto& m : m_reactUpMap) {
				m.second=true;
			}
		m_reactLock.unlock();
		usleep(10000);
	}
}


//-----------------------------Layers-------------------------------------

RevealedLayer::RevealedLayer(): m_playing(false), m_speed(1), 
                                m_update(true), m_accumTimeDiff(0){}

bool RevealedLayerImage::update(const int& timeDiffMs, const int& depth, 
                                char* data,
                                const int& width, const int& height) {
    if(m_update) {
        for(int i=0; i<width; ++i) {
            for(int j=0; j<height; ++j) {
                if(m_image->d()==4) {
                    data[(depth*width*height+j*width+i)*3]
                       =m_image->data()[0][(j*m_image->w()+i)*m_image->d()]
                       *((m_image->data()[0][(j*m_image->w()+i)*m_image->d()+3]
                                   ==0)?0:1);
                    data[(depth*width*height+j*width+i)*3+1]
                       =m_image->data()[0][(j*m_image->w()+i)*m_image->d()+1]
                       *((m_image->data()[0][(j*m_image->w()+i)*m_image->d()+3]
                                   ==0)?0:1);
                    data[(depth*width*height+j*width+i)*3+2]
                       =m_image->data()[0][(j*m_image->w()+i)*m_image->d()+2]
                       *((m_image->data()[0][(j*m_image->w()+i)*m_image->d()+3]
                                   ==0)?0:1);
                }
                else {
					data[(depth*width*height+j*width+i)*3]
						= m_image->data()[0][(j*m_image->w()+i)*m_image->d()];
					data[(depth*width*height+j*width+i)*3+1]
						= m_image->data()[0][(j*m_image->w()+i)*m_image->d()+1];
					data[(depth*width*height+j*width+i)*3+2]
						= m_image->data()[0][(j*m_image->w()+i)*m_image->d()+2];
                }
            }
        }
        m_update=false;
    }
    return false;
}

bool RevealedLayerVideo::update(const int& timeDiffMs, const int& depth, 
                                char* data,
                                const int& width, const int& height) {
    bool res=false;
    //try lock 
    if(m_gotFrame) {
        if(m_lock.try_lock()) {
            //copy frame to data
            memcpy(&data[depth*width*height*3], &m_data[0], width*height*3);
            res=true;
            m_gotFrame=false;
            m_lock.unlock();
        }
    }

    return res;
}


void RevealedLayerVideo::read() {
    m_video.set(cv::CAP_PROP_POS_FRAMES, 0);
    while(m_playing) {

		int nbFrames=0;
		bool gotFrame=false;
        if(m_playing) {

            timeval curTime, timeDiff;
            gettimeofday(&curTime,NULL);
            timersub(&curTime, &m_prevTime, &timeDiff);
            int timeDiffMs = timeDiff.tv_sec*1000.0+timeDiff.tv_usec/1000.0;
            m_accumTimeDiff=float(timeDiffMs)*m_speed;
			int supFrame = m_accumTimeDiff
								/(1000.0/m_vidFps);
			if(supFrame>m_curFrame) {
                m_update=true;
				nbFrames=supFrame-m_curFrame;
			}
        }

        for(; nbFrames>0; nbFrames--) {
			try {
				//if(!m_video.read(m_frame)) {
				if(!m_video.grab()) {
					/*
					m_video.set(cv::CAP_PROP_POS_FRAMES, 0);
					gettimeofday(&m_prevTime,NULL);
					m_curFrame=0;
					*/
					m_playing=false;
				}
				else {
					gotFrame=true;
					m_curFrame++;
				}
			}catch(const std::exception& e) {

			}
		}
        if(m_update) {
			if(gotFrame) {
				cv::Mat frame;
				m_video.retrieve(frame);
                m_lock.lock();
                float scaX = float(m_vidWidth)/float(m_texWidth);
                float scaY = float(m_vidHeight)/float(m_texHeight);
                for(float i=0; i<m_texWidth; ++i) {
                    for(float j=0; j<m_texHeight; ++j) {
                        m_data[int(j*m_texWidth+i)*3]
                           = frame.data[(int(j*scaY)*m_vidWidth+int(i*scaX))*3+2];
                        m_data[int(j*m_texWidth+i)*3+1]
                           = frame.data[(int(j*scaY)*m_vidWidth+int(i*scaX))*3+1];
                        m_data[int(j*m_texWidth+i)*3+2]
                           = frame.data[(int(j*scaY)*m_vidWidth+int(i*scaX))*3];
                    }
                }
                m_gotFrame=true;
                m_lock.unlock();
            }
            m_update=false;
        }

		
		/*
        if(m_playing) {
            timeval curTime, timeDiff;
            gettimeofday(&curTime,NULL);
            timersub(&curTime, &m_prevTime, &timeDiff);
            int timeDiffMs = timeDiff.tv_sec*1000.0+timeDiff.tv_usec/1000.0;
            m_accumTimeDiff=float(timeDiffMs)*m_speed;
			int nextFrame = m_accumTimeDiff
								/(1000.0/m_vidFps);
			if(nextFrame!=m_curFrame) {
				if(nextFrame>=m_vidFrames) {
					m_curFrame = nextFrame-m_vidFrames;;
				}
				else {
					m_curFrame=nextFrame;
				}
                m_update=true;
			}
        }

		cout<<"blup?"<<endl;
		if(m_update) {
			cout<<"blip?"<<endl;
			m_video.set(cv::CAP_PROP_POS_FRAMES, m_curFrame);
			if(m_video.read(m_frame)) {
                m_lock.lock();
                float scaX = m_vidWidth/m_texWidth;
                float scaY = m_vidHeight/m_texHeight;
                for(int i=0; i<m_texWidth; ++i) {
                    for(int j=0; j<m_texHeight; ++j) {
                        m_data[(j*m_texWidth+i)*3]
                           = m_frame.data[int(j*scaY*m_vidWidth+i*scaX)*3+2];
                        m_data[(j*m_texWidth+i)*3+1]
                           = m_frame.data[int(j*scaY*m_vidWidth+i*scaX)*3+1];
                        m_data[(j*m_texWidth+i)*3+2]
                           = m_frame.data[int(j*scaY*m_vidWidth+i*scaX)*3];
                    }
                }
                m_gotFrame=true;
                m_lock.unlock();
				m_update=false;
			}
		}
		*/

		/*
		//img test
        if(m_playing) {

            timeval curTime, timeDiff;
            gettimeofday(&curTime,NULL);
            timersub(&curTime, &m_prevTime, &timeDiff);
            int timeDiffMs = timeDiff.tv_sec*1000.0+timeDiff.tv_usec/1000.0;
            m_accumTimeDiff=float(timeDiffMs)*m_speed;
			int nextFrame = m_accumTimeDiff
								/(1000.0/m_vidFps);
			if(nextFrame!=m_curFrame) {
				if(nextFrame>=m_vidFrames) {
					m_curFrame = nextFrame-m_vidFrames;;
				}
				else {
					m_curFrame=nextFrame;
				}
                m_update=true;
			}
        }

		if(m_update) {
			m_lock.lock();
				memcpy(&m_data[0], 
						&m_vidData[m_curFrame*m_texWidth*m_texHeight*3], 
						m_texWidth*m_texHeight*3);
			m_gotFrame=true;
			m_lock.unlock();
		}
		*/



        usleep(5000);
    }
}

void RevealedLayerVideo::setVideo(cv::VideoCapture vid) {
	m_video=vid;
	m_vidWidth = m_video.get(cv::CAP_PROP_FRAME_WIDTH);
	m_vidHeight = m_video.get(cv::CAP_PROP_FRAME_HEIGHT);
	m_vidFps = vid.get(cv::CAP_PROP_FPS);
	m_vidFrames = vid.get(cv::CAP_PROP_FRAME_COUNT);

	/*
	//test with images
	m_video=vid;
	m_vidWidth = vid.get(cv::CAP_PROP_FRAME_WIDTH);
	m_vidHeight = vid.get(cv::CAP_PROP_FRAME_HEIGHT);
	
	if(m_vidData!=NULL) {
		delete [] m_vidData;
	}
	
	m_vidData = new char[m_vidFrames*m_texWidth*m_texHeight*3];
	float scaX = m_vidWidth/m_texWidth;
	float scaY = m_vidHeight/m_texHeight;

	for(int f=0; f<m_vidFrames; ) {
		if(vid.read(m_frame)) {
			for(int i=0; i<m_texWidth; ++i) {
				for(int j=0; j<m_texHeight; ++j) {
					m_vidData[(f*m_texWidth*m_texHeight+j*m_texWidth+i)*3]
					   = m_frame.data[int(j*scaY*m_vidWidth+i*scaX)*3+2];
					m_vidData[(f*m_texWidth*m_texHeight+j*m_texWidth+i)*3+1]
					   = m_frame.data[int(j*scaY*m_vidWidth+i*scaX)*3+1];
					m_vidData[(f*m_texWidth*m_texHeight+j*m_texWidth+i)*3+2]
					   = m_frame.data[int(j*scaY*m_vidWidth+i*scaX)*3];
				}
			}
			f++;
		}
		usleep(1000);
	}

	vid.release();
	*/
}

void RevealedLayerVideo::setAnimPos(const float& pos) {
    m_video.set(cv::CAP_PROP_POS_FRAMES, 
                float(m_video.get(cv::CAP_PROP_FRAME_COUNT))*pos);
    m_update=true;
}

RevealedLayerAnim::RevealedLayerAnim():RevealedLayer() {
    m_current=0;
}

bool RevealedLayerAnim::update(const int& timeDiffMs, const int& depth, 
                               char* data,
                               const int& width, const int& height) {
    bool res=false;
    if(m_playing) {
        m_accumTimeDiff+=float(timeDiffMs)*m_speed;
        if(m_accumTimeDiff>200.0) {
            m_current++;
            if(m_current>=int(m_images.size())) {
                m_current=0;
            }
            m_accumTimeDiff=0;
            m_update=true;
        }
    }
    if(m_update) {
        Fl_Shared_Image* im = m_images[m_current];
        for(int i=0; i<width; ++i) {
            for(int j=0; j<height; ++j) {
                data[(depth*width*height+j*width+i)*3]
                   =im->data()[0][(j*im->w()+i)*im->d()];
                data[(depth*width*height+j*width+i)*3+1]
                   =im->data()[0][(j*im->w()+i)*im->d()+1];
                data[(depth*width*height+j*width+i)*3+2]
                   =im->data()[0][(j*im->w()+i)*im->d()+2];
            }
        }
        res=true;
        m_update=false;
    }
    return res;
}

void RevealedLayerAnim::setAnimPos(const float& pos) {
    m_current = float(m_images.size()-1)*pos;
    m_update=true;
}

/*
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR) {
    }
    while ((err = glGetError()) != GL_NO_ERROR) {
        string error;
        switch(err) {
            case GL_INVALID_OPERATION:  error="INVALID_OPERATION"; break;
            case GL_INVALID_ENUM:  error="INVALID_ENUM";           break;
            case GL_INVALID_VALUE: error="INVALID_VALUE"; break;
            case GL_OUT_OF_MEMORY: error="OUT_OF_MEMORY"; break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:  error="INVALID_FRAMEBUFFER_OPERATION";  break;
        }
        cout << "OpenGL error before draw: " << error << endl;
    }
*/

