/***************************************************************************
 *  GroupModule.cpp
 *  Part of GroupModule
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "GroupModule.hpp" 

#include <iostream>
#include "../Reveal.hpp"

using namespace std;

GroupModule::GroupModule():Module() {
    m_type="Group";
}

GroupModule::~GroupModule() {}

void GroupModule::draw() {
    vector<Module*>::iterator itChild = m_children.begin();
    for(; itChild!=m_children.end(); ++itChild) {
        (*itChild)->draw();
    }
}

void GroupModule::deleteModule() {
    deleteChildren();
    Module::deleteModule();
}

void GroupModule::deleteChildren() {
    while(m_children.size()>0) {
        m_children.back()->deleteModule();
    }
    m_childrenMap.clear();
}

void GroupModule::addChild(Module* mod) {
    unsigned int newChildID=0;
    while(m_childrenMap.find(newChildID)!=m_childrenMap.end()) {
        newChildID++;
    }
    m_childrenMap[newChildID]=mod;
    m_children.push_back(mod);
    mod->setChildID(newChildID);
    mod->setParent(this);
    mod->setDepth(m_depth+1);
    updateModelMatrix();
    setVisible(m_visible);
}

void GroupModule::removeChild(Module* mod) {
    m_childrenMap.erase(mod->getChildID());
    vector<Module*>::iterator itChild = m_children.begin();
    for(; itChild!=m_children.end();) {
        if((*itChild)->getChildID()==mod->getChildID()) {
            itChild=m_children.erase(itChild);
        }
        else {
            ++itChild;
        }
    }
}

void GroupModule::getSubTree(std::vector<Module*>& children) {
    children.push_back(this);
    vector<Module*>::iterator itChild = m_children.begin();
    for(; itChild!=m_children.end(); ++itChild) {
        (*itChild)->getSubTree(children);
    }
}

void GroupModule::load(xmlNodePtr node) {
    Module::load(node);
    xmlNodePtr modsNode;
    for(modsNode=node->children; modsNode; modsNode=modsNode->next){
        if(modsNode->type == XML_ELEMENT_NODE &&
                !xmlStrcmp(modsNode->name, (const xmlChar *) "Modules")) {
            xmlNodePtr modNode;
            for(modNode=modsNode->children; modNode; modNode=modNode->next){
                if(modNode->type == XML_ELEMENT_NODE) {
                    string typeStr((char*) modNode->name);
                    Module* mod = Reveal::getInstance()->createModule(typeStr);
                    if(mod) {
                        addChild(mod);
                        mod->load(modNode);
                    }
                }
            }
        }
    }
}

xmlNodePtr GroupModule::save(xmlNodePtr parentNode) {
    xmlNodePtr newNode = Module::save(parentNode);
    xmlNodePtr modsNode = xmlNewChild(newNode, NULL, 
                                      BAD_CAST "Modules", NULL);
    vector<Module*>::iterator itChild = m_children.begin();
    for(; itChild!=m_children.end(); ++itChild) {
        (*itChild)->save(modsNode);
    }
    return newNode;
}

void GroupModule::listModulesAndAttributes(const std::string& parentName, 
                                      std::map<std::string, Module*>& mods,
                                      std::map<std::string, Attribute*>& atts) {
    Module::listModulesAndAttributes(parentName, mods, atts);
    vector<Module*>::iterator itChild = m_children.begin();
    for(; itChild!=m_children.end(); ++itChild) {
        (*itChild)->listModulesAndAttributes(parentName+"/"+m_name, mods, atts);
    }
}

void GroupModule::updateModelMatrix() {
    Module::updateModelMatrix();
    vector<Module*>::iterator itChild = m_children.begin();
    for(; itChild!=m_children.end(); ++itChild) {
        (*itChild)->setParentMatrix(m_modelMat);
        (*itChild)->updateModelMatrix();
    }
}

void GroupModule::setVisible(bool vis) {
    m_visible=vis;
    vector<Module*>::iterator itChild = m_children.begin();
    for(; itChild!=m_children.end(); ++itChild) {
        (*itChild)->setParentVisible(m_parentVisible&&m_visible);
    }
}

void GroupModule::setParentVisible(bool pvis) {
    m_parentVisible=pvis;
    setVisible(m_visible);
}

void GroupModule::getVisibleFromList(std::vector<string>& modVec, 
                                     std::map<std::string, int>& modMap) { 
    vector<Module*>::iterator itChild = m_children.begin();
    for(; itChild!=m_children.end(); ++itChild) {
        (*itChild)->getVisibleFromList(modVec, modMap);
    }
}

