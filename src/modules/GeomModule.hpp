/***************************************************************************
 *  GeomModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef GeomModule_h
#define GeomModule_h

#include "Module.hpp"

#include "../Reveal.hpp"

class GeomModule : public virtual Module {
    public:
        GeomModule();
        virtual ~GeomModule();
        void deleteModule();

        virtual void draw(const int& contextID,
                          const Reveal::REVIL_PROGRAM& prog, 
                          std::map<Reveal::REVIL_UNIFORM, GLint>& uniforms, 
                          const unsigned int& component=0);

        virtual void setGeomID(const unsigned int& id){m_geomID=id;}
        virtual const unsigned int& getGeomID(){return m_geomID;}

    protected:
        unsigned int m_geomID;
        bool m_localInsideAxes;
        Geometry* m_currentGeom;
		std::vector<glm::mat4> m_subShapeInvMats;
        std::map<int, glm::vec4> m_boundingRect;
		std::map<int, bool> m_boundingRectUpdate;
        glm::vec3 m_bbPnts[2];
};


#endif

