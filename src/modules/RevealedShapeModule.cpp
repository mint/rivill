/***************************************************************************
 *  RevealedShapeModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "RevealedShapeModule.hpp" 

#include "../geoms/Geometry.hpp"

using namespace std;

RevealedShapeModule::RevealedShapeModule(): ShapeModule(), RevealedModule(){
    m_type="Shape";
    m_name="shape";
    addNameAttribute();
    addRemoveAttribute();
    addVisibleAttribute();
    addVisibleFromAttribute();
    addRevealedByAttribute();
    addShapeAttributes();
    addTransformationAttributes();
	addFollowMarkerAttribute();
    addRevealedAttributes();
    addRevealedInsideAttributes();
    addRevealedOutputAttributes();
	/*
    vector<string> inside(1, "color_gradient");
    m_attributesMap["inside"]->setStrings(inside);
	*/
    vector<string> shape(1, "sphere");
    m_attributesMap["shape"]->setStrings(shape);
    vector<float> dims(3,200.0);
    m_attributesMap["dimensions"]->setFloats(dims);
}

RevealedShapeModule::~RevealedShapeModule() {
    Reveal::getInstance()
                ->getGeom(m_shapesMap[m_currentShape])
                                ->unregisterModule(this);
}

void RevealedShapeModule::setShape(const std::string& shape) {
	if(m_shapesMap.find(shape)!=m_shapesMap.end()) {
        if(m_currentShape.compare("")!=0) {
            Reveal::getInstance()
                ->getGeom(m_shapesMap[m_currentShape])
                    ->unregisterModule(this);
        }
        m_currentShape=shape;
        Reveal::getInstance()
            ->getGeom(m_shapesMap[m_currentShape])
                ->registerModule(this);
        m_currentGeom=Reveal::getInstance()
                        ->getGeom(m_shapesMap[m_currentShape]);
		m_shapeGeom = m_shapesMap[m_currentShape];
    }
}

