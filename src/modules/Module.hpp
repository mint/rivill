/***************************************************************************
 *  Module.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef Module_h
#define Module_h

#include "Attribute.hpp"

#define GLM_ENABLE_EXPERIMENTAL

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp> 
#include <glm/mat4x4.hpp> 
#include <glm/gtc/quaternion.hpp> 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class GroupModule;
class MarkerListener;

class Module {
    public:
        Module();
        virtual ~Module();

        virtual void draw(){}

        virtual void load(xmlNodePtr node);
        virtual xmlNodePtr save(xmlNodePtr parentNode);

        void setParent(GroupModule* groupmod){m_parent=groupmod;}
        void setChildID(const unsigned int& id){m_childID=id;}
        const unsigned int& getChildID(){return m_childID;}
        virtual void getSubTree(std::vector<Module*>& children) {
            children.push_back(this);
        }

        inline unsigned int getNbAttributes(){return m_attributesVec.size();}
        inline Attribute* getAttribute(const unsigned int& attr) { 
            return m_attributesVec[attr];
        }
        inline Attribute* getAttribute(const std::string& attr) { 
            return m_attributesMap[attr];
        }
        inline std::vector<Attribute*>& editAttributes() { 
            return m_attributesVec;
        }
        inline void addAttribute(Attribute* attr) { 
            m_attributesMap[attr->getName()]=attr;
            m_attributesVec.push_back(attr);
        }
        static void nameCallback(Module* mod, 
                                 const std::vector<std::string>& vals) {
            static_cast<Module*>(mod)->setModuleName(vals[0]);
        }
        static void removeCallback(Module* mod) {
            static_cast<Module*>(mod)->deleteModule();        
        }
        virtual void deleteModule();
        static void positionCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            static_cast<Module*>(mod)->setPosition(vals[0], 
                                                   vals[1], 
                                                   vals[2]);
        }
        virtual void setPosition(const float& posX, 
                                 const float& posY, 
                                 const float& posZ);
        static void absPositionCallback(Module* mod, 
                                     const std::vector<float>& vals) {}

        static void scaleCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            static_cast<Module*>(mod)->setScale(vals[0], 
                                                vals[1], 
                                                vals[2]);
        }
        virtual void setScale(const float& scaX, 
                              const float& scaY, 
                              const float& scaZ);
        static void rotationCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            static_cast<Module*>(mod)->setRotation(vals[0], 
                                                   vals[1], 
                                                   vals[2]);
        }
        virtual void setRotation(const float& rotX, 
                                 const float& rotY, 
                                 const float& rotZ);
        static void quaternionCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            static_cast<Module*>(mod)->setQuaternion(vals[0], 
                                                     vals[1], 
                                                     vals[2],
                                                     vals[3]);
        }
        virtual void setQuaternion(const float& rotX, 
                                   const float& rotY, 
                                   const float& rotZ,
                                   const float& rotW);

        const glm::mat4& getModelMat(){return m_modelMat;}
        virtual void setModelMat(const glm::mat4& mat);

        static void followMarkerCallback(Module* mod, 
                                     const std::vector<int>& vals) {
            static_cast<Module*>(mod)->setFollowMarker(vals[0]);
        }
        virtual void setFollowMarker(const int& mark);

        static void visibleCallback(Module* mod, 
                                    const std::vector<bool>& vals) {
            static_cast<Module*>(mod)->setVisible(vals[0]);
        }

        static void visibleFromCallback(Module* mod, 
                                        const std::vector<std::string>& vals) {
            static_cast<Module*>(mod)->setVisibleFrom(vals[0]);
        }

        static void revealedByCallback(Module* mod, 
                                       const std::vector<std::string>& vals) {
            static_cast<Module*>(mod)->setRevealedBy(vals[0]);
        }
        void setRevealedBy(const std::string& by);

        inline const std::string& getName(){return m_name;}
        void setName(const std::string& val);
        inline virtual void setModuleName(const std::string& val){setName(val);}
        inline const std::string& getType(){return m_type;}
        inline void setDepth(const unsigned int& depth){m_depth=depth;}
        inline const unsigned int& getDepth(){return m_depth;}
        inline const std::string& getFullName(){return m_fullName;}

        inline virtual void setVisible(bool vis){m_visible=vis;}
        inline virtual void setParentVisible(bool pvis){m_parentVisible=pvis;}
        inline bool isVisible(){return m_visible && m_parentVisible;}
        void setVisibleFrom(const std::string& visibleFrom);
        void setVisibleFromList(std::vector<std::string>& modVec);
        virtual void getVisibleFromList(std::vector<std::string>& modVec, 
                                        std::map<std::string, int>& modMap){}
        inline const int& getVisibilityMask(){return m_visibilityMask;}
        inline void  setVisibilityMask(int mask){m_visibilityMask=mask;}

        virtual void listModulesAndAttributes(const std::string& parentName, 
                                      std::map<std::string, Module*>& mods,
                                      std::map<std::string, Attribute*>& atts);

        inline void setParentMatrix(const glm::mat4& mat){m_parentMat=mat;} 
        virtual void updateModelMatrix();

        virtual void gotAttributeListenersUpdate(Attribute*){}

        virtual void addNameAttribute();
        virtual void addRemoveAttribute();
        virtual void addTransformationAttributes();
        virtual void addVisibleAttribute();
        virtual void addVisibleFromAttribute();
        virtual void addRevealedByAttribute();
		virtual void addFollowMarkerAttribute();

        inline void setUpID(const unsigned int& id){m_upID=id;}
        inline const int& getUpID(){return m_upID;}
        virtual void update(const int& timeDiffMs){}

    protected:
        void getQuatFromTwoVecs(glm::quat& q, glm::vec3& from, glm::vec3& to);

    protected:
        std::string m_name;
        std::string m_fullName;
        std::string m_type;
        bool m_visible;
        bool m_parentVisible;
        int m_visibilityMask;
        unsigned int m_depth;
        GroupModule* m_parent;
        unsigned int m_childID;
        glm::mat4 m_transform;
        std::map<std::string, Attribute*> m_attributesMap;
        std::vector<Attribute*> m_attributesVec;
        std::vector<std::string> m_revealedBys;
        int m_revealedBy;
        int m_upID;
        glm::vec3 m_position;
        glm::vec3 m_scale;
        glm::quat m_rotQuat;
        glm::vec3 m_rotEuler;
        glm::mat4 m_rotMat;
        glm::mat4 m_modelMat;
        glm::mat4 m_invModelMat;
        glm::mat4 m_parentMat;
		glm::vec3 m_modelScale;
        int m_texWidth;
        int m_texHeight;

		std::vector<MarkerListener> m_markerListeners;
};

class MarkerListener : public Listener {
	public : 
		MarkerListener(Module* mod);
		virtual ~MarkerListener();
        virtual void update(std::vector<float>& floatValues, 
                            std::vector<int>& intValues,
                            std::vector<bool>& boolValues,
                            std::vector<std::string>& stringValues);
		inline void setMarkerID(const int& m){m_markerID=m;}
	private:
		int m_markerID;
		Module* m_mod;
};


#endif
