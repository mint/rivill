/***************************************************************************
 *  Listener.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef Listener_h
#define Listener_h

#include <string>
#include <vector>
#include <libxml/parser.h>
#include <libxml/tree.h>

class Attribute;

class Listener {
    public:
        Listener();
        virtual ~Listener(){}
        void deleteListener();

        virtual void load(xmlNodePtr node);
        virtual void save(xmlNodePtr parentNode);

        virtual void update(std::vector<float>& floatValues, 
                            std::vector<int>& intValues,
                            std::vector<bool>& boolValues,
                            std::vector<std::string>& stringValues)=0;
                            
        inline const int& getID(){return m_listenerID;}
        inline void setID(const int& id){m_listenerID=id;}
        inline virtual void setAttribute(Attribute* att){m_attribute=att;}
		inline Attribute* getAttribute(){return m_attribute;}

        inline std::vector<std::string>& editParameters(){return m_parameters;}
        inline std::vector<std::vector<std::string> >& editParametersValues() {
            return m_parametersValues;
        }
        virtual void setParameters(const std::vector<std::string>& par);

        inline const std::string& getListenerType(){return m_listenerType;}
        inline std::string& editListenerType(){return m_listenerType;}


    protected:
        int m_listenerID;
        Attribute* m_attribute;
        std::string m_listenerType;
        std::vector<std::string> m_parameters;
        std::vector<std::vector<std::string> > m_parametersValues;
        bool m_saved;
};

#endif
