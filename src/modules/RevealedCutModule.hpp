/***************************************************************************
 *  RevealedCutModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef RevealedCutModule_h
#define RevealedCutModule_h


#include "ShapeModule.hpp"
#include "RevealedModule.hpp"

#include <FL/Fl_Box.H>
#include <FL/Fl_Image_Surface.H>

#ifdef LINUX
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#endif


class CutWindow;
class CutWindowsManager;

class RevealedCutModule : public ShapeModule, public RevealedModule {
    public:
        RevealedCutModule();
        virtual ~RevealedCutModule();
        void draw(const int& contextID,
                  const Reveal::REVIL_PROGRAM& prog, 
                  std::map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                  const unsigned int& comp=0);
        virtual void update(const int& timeDiffMs);

        static void windowCallback(Module* mod, 
                                 const std::vector<std::string>& vals) {
            dynamic_cast<RevealedCutModule*>(mod)->setWindow(vals[0]);
        }
        void setWindow(const std::string& text);
        static void refreshWindowsCallback(Module* mod) {
            dynamic_cast<RevealedCutModule*>(mod)->refreshWindows();
        }
        virtual void refreshWindows();
        static void selPosCallback(Module* mod, 
                                   const std::vector<int>& vals) {
            dynamic_cast<RevealedCutModule*>(mod)->selPos(vals[0], vals[1]);
        }
        void selPos(const int& posX, const int& posY);
        static void selSizeCallback(Module* mod, 
                                    const std::vector<int>& vals) {
            dynamic_cast<RevealedCutModule*>(mod)->selSize(vals[0], vals[1]);
        }
        void selSize(const int& sizeX, const int& sizeY);
        static void updateOnceCallback(Module* mod, 
                                       const std::vector<bool>& vals) {
            dynamic_cast<RevealedCutModule*>(mod)->updateOnce(vals[0]);
        }
        void updateOnce(bool);
        void recomputePixels();

    protected:
        std::string m_text;
        CutWindow* m_cutWin;
        int m_winX, m_winY, m_winW, m_winH;
        float* m_coordImage;
        int m_nbPixPerRow;
        std::vector<int> m_srcIndices;
        std::vector<std::vector<int> > m_srcCoords;
        std::vector<int> m_destIndices;
        bool m_updateOnce;
        bool m_updateOnceDone;
		std::vector<std::string> m_windowNames;
		bool m_initTex;
};

class CutWindow {
    public:
#ifdef LINUX
        CutWindow(CutWindowsManager* man, Display* disp, const Window& win);
        ~CutWindow();
#endif
        int computeNbPixPerRow(const int& srcW, const int& srcH);
        uchar* grabImage();
        void releaseImage();

        void setName(const std::string& name){m_name=name;}
        const std::string& getName(){return m_name;}
        const int& getHeight(){return m_height;} 
        const int& getWidth(){return m_width;} 

    private:
        CutWindowsManager* m_winsMan;
        int m_winID;
        std::string m_name;
        int m_posX, m_posY, m_width, m_height;
        int m_offsetX, m_offsetY, m_pixPerRow;
        uchar* m_imgData;
        uchar* m_defaultImgData;
        bool m_grabbed;
        bool m_init;

#ifdef LINUX
        Display* m_disp;
        Window m_win;
        XImage* m_ximg;
#endif
};

class CutWindowsManager {
    public:
        static CutWindowsManager* getInstance();
        ~CutWindowsManager();
        void updateWindowsList();
        std::vector<CutWindow*>& editWindowList(){return m_windowList;}
        CutWindow* getWindow(const std::string& name) {
            CutWindow* res=NULL;
            std::vector<CutWindow*>::iterator itWin=m_windowList.begin();
            for(; itWin!=m_windowList.end(); ++itWin) {
                if((*itWin)->getName().compare(name)==0 && name.compare("")!=0){
                    res=(*itWin);
                }
            }
            return res;
        }
        int getNbWindows(){return m_windowList.size();}

        #ifdef LINUX
        Window* getWinList(Display* disp, unsigned long* len);
        char* getWinName(Display* disp, Window);
        #endif

    private:
        CutWindowsManager();
        std::vector<CutWindow*> m_windowList;
        std::map<unsigned int, CutWindow*> m_windowMap;

        #ifdef LINUX
        Display* m_disp;
        #endif
};


#endif

