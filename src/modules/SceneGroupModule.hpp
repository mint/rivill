/***************************************************************************
 *  SceneGroupModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef SceneGroupModule_h
#define SceneGroupModule_h

#include "RootSceneGroupModule.hpp"

class SceneGroupModule: public RootSceneGroupModule {
    public:
        SceneGroupModule();
        virtual ~SceneGroupModule(){}

        static void nameCallback(Module* mod, 
                                 const std::vector<std::string>& vals) {
            dynamic_cast<SceneGroupModule*>(mod)->setName(vals[0]);
        }
        static void deleteCallback(Module* mod) {
            dynamic_cast<SceneGroupModule*>(mod)->deleteModule();
        }
        static void visibleCallback(Module* mod, 
                                    const std::vector<bool>& vals) {
            dynamic_cast<SceneGroupModule*>(mod)->setVisible(vals[0]);
        }
        static void positionCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            dynamic_cast<SceneGroupModule*>(mod)->setPosition(vals[0], 
                                                             vals[1], 
                                                             vals[2]);
        }
        static void scaleCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            dynamic_cast<SceneGroupModule*>(mod)->setScale(vals[0], 
                                                          vals[1], 
                                                          vals[2]);
        }
        static void rotationCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            dynamic_cast<SceneGroupModule*>(mod)->setRotation(vals[0], 
                                                             vals[1], 
                                                             vals[2]);
        }
};

#endif


