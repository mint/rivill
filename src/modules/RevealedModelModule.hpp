/***************************************************************************
 *  RevealedModelModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef RevealedModelModule_h
#define RevealedModelModule_h

#include "ModelModule.hpp"
#include "RevealedModule.hpp"

class RevealedModelModule : public ModelModule, public RevealedModule {
    public:
        RevealedModelModule();
        virtual ~RevealedModelModule();
        void draw(const int& contextID,
                  const Reveal::REVIL_PROGRAM& prog, 
                  std::map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                  const unsigned int& comp=0);
        void gotAttributeListenersUpdate(Attribute* att) { 
            RevealedModule::gotAttributeListenersUpdate(att);
        }

        static void generateSurfaceDistanceCallback(Module* mod) {
            dynamic_cast<RevealedModelModule*>(mod)
							->generateSurfaceDistanceTexture();
        }
		void generateSurfaceDistanceTexture();
		void setModel(const std::string& fileName);

    protected:
};


#endif

