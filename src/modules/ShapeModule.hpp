/***************************************************************************
 *  ShapeModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef ShapeModule_h
#define ShapeModule_h

#include "GeomModule.hpp"

class ShapeModule : public virtual GeomModule {
    public:
        ShapeModule();
        virtual ~ShapeModule();

        static void shapeCallback(Module* mod, 
                                  const std::vector<std::string>& vals) {
            dynamic_cast<ShapeModule*>(mod)->setShape(vals[0]);
        }
        virtual void setShape(const std::string&){}
        static void dimensionsCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            dynamic_cast<ShapeModule*>(mod)->setDimensions(vals[0], 
                                                           vals[1], 
                                                           vals[2]);
        }
        void setDimensions(const float&, const float&, const float&);

        virtual void updateModelMatrix();
        void addShapeAttributes();

    protected:
        std::vector<std::string> m_shapes;
        std::map<std::string, Reveal::GEOM_ID> m_shapesMap;
        glm::vec3 m_dimensions;
        std::string m_currentShape;
};


#endif

