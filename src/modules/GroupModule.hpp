/***************************************************************************
 *  GroupModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef GroupModule_h
#define GroupModule_h

#include "Module.hpp"

class GroupModule: public virtual Module {
    public:
        GroupModule();
        virtual ~GroupModule();
        virtual void draw();
        virtual void addChild(Module* mod);
        virtual void removeChild(Module* mod);
        virtual void getSubTree(std::vector<Module*>& children); 
        void deleteModule();
        void deleteChildren();

        virtual void load(xmlNodePtr node);
        virtual xmlNodePtr save(xmlNodePtr parentNode);

        void listModulesAndAttributes(const std::string& parentName, 
                                      std::map<std::string, Module*>& mods,
                                      std::map<std::string, Attribute*>& atts);
        virtual void updateModelMatrix();
        virtual void setVisible(bool vis);
        virtual void setParentVisible(bool pvis);

        virtual void getVisibleFromList(std::vector<std::string>& modVec, 
                                        std::map<std::string, int>& modMap);

    protected:
        std::vector<Module*> m_children;
        std::map<unsigned int, Module*> m_childrenMap;
};

#endif

