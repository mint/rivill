/***************************************************************************
 *  RevealedModelModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "RevealedModelModule.hpp" 

#include "../geoms/Geometry.hpp"

#include <math.h>
#define EPSILON 0.0001

using namespace std;

RevealedModelModule::RevealedModelModule(): ModelModule(), RevealedModule(){
    m_type="Model";
    m_name="model";
    addNameAttribute();
    addRemoveAttribute();
    addVisibleAttribute();
    addVisibleFromAttribute();
    addRevealedByAttribute();
    addModelAttributes();
    addTransformationAttributes();
	addFollowMarkerAttribute();
    addRevealedAttributes();

    addRevealedInsideStructureAttributes();

    addAttribute(new Attribute("generate_texture_from_surface",
                               Attribute::ACTION_ATTRIBUTE,
                               generateSurfaceDistanceCallback, this,
                               Attribute::LOCAL));

    addRevealedInsideGradientAttributes();
    addRevealedInsideDensityAttributes();
    addRevealedInsideTextureAttributes();

    addRevealedOutputAttributes();
    m_currentGeom = m_modelGeom;
	m_shapeGeom = Reveal::GEOM_MODEL;
}

RevealedModelModule::~RevealedModelModule() {
/*
    Reveal::getInstance()
                ->getGeom(m_shapesMap[m_currentModel])
                                ->unregisterModule(this);
*/
}

void RevealedModelModule::draw(const int& contextID, 
                               const Reveal::REVIL_PROGRAM& prog, 
                               map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                               const unsigned int& comp) {

    RevealedModule::draw(contextID, prog, uniforms, comp);
    if(prog==Reveal::RENDERPROG) {
		//send the surface distance texture
        glUniform1i(uniforms[Reveal::SURFDISTEX], 5);
        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_3D, m_coordsIDMap[contextID]);
        if(m_coordsUpMap[contextID]) {
			glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexImage3D(GL_TEXTURE_3D, 0, GL_R32F,
						m_surfDistDims.x, m_surfDistDims.y, m_surfDistDims.z,
						0, GL_RED, GL_FLOAT, m_surfDistData.data());
            m_coordsUpMap[contextID]=false;
			cout<<"update surfdist txture"<<endl;
        }
    }
}

void RevealedModelModule::setModel(const std::string& fileName) {
	ModelModule::setModel(fileName);
	//try opening texture from surface
	if(!m_modelStr.empty()) {
		openSurfaceDistanceTexture(m_modelStr+"_surfdist.pvm");
	}
	//ask for reinitializing texture uniform
	map<int, bool>::iterator itUp = m_coordsUpMap.begin();
	for(; itUp!=m_coordsUpMap.end(); ++itUp) {
		itUp->second=true;
	}
}


void RevealedModelModule::generateSurfaceDistanceTexture() {
	if(!m_modelStr.empty()) {
		RevealedModule::generateSurfaceDistanceTexture(m_modelStr
														+ "_surfdist.pvm");
	}
}


