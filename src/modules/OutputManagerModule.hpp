/***************************************************************************
 *  OutputManagerModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef OutputManagerModule_h
#define OutputManagerModule_h

#include "GroupModule.hpp"

#include "OutputOscModule.hpp"
#include "OutputPdModule.hpp"

class OutputManagerModule : public GroupModule {
    public :
        OutputManagerModule();
        virtual ~OutputManagerModule();

        static void addOscOutputCallback(Module* mod,
                                         const std::vector<std::string>& vals) {
            dynamic_cast<OutputManagerModule*>(mod)->addOscOutput(vals[0]);
        }
        void addOscOutput(const std::string&);
        static void addPdOutputCallback(Module* mod,
                                         const std::vector<std::string>& vals) {
            dynamic_cast<OutputManagerModule*>(mod)->addPdOutput(vals[0]);
        }
        void addPdOutput(const std::string&);

    private :


};

#endif

