/***************************************************************************
 *  ContextHandler.hpp
 *  Part of Revil
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef ContextHandler_h
#define ContextHandler_h

#include <glm/mat4x4.hpp> 

class ContextHandler {
    public:
        void setContextHandlerID(const int& id) {m_contextHandlerID=id;}
        const int& getContextHandlerID(){return m_contextHandlerID;}
		
		inline glm::mat4& getProjMat(){return m_projMat;}
		inline glm::mat4& getViewProjMat(){return m_viewProjMat;}

		inline const float& getWidth(){return m_width;}
		inline const float& getHeight(){return m_height;}

    protected:
        int m_contextHandlerID;
        float m_width, m_height;
        glm::mat4 m_projMat;
        glm::mat4 m_viewProjMat;
        glm::mat4 m_viewMat;
};


#endif

