/***************************************************************************
 *  RevealedPathModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "RevealedPathModule.hpp" 

#include <iostream>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp> 

#include "../Reveal.hpp"

#include "../geoms/Geometry.hpp"

using namespace std;
using namespace glm;

RevealedPathModule::RevealedPathModule(): RevealedModule() {
    m_type="Path";
    m_name="path";

    m_pathGeom = new Geometry();
    Reveal::getInstance()->registerGeom(m_pathGeom);
    m_pathGeom->registerModule(this);

    addNameAttribute();
    addRemoveAttribute();
    addVisibleAttribute();
    addVisibleFromAttribute();
    addRevealedByAttribute();
    addAttribute(new Attribute("add_point", 
                                Attribute::ACTION_FLOAT_ATTRIBUTE,
                                addPointCallback, this, 3));
    addAttribute(new Attribute("add_module_point", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addModulePointCallback, this, 1));
    addAttribute(new Attribute("remove_last_point", 
                                Attribute::ACTION_ATTRIBUTE,
                                removePointCallback, this));
    addAttribute(new Attribute("clear_points", 
                                Attribute::ACTION_ATTRIBUTE,
                                clearPointsCallback, this));
    addAttribute(new Attribute("thickness", 
                                Attribute::FLOAT_ATTRIBUTE,
                                thicknessCallback, this, 1));
    addAttribute(new Attribute("edges", 
                                Attribute::INT_ATTRIBUTE,
                                edgesCallback, this, 1));
    //addTransformationAttributes();
    addRevealedAttributes();
    addRevealedInsideAttributes();
    addRevealedOutputAttributes();

    m_attributesMap["inside_structure"]->setStrings(vector<string>(1,"along_z"));

    m_attributesMap["thickness"]->setFloats(vector<float>(1, 20));
    m_attributesMap["edges"]->setInts(vector<int>(1,8));
    m_attributesMap["gradient_type"]->setStrings(vector<string>(1, "along_z"));
    vector<string> mods(1, "select_module");
    m_attributesMap["add_module_point"]->editStringValuesChoices()
                                            .assign(1,mods);
    Reveal::getInstance()->addModuleListObserver(this);

    m_shapeGeom=Reveal::GEOM_PATH;
	m_pathInitialised=2;
    m_modelScale = vec3(1.0,1.0,1.0);
}

RevealedPathModule::~RevealedPathModule() {
    Reveal::getInstance()->unregisterGeom(m_pathGeom);
    Reveal::getInstance()->removeModuleListObserver(this);
}

void RevealedPathModule::deleteModule() {
    RevealedModule::deleteModule();
    Reveal::getInstance()->removeModuleListObserver(this);
}

void RevealedPathModule::load(xmlNodePtr node) {
    Module::load(node);
    xmlNodePtr pntsNode;
    for(pntsNode=node->children; pntsNode; pntsNode=pntsNode->next){
        if(pntsNode->type == XML_ELEMENT_NODE &&
                !xmlStrcmp(pntsNode->name, (const xmlChar *) "Points")) {
            xmlNodePtr pntNode;
            for(pntNode=pntsNode->children; pntNode; pntNode=pntNode->next){
                if(pntNode->type == XML_ELEMENT_NODE) {
                    if(!xmlStrcmp(pntNode->name, (const xmlChar *) "Point")) {
                        m_points.push_back(new PathPoint(glm::vec4(0,0,0,1)));
                        m_points.back()->loadPnt(pntNode);
                    }
                    else if(!xmlStrcmp(pntNode->name, 
                                       (const xmlChar *) "PointModule")) {
                        m_points.push_back(new PathPointModule(this));
                        m_points.back()->loadPnt(pntNode);
                    }
                }
            }
        }
    }
    refreshGeometry();    
}

xmlNodePtr RevealedPathModule::save(xmlNodePtr parentNode) {
    xmlNodePtr newNode = Module::save(parentNode);
    xmlNodePtr pntsNode = xmlNewChild(newNode, NULL, BAD_CAST"Points", NULL);
    vector<PathPoint*>::iterator itPnt = m_points.begin();
    for(; itPnt!=m_points.end(); ++itPnt) {
        (*itPnt)->savePnt(pntsNode);
    }
    return newNode;
}

void RevealedPathModule::addPoint(const float& x,   
                                  const float& y, 
                                  const float& z) {
    m_points.push_back(new PathPoint(glm::vec4(x, y, z, 1)));
    refreshGeometry();
}

void RevealedPathModule::updateModulesList(const map<string, Module*>& mods) {

    vector<vector<string> >& moduleNames = m_attributesMap["add_module_point"]
                                                ->editStringValuesChoices();
    moduleNames.clear();
    moduleNames.push_back(vector<string>());
    map<string, Module*>::const_iterator itMod = mods.begin();
    for(; itMod!=mods.end(); ++itMod) { //select names from scene modules
        if(itMod->first.find("/revil/scene/")==0) {
            moduleNames[0].push_back(itMod->first);
        }
    }
}

void RevealedPathModule::addModulePoint(const std::string& moduleStr) {
    Module* mod = NULL;
    mod = Reveal::getInstance()->findModule(moduleStr);
    if(mod) {
        m_points.push_back(new PathPointModule(this, mod));
        refreshGeometry();
    }
}

void RevealedPathModule::removePoint() {
    if(m_points.size()>0) {
        PathPoint* pnt = m_points.back();
        delete pnt;
        m_points.pop_back();
        refreshGeometry();
    }
}

void RevealedPathModule::clearPoints() {
    for(unsigned int p=0; p<m_points.size(); ++p) {
        PathPoint* pnt = m_points[p];
        delete pnt;
    }
    m_points.clear();
    refreshGeometry();
}

void RevealedPathModule::refreshGeometry() {
    //refresh local position of all points 
    for(unsigned int p=0; p<m_points.size(); ++p) {
        m_points[p]->refreshPos();
    }

    vector<GLfloat>& vertices = m_pathGeom->editVertexBufferData();
    vector<GLuint>& indices = m_pathGeom->editIndexBufferData();
    vector<int>& sizes = m_pathGeom->editCompoSizes();
    
    vertices.clear();
    indices.clear();
    sizes.clear();
    m_compoLocalSizes.clear();
    m_compoVertices.clear();
    m_compoRots.clear();

    m_subShapeInvMats.clear();

    if(m_points.size()>1) {
        //create all vertices
        glm::vec3 dirZ(0, 0, 1);
        unsigned int preC=0;
        unsigned int nexC=0;
        for(unsigned int p=0; p<m_points.size(); ++p) {
            glm::mat4 rot=glm::mat4(1.0);
            glm::mat4 rotSub=glm::mat4(1.0);

            //find the corresponding component
            if(m_points.size()<3) {
                preC=0;
                nexC=0;
            }
            else {
                preC=(p<=0)?0:int(p)-1;
                nexC=(p>=(m_points.size()-1))?p-1:p;
            }
			
			glm::vec3 dir(0.0);
			glm::vec3 dirSub(0.0);
            if(p>0) {
                // Direction for sub shape only uses current segment direction
                dirSub = glm::vec3(m_points[p]->editPos()
                                - m_points[int(p)-1]->editPos());
                // Direction for vertices adapts to next segment
                if(p+1==m_points.size()) {
                    dir = glm::vec3(m_points[p]->editPos()
                                    - m_points[int(p)-1]->editPos());
                }
                else {
                    dir = (glm::vec3(m_points[p]->editPos()
                                    - m_points[int(p)-1]->editPos())
                           + glm::vec3(m_points[p+1]->editPos()
                                    - m_points[p]->editPos()))
                        /2.0f;
                }
                rot = toMat4(rotation(dirZ, normalize(dir)));
                rotSub = toMat4(rotation(dirZ, normalize(dirSub)));
                /*
                glm::vec3 markR(1.0,0.0,0.0);
                glm::vec3 markUp = 
                    glm::normalize(glm::cross(markR, dirN));
                rot = glm::mat4x4( //r,up,n
                    markR[0], markR[1], markR[2], 0.0,
                    markUp[0], markUp[1], markUp[2],0.0,
                    dirN[0], dirN[1], dirN[2], 0.0,
                    0.0, 0.0, 0.0, 1.0
                    );
                    */
                /*
                float rx, ry, rz;
                glm::extractEulerAngleXYZ(rot, rx, ry, rz);
                rot = glm::eulerAngleXYZ(rx,ry,rz);
                */

                m_compoLocalSizes[preC] = 
                    glm::vec4(m_thickness, m_thickness, length(dir), 1.0);

                //build and store sub inverse matrix
                mat4 subMat = scale(mat4(1.0),vec3(m_thickness*2.0,
                                        m_thickness*2.0,
                                        length(dirSub)*2.0));
                subMat = rotSub * subMat;
                subMat = translate(mat4(1.0),vec3(m_points[int(p)-1]->editPos()
                                    + vec4(dirSub.x/2.0,dirSub.y/2.0,dirSub.z/2.0,1.0)))
                         * subMat;
                m_subShapeInvMats.push_back(inverse(subMat));
            }
            else {
                dir = glm::vec3(m_points[p+1]->editPos()
								- m_points[p]->editPos());
                glm::vec3 dirN = normalize(dir);
                rot = toMat4(rotation(dirZ, dirN));
                m_compoLocalSizes[preC] = 
                    glm::vec4(m_thickness,m_thickness, length(dir), 1.0);
            }

            //add the vertices
            float angle=0;
            float angleStep=M_PI*2.0/float(m_nbEdges);
            for(int v=0; v<m_nbEdges; ++v, angle+=angleStep) {
                glm::vec4 pnt = m_points[p]->editPos()
                                  +rot*glm::vec4(cos(angle)*m_thickness, 
                                                 sin(angle)*m_thickness, 
                                                 0, 1);
                vertices.push_back(pnt[0]);
                vertices.push_back(pnt[1]);
                vertices.push_back(pnt[2]);
                vertices.push_back(0);
                vertices.push_back(0);

                m_compoVertices[nexC].push_back(pnt);
                m_compoVertices[preC].push_back(pnt);
            }

            //save rotations/dirs
            m_compoRots[nexC]=rot;
            m_compoRots[preC]=rot;
        }

        //indices
        if(m_points.size()>1) {
            //create segments and backs
            for(int s=0; s<int(m_points.size())-1; ++s) {
                for(int v=0; v<m_nbEdges-2; ++v) {
                    indices.push_back(s*m_nbEdges+v);
                    indices.push_back(s*m_nbEdges+m_nbEdges-1);
                    indices.push_back(s*m_nbEdges+v+1);
                }
                for(int v=0; v<m_nbEdges; ++v) {
                    indices.push_back((s*m_nbEdges)+v);
                    indices.push_back((s*m_nbEdges)+(v+1)%m_nbEdges);
                    indices.push_back(((s+1)*m_nbEdges)+v);

                    indices.push_back(((s+1)*m_nbEdges)+v);
                    indices.push_back((s*m_nbEdges)+(v+1)%m_nbEdges);
                    indices.push_back(((s+1)*m_nbEdges)+(v+1)%m_nbEdges);
                }
                for(int v=0; v<m_nbEdges-2; ++v) {
                    indices.push_back((s+1)*m_nbEdges+v);
                    indices.push_back((s+1)*m_nbEdges+v+1);
                    indices.push_back((s+1)*m_nbEdges+m_nbEdges-1);
                }
                sizes.push_back(m_nbEdges*6+2*(m_nbEdges-2)*3);
            }
        }
    }
    m_pathGeom->refresh();
}

void RevealedPathModule::setThickness(const float& t) {
    if(t>0) {
        m_thickness=t;
    }
    refreshGeometry();
}

void RevealedPathModule::setEdges(const int& e) {
    if(e>=3) {
        m_nbEdges=e;
    }
    refreshGeometry();
}

void RevealedPathModule::updateModelMatrix() {
    GeomModule::updateModelMatrix();
    
}

void RevealedPathModule::processReveal(const vector<string>& names,
                                       const vector<float>& surfaces, 
                                       const vector<float>& insides,
                                       const vector<float>& center, 
                                       const vector<float>& extent,
                                       const vector<float>& color) {

    m_attributesMap["revealed_name"]->setStringsIfChanged(names);
    m_attributesMap["revealed_surface"]->setFloatsIfChanged(surfaces);
    m_attributesMap["revealed_inside"]->setFloatsIfChanged(insides);
    m_attributesMap["revealed_center"]->setFloatsIfChanged(center);
    m_attributesMap["revealed_extent"]->setFloatsIfChanged(extent);
    m_attributesMap["revealed_color"]->setFloatsIfChanged(color);
}

void RevealedPathModule::draw(const int& contextID,
							  const Reveal::REVIL_PROGRAM& prog, 
							  map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
							  const unsigned int& comp){

	RevealedModule::draw(contextID,prog,uniforms,comp);

	if(m_pathInitialised>0) {
		refreshGeometry();
		m_pathInitialised--;
	}
}


//-------------------------Path Points---------------------------------------

void PathPoint::loadPnt(xmlNodePtr node) {
    char* value = (char*)xmlGetProp(node, (xmlChar*)"x");
    if(value!=NULL) {
        m_point[0]=atof(value);
    }
    value = (char*)xmlGetProp(node, (xmlChar*)"y");
    if(value!=NULL) {
        m_point[1]=atof(value);
    }
    value = (char*)xmlGetProp(node, (xmlChar*)"z");
    if(value!=NULL) {
        m_point[2]=atof(value);
    }
}

xmlNodePtr PathPoint::savePnt(xmlNodePtr parentNode) {
    ostringstream oss, oss1, oss2;
    xmlNodePtr pntNode = xmlNewChild(parentNode, NULL, BAD_CAST "Point", NULL);
    oss<<m_point[0];
    oss1<<m_point[1];
    oss2<<m_point[2];
    xmlNewProp(pntNode, BAD_CAST "x", BAD_CAST oss.str().c_str());
    xmlNewProp(pntNode, BAD_CAST "y", BAD_CAST oss1.str().c_str());
    xmlNewProp(pntNode, BAD_CAST "z", BAD_CAST oss2.str().c_str());
    return pntNode;
}

PathPointModule::PathPointModule(RevealedPathModule* path): 
                                    PathPoint(), Listener(), m_module(NULL) {
    m_path=path;
    m_listenerType="PathListener";
}

PathPointModule::PathPointModule(RevealedPathModule* path, 
                                 Module* mod): PathPoint(), Listener() {
    m_path=path;
    m_listenerType="PathListener";
    m_module=mod;
    m_module->getAttribute("absolute_position")->addListener(this);
}

PathPointModule::~PathPointModule() {
    if(m_module) {
        m_module->getAttribute("absolute_position")->removeListener(this);
    }
}

void PathPointModule::update(vector<float>& floatValues,
                             vector<int>& intValues,
                             vector<bool>& boolValues,
                             vector<string>& stringValues) {
    if(floatValues.size()>=3) {
        m_path->refreshGeometry();
    }
}

void PathPointModule::refreshPos() {
    if(m_module) {
        const vector<float>& floatValues = m_module
                                            ->getAttribute("absolute_position")
                                                ->getFloats();
        m_absPoint[0]=floatValues[0];
        m_absPoint[1]=floatValues[1];
        m_absPoint[2]=floatValues[2];
        m_absPoint[3]=1;
        m_point = inverse(m_path->getModelMat())*m_absPoint;
    }
    else {
        m_module=Reveal::getInstance()->findModule(m_moduleName);
        if(m_module) {
            m_module->getAttribute("absolute_position")->addListener(this);
        }
    }
}

void PathPointModule::loadPnt(xmlNodePtr node) {
    char* value = (char*)xmlGetProp(node, (xmlChar*)"module_name");
    if(value!=NULL) {
        m_moduleName=string(value);
        m_module=Reveal::getInstance()->findModule(m_moduleName);
        if(m_module) {
            m_module->getAttribute("absolute_position")->addListener(this);
        }
    }
}

xmlNodePtr PathPointModule::savePnt(xmlNodePtr parentNode) {
    xmlNodePtr pntNode=xmlNewChild(parentNode, NULL, 
                                   BAD_CAST"PointModule",NULL);
    if(m_module) {
        xmlNewProp(pntNode, BAD_CAST"module_name", 
                            BAD_CAST m_module->getFullName().c_str());
    }
    return pntNode;
}

