/***************************************************************************
 *  DepthModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef DepthModule_h
#define DepthModule_h

#include "ShapeModule.hpp"

class SpaceModule;

class DepthModule : public virtual GeomModule {
    public:
        DepthModule();
        virtual ~DepthModule();
        void deleteModule();

        void setDepthID(const unsigned int&);
        const unsigned int& getDepthID(){return m_depthID;}

        virtual void setSpace(SpaceModule* space);

        virtual void draw(const int& contextID,
                          const Reveal::REVIL_PROGRAM& prog, 
                          std::map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                          const unsigned int& component);
        inline virtual int getNbSubDepthIDs(){return 1;}

        inline virtual bool isDepthVisible(const unsigned int& id) { 
            return m_visible;
        }

    protected:
        unsigned int m_depthID;
        int m_depthType;
        SpaceModule* m_space;
};


#endif

