/***************************************************************************
 *  ProjectorModule.hpp
 *  Part of Rivill
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef ProjectorModule_h
#define ProjectorModule_h

#include "GroupModule.hpp"
#include <iostream>
#include <pthread.h>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include "../geoms/QuadGeometry.hpp"

#include "../Reveal.hpp"

class SpaceModule;
class DepthCamModule;

class ProjectorModule : public Module, public ContextHandler {
    public:
        ProjectorModule();
        virtual ~ProjectorModule();
        virtual void draw();
        void deleteModule();

        static void activeCallback(Module* mod, 
                                           const std::vector<bool>& vals) {
            static_cast<ProjectorModule*>(mod)->setActive(vals[0]);
        }
        inline void setActive(bool active) {m_active=active;}
        static void calibrateCallback(Module* mod, 
                                        const std::vector<std::string>& vals) {
            static_cast<ProjectorModule*>(mod)->calibrate(vals[0]);
        }
        void calibrate(const std::string& camName);
        void calibrateCamWithMarker(const std::string& camName);
        static void windowDimensionsCallback(Module* mod, 
                                             const std::vector<int>& vals) {
            static_cast<ProjectorModule*>(mod)->setWindowDimensions(vals[0],
                                                                    vals[1],
                                                                    vals[2],
                                                                    vals[3]);
        }
        void setWindowDimensions(const int& x, const int& y, 
                                 const int& w, const int& h);
        static void windowSizeCallback(GLFWwindow* win, int w, int h) {
            static_cast<ProjectorModule*>(glfwGetWindowUserPointer(win)) 
                            ->updateWindowSize(w, h);
        }
        static void framebufferSizeCallback(GLFWwindow* win, int w, int h) {
            static_cast<ProjectorModule*>(glfwGetWindowUserPointer(win)) 
                            ->updateWindowSize(w, h);
        }
        void updateWindowSize(const int& w, const int& h);
        static void windowPositionCallback(GLFWwindow* win, int x, int y) {
            static_cast<ProjectorModule*>(glfwGetWindowUserPointer(win)) 
                            ->updateWindowPos(x, y);
        }
        void updateWindowPos(const int& x, const int& y);
        void updateWindowDims();

        static void windowDecorationCallback(Module* mod, 
                                             const std::vector<bool>& vals) {
            static_cast<ProjectorModule*>(mod)->setWindowDecoration(vals[0]);
        }
        void setWindowDecoration(bool d);
        static void fullscreenMonitorCallback(Module* mod, 
                                        const std::vector<std::string>& vals) {
            static_cast<ProjectorModule*>(mod)->fullscreenMonitor(vals[0]);
        }
        void fullscreenMonitor(const std::string& monitor);
        static void viewMatrixCallback(Module* mod, 
                                             const std::vector<float>& vals) {
            static_cast<ProjectorModule*>(mod)->setViewMatrix(vals);
        }
        void setViewMatrix(const std::vector<float>& vals);
        static void projectionMatrixCallback(Module* mod, 
                                             const std::vector<float>& vals) {
            static_cast<ProjectorModule*>(mod)->setProjectionMatrix(vals);
        }
        void setProjectionMatrix(const std::vector<float>& vals);
        static void attachToCamCallback(Module* mod, 
                                        const std::vector<std::string>& vals) {
            static_cast<ProjectorModule*>(mod)->attachToCam(vals[0]);
        }
        void attachToCam(const std::string& camName);
        static void outputRevealedCallback(Module* mod, 
                                           const std::vector<bool>& vals) {
            static_cast<ProjectorModule*>(mod)->outputRevealed(vals[0]);
        }
        void outputRevealed(bool output);
        static void audioProcessTypeCallback(Module* mod, 
									   const std::vector<std::string>& vals) {
            static_cast<ProjectorModule*>(mod)->setAudioProcessType(vals[0]);
        }
        void setAudioProcessType(const std::string& val);
        static void mirroredCallback(Module* mod, 
                                     const std::vector<bool>& vals) {
            static_cast<ProjectorModule*>(mod)->setMirrored(vals[0]);
        }
        inline void setMirrored(bool m){m_mirrored=m;}

        static void postFilterCallback(Module* mod, 
                                        const std::vector<int>& vals) {
            static_cast<ProjectorModule*>(mod)->postFilter(vals[0]);
        }
        void postFilter(const int& filt);

        static void transparentWindowCallback(Module* mod, 
                                        const std::vector<bool>& vals) {
            static_cast<ProjectorModule*>(mod)->setTransparentWindow(vals[0]);
        }
        void setTransparentWindow(const bool& transp);


        void setPatternSquareWidth(const int&);
        void setPatternPosOX(const int&);
        void setPatternPosOY(const int&);
        void setPatternPosRX(const int&);
        void setPatternPosRY(const int&);
        void setPatternPositionY(const int&);
        void setPatternBrightness(const int&);
        void setCalib(const int&);
        void setModuleName(const std::string&);

		inline const bool& getMirrored(){return m_mirrored;}

        void setSpace(SpaceModule* space);
        virtual void setModelMat(const glm::mat4& mat);
        void updateModelMatrix();
        void updateViewProjMatrix();
        inline void setProjID(const int& id){m_projID=id;}
        inline const int& getProjID(){return m_projID;}

        void refreshMonitors();

        void refreshDepthCamList(const std::vector<DepthCamModule*>);
        void processOutput();
		void processAudio();

    private:
        void initGL();
        void createRTT(GLuint& tex, GLuint& fbo, 
                        const int& width, const int& height, bool filter);

        void updateCalibrationPattern();


    private:
        int m_projID;
        GLFWwindow* m_projWindow;
        SpaceModule* m_space;
        DepthCamModule* m_attachedToCam;
        DepthCamModule* m_calibrationCam;
        glm::mat4 m_transViewMat;
        bool m_initialized;
        bool m_mirrored;
        bool m_active;
        GLuint m_selectTex;
        GLuint m_selectFBO;
        GLuint m_sliceTex;
        GLuint m_sliceRttTex;
        GLuint m_sliceSmpTex;
        GLuint m_sliceFBO;
        GLuint m_renderTex;
        GLuint m_renderFBO;
        float m_posX, m_posY;
        int m_rttWidth, m_rttHeight;
        bool m_processOutput;
        float m_postFilter;
		bool m_transparentBackground;

        QuadGeometry* m_calibGeom;

        //calibration
        cv::Mat m_projectedImg;
        std::vector<cv::Point2f> m_foundCorners;
        cv::Size m_patternSize;
        int m_patternSquareWidth; 
        int m_patternThresh;
        cv::Size m_patternPosOffset;
        cv::Size m_patternPosRange;
        cv::Size m_patternPosition; 
        int m_neededFramesNb;
        std::vector<cv::Point2f> m_generatedCorners;
        bool m_calibrated;
        int m_calibrating;
        int m_patternBrightness;
        bool m_calibrationStopped;
        cv::Size m_imageSize;

        //shader/programs
        std::map<Reveal::REVIL_PROGRAM, GLint> m_programs;
        std::map<Reveal::REVIL_PROGRAM, 
                 std::map<Reveal::REVIL_UNIFORM, GLint> > m_uniforms;

        //output
        GLuint m_outputTex;
        static const unsigned int m_outputImgSize=500;
        unsigned int* m_outputImg;
        unsigned int* m_outputImgInit;
        int m_outShapeSize;
        int m_outVoxelSize;
        int m_outHistoSize;
        int m_outDepthSize;
        int m_outNbDepth;

		//audio output
        GLuint m_audioTex;
        int* m_audioImg;
        int* m_audioImgInit;
		int m_nbTracks;
		int m_audioBufSize;
		int m_audioMixSize;
		int m_imgBufSize;
		float* m_audioBuffer;
		float m_maxAudioValue;
		int m_audioNextBuf;
		std::vector<std::string> m_audioProcessTypes;
		int m_audioProcessType;


};


#endif

