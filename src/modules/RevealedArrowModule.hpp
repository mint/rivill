/***************************************************************************
 *  RevealedArrowModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef RevealedArrowModule_h
#define RevealedArrowModule_h

#include "GroupModule.hpp"
#include "RevealedShapeModule.hpp"

class ArrowPoint;
class ArrowElementModule;

class RevealedArrowModule : public GroupModule, 
                            public ModuleListObserver {
    public:
        RevealedArrowModule();
        virtual ~RevealedArrowModule();
        virtual void deleteModule();

        void draw();
        void setVisible(bool vis);

        static void originModuleCallback(Module* mod, 
                                   const std::vector<std::string>& vals) {
            dynamic_cast<RevealedArrowModule*>(mod)->setOriginModule(vals[0]);
        }
        void setOriginModule(const std::string& mod);

        static void originOffsetCallback(Module* mod, 
                                   const std::vector<float>& vals) {
            dynamic_cast<RevealedArrowModule*>(mod)->setOriginOffset(
                                                        vals[0],
                                                        vals[1],
                                                        vals[2]);
        }
        void setOriginOffset(const float& x, 
                             const float& y, 
                             const float& z);

        static void destinationModuleCallback(Module* mod, 
                                   const std::vector<std::string>& vals) {
            dynamic_cast<RevealedArrowModule*>(mod)
                ->setDestinationModule(vals[0]);
        }
        void setDestinationModule(const std::string& mod);

        static void destinationOffsetCallback(Module* mod, 
                                   const std::vector<float>& vals) {
            dynamic_cast<RevealedArrowModule*>(mod)->setDestinationOffset(
                                                        vals[0],
                                                        vals[1],
                                                        vals[2]);
        }
        void setDestinationOffset(const float& x, 
                                  const float& y, 
                                  const float& z);

        static void elementsNumberCallback(Module* mod, 
                                   const std::vector<int>& vals) {
            dynamic_cast<RevealedArrowModule*>(mod)->setElementsNumber(vals[0]);
        }
        void setElementsNumber(const int& mod);

        static void elementsSpeedCallback(Module* mod, 
                                   const std::vector<float>& vals) {
            dynamic_cast<RevealedArrowModule*>(mod)->setElementsSpeed(vals[0]);
        }
        void setElementsSpeed(const float& mod);

        static void elementsThicknessCallback(Module* mod, 
                                   const std::vector<float>& vals) {
            dynamic_cast<RevealedArrowModule*>(mod)->setThickness(vals[0]);
        }
        void setThickness(const float& th);

        static void elementsDirectionCallback(Module* mod, 
                                   const std::vector<float>& vals) {
            dynamic_cast<RevealedArrowModule*>(mod)->setElementsDirection(
                                                        vals[0],
                                                        vals[1],
                                                        vals[2]);
        }
        void setElementsDirection(const float& x, 
                                 const float& y, 
                                 const float& z);

        static void followDirectionCallback(Module* mod, 
                                   const std::vector<bool>& vals) {
         dynamic_cast<RevealedArrowModule*>(mod)->setFollowDirection(vals[0]);
        }
        void setFollowDirection(bool f);

        static void elementsRevealedCallback(Module* mod, 
                                   const std::vector<float>& vals) {}
	void processRevealedElement();


        void update(const int& timeDiffMs);
        virtual void updateModulesList(const std::map<std::string, Module*>&);

	void gotAttributeListenersUpdate(Attribute* att);
        void updateArrowPoints();

    protected:
        int m_nbElements;
        float m_speed;
        ArrowPoint* m_origPnt;
        ArrowPoint* m_destPnt;
        glm::vec3 m_origin;
        glm::vec3 m_originOffset;
        glm::vec3 m_dest;
        glm::vec3 m_destOffset;
        glm::vec3 m_dir;
        float m_distance;
        std::vector<ArrowElementModule*> m_elements;
        std::vector<float> m_positions;
        std::string m_origModStr;
        std::string m_destModStr;
        float m_thickness;
        glm::vec3 m_elementsDirection;
        bool m_followDirection;
	std::vector<Attribute*> m_revAttributes;
	std::vector<float> m_revealed;
	bool m_revealedChanged;
};

class ArrowPoint : public Listener {
    public:
        ArrowPoint(RevealedArrowModule*);
        void setModule(Module* mod);
        inline Module* getModule(){return m_mod;}
        void update(std::vector<float>& floatValues, 
                    std::vector<int>& intValues,
                    std::vector<bool>& boolValues,
                    std::vector<std::string>& stringValues);
    private:
        RevealedArrowModule* m_arrowMod;
        Module* m_mod;
};

class ArrowElementModule : public RevealedShapeModule {
  public:
    ArrowElementModule(	const int& elID, 
			RevealedArrowModule* arr) : RevealedShapeModule(),
						    m_elementID(elID),
						    m_arrow(arr),
						    m_revealed(0) {
    }


    virtual void processReveal(const unsigned int& id,
			       const std::vector<std::string>& name, 
			       const std::vector<float>& surface, 
			       const std::vector<float>& inside,
			       const std::vector<float>& center,
			       const std::vector<float>& extent,
			       const std::vector<float>& color,
			       const std::vector<float>& blob) {

      if(m_revealed!=inside[0]) {
            m_revealed=inside[0];
            m_arrow->processRevealedElement();
      }
    }

    inline const float& isRevealed(){return m_revealed;}

  private:
    int m_elementID;
    int m_elementRevealed;
    RevealedArrowModule* m_arrow;
    float m_revealed;

};



#endif

