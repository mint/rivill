/***************************************************************************
 *  ModelModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#define AI_CONFIG_PP_SBP_REMOVE aiPrimitiveType_LINE|aiPrimitiveType_POINT

#include "ModelModule.hpp" 

#include <iostream>
#include <fstream>
#include <sstream>

#include "../Reveal.hpp"
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/polar_coordinates.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/highgui/highgui.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "../geoms/Geometry.hpp"

using namespace std;
using namespace glm;

ModelModule::ModelModule(): GeomModule() {
    m_type="Model";
    m_name="model";

    m_modelGeom = new Geometry();
    Reveal::getInstance()->registerGeom(m_modelGeom);
    m_modelGeom->registerModule(this);
    m_modelGeom->setIsModel(true);

    m_coordImage = new float[m_texWidth*m_texHeight*2];
}

ModelModule::~ModelModule() {}

void ModelModule::addModelAttributes() {
    addAttribute(new Attribute("model", 
                                Attribute::FILE_OPEN_ATTRIBUTE,
                                modelCallback, this));
    addAttribute(new Attribute("dimensions", 
                                Attribute::FLOAT_ATTRIBUTE,
                                dimensionsCallback, this, 3));
    m_attributesMap["dimensions"]->setFloats(vector<float>(3,100.0));
}

void ModelModule::updateModelMatrix() {
    Module::updateModelMatrix();

    mat4 dimMat = scale(m_dimensions);
    m_modelMat = m_modelMat * dimMat;
	m_invModelMat = inverse(m_modelMat);

    vec3 decTrans, decSkew;
    quat decRot;
    vec4 decPers;
    decompose(m_modelMat, m_modelScale, decRot, decTrans, decSkew, decPers);

	/*
    //update bounding box as well 
	vec4 pnts[8];
    vec4 locpnts[8];
    int p=0;
    for(float x=-0.5; x<1; x+=1.0) {
        for(float y=-0.5; y<1; y+=1.0) {
            for(float z=-0.5; z<1; z+=1.0) {
                pnts[p] = m_modelMat*vec4(x, y, z, 1.0);
                locpnts[p] = vec4(m_modelScale[0]*x, 
                                  m_modelScale[1]*y, 
                                  m_modelScale[2]*z, 
                                  1.0);
                ++p;
            }
        }
    }
    m_bbPnts[0]=pnts[0];
    m_bbPnts[1]=pnts[0];
    for(p=1; p<8; ++p) {
        for(int c=0; c<3;++c) {
            m_bbPnts[0][c]=std::min<float>(pnts[p][c], m_bbPnts[0][c]);
            m_bbPnts[1][c]=std::max<float>(pnts[p][c], m_bbPnts[1][c]);
        }
    }

	for(auto &up : m_boundingRectUpdate) {
		up.second=true;
	}
*/

    //update bounding box as well 
	/*
    vec4 pnts[8];
    vec4 locpnts[8];
    int p=0;
    for(int x=0; x<2; x++) {
        for(int y=0; y<2; y++) {
            for(int z=0; z<2; z++) {
                pnts[p] = m_modelMat*vec4(m_bbPnts[x].x, m_bbPnts[y].y, 
                                          m_bbPnts[z].z, 1.0);
                locpnts[p] = vec4(decScale[0]*m_bbPnts[x].x, 
                                  decScale[1]*m_bbPnts[y].y, 
                                  decScale[2]*m_bbPnts[z].z, 
                                  1.0);
                ++p;
            }
        }
    }
    m_bboxMins[0]=pnts[0];
    vec4 bboxMax=pnts[0];
    vec4 bboxLocMin=locpnts[0];
    vec4 bboxLocMax=locpnts[0];
    for(p=1; p<8; ++p) {
        for(int c=0; c<3;++c) {
            m_bboxMins[0][c]=std::min<float>(pnts[p][c], m_bboxMins[0][c]);
            bboxMax[c]=std::max<float>(pnts[p][c], bboxMax[c]);
            bboxLocMin[c]=std::min<float>(locpnts[p][c], bboxLocMin[c]);
            bboxLocMax[c]=std::max<float>(locpnts[p][c], bboxLocMax[c]);
        }
    }

    for(unsigned int c=0; c<m_modelGeom->editCompoSizes().size(); ++c) {
        //store min and size (max-min)
        m_bboxMins[c]=m_bboxMins[0];
        m_bboxSizes[c] = bboxMax-m_bboxMins[c];
        m_bboxLocalMin[c] = bboxLocMin;
        m_bboxLocalSizes[c] = bboxLocMax-bboxLocMin;
        
        if(m_localInsideAxes) {
			m_bboxRots[0] = glm::inverse(toMat4(decRot));
        }
        else {
            m_bboxRots[c] = mat4(1.0);
        }
    }*/

}

void ModelModule::setDimensions(const float& x, const float& y,const float& z) {
    m_dimensions=vec3(x, y, z);
    updateModelMatrix();
}

void ModelModule::setModel(const std::string& fileName) {
	m_modelStr = "";

    vector<GLfloat>& vertices = m_modelGeom->editVertexBufferData();
    vector<GLuint>& indices = m_modelGeom->editIndexBufferData();
    vector<int>& sizes = m_modelGeom->editCompoSizes();
    m_modelGeom->hasTexCoords(true);

    vertices.clear();
    indices.clear();
    sizes.clear();

    float fMax = std::numeric_limits<float>::max();
    float fMin = -std::numeric_limits<float>::max();
    m_bbPnts[0] = glm::vec3(fMax, fMax, fMax);
    m_bbPnts[1] = glm::vec3(fMin, fMin, fMin);

    memset(m_coordImage, 0, m_texWidth*m_texHeight*2*sizeof(float));


    string dir = fileName.substr(0, fileName.find_last_of("/"))+"/";


    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile( fileName, 
                 aiProcess_Triangulate            |
                 aiProcess_JoinIdenticalVertices  |
                 aiProcess_SortByPType); 
    if(!scene) {
        cout<<"Error opening model "<<fileName<<endl;
        return;
    }

    if(scene->HasMeshes()) {
        for(unsigned int ma=0;ma<scene->mNumMaterials;++ma) {
            int nbTex=scene->mMaterials[ma]->GetTextureCount(
                                                aiTextureType_DIFFUSE);
            if(nbTex>0) {
                aiString texName;
                scene->mMaterials[ma]->GetTexture(aiTextureType_DIFFUSE,
                                                  0, &texName);
                if(m_attributesMap.find("surface_texture")
                        !=m_attributesMap.end()) {
                    string tName = texName.C_Str();
                    m_attributesMap["surface_texture"]->setFile(dir+"/"+tName);
                }
            }
        }

		//compute bbox size
		aiVector3D minV = aiVector3D(fMax, fMax, fMax);
		aiVector3D maxV = aiVector3D(fMin, fMin, fMin);
        for(unsigned int m=0; m<scene->mNumMeshes; ++m) {
            aiMesh* mesh = scene->mMeshes[m];
            for(unsigned int v=0; v<mesh->mNumVertices; ++v) {
                aiVector3D& vert = mesh->mVertices[v];
				for(int i=0; i<3; i++) {
					minV[i] = std::min<float>(minV[i], vert[i]);
					maxV[i] = std::max<float>(maxV[i], vert[i]);
				}
			}
		}

		vector<float> dims = {fabs(maxV[0]-minV[0]), 
								fabs(maxV[1]-minV[1]),
								fabs(maxV[2]-minV[2])};
		vector<float> cnt = {(maxV[0]+minV[0])/2.0f, 
							(maxV[1]+minV[1])/2.0f,
							(maxV[2]+minV[2])/2.0f};

        unsigned int meshCnt=0;
        for(unsigned int m=0; m<scene->mNumMeshes; ++m) {
            aiMesh* mesh = scene->mMeshes[m];
            //write indices
            for(unsigned int f=0; f<mesh->mNumFaces; ++f) {
                aiFace& face = mesh->mFaces[f];
                for(unsigned int i=0; i<face.mNumIndices; ++i) {
                    indices.push_back(face.mIndices[i]+meshCnt);
                }
            }
            sizes.push_back(mesh->mNumFaces*3);
            meshCnt+=mesh->mNumVertices;

            //write normalized vertices and texcoords
            for(unsigned int v=0; v<mesh->mNumVertices; ++v) {
                aiVector3D& vert = mesh->mVertices[v];
                for(int i=0; i<3; ++i) {
					vertices.push_back((vert[i]-minV[i])/(maxV[i]-minV[i])-0.5);
                }
                if(mesh->HasTextureCoords(0)) {
                    vertices.push_back(mesh->mTextureCoords[0][v][0]);
                    vertices.push_back(mesh->mTextureCoords[0][v][1]);
                }
                else {
                    vertices.push_back(0);
                    vertices.push_back(0);
                }

            }
        }

		//write bounding box
		m_bbPnts[0].x = -0.5;
		m_bbPnts[0].y = -0.5;
		m_bbPnts[0].z = -0.5;
		m_bbPnts[1].x = 0.5;
		m_bbPnts[1].y = 0.5;
		m_bbPnts[1].z = 0.5;

		//write dimensions
		m_attributesMap["dimensions"]->setFloats(dims);
		m_attributesMap["position"]->setFloats(cnt);

		m_modelStr = fileName;
    }


    m_modelGeom->refresh();
    updateModelMatrix();            
}

