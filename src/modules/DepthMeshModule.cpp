/***************************************************************************
 *  DepthMeshModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "DepthMeshModule.hpp" 

#include <iostream>

#include "../Reveal.hpp"
#include "SpaceModule.hpp"
#include "../geoms/Geometry.hpp"

using namespace std;

DepthMeshModule::DepthMeshModule(): DepthModule() {
    m_type="DepthMesh";
    m_name="depthmesh";
    m_depthType=1;

    m_meshGeom = new Geometry();
    m_meshGeom->registerModule(this);
    m_meshGeom->editCompoSizes().push_back(0);
    

    addNameAttribute();
    addRemoveAttribute();
    addVisibleAttribute();

    addAttribute(new Attribute("add_vertex", 
                                Attribute::ACTION_FLOAT_ATTRIBUTE,
                                addVertexCallback, this, 3));
    addAttribute(new Attribute("clear_vertices", 
                                Attribute::ACTION_ATTRIBUTE,
                                clearVerticesCallback, this));

    addTransformationAttributes();
}

DepthMeshModule::~DepthMeshModule() {
}

void DepthMeshModule::updateModelMatrix() {
    DepthModule::updateModelMatrix();
}

void DepthMeshModule::setSpace(SpaceModule* space) {
    DepthModule::setSpace(space);
    m_space->registerGeom(m_meshGeom);
    
}

void DepthMeshModule::addVertex(const float& x,   
                                const float& y, 
                                const float& z) {
    vector<GLfloat>& vertices = m_meshGeom->editVertexBufferData();
    vector<GLuint>& indices = m_meshGeom->editIndexBufferData();
    vector<int>& sizes = m_meshGeom->editCompoSizes();

    vertices.push_back(x);
    vertices.push_back(y);
    vertices.push_back(z);
    indices.push_back(indices.size());
    sizes[0]+=1;
    m_meshGeom->refresh();
}

void DepthMeshModule::clearVertices() {
    vector<GLfloat>& vertices = m_meshGeom->editVertexBufferData();
    vector<GLuint>& indices = m_meshGeom->editIndexBufferData();
    vector<int>& sizes = m_meshGeom->editCompoSizes();
    indices.clear();
    vertices.clear();
    sizes[0]=0;

}



