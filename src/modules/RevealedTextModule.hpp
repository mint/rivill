/***************************************************************************
 *  RevealedTextModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef RevealedTextModule_h
#define RevealedTextModule_h

#include "ShapeModule.hpp"
#include "RevealedModule.hpp"
#include <FL/Fl_Box.H>
#include <FL/Fl_Multiline_Output.H>
#include <FL/Fl_Image_Surface.H>

class RevealedTextModule : public ShapeModule, public RevealedModule {
    public:
        RevealedTextModule();
        virtual ~RevealedTextModule();

        static void textCallback(Module* mod, 
                                 const std::vector<std::string>& vals) {
            dynamic_cast<RevealedTextModule*>(mod)->setText(vals[0]);
        }
        void setText(const std::string& text);
        static void fontSizeCallback(Module* mod, 
                                     const std::vector<int>& vals) {
            dynamic_cast<RevealedTextModule*>(mod)->setFontSize(vals[0]);
        }
        void setFontSize(const int& size);
        virtual void setColor(const std::vector<float>& hsv);

        static void dimensionsCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            dynamic_cast<RevealedTextModule*>(mod)->setDimensions(vals[0], 
                                                                  vals[1], 
                                                                  vals[2]);
        }
        void setShape(const std::string&);

    protected:
        std::string m_text;
	std::vector<std::string> m_tokens;
        Fl_RGB_Image* m_textImg;
        Fl_Box* m_label;
        Fl_Image_Surface* m_surf;
};


#endif

