/***************************************************************************
 *  RevealedTextModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "RevealedTextModule.hpp" 

#include <iostream>
#include <glm/gtx/quaternion.hpp>

#include "../Reveal.hpp"

#include "../geoms/Geometry.hpp"

using namespace std;

RevealedTextModule::RevealedTextModule(): ShapeModule(), RevealedModule() {
    m_type="Text";
    m_name="text";

    m_layersNb=1;
    int nbBytes = m_texWidth*m_texHeight*m_layersNb*3;
    m_layersData = new char[nbBytes];
    m_surf = new Fl_Image_Surface(m_texWidth, m_texHeight);

    m_label = new Fl_Box(0, 0, m_texWidth, m_texHeight, "text");
    m_label->color(FL_BLACK);
    m_label->labelcolor(FL_WHITE);
    m_label->box(FL_FLAT_BOX);

    addNameAttribute();
    addRemoveAttribute();
    addVisibleAttribute();
    addVisibleFromAttribute();
    addRevealedByAttribute();

    addAttribute(new Attribute("text", 
                               Attribute::STRING_ATTRIBUTE,
                               textCallback, this, 1));
    vector<string> txt(1,"text");
    m_attributesMap["text"]->setStrings(txt);
/*
    addAttribute(new Attribute("font_size", 
                               Attribute::INT_ATTRIBUTE,
                               fontSizeCallback, this, 1));
    addAttribute(new Attribute("dimensions", 
                                Attribute::FLOAT_ATTRIBUTE,
                                dimensionsCallback, this, 3));
    m_attributesMap["dimensions"]->setFloats(vector<float>(3,100.0));
*/

    addShapeAttributes();
    addTransformationAttributes();
    addRevealedAttributes();
    addRevealedOutputAttributes();
    m_localInsideAxes=true;
	m_insideDensityAlpha=0;
	m_insideGradientAlpha=0;
	m_insideTextureAlpha=1.0;
	m_insideVisible=true;
    m_insideTextureBlend=false;


    vector<string> shp(1,"frame");
    m_attributesMap["shape"]->setStrings(shp);

    vector<float> dims(3,200.0);
    m_attributesMap["dimensions"]->setFloats(dims);

	//set default text
//	setText("text");
}

RevealedTextModule::~RevealedTextModule() {
    Reveal::getInstance()
                ->getGeom(m_shapesMap[m_currentShape])
                                ->unregisterModule(this);
}

void RevealedTextModule::setColor(const vector<float>& hsv) {
    RevealedModule::setColor(hsv);
    m_label->labelcolor(fl_rgb_color(m_color[0]*255, 
                                     m_color[1]*255, 
                                     m_color[2]*255));
    setText(m_text);
}

void RevealedTextModule::setFontSize(const int& size) {
    m_label->labelsize(size);
    setText(m_text);
}

void RevealedTextModule::setText(const std::string& text) {
    m_text=text;


    //layers in text are separated by  | 
    vector<int> sizes;
    string token;
    m_tokens.clear();
    istringstream tokenStream(m_text);
    while (getline(tokenStream, token, '|')) {
		sizes.push_back(0);

		//for each token recreate lines separated by ; 
		bool done=false;
		size_t linePos=-1;
		while(!done) {
		  size_t newPos = token.find(";",linePos+1);
		  if(newPos!=string::npos) {
			token.replace(newPos, size_t(1), "\n");
			sizes.back()=max<int>(sizes.back(), 
					  int(newPos-linePos));
		  }
		  else {
			sizes.back()=max<int>(sizes.back(), 
					  int(token.length()-linePos));
			done=true;
		  }
		  linePos=newPos;
		}
        m_tokens.push_back(token);
    }

    //recreate layers data
    if(int(m_tokens.size())>m_layersNb || !m_layersData) {
    	if(m_layersData) {
            delete [] m_layersData;
        }
        int nbBytes = m_texWidth*m_texHeight*m_tokens.size()*3;
        m_layersData = new char[nbBytes];
    }
    m_layersNb=m_tokens.size();

    //fill data with each part of the string
    for(unsigned int d=0; d<m_tokens.size(); ++d) {
		//find ; to split in lines
        m_label->label(m_tokens[(m_layersNb-1)-d].c_str());

        m_label->labelsize(float(900)
			  / float(sizes[(m_layersNb-1)-d]));//FIXME 

        m_surf->set_current();
        m_surf->draw(m_label);
        m_textImg = m_surf->image();
        Fl_Display_Device::display_device()->set_current();
        for(int i=0; i<m_texWidth; ++i) {
            for(int j=0; j<m_texHeight; ++j) {
                m_layersData[(d*m_texHeight*m_texWidth+j*m_texWidth+i)*3]
                   =m_textImg->data()[0][(j*m_textImg->w()+i)*m_textImg->d()];
                m_layersData[(d*m_texHeight*m_texWidth+j*m_texWidth+i)*3+1]
                   =m_textImg->data()[0][(j*m_textImg->w()+i)*m_textImg->d()+1];
                m_layersData[(d*m_texHeight*m_texWidth+j*m_texWidth+i)*3+2]
                   =m_textImg->data()[0][(j*m_textImg->w()+i)*m_textImg->d()+2];
            }
        }
		delete m_textImg;
    }
    map<int, bool>::iterator itUp = m_insideUpMap.begin();
    for(; itUp!=m_insideUpMap.end(); ++itUp) {
        itUp->second=true;
    }
}

void RevealedTextModule::setShape(const std::string& shape) {
	if(m_shapesMap.find(shape)!=m_shapesMap.end()) {
        if(m_currentShape.compare("")!=0) {
            Reveal::getInstance()
                ->getGeom(m_shapesMap[m_currentShape])
                    ->unregisterModule(this);
        }
        m_currentShape=shape;
		m_shapeGeom=m_shapesMap[m_currentShape];
        Reveal::getInstance()
            ->getGeom(m_shapesMap[m_currentShape])
                ->registerModule(this);
        m_currentGeom=Reveal::getInstance()
                        ->getGeom(m_shapesMap[m_currentShape]);
    }
}


