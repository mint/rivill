/***************************************************************************
 *  RootSceneGroupModule.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "RootSceneGroupModule.hpp"
#include "RevealedShapeModule.hpp"
#include "RevealedPathModule.hpp"
#include "RevealedTextModule.hpp"
#include "RevealedCutModule.hpp"
#include "RevealedArrowModule.hpp"
#include "RevealedGridModule.hpp"
#include "RevealedModelModule.hpp"
#include "SceneGroupModule.hpp"
#include "../Reveal.hpp"

using namespace std;

RootSceneGroupModule::RootSceneGroupModule(): GroupModule() {
    m_type="Scene";
    m_name="scene";
}

void RootSceneGroupModule::addGroupAttributes() {
    addAttribute(new Attribute("add_shape", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addShapeCallback, this, 1));
    m_attributesMap["add_shape"]->initStrings(vector<string>(1, "shape"));
    addAttribute(new Attribute("add_model", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addModelCallback, this, 1));
    m_attributesMap["add_model"]->initStrings(vector<string>(1, "model"));
    addAttribute(new Attribute("add_path", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addPathCallback, this, 1));
    m_attributesMap["add_path"]->initStrings(vector<string>(1, "path"));
    addAttribute(new Attribute("add_text", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addTextCallback, this, 1));
    m_attributesMap["add_text"]->initStrings(vector<string>(1, "text"));
    addAttribute(new Attribute("add_cut", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addCutCallback, this, 1));
    m_attributesMap["add_cut"]->initStrings(vector<string>(1, "cut"));
    addAttribute(new Attribute("add_arrow", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addArrowCallback, this, 1));
    m_attributesMap["add_arrow"]->initStrings(vector<string>(1, "arrow"));
    addAttribute(new Attribute("add_grid", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addGridCallback, this, 1));
    m_attributesMap["add_grid"]->initStrings(vector<string>(1, "grid"));
    addAttribute(new Attribute("add_group", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addGroupCallback, this, 1));
    m_attributesMap["add_group"]->initStrings(vector<string>(1, "group"));
}

void RootSceneGroupModule::addShape(const std::string& name) {
    RevealedShapeModule* tmp = new RevealedShapeModule();
    addChild(tmp);
    tmp->getAttribute("name")->setStrings(vector<string>(1,name));
    Reveal::getInstance()->refreshModules();
}

void RootSceneGroupModule::addModel(const std::string& name) {
    RevealedModelModule* tmp = new RevealedModelModule();
    addChild(tmp);
    tmp->getAttribute("name")->setStrings(vector<string>(1,name));
    Reveal::getInstance()->refreshModules();
}

void RootSceneGroupModule::addPath(const std::string& name) {
    RevealedPathModule* tmp = new RevealedPathModule();
    addChild(tmp);
    tmp->getAttribute("name")->setStrings(vector<string>(1,name));
    Reveal::getInstance()->refreshModules();
}

void RootSceneGroupModule::addText(const std::string& name) {
    RevealedTextModule* tmp = new RevealedTextModule();
    addChild(tmp);
    tmp->getAttribute("name")->setStrings(vector<string>(1,name));
    Reveal::getInstance()->refreshModules();
}

void RootSceneGroupModule::addCut(const std::string& name) {
    RevealedCutModule* tmp = new RevealedCutModule();
    addChild(tmp);
    tmp->getAttribute("name")->setStrings(vector<string>(1,name));
    Reveal::getInstance()->refreshModules();
}

void RootSceneGroupModule::addArrow(const std::string& name) {
    RevealedArrowModule* tmp = new RevealedArrowModule();
    addChild(tmp);
    tmp->getAttribute("name")->setStrings(vector<string>(1,name));
    Reveal::getInstance()->refreshModules();
}

void RootSceneGroupModule::addGrid(const std::string& name) {
    RevealedGridModule* tmp = new RevealedGridModule();
    addChild(tmp);
    tmp->getAttribute("name")->setStrings(vector<string>(1,name));
    Reveal::getInstance()->refreshModules();
}

void RootSceneGroupModule::addGroup(const std::string& name) {
    SceneGroupModule* tmp = new SceneGroupModule();
    addChild(tmp);
    tmp->getAttribute("name")->setStrings(vector<string>(1,name));
    Reveal::getInstance()->refreshModules();
}

