/***************************************************************************
 *  ModelModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef ModelModule_h
#define ModelModule_h

#include "GeomModule.hpp"

class ModelModule : public virtual GeomModule {
    public:
        ModelModule();
        virtual ~ModelModule();

        static void modelCallback(Module* mod, 
                                  const std::string& f) {
            dynamic_cast<ModelModule*>(mod)->setModel(f);
        }
        virtual void setModel(const std::string&);
        static void dimensionsCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            dynamic_cast<ModelModule*>(mod)->setDimensions(vals[0], 
                                                           vals[1], 
                                                           vals[2]);
        }
        void setDimensions(const float&, const float&, const float&);

        virtual void updateModelMatrix();
        void addModelAttributes();
    
    protected:
        Geometry* m_modelGeom;
        float* m_coordImage;
        glm::vec3 m_dimensions;
		std::string m_modelStr;

    private:
        struct ModelVertex {
            glm::vec3 pos;
            glm::vec2 tex;
            bool compare(ModelVertex& v) {
                if(v.pos.x==pos.x && v.pos.y==pos.y && v.pos.z==pos.z 
                        && v.tex.x==tex.x && v.tex.y==tex.y) {
                    return true; 
                }
                return false;
            }
        };
};


#endif

