/***************************************************************************
 *  RevealedArrowModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "RevealedArrowModule.hpp" 

#include "../geoms/Geometry.hpp"

#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/polar_coordinates.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/euler_angles.hpp> 

using namespace std;

RevealedArrowModule::RevealedArrowModule(): GroupModule()  {
    m_type="Arrow";
    m_name="arrow";
    addNameAttribute();
    addRemoveAttribute();
    addVisibleAttribute();
    addVisibleFromAttribute();

    addAttribute(new Attribute("origin_module", 
                                Attribute::STRING_ATTRIBUTE,
                                originModuleCallback, this, 1));
    addAttribute(new Attribute("origin_offset", 
                                Attribute::FLOAT_ATTRIBUTE,
                                originOffsetCallback, this, 3));
    addAttribute(new Attribute("destination_module", 
                                Attribute::STRING_ATTRIBUTE,
                                destinationModuleCallback, this, 1));
    addAttribute(new Attribute("destination_offset", 
                                Attribute::FLOAT_ATTRIBUTE,
                                destinationOffsetCallback, this, 3));
    addAttribute(new Attribute("elements_number", 
                                Attribute::INT_ATTRIBUTE,
                                elementsNumberCallback, this, 1));
    addAttribute(new Attribute("elements_speed", 
                                Attribute::FLOAT_ATTRIBUTE,
                                elementsSpeedCallback, this, 1));
    addAttribute(new Attribute("elements_thickness", 
                                Attribute::FLOAT_ATTRIBUTE,
                                elementsThicknessCallback, this, 1));
    addAttribute(new Attribute("elements_direction", 
                                Attribute::FLOAT_ATTRIBUTE,
                                elementsDirectionCallback, this, 3));
    addAttribute(new Attribute("follow_direction", 
                                Attribute::BOOL_ATTRIBUTE,
                                followDirectionCallback, this, 1));

    addAttribute(new Attribute("revealed_elements", 
                                Attribute::FLOAT_ATTRIBUTE,
                                elementsRevealedCallback, this, 1));
    m_revAttributes.push_back(m_attributesMap["revealed_elements"]);

    m_thickness=0.5;
    m_followDirection=false;
    m_origPnt = new ArrowPoint(this);
    m_destPnt = new ArrowPoint(this);
    
    m_nbElements=0;
    m_attributesMap["elements_number"]->setInts(vector<int>(1, 10));
    m_attributesMap["elements_thickness"]->setFloats(vector<float>(1, 0.2));
    m_attributesMap["elements_speed"]->setFloats(vector<float>(1, 5));
    vector<float> dir(3,0);
    dir[2]=-1;
    m_attributesMap["elements_direction"]->setFloats(dir);
    m_distance=0;

    m_attributesMap["origin_offset"]->setFloats(vector<float>(3,0));
    m_attributesMap["destination_offset"]->setFloats(vector<float>(3,0));

    m_revealed.push_back(0);
    m_revealedChanged=false;

    Reveal::getInstance()->addModuleListObserver(this);
    Reveal::getInstance()->addUpdateObserver(this);
}

RevealedArrowModule::~RevealedArrowModule() {
}

void RevealedArrowModule::deleteModule() {
    GroupModule::deleteModule();
    for(int i=0; i<m_nbElements; ++i) {
        m_elements[i]->deleteModule();
    }
}

void RevealedArrowModule::draw() {
}

void RevealedArrowModule::setVisible(bool vis) {
    GroupModule::setVisible(vis);
    for(int i=0; i<m_nbElements; ++i) {
        m_elements[i]->setParentVisible(m_parentVisible&&m_visible);
    }
}

void RevealedArrowModule::setOriginModule(const std::string& moduleStr) {
    Module* mod = NULL;
    mod = Reveal::getInstance()->findModule(moduleStr);
    m_origModStr=moduleStr;
    if(mod) {
        m_origPnt->setModule(mod);
        mod->getAttribute("absolute_position")->addListener(m_origPnt);
    }
    updateArrowPoints();
}

void RevealedArrowModule::setOriginOffset(const float& x, 
                                          const float& y,
                                          const float& z) {
    m_originOffset[0]=x;
    m_originOffset[1]=y;
    m_originOffset[2]=z;
    updateArrowPoints();
}

void RevealedArrowModule::setDestinationModule(const std::string& moduleStr) {
    Module* mod = NULL;
    mod = Reveal::getInstance()->findModule(moduleStr);
    m_destModStr=moduleStr;
    if(mod) {
        m_destPnt->setModule(mod);
        mod->getAttribute("absolute_position")->addListener(m_destPnt);
    }
    updateArrowPoints();
}

void RevealedArrowModule::setDestinationOffset(const float& x, 
                                               const float& y,
                                               const float& z) {
    m_destOffset[0]=x;
    m_destOffset[1]=y;
    m_destOffset[2]=z;
    updateArrowPoints();
}

void RevealedArrowModule::setElementsNumber(const int& nb) {
    if(nb>0) {
        if(nb>m_nbElements) {
            for(int i=m_nbElements; i<nb; ++i) {
                m_elements.push_back(new ArrowElementModule(m_elements.size(),
							    this));
                m_elements.back()->setShape("cone");
                m_elements.back()->setSurfaceThickness(5);
                m_elements.back()->setInsideVisible(false);
                m_elements.back()->setGradientType("along_y");
                m_positions.push_back(0);
            }
        }
        m_nbElements=nb;
        for(int i=0; i<m_nbElements; ++i) {
            m_elements[i]->setVisible(true);
        }
        for(unsigned int i=m_nbElements; i<m_elements.size(); ++i) {
            m_elements[i]->setVisible(false);
        }
        updateArrowPoints();
    }
}

void RevealedArrowModule::setElementsSpeed(const float& sp) {
    if(sp>=0) {
        m_speed=sp/1000;
        updateArrowPoints();
    }
}

void RevealedArrowModule::setThickness(const float& th) {
    if(th>0) {
        m_thickness=th;
        updateArrowPoints();
    }
}

void RevealedArrowModule::setElementsDirection(const float& x, 
                                              const float& y,
                                              const float& z) {
    m_elementsDirection[0]=x;
    m_elementsDirection[1]=y;
    m_elementsDirection[2]=z;
    updateArrowPoints();
}

void RevealedArrowModule::setFollowDirection(bool f) {
    m_followDirection=f;
    updateArrowPoints();
}

void RevealedArrowModule::update(const int& timeDiffMs) {
  if(m_visible) {
    if(m_speed>0) {
        for(int i=0; i<m_nbElements; ++i) {
            m_positions[i]=fmod(m_positions[i]+m_speed,1);
            float pos = m_positions[i]*m_distance;
            m_elements[i]->setPosition(m_origin[0]+m_dir[0]*pos,
                                       m_origin[1]+m_dir[1]*pos,
                                       m_origin[2]+m_dir[2]*pos);
        }
    }
    if(m_revealedChanged) {
      m_revealed[0]=0;
      for(int i=0; i<m_nbElements; ++i) {
        if(m_elements[i]->isRevealed()>0) {
          m_revealed[0]=1;
        }
      }
      m_attributesMap["revealed_elements"]->setFloatsIfChanged(m_revealed);
      m_revealedChanged=false;
    }
  }
}


void RevealedArrowModule::updateModulesList(const map<string, Module*>& mods) {
    vector<vector<string> >& oriModNames = m_attributesMap["origin_module"]
                                                ->editStringValuesChoices();
    vector<vector<string> >& desModNames = m_attributesMap["destination_module"]
                                                ->editStringValuesChoices();
    oriModNames.clear();
    desModNames.clear();
    oriModNames.push_back(vector<string>());
    desModNames.push_back(vector<string>());
    map<string, Module*>::const_iterator itMod = mods.begin();
    for(; itMod!=mods.end(); ++itMod) { //select names from scene modules
        if(itMod->first.find("/rivill/scene/")==0 
                && itMod->first.compare(m_fullName)!=0) {
            oriModNames[0].push_back(itMod->first);
            desModNames[0].push_back(itMod->first);
        }
    }
    setOriginModule(m_origModStr);
    setDestinationModule(m_destModStr);
}

void RevealedArrowModule::updateArrowPoints() {
    if(m_origPnt->getModule()!=NULL 
            && m_destPnt->getModule()!=NULL) {

        const vector<float>& floatValues1 
        =m_origPnt->getModule()->getAttribute("absolute_position")->getFloats();
        for(int i=0; i<3; ++i) {
            m_origin[i]=floatValues1[i]+m_originOffset[i];
        }

        const vector<float>& floatValues2 
        =m_destPnt->getModule()->getAttribute("absolute_position")->getFloats();
        for(int i=0; i<3; ++i) {
            m_dest[i]=floatValues2[i]+m_destOffset[i];
        }

        m_dir = m_dest - m_origin;
        m_distance=length(m_dir);
        m_dir = glm::normalize(m_dir);

        glm::quat q;
        glm::vec3 up(0,1,0);
        getQuatFromTwoVecs(q, up, m_dir);
        //glm::vec3 e = glm::eulerAngles(q);
        for(int i=0; i<m_nbElements; ++i) {
            m_positions[i]=(float(i)+0.5)/float(m_nbElements);
            float pos = m_positions[i]*m_distance;
            m_elements[i]->setPosition(m_origin[0]+m_dir[0]*pos,
                                       m_origin[1]+m_dir[1]*pos,
                                       m_origin[2]+m_dir[2]*pos);
            if(m_followDirection) {
                m_elements[i]->setQuaternion(q[0],q[1],q[2],q[3]);
                m_elements[i]->setScale(m_thickness,
                                        (m_distance/float(m_nbElements))/200,
                                        m_thickness);
            }
            else {
                glm::quat q2;
                getQuatFromTwoVecs(q2, up, m_elementsDirection);
                m_elements[i]->setQuaternion(q2[0],q2[1],q2[2],q2[3]);
                m_elements[i]->setScale((m_distance/float(m_nbElements))/200,
                                        m_thickness,
                                        (m_distance/float(m_nbElements))/200);
            }
        }
    }
}

void RevealedArrowModule::gotAttributeListenersUpdate(Attribute* att) {
    bool active=false;
    vector<Attribute*>::iterator itAtt = m_revAttributes.begin();
    for(; itAtt!=m_revAttributes.end(); ++itAtt) {
        if((*itAtt)->editListeners().size()>0) {
            active=true;
        }
    } 
    if(active) {
      for(int i=0; i<m_nbElements; ++i) {
	Reveal::getInstance()->registerOutputRevealed(m_elements[i]);
      }
    }
    else {
      for(int i=0; i<m_nbElements; ++i) {
	Reveal::getInstance()->unregisterOutputRevealed(m_elements[i]);
      }
    }
}

void RevealedArrowModule::processRevealedElement() {
  m_revealedChanged=true;
}

ArrowPoint::ArrowPoint(RevealedArrowModule* mod) {
    m_saved=false;
    m_arrowMod=mod;
    m_mod=NULL;
}

void ArrowPoint::setModule(Module* mod) {
    m_mod=mod;
}

void ArrowPoint::update(std::vector<float>& floatValues, 
                        std::vector<int>& intValues,
                        std::vector<bool>& boolValues,
                        std::vector<std::string>& stringValues) {
    m_arrowMod->updateArrowPoints();
}


