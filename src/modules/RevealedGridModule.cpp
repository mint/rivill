/***************************************************************************
 *  RevealedGridModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "RevealedGridModule.hpp" 

#include "../geoms/Geometry.hpp"

#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/polar_coordinates.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/euler_angles.hpp> 

using namespace std;

RevealedGridModule::RevealedGridModule(): GroupModule()  {
    m_type="Grid";
    m_name="grid";
    addNameAttribute();
    addRemoveAttribute();
    addVisibleAttribute();
    //addVisibleFromAttribute(); //TODO
    
    addAttribute(new Attribute("dimensions", 
                                Attribute::FLOAT_ATTRIBUTE,
                                dimensionsCallback, this, 3));

    addTransformationAttributes();

    addAttribute(new Attribute("elements_number", 
                                Attribute::INT_ATTRIBUTE,
                                elementsNumberCallback, this, 3));
    addAttribute(new Attribute("elements_size", 
                                Attribute::FLOAT_ATTRIBUTE,
                                elementsSizeCallback, this, 1));
    addAttribute(new Attribute("revealed_elements", 
                                Attribute::FLOAT_ATTRIBUTE,
                                elementsRevealedCallback, this, 2));
    m_revAttributes.push_back(m_attributesMap["revealed_elements"]);

    //TODO add mode : random, aligned, personnalized
    //TODO add element pos attribute : element id then pos 3D 

    m_totalNbElements=0;
    m_nbElements[0]=m_nbElements[1]=m_nbElements[2]=0;
    m_gridDims[0]=m_gridDims[1]=m_gridDims[2]=200;
    m_elementsSize=20;
    m_attributesMap["elements_size"]->setFloats(vector<float>(1, 20));
    m_attributesMap["elements_number"]->setInts(vector<int>(3, 5));
    m_attributesMap["dimensions"]->setFloats(vector<float>(3, 200));

    m_revealed.push_back(0);
    m_revealed.push_back(0);
}

RevealedGridModule::~RevealedGridModule() {
}

void RevealedGridModule::deleteModule() {
    GroupModule::deleteModule();
    for(int i=0; i<m_totalNbElements; ++i) {
        m_elements[i]->deleteModule();
    }
}

void RevealedGridModule::draw() {
}

void RevealedGridModule::updateModelMatrix() {
  GroupModule::updateModelMatrix();
  for(GridElementModule* el : m_elements) {
    el->setParentMatrix(m_modelMat);
    el->updateModelMatrix();
  }
}

void RevealedGridModule::setVisible(bool vis) {
  GroupModule::setVisible(vis);
  for(int i=0; i<m_totalNbElements && i<int(m_elements.size()); ++i) {
    m_elements[i]->setParentVisible(m_parentVisible&&m_visible);
  }
}

void RevealedGridModule::setGridDimensions(const float& x, 
                                           const float& y,
                                           const float& z) {
  if(x>0 && y>0 && z>0) {
      m_gridDims[0]=x;
      m_gridDims[1]=y;
      m_gridDims[2]=z;
      updateGridPoints();
  }
}

void RevealedGridModule::setElementsNumber(const int& x, 
                                           const int& y,
                                           const int& z) {
  int nb = x*y*z;
  if(nb>0) {
    m_nbElements[0]=x;
    m_nbElements[1]=y;
    m_nbElements[2]=z;
    if(nb>m_totalNbElements) {
      for(int i=m_totalNbElements; i<nb; ++i) {
        m_elements.push_back(new GridElementModule(m_elements.size(),
                                                   this));
      }
    }
    m_totalNbElements=nb;
    for(int i=0; i<m_totalNbElements; ++i) {
      m_elements[i]->setVisible(true);
    }
    for(unsigned int i=m_totalNbElements; i<m_elements.size(); ++i) {
      m_elements[i]->setVisible(false);
    }
    updateGridPoints();
  }
}

void RevealedGridModule::setElementsSize(const float& s) {
  if(s>0) {
    m_elementsSize=s;
    updateGridPoints();
  }
}

void RevealedGridModule::update(const int& timeDiffMs) {
}

void RevealedGridModule::updateModulesList(const map<string, Module*>& mods) {
}

void RevealedGridModule::updateGridPoints() {
  int el=0;
  vector<float> pos(3,0);
  float steps[3];
  for(int i=0; i<3; ++i) {
    steps[i]=m_gridDims[i]/m_nbElements[i];
  }
  vector<float> dims(3, m_elementsSize);
  for(int x=0; x<m_nbElements[0]; x++) {
    for(int y=0; y<m_nbElements[1]; y++) {
      for(int z=0; z<m_nbElements[2]; z++) {
        pos[0]=(x-m_nbElements[0]/2.0+0.5)*steps[0];
        pos[1]=(y-m_nbElements[1]/2.0+0.5)*steps[1];
        pos[2]=(z-m_nbElements[2]/2.0+0.5)*steps[2];
        m_elements[el]->getAttribute("position")->setFloats(pos);
        m_elements[el]->getAttribute("dimensions")->setFloats(dims);
        el++;
      }
    }
  }
  updateModelMatrix();
}

void RevealedGridModule::gotAttributeListenersUpdate(Attribute* att) {
    bool active=false;
    for(Attribute *att : m_revAttributes) {
      if(att->editListeners().size()>0) {
        active=true;
      }
    } 
    if(active) {
        for(GridElementModule* el : m_elements) {
            Reveal::getInstance()->registerOutputRevealed(el);
        }
    }
    else {
        vector<GridElementModule*>::iterator el=m_elements.begin();        
        for(; el!=m_elements.end(); el++) {
            Reveal::getInstance()->unregisterOutputRevealed(*el);
        }
    }
}

void RevealedGridModule::processRevealedElement(const int& elID, 
                                                const float& val) {
  m_revealed[0]=elID;
  m_revealed[1]=val;
  m_attributesMap["revealed_elements"]->setFloatsIfChanged(m_revealed);
}

