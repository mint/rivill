/***************************************************************************
 *  OutputOscModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef OutputOscModule_h
#define OutputOscModule_h

#include "Module.hpp"

class OutputOscModule : public Module {
    public :
        OutputOscModule();
        virtual ~OutputOscModule();

        static void addressCallback(Module* mod,
                                   const std::vector<std::string>& vals) {
            dynamic_cast<OutputOscModule*>(mod)->setAddress(vals[0]);
        }
        void setAddress(const std::string& add);

        static void portCallback(Module* mod,
                                   const std::vector<int>& vals) {
            dynamic_cast<OutputOscModule*>(mod)->setPort(vals[0]);
        }
        void setPort(const int& add);

    private :
        std::string m_address;
        int m_port;

};

#endif

