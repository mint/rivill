/***************************************************************************
 *  DepthShapeModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "DepthShapeModule.hpp" 

#include <iostream>

#include "../Reveal.hpp"
#include "SpaceModule.hpp"
#include "../geoms/Geometry.hpp"

using namespace std;

DepthShapeModule::DepthShapeModule(): ShapeModule(), DepthModule() {
    m_type="DepthShape";
    m_name="depthshape";
    m_depthType=1;

    addNameAttribute();
    addRemoveAttribute();
    addVisibleAttribute();
    addShapeAttributes();
    addTransformationAttributes();
	addFollowMarkerAttribute();

    vector<float> dims(3,200.0);
    dims[2]=1;
    m_attributesMap["dimensions"]->setFloats(dims);
}

DepthShapeModule::~DepthShapeModule() {
    m_space->getGeom(m_shapesMap[m_currentShape])
                ->unregisterModule(this);
}

void DepthShapeModule::updateModelMatrix() {
    ShapeModule::updateModelMatrix();
}

void DepthShapeModule::setSpace(SpaceModule* space) {
    DepthModule::setSpace(space);
    setShape(m_currentShape);
}

void DepthShapeModule::setShape(const std::string& shape) {
    bool found=false;
    vector<string>::iterator itIns=m_shapes.begin();
    for(; itIns!=m_shapes.end(); ++itIns) {
        if((*itIns).compare(shape)==0) {
            found=true;
        }
    }
    if(found) {
        if(m_currentShape.compare("")!=0) {
            m_space->getGeom(m_shapesMap[m_currentShape])
                        ->unregisterModule(this);
        }
        m_currentShape=shape;
        m_space->getGeom(m_shapesMap[m_currentShape])
                    ->registerModule(this);
    }
}

