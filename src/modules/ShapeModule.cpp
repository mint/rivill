/***************************************************************************
 *  ShapeModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "ShapeModule.hpp" 

#include <iostream>

#include "../Reveal.hpp"
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp> 

using namespace std;
using namespace glm;

ShapeModule::ShapeModule(): GeomModule() {
    m_shapes.push_back("box");
    m_shapesMap["box"]=Reveal::GEOM_BOX;
    m_shapes.push_back("sphere");
    m_shapesMap["sphere"]=Reveal::GEOM_SPHERE;
    m_shapes.push_back("tube");
    m_shapesMap["tube"]=Reveal::GEOM_TUBE;
    m_shapes.push_back("cone");
    m_shapesMap["cone"]=Reveal::GEOM_CONE;
    m_shapes.push_back("frame");
    m_shapesMap["frame"]=Reveal::GEOM_FRAME;
    m_dimensions=vec3(200.0, 200.0, 200.0);
}

ShapeModule::~ShapeModule() {}

void ShapeModule::setDimensions(const float& x, const float& y,const float& z) {
    m_dimensions=vec3(x, y, z);
    updateModelMatrix();
}

void ShapeModule::addShapeAttributes() {
    addAttribute(new Attribute("shape", 
                                Attribute::STRING_ATTRIBUTE,
                                shapeCallback, this, 1));
    m_attributesMap["shape"]->editStringValuesChoices().assign(1, m_shapes);
    m_attributesMap["shape"]->initStrings(vector<string>(1, m_shapes[0]));

    addAttribute(new Attribute("dimensions", 
                                Attribute::FLOAT_ATTRIBUTE,
                                dimensionsCallback, this, 3));
    m_attributesMap["dimensions"]->setFloats(vector<float>(3,100.0));
}

void ShapeModule::updateModelMatrix() {
    Module::updateModelMatrix();
    mat4 dimMat = scale(m_dimensions);
    m_modelMat = m_modelMat * dimMat;
	m_invModelMat = inverse(m_modelMat);
    vec3 decTrans, decSkew;
    quat decRot;
    vec4 decPers;
    decompose(m_modelMat, m_modelScale, decRot, decTrans, decSkew, decPers);

    //update bounding box as well 
	vec4 pnts[8];
    vec4 locpnts[8];
    int p=0;
    for(float x=-0.5; x<1; x+=1.0) {
        for(float y=-0.5; y<1; y+=1.0) {
            for(float z=-0.5; z<1; z+=1.0) {
                pnts[p] = m_modelMat*vec4(x, y, z, 1.0);
                locpnts[p] = vec4(m_modelScale[0]*x, 
                                  m_modelScale[1]*y, 
                                  m_modelScale[2]*z, 
                                  1.0);
                ++p;
            }
        }
    }
    m_bbPnts[0]=pnts[0];
    m_bbPnts[1]=pnts[0];
    for(p=1; p<8; ++p) {
        for(int c=0; c<3;++c) {
            m_bbPnts[0][c]=std::min<float>(pnts[p][c], m_bbPnts[0][c]);
            m_bbPnts[1][c]=std::max<float>(pnts[p][c], m_bbPnts[1][c]);
        }
    }

	/*
    //store min and size (max-min)
    m_bboxSizes[0] = bboxMax-m_bboxMins[0];
    m_bboxLocalSizes[0] = bboxLocMax-m_bboxLocalMin[0];
    
    if(m_localInsideAxes) {
        m_bboxRots[0] = glm::inverse(toMat4(decRot));
    }
    else {
        m_bboxRots[0] = mat4(1.0);
    }
	*/


	for(auto &up : m_boundingRectUpdate) {
		up.second=true;
	}
}

