/***************************************************************************
 *  RevealedGridModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef RevealedGridModule_h
#define RevealedGridModule_h

#include "GroupModule.hpp"
#include "RevealedShapeModule.hpp"

class GridElementModule;

class RevealedGridModule : public GroupModule, 
                           public ModuleListObserver {
    public:
        RevealedGridModule();
        virtual ~RevealedGridModule();
        virtual void deleteModule();

        void draw();
        void setVisible(bool vis);

        static void dimensionsCallback(Module* mod, 
                                       const std::vector<float>& vals) {
            dynamic_cast<RevealedGridModule*>(mod)->setGridDimensions(vals[0], 
                                                                      vals[1],
                                                                      vals[2]);
        }
        void setGridDimensions(const float& x, 
                               const float& y, 
                               const float& z);

        static void elementsNumberCallback(Module* mod, 
                                           const std::vector<int>& vals) {
            dynamic_cast<RevealedGridModule*>(mod)->setElementsNumber(vals[0], 
                                                                      vals[1],
                                                                      vals[2]);
        }
        void setElementsNumber(const int& x, 
                               const int& y, 
                               const int& z);


        static void elementsSizeCallback(Module* mod, 
                                         const std::vector<float>& vals) {
            dynamic_cast<RevealedGridModule*>(mod)->setElementsSize(vals[0]);
        }
        void setElementsSize(const float& th);

        static void elementsRevealedCallback(Module* mod, 
                                   const std::vector<float>& vals) {}
	void processRevealedElement(const int&, const float&);


        void update(const int& timeDiffMs);
        virtual void updateModulesList(const std::map<std::string, Module*>&);

	void gotAttributeListenersUpdate(Attribute* att);
        void updateGridPoints();

        virtual void updateModelMatrix();

  protected:
        int m_totalNbElements;
        int m_nbElements[3];
        float m_gridDims[3];
        std::vector<GridElementModule*> m_elements;
        std::vector<float> m_positions;
        float m_elementsSize;
	std::vector<Attribute*> m_revAttributes;
	std::vector<float> m_revealed;
	bool m_revealedChanged;
};

class GridElementModule : public RevealedShapeModule {
  public:
    GridElementModule(const int& elID, 
		      RevealedGridModule* arr) : RevealedShapeModule(),
                                                 m_elementID(elID),
                                                 m_grid(arr),
                                                 m_revealed(0) {
    }

    virtual void processReveal(const unsigned int& id,
			       const std::vector<std::string>& name, 
			       const std::vector<float>& surface, 
			       const std::vector<float>& inside,
			       const std::vector<float>& center,
			       const std::vector<float>& extent,
			       const std::vector<float>& color,
			       const std::vector<float>& blob) {

      if(m_revealed!=(inside[0]+surface[0]>0)?1:0) {
          m_revealed=(inside[0]+surface[0]>0)?1:0;
          m_grid->processRevealedElement(m_elementID, m_revealed);
      }
    }

  private:
    int m_elementID;
    int m_elementRevealed;
    RevealedGridModule* m_grid;
    float m_revealed;
};

#endif

