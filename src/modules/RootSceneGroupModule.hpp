/***************************************************************************
 *  RootSceneGroupModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef RootSceneGroupModule_h
#define RootSceneGroupModule_h

#include "GroupModule.hpp"

class RootSceneGroupModule: public GroupModule {
    public:
        RootSceneGroupModule();
        virtual ~RootSceneGroupModule(){}

        void addGroupAttributes();
        static void addShapeCallback(Module* mod,
                                     const std::vector<std::string>& vals) {
            dynamic_cast<RootSceneGroupModule*>(mod)->addShape(vals[0]);
        }
        void addShape(const std::string&);
        static void addModelCallback(Module* mod,
                                     const std::vector<std::string>& vals) {
            dynamic_cast<RootSceneGroupModule*>(mod)->addModel(vals[0]);
        }
        void addModel(const std::string&);
        static void addPathCallback(Module* mod,
                                    const std::vector<std::string>& vals) {
            dynamic_cast<RootSceneGroupModule*>(mod)->addPath(vals[0]);
        }
        void addPath(const std::string&);
        static void addTextCallback(Module* mod,
                                    const std::vector<std::string>& vals) {
            dynamic_cast<RootSceneGroupModule*>(mod)->addText(vals[0]);
        }
        void addText(const std::string&);
        static void addCutCallback(Module* mod,
                                    const std::vector<std::string>& vals) {
            dynamic_cast<RootSceneGroupModule*>(mod)->addCut(vals[0]);
        }
        void addCut(const std::string&);
        static void addArrowCallback(Module* mod,
                                    const std::vector<std::string>& vals) {
            dynamic_cast<RootSceneGroupModule*>(mod)->addArrow(vals[0]);
        }
        void addArrow(const std::string&);
        static void addGridCallback(Module* mod,
                                    const std::vector<std::string>& vals) {
            dynamic_cast<RootSceneGroupModule*>(mod)->addGrid(vals[0]);
        }
        void addGrid(const std::string&);
        static void addGroupCallback(Module* mod,
                                     const std::vector<std::string>& vals) {
            dynamic_cast<RootSceneGroupModule*>(mod)->addGroup(vals[0]);
        }
        void addGroup(const std::string&);
};

#endif

