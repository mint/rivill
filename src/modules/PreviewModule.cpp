/***************************************************************************
 *	PreviewModule.cpp
 *	Part of PreviewModule
 *	2015-  Florent Berthaut
 *	hitmuri.net
 ****************************************************************************/
/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "PreviewModule.hpp" 

#include <iostream>
#include "../Reveal.hpp"
#include "../geoms/BoxGeometry.hpp"
#include "../geoms/SphereGeometry.hpp"
#include <glm/gtc/matrix_transform.hpp>

using namespace std;
using namespace glm;

PreviewModule::PreviewModule():Module() {
	m_type="Preview";
	m_name="preview";
	m_active=true;

	addAttribute(new Attribute("active", 
								Attribute::BOOL_ATTRIBUTE,
								activeCallback, this, 1, 
								Attribute::LOCAL));
	addAttribute(new Attribute("look_at", 
								Attribute::FLOAT_ATTRIBUTE,
								lookAtCallback, this, 3));
	addAttribute(new Attribute("from", 
								Attribute::FLOAT_ATTRIBUTE,
								eyePositionCallback, this, 3));

	m_attributesMap["active"]->initBools(vector<bool>(1, true));

	//initialize matrices
	vector<float> initPos(3,0);
	m_attributesMap["look_at"]->setFloats(initPos);
	initPos[2]=-700.0;
	m_attributesMap["from"]->setFloats(initPos);
	updateViewProjMatrix();

	//create preview window
	m_previewWindow = glfwCreateWindow(600, 480, "Rivill - Preview", NULL, NULL);
	if (!m_previewWindow) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwPollEvents();
	glfwSetWindowSize(m_previewWindow, 640, 480);

	glfwSetWindowUserPointer(m_previewWindow, this);
	glfwSetMouseButtonCallback(m_previewWindow, 
							   (GLFWmousebuttonfun)previewMouseBtnCB);
	glfwSetCursorPosCallback(m_previewWindow, 
							 (GLFWcursorposfun)previewMousePosCB);
	glfwSetScrollCallback(m_previewWindow, 
							 (GLFWscrollfun)previewMouseScrollCB);

	Reveal::getInstance()->addContextHandler(this);
	
	m_initialized=false;
}

PreviewModule::~PreviewModule() {}

void PreviewModule::load(xmlNodePtr node) {
	Module::load(node);
}

void PreviewModule::draw() {
	//set context for window 
	glfwMakeContextCurrent(m_previewWindow);

	//initialize
	if(!m_initialized) {
		initGL();
		m_initialized=true;
	}

	//clear window
	int width, height;
	glfwGetFramebufferSize(m_previewWindow, &width, &height);
	glViewport(0, 0, width, height);
	glClearColor(0.1, 0.1, 0.1, 1);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	//load preview shader 
	glUseProgram(m_program);

	//set view/projection uniforms 
	glUniformMatrix4fv(m_uniforms[Reveal::VIEWPROJMAT], 1, 
					   GL_FALSE, value_ptr(m_viewProjMat));

	//draw all scene geometry and their connected shape/ modules 
	const vector<Geometry*>& sceneGeoms = Reveal::getInstance()->getGeoms();
	vector<Geometry*>::const_iterator itScGeom=sceneGeoms.begin();
	for(; itScGeom!=sceneGeoms.end(); ++itScGeom) {
		(*itScGeom)->draw(m_contextHandlerID, 
						  Reveal::PREVIEWPROG, 
						  m_uniforms, 
						  1);
	}


	//for each space from reveal
		//draw all projectors and depth cameras


	glfwSwapBuffers(m_previewWindow);
}

void PreviewModule::mouseBtnCB(const int& btn, const int& action, 
							   const int& mods) {
	switch(action) {
		case GLFW_PRESS : {
			m_mouseStartPosX = m_mousePosX;
			m_mouseStartPosY = m_mousePosY;
			switch(btn) {
				case 0: {
					m_control=MOVING;
				}break;
				case 1: {
					m_control=ORBITING;
				}break;
				case 2: {
					m_control=TURNING;
				}break;
			}
		}break;
		case GLFW_RELEASE: {
			m_control=NONE;
		}break;
		default:break;
	}
}

void PreviewModule::mousePosCB(const double& x, const double& y) {
	m_mousePosX=x;
	m_mousePosY=y;
	//FIXME recompute lookAt based on eye pos and vector
	switch(m_control) {
		case TURNING : {
			
		} break;
		case MOVING : {
			float diffX = (m_mousePosX-m_mouseStartPosX)/1;
			float diffY = (m_mousePosY-m_mouseStartPosY)/1;
			setEyePosition(m_eyePos[0]+diffX, m_eyePos[1]+diffY, m_eyePos[2]);
			setLookAt(m_lookAt[0]+diffX, m_lookAt[1]+diffY, m_lookAt[2]);
			m_mouseStartPosX=m_mousePosX;
			m_mouseStartPosY=m_mousePosY;
		} break;
		case ORBITING : {

		} break;
		default:break;
	}
}

void PreviewModule::mouseScrollCB(const double& x, const double& y) {

}

void PreviewModule::initGL() {
	#ifdef OSX
		glewExperimental = GL_TRUE; 
	#endif
	if(glewInit()!=GLEW_OK) {
		cout<<"Could not initialize glew for Preview Window"<<endl;
	}

	const GLubyte* renderer = glGetString (GL_RENDERER); 
	const GLubyte* version = glGetString (GL_VERSION);
	cout<<"Renderer: "<<renderer<<endl;
	cout<<"OpenGL version supported "<<version<<endl;



	//create preview program
	const char * vertSource = {
	#ifdef GL42
	"#version 430 \n"
	#else 
	"#version 410 \n"
	#endif
	"\n"
	"in vec3 vertex;\n"
	"in vec2 texCoords;\n"
	"\n"
	"uniform mat4 modelMat;\n"
	"uniform mat4 viewProjMat;\n"
	"\n"
	"out vec2 uvCoords;\n"
	"out float light;\n"
	"\n"
	"void main(void) {\n"
	"	gl_Position = viewProjMat*modelMat*vec4(vertex, 1.0);\n"
	"	light = distance(\n"
	"				vec3(0,0,0),\n"
	"				vec3(modelMat[3][0], modelMat[3][1], modelMat[3][2]));\n"
	"	uvCoords=texCoords;\n"
	"}\n"};
	const char * fragSource = {
#ifdef GL42
	"#version 430 \n"
#else 
	"#version 410 \n"
#endif
	"\n"
	"in vec2 uvCoords;\n"
	"in float light;\n"
	"\n"
	"uniform vec3 shapeColor;\n"
#ifdef GL42
	"layout(binding=3) uniform sampler2D surfaceTex;\n"
#else
	"uniform sampler2D surfaceTex;\n"
#endif
	"\n"
	"out vec4 color;\n"
	"\n"
	"void main(void) {\n"
	"	float pixZ=pow(gl_FragCoord.z,3);\n"
	//"   float pixZ=light;\n"
	"	color = vec4(shapeColor.x*pixZ, shapeColor.y*pixZ,\n"
	"				 shapeColor.z*pixZ, 1);\n"
	//"   color = vec4(uvCoords,0,1);\n"
	"}\n"};
	m_program = Reveal::getInstance()->createProgram(vertSource, fragSource);
	m_uniforms[Reveal::VIEWPROJMAT]
		= glGetUniformLocation(m_program, "viewProjMat");
	m_uniforms[Reveal::MODELMAT]
		= glGetUniformLocation(m_program, "modelMat");
	m_uniforms[Reveal::SHAPECOLOR]
		= glGetUniformLocation(m_program, "shapeColor");
	m_uniforms[Reveal::SURFACETEX]
		= glGetUniformLocation(m_program, "surfaceTex");
}

void PreviewModule::setEyePosition(const float& x, 
								   const float& y, 
								   const float& z) {
	m_eyePos = vec3(x, y, z);
	updateViewProjMatrix();
}

void PreviewModule::setLookAt(const float& x, 
							  const float& y,
							  const float& z) {
	m_lookAt = vec3(x, y, z);
	updateViewProjMatrix();
}


void PreviewModule::updateViewProjMatrix() {
	m_viewMat = lookAt(m_eyePos, 
					   m_lookAt, 
					   vec3(0.0, 1.0, 0.0));
	m_projMat = perspective(45.0, 4.0/3.0, 100.0, 10000.0);
	m_viewProjMat = m_projMat*m_viewMat;
}

/*
	float width = m_screenDim.x();
	float height = m_screenDim.y();
	float dl=width/2.0+m_eyePos.x();
	float db=m_eyePos.y();
	float f=-m_near/(fabs(m_eyePos.z())+1);
	Reveal::getInstance()->getPreviewCamera()
				->setProjectionMatrixAsFrustum((width-dl)*f,-dl*f,	
											   (height-db)*f, -db*f, 
											   m_near, m_far);
	Matrixf rotMat = Matrixf::rotate(m_screenRot[0], Vec3f(1, 0, 0), 
									 m_screenRot[1], Vec3f(0, 1, 0), 
									 m_screenRot[2], Vec3f(0, 0, 1));
	Vec3f eyeToWPos = m_screenPos + m_eyePos*rotMat;
	Vec3f lookAt = m_screenPos+(m_eyePos+Vec3f(0, 0, 1))*rotMat;
	Matrixf viewMat;
	viewMat.makeLookAt(eyeToWPos,
					   lookAt,
					   Vec3f(0,1,0));
	osg::Matrixf glCoordsMat(1, 0, 0, 0, 
							  0, 1, 0, 0,
							  0, 0, 1, 0,
							  0, 0, 0, 1);
	viewMat=viewMat*glCoordsMat;
	Reveal::getInstance()->getPreviewCamera()->setViewMatrix(viewMat);
}
*/

