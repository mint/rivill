/***************************************************************************
 *  RevealedShapeModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef RevealedShapeModule_h
#define RevealedShapeModule_h

#include "ShapeModule.hpp"
#include "RevealedModule.hpp"

class RevealedShapeModule : public ShapeModule, public RevealedModule {
    public:
        RevealedShapeModule();
        virtual ~RevealedShapeModule();
        void gotAttributeListenersUpdate(Attribute* att) { 
            RevealedModule::gotAttributeListenersUpdate(att);
        }
        void setShape(const std::string&);

    protected:
};


#endif

