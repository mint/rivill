/***************************************************************************
 *  DepthGroupModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "DepthGroupModule.hpp" 

#include <iostream>

#include "../Reveal.hpp"

#include "ProjectorModule.hpp"
#include "DepthCamModule.hpp"
#include "DepthMeshModule.hpp"
#include "DepthShapeModule.hpp"
#include "SpaceModule.hpp"

using namespace std;

DepthGroupModule::DepthGroupModule(): GroupModule(), DepthModule() {
    m_type="DepthGroup";
    m_name="group";

    addAttribute(new Attribute("name", 
                                Attribute::STRING_ATTRIBUTE,
                                nameCallback, this, 1, Attribute::LOCAL));
    m_attributesMap["name"]->setStrings(vector<string>(1, m_name));

    addRemoveAttribute();
    addVisibleAttribute();
    addAttribute(new Attribute("add_projector", 
                                Attribute::ACTION_ATTRIBUTE,
                                addProjCallback, this, Attribute::LOCAL));
    addAttribute(new Attribute("add_depth_group", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addDepthGroupCallback, this, 1, 
                                Attribute::LOCAL));
    m_attributesMap["add_depth_group"]->initStrings(vector<string>(1,"group"));
    addAttribute(new Attribute("add_depth_camera", 
                                Attribute::ACTION_ATTRIBUTE,
                                addCamCallback, this, Attribute::LOCAL));
    addAttribute(new Attribute("add_depth_shape", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addDepthShapeCallback, this, 1));
    m_attributesMap["add_depth_shape"]->initStrings(vector<string>(1,"shape"));
    addAttribute(new Attribute("add_depth_mesh", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addDepthMeshCallback, this, 1));
    m_attributesMap["add_depth_mesh"]->initStrings(vector<string>(1,"mesh"));

    addTransformationAttributes();
}

DepthGroupModule::~DepthGroupModule() {
}

void DepthGroupModule::deleteModule() {
    Module::deleteModule();
}

void DepthGroupModule::updateModelMatrix() {
    GroupModule::updateModelMatrix();
}

void DepthGroupModule::addChild(Module* mod) {
    GroupModule::addChild(mod);
    m_space->addSpaceModule(mod);
    Reveal::getInstance()->refreshModules();
}

void DepthGroupModule::addProj() {
    ProjectorModule* proj = new ProjectorModule();
    addChild(proj);
}

void DepthGroupModule::addCam() {
    DepthCamModule* cam = new DepthCamModule();
    addChild(cam);
}

void DepthGroupModule::addDepthShape(const string& name) {
    DepthShapeModule* tmp = new DepthShapeModule();
    tmp->setName(name);
    addChild(tmp);
}

void DepthGroupModule::addDepthMesh(const string& name) {
    DepthMeshModule* tmp = new DepthMeshModule();
    tmp->setName(name);
    addChild(tmp);
}

void DepthGroupModule::addDepthGroup(const string& name) {
    DepthGroupModule* tmp = new DepthGroupModule();
    tmp->setName(name);
    addChild(tmp);
}

