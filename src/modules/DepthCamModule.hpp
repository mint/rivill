/***************************************************************************
 *  DepthCamModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef DepthCamModule_h
#define DepthCamModule_h

#include "GroupModule.hpp"
#include "DepthModule.hpp"

#include <vector>
#include <deque>
#include <thread>
#include <mutex>

#include <OpenNI.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/aruco.hpp>
#include <FL/filename.H>

class SpaceModule;
class ProjectorModule;
class CamGeometry;

struct s_markerAndTime;
struct DepthMode;
struct ColorMode;

class Tracker;
class ArucoTracker;
class ImageTracker;

class DepthCamModule : public GroupModule, public DepthModule {
    public:
        DepthCamModule();
        virtual ~DepthCamModule();
        void deleteModule();
        void setModuleName(const std::string& val);
        
        inline virtual bool isDepthVisible(const unsigned int& id) { 
            return m_open && m_visible && (m_detectingMarkers||id==m_depthID);
        }

        virtual void draw(const int& contextID,
                          const Reveal::REVIL_PROGRAM& prog,
                          std::map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                          const unsigned int& component);

        virtual void setSpace(SpaceModule*);
        inline virtual int getNbSubDepthIDs(){return m_maxNbMarkers;}

        virtual void update(const int& timeDiffMs);
        virtual void updateModelMatrix();

        void refreshDeviceList();
        void activateColor();
        void deactivateColor();
        bool getFrames();
        inline openni::VideoFrameRef getDepthFrame(){return m_depthFrame;}
        inline openni::VideoFrameRef getColorFrame(){return m_colorFrame;}
        inline openni::VideoStream& getDepthStream(){return m_depth;}


        static void refreshCallback(Module* mod) {
            dynamic_cast<DepthCamModule*>(mod)->refreshDeviceList();
        }
        static void deviceCallback(Module* mod,
                                   const std::vector<std::string>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->openDevice(vals[0]);
        }
        virtual void openDevice(const std::string& dev, bool calib=false);
        static void closeCallback(Module* mod) {
            dynamic_cast<DepthCamModule*>(mod)->closeDevice(true);
        }
        void closeDevice(const bool& clear=false);
        static void modeCallback(Module* mod,
                                   const std::vector<std::string>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->setMode(vals[0]);
        }
        virtual void setMode(const std::string& mode);

        //filter callbacks
        static void filterCallback(Module* mod,
                                   const std::vector<std::string>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->setFilter(vals[0]);
        }
        void setFilter(const std::string& fil);
        static void filterSizeCallback(Module* mod,
                                       const std::vector<float>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->setFilterSize(vals[0]);
        }
        void setFilterSize(int siz);
        static void filterSpatCallback(Module* mod,
                                       const std::vector<int>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->setFilterSpat(vals[0]);
        }
        void setFilterSpat(int siz);
        static void filterMCOCallback(Module* mod,
                                      const std::vector<float>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->setFilterMCO(vals[0]);
        }
        void setFilterMCO(float mco);
        static void filterBetaCallback(Module* mod,
                                       const std::vector<float>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->setFilterBeta(vals[0]);
        }
        void setFilterBeta(float beta);

        //contour callback
        static void contourThresholdCallback(Module* mod,
                                   const std::vector<float>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->setContourThresh(vals[0]);
        }
        inline void setContourThresh(float cont) {m_contourThreshold=cont;}

        static void markersDetectionCallback(Module* mod,
                                 const std::vector<std::string>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->setMarkersDetection(vals[0]);
        }
        void setMarkersDetection(const std::string& val);
        static void imageMarkersFolderCallback(Module* mod, 
                                               const std::string& f) {
            dynamic_cast<DepthCamModule*>(mod)->setImageMarkersFolder(f);
        }
        virtual void setImageMarkersFolder(const std::string& f);

        static void markersFillMethodCallback(Module* mod,
                                 const std::vector<std::string>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->setMarkersFillMethod(vals[0]);
        }
        void setMarkersFillMethod(const std::string& val);
        void detectMarkers();
        static void outputMarkersCallback(Module* mod,
                                          const std::vector<float>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->outputMarkers(vals);
        }
        inline void outputMarkers(const std::vector<float>& vals){}
        static void debugMarkersCallback(Module* mod,
                                         const std::vector<bool>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->debugMarkers(vals[0]);
        }
        void debugMarkers(const bool& val);

        static void captureBackgroundCallback(Module* mod,
                                             const std::vector<bool>& vals) {
            dynamic_cast<DepthCamModule*>(mod)->setCaptureBackground(vals[0]);
        }
        void setCaptureBackground(const bool& cap);
        void captureBackground();

        static void calibrationCallback(Module* mod) {
            dynamic_cast<DepthCamModule*>(mod)->calibrate();
        }
        void calibrate();
        void refreshProjList(const std::vector<ProjectorModule*> projs);

        static void cursCallback(Module* mod, const std::vector<float>& vals) {}

        void setCalibMarkerPosY(const int&);
        void setCalibMarkerRotX(const int&);
        void setCalibMarkerRotY(const int&);
        void setCalib(const int&);
        inline double& getHorizontalFOV(){return m_horizontalFOV;}
        inline double& getVerticalFOV(){return m_verticalFOV;}

        bool isOpen(){return m_open;}

        void processFrames();

        void gotAttributeListenersUpdate(Attribute*);
        void processCursor(const std::vector<float>& pos);
        inline const std::string& getDeviceID(){return m_deviceID;}

        void attachProj(ProjectorModule*);
        void detachProj(ProjectorModule*);

    private:

        //filter
        void filterDepthFrame();

        //marker
        void draw_point(cv::Mat& img, cv::Point2f fp);
        void filterMarkers(std::vector<std::vector<cv::Point2f> >& cor,
                           std::vector<int>& ids);
        void showAliveMarkers();
        std::vector<cv::Point2f> newSetOfPoint(std::vector<cv::Point2f> points, 
                                               bool readyToCoop);
        int jointly(std::vector<cv::Point2f> points, cv::Point2f pt);
        void delimitation(cv::Mat& img);
        cv::Point calculationCenterOfMarker(s_markerAndTime& marker);
        void globalFunction(cv::Mat &mat);
        cv::Scalar voronoiColor(cv::Mat mat, cv::Point2f pt);
        void drawVoronoi(cv::Mat& img, cv::Subdiv2D& subdiv);
        void drawDelaunay(cv::Mat& img, cv::Subdiv2D& subdiv, 
                          cv::Scalar delaunay_color);
        bool createMatDepth();
        bool createMatColor();
        bool createMatDepthAndColor();
        
    private:
        std::string m_deviceID;
        bool m_open;
		bool m_init;
        std::vector<std::string> m_modes;
        std::map<std::string, DepthMode> m_depthModes;

        openni::Device m_device;
        std::vector<std::string> m_serialsVec;
        std::map<std::string, std::string> m_serialsMap;
        openni::VideoStream m_depth, m_color;
        openni::VideoStream** m_streams;
        openni::VideoFrameRef m_depthFrame;
        openni::VideoFrameRef m_colorFrame;
        bool m_gotDepth, m_gotColor;
        bool m_colorActivated;

        unsigned short* m_defaultDepthFrame;

        CamGeometry* m_camGeom;

        std::map<int, GLuint> m_camTexMap;
        std::map<int, GLuint> m_camTexMap1;
        std::map<int, GLuint> m_camTexMap2;
        std::map<int, int> m_upCamCounter;
        std::map<int, bool> m_camTexUpMap;

        std::map<int, GLuint> m_markTexMap;
        std::map<int, bool> m_markTexUpMap;

        uint16_t m_width, m_height, m_fps;
        uint16_t m_defaultWidth, m_defaultHeight, m_defaultFps;
        double m_horizontalFOV, m_verticalFOV;
        bool m_mirrored;
        float m_contourThreshold;

        //frames processing
        cv::Size m_procSize;
        std::thread* m_framesThread;
        std::mutex m_framesLock;

        //filter
        bool m_filtering;
        std::string m_filter;
        int m_filterSize;
        std::vector<std::string> m_filters;
        int m_maxFilterSize;
        int m_filterIndex;
        unsigned short** m_prevDepthFrames;
        int* m_sumDepthFrame;
        unsigned short* m_filtDepthFrame;
        std::map<int, GLuint> m_filtTexMap;
        std::map<int, bool> m_filtTexUpMap;
        int m_filterSpat;

        //one euro filter
        float* m_oefXFrame;
        float* m_oefDXFrame;
        float m_oefRate;
        float m_oefMinCutoff;
        float m_oefBeta;
        float m_oefDCutoff;
        bool m_oefInit;

        //background
        int m_capturingBackground;
        bool m_capturedBackground;
        bool m_backgroundActive;
        cv::Mat m_backImg;

        //markers
        bool m_detectingMarkers;
        unsigned char *m_markersFrame;
        cv::Mat m_detRgbImg;
        cv::Mat m_detMarkImg;
		cv::Mat m_detDepthImg;
        int m_maxNbMarkers;
        bool m_fillingMarkers;
        Tracker* m_currentTracker;
        ArucoTracker* m_arucoTracker;
        ImageTracker* m_imageTracker;
        bool m_debuggingMarkers;

        //calibration
        int m_calibrating;
        bool m_calibrationStopped;
        int m_redXmm, m_redYmm, m_redZmm;
        int m_greenXmm, m_greenYmm, m_greenZmm;
        int m_blueXmm, m_blueYmm, m_blueZmm;
        int m_patternWidth;
        int m_patternPositionX;
        int m_patternPositionY;
        int m_markerPosY;
        int m_markerRotX;
        int m_markerRotY;
        ProjectorModule* m_calibrationProj;
        std::vector<cv::Point3f> m_worldCorners;
        cv::Size m_patternSize;

        //projectors
        std::map<int, ProjectorModule*> m_attachedProjs;

        //cursor
        float m_filterCursorSize;
        std::vector<float> m_filterCursorSums;
        std::vector<std::deque<float> > m_filterCursorValues;
        std::vector<float> m_cursorValues;

        //markerDetection
        cv::Mat m_finalCombin;
        cv::Mat m_rgbImg;
        cv::Mat m_depthImg;
        cv::Mat m_rgbDepthImg;
        cv::Mat m_resImg;
        std::vector<s_markerAndTime> m_detectedMarkers;
        std::map<int, s_markerAndTime> m_aliveMarkers;
        std::vector<s_markerAndTime> m_removedMarkers;
        std::vector<s_markerAndTime> m_sentDetectedMarkers;
        std::vector<s_markerAndTime> m_sentRemovedMarkers;
        bool m_notVoronoi;
};


//------------------------------Trackers-------------------

class Tracker {
    public :
        virtual ~Tracker(){}
        virtual void init()=0;
        virtual void track(cv::Mat&,
                           std::vector<std::vector<cv::Point2f> >&, 
                           std::vector<int>&)=0;
};

class ArucoTracker : public Tracker {
    public:
        ~ArucoTracker(){}
        void init();
        void track(cv::Mat&, 
                   std::vector<std::vector<cv::Point2f> >&, 
                   std::vector<int>&);

};

struct s_markerAndTime {
    int id;
    float x;
    float y;
    std::vector<cv::Point2f> corners;
    std::vector<glm::vec3> corners3D;
    int plan = -1;
    time_t detection;
    time_t timeLess;
    glm::vec3 orientation;
    bool detected;
};

class ImageTracker : public Tracker {
    public:
        ImageTracker();
        ~ImageTracker(){}
        void init();
        void track(cv::Mat&,
                   std::vector<std::vector<cv::Point2f> >&, 
                   std::vector<int>&);
        void setImgFolder(const std::string& fol){m_imgFolder=fol;}
    private:
        std::string m_imgFolder;
        std::vector<cv::Mat> m_descriptors;
        std::vector<std::vector<cv::KeyPoint> > m_keypoints;
        cv::Ptr<cv::Feature2D> m_detector;
        std::vector<int> m_rows;
        std::vector<int> m_cols;
        cv::Ptr<cv::DescriptorMatcher> m_matcher;
};

struct DepthMode {
    int width;
    int height;
    int fps;
};

#endif

