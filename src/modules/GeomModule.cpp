/***************************************************************************
 *  GeomModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "GeomModule.hpp" 
#include "../geoms/Geometry.hpp"

using namespace std;
using namespace glm;

GeomModule::GeomModule(): Module(), 
                          m_localInsideAxes(true), 
                          m_currentGeom(NULL) {
	m_subShapeInvMats.push_back(glm::mat4(1));
}

GeomModule::~GeomModule() {}

void GeomModule::draw(const int& contextID,
                      const Reveal::REVIL_PROGRAM& prog, 
                      map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                      const unsigned int& component) {
    glUniformMatrix4fv(uniforms[Reveal::MODELMAT], 1, 
                       GL_FALSE, value_ptr(m_modelMat));
    if(prog==Reveal::RENDERPROG) {
		glUniformMatrix4fv(uniforms[Reveal::INVMODELMAT], 1, 
						   GL_FALSE, value_ptr(m_invModelMat));
		glUniform3fv(uniforms[Reveal::MODELSCALE], 1, value_ptr(m_modelScale));

        glUniformMatrix4fv(uniforms[Reveal::SUBSHAPEINVMAT], 1,
						   GL_FALSE, value_ptr(m_subShapeInvMats[component]));
        glUniform1f(uniforms[Reveal::SUBSHAPEID], component);
        glUniform1f(uniforms[Reveal::SUBSHAPESNB], m_subShapeInvMats.size());
    }
}

void GeomModule::deleteModule() {
    if(m_currentGeom) {
        m_currentGeom->unregisterModule(this);
    }
    Module::deleteModule();
}



