/***************************************************************************
 *  RevealedModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef RevealedModule_h
#define RevealedModule_h

#include "GeomModule.hpp"
#include "opencv2/opencv.hpp"
#include <FL/Fl_Shared_Image.H>
#include <sys/time.h>
#include <thread>
#include <mutex>

struct RevealedLayer;

class RevealedModule : public virtual GeomModule {
    public:
        RevealedModule();
        virtual ~RevealedModule();
        void deleteModule();

        void draw(const int& contextID,
                  const Reveal::REVIL_PROGRAM& prog, 
                  std::map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                  const unsigned int& comp=0);

        static void colorCallback(Module* mod, 
                                  const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setColor(vals);
        }
        virtual void setColor(const std::vector<float>&);

        static void insideVisibleCallback(Module* mod, 
									   const std::vector<bool>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setInsideVisible(vals[0]);
        }
        virtual void setInsideVisible(bool v){m_insideVisible=v;}

        static void insideAxesCallback(Module* mod, 
                                       const std::vector<std::string>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setInsideAxes(vals[0]);
        }
        virtual void setInsideAxes(const std::string&);

        static void insideStructureCallback(Module* mod, 
                                       const std::vector<std::string>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setInsideStructure(vals[0]);
        }
        virtual void setInsideStructure(const std::string&);

        virtual void generateSurfaceDistanceTexture(const std::string& f);
        virtual void openSurfaceDistanceTexture(const std::string& f);

        static void structureRatioCallback(Module* mod, 
                                    const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setStructureRatio(vals[0]);
        }
        virtual void setStructureRatio(const float& r){m_structureRatio=r;}

		//Gradient
        static void gradientAlphaCallback(Module* mod, 
                                    const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setGradientAlpha(vals[0]);
        }
        virtual void setGradientAlpha(const float& a){m_insideGradientAlpha=a;}

        static void gradientTypeCallback(Module* mod, 
                                       const std::vector<std::string>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setGradientType(vals[0]);
        }
        virtual void setGradientType(const std::string&);

        static void gradientStepsCallback(Module* mod, 
                                    const std::vector<int>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setGradientSteps(vals[0]);
        }
        virtual void setGradientSteps(const int& s){m_insideGradientSteps=s;}

        static void gradientCurveRatioCallback(Module* mod, 
                                    const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setGradientCurveRatio(vals[0]);
        }
        virtual void setGradientCurveRatio(const float& r){
			m_insideGradientCurveRatio=r;
		}

        static void gradientTextureCallback(Module* mod, 
											const std::string& val) {
            dynamic_cast<RevealedModule*>(mod)->setGradientTexture(val);
        }
        virtual void setGradientTexture(const std::string&);


		//Texture
        static void textureAlphaCallback(Module* mod, 
                                    const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setTextureAlpha(vals[0]);
        }
        virtual void setTextureAlpha(const float& a){m_insideTextureAlpha=a;}

        static void textureBlendCallback(Module* mod, 
                                    const std::vector<bool>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setTextureBlend(vals[0]);
        }
        virtual void setTextureBlend(bool b);

        static void textureGrayscaleCallback(Module* mod, 
                                    const std::vector<bool>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setTextureGrayscale(vals[0]);
        }
        virtual void setTextureGrayscale(bool b){m_textureGrayscale=b;}


        static void textureCallback(Module* mod, const std::string& f) {
            dynamic_cast<RevealedModule*>(mod)->setTexture(f);
        }
        virtual void setTexture(const std::string& f);

        static void textureLayersFolderCallback(Module* mod, 
                                                const std::string& f) {
            dynamic_cast<RevealedModule*>(mod)->setTextureLayersFolder(f);
        }
        virtual void setTextureLayersFolder(const std::string& f);


        static void textureOffsetCallback(Module* mod, 
                                          const std::vector<float>& f) {
            dynamic_cast<RevealedModule*>(mod)
                                ->setTextureOffset(f[0],f[1],f[2]);
        }
        virtual void setTextureOffset(const float& x, 
                                      const float& y,
                                      const float& z);
        static void textureScaleCallback(Module* mod, 
                                         const std::vector<float>& f) {
            dynamic_cast<RevealedModule*>(mod)
                                ->setTextureScale(f[0],f[1],f[2]);
        }
        virtual void setTextureScale(const float& x, 
                                     const float& y,
                                     const float& z);
		
        static void textureReactCallback(Module* mod, 
                                         const std::vector<std::string>& s) {
            dynamic_cast<RevealedModule*>(mod)->setTextureReact(s[0]);
        }
        virtual void setTextureReact(const std::string& s);

        static void animPosCallback(Module* mod, 
                                      const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setAnimPos(vals[0]);
        }
        virtual void setAnimPos(const float&);
        static void animPlayCallback(Module* mod, 
                                     const std::vector<bool>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setAnimPlay(vals[0]);
        }
        virtual void setAnimPlay(const bool&);
        static void animSpeedCallback(Module* mod, 
                                      const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setAnimSpeed(vals[0]);
        }
        virtual void setAnimSpeed(const float&);

		//Density
        static void densityAlphaCallback(Module* mod, 
                                    const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setDensityAlpha(vals[0]);
        }
        virtual void setDensityAlpha(const float& a){m_insideDensityAlpha=a;}

        static void densityTypeCallback(Module* mod, 
                                    const std::vector<std::string>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setDensityType(vals[0]);
        }
        virtual void setDensityType(const std::string& t);

        static void densityRatioCallback(Module* mod, 
                                    const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setDensityRatio(vals[0]);
        }
        virtual void setDensityRatio(const float& r){m_insideDensityRatio=r;}

        static void densitySizeCallback(Module* mod, 
                                    const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setDensitySize(vals[0]);
        }
        virtual void setDensitySize(const float& s){m_insideDensitySize=s;}

        static void densityCurveRatioCallback(Module* mod, 
                                    const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setDensityCurveRatio(vals[0]);
        }
        virtual void setDensityCurveRatio(const float& r) { 
			m_insideDensityCurveRatio=r;
		}

		//Surface
        static void surfaceCallback(Module* mod, 
                                   const std::vector<std::string>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setSurface(vals[0]);
        }
        virtual void setSurface(const std::string&);

        static void surfaceColorCallback(Module* mod, 
                                   const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setSurfaceColor(vals);
        }
        virtual void setSurfaceColor(const std::vector<float>&);

        static void surfaceThicknessCallback(Module* mod, 
                                    const std::vector<float>& vals) {
            dynamic_cast<RevealedModule*>(mod)->setSurfaceThickness(vals[0]);
        }
        virtual void setSurfaceThickness(const float&);

        static void surfaceTextureCallback(Module* mod, const std::string& f) {
            dynamic_cast<RevealedModule*>(mod)->setSurfaceTexture(f);
        }
        virtual void setSurfaceTexture(const std::string& f);

		//Revealed 
        static void revnamCallback(Module* mod, 
                                   const std::vector<std::string>& vals){}
        static void revsurCallback(Module* mod,const std::vector<float>& vals){}
        static void revinsCallback(Module* mod,const std::vector<float>& vals){}
        static void revcentCallback(Module* mod,const std::vector<float>&vals){}
        static void revextCallback(Module* mod,const std::vector<float>&vals){}
        static void revcolCallback(Module* mod,const std::vector<float>& vals){}
        static void revhistoCallback(Module* mod,const std::vector<float>&vals){}
        static void revvoxelCallback(Module* mod,const std::vector<float>&vals){}


        inline bool isOutputActive(){return m_outputActive;}

        inline void setRevID(const unsigned int& id) { 
            m_revID = id;
            m_revIDBit = double(1<<(int(id)%15));
        }
        inline const unsigned int& getRevID(){return m_revID;}
        inline const float& getRevIDBit(){return m_revIDBit;}
		void updateRevOffset(unsigned int& offset);

        void gotAttributeListenersUpdate(Attribute*);


		//Updating
        virtual void update(const int& timeDiffMs);

        virtual void processReveal(const unsigned int& depthId,
                                   const std::string& depthName, 
								   const unsigned int& depthNb,
								   unsigned int* outputImg);
		void textureReact();


		//utilities
        void hsv2rgb(const std::vector<float>& hsv, 
                     std::vector<float>& rgb);

        void addRevealedAttributes();
        void addRevealedInsideAttributes();
        void addRevealedInsideStructureAttributes();
        void addRevealedInsideGradientAttributes();
        void addRevealedInsideDensityAttributes();
        void addRevealedInsideTextureAttributes();
        void addRevealedOutputAttributes();

		void parsePvm(const std::string& file, 
					  glm::ivec3& dimensions, 
					  std::vector<float>& data);

    protected:
        unsigned int m_revID;
        float m_revIDBit;
        std::vector<float> m_color;
        std::vector<float> m_surfaceColor;
		int m_shapeGeom;
        int m_surface;
        std::vector<std::string> m_surfaces;
        float m_surfaceThickness;

        int m_inside;
		bool m_insideVisible;

        std::vector<std::string> m_insideAxes;
		int m_insideStructure;
        std::vector<std::string> m_insideStructures;
		float m_structureRatio;

		float m_insideGradientAlpha;
        int m_gradientType;
        std::vector<std::string> m_gradientTypes;
		int m_insideGradientSteps; //0-10
		float m_insideGradientCurveRatio; //sign/linearity (-y -> -pow(x,y))
        std::map<int, bool> m_gradientUpMap;
        std::map<int, GLuint> m_gradientIDMap;
        Fl_Shared_Image* m_gradientImage;

		float m_insideDensityAlpha;
		int m_insideDensityType;
        std::vector<std::string> m_insideDensityTypes;
		float m_insideDensityRatio;
		float m_insideDensitySize;
		float m_insideDensityCurveRatio;

		float m_insideTextureAlpha;
		bool m_insideTextureBlend;
        float m_textureOffset[3];
        float m_textureScale[3];
        std::vector<std::string> m_reactivityTypes;
		int m_reactivityType;
		bool m_textureGrayscale;
		bool m_textureCanDelete;

        std::map<int, bool> m_surfaceUpMap;
        std::map<int, GLuint> m_surfaceIDMap;
        Fl_Shared_Image* m_surfaceImage;
        char* m_layersData;
        std::map<int, bool> m_insideUpMap;
        std::map<int, GLuint> m_insideIDMap;
        int m_layersNb;
        std::vector<Fl_Shared_Image*> m_layersImages;
        Fl_Shared_Image* m_insideImage;
        std::vector<RevealedLayer*> m_layers;
        std::map<int, GLuint> m_coordsIDMap;
        std::map<int, bool> m_coordsUpMap;
        std::map<unsigned int, bool> m_depthModsMap;
        int m_volumeTextureWidth;
        int m_volumeTextureHeight;

        //std::vector<std::string> m_texMirrors;

        bool m_outputActive;
        std::vector<Attribute*> m_revAttributes;
		int m_revOffset, m_revSize;
        int m_revSurf, m_revIns, m_revCent, m_revCol, m_revHis, m_revVox;
        int m_outShapeSize;
        int m_outVoxelSize;
        int m_outHistoSize;
        int m_outDepthSize;
        int m_outNbDepth;

        std::thread* m_reactThread;
        std::mutex m_reactLock;
		bool m_reactActive;
		int m_voxDim;
		int m_voxSize;
		float *m_reactTexInput, *m_reactTexInternal;
	   	char *m_reactTexOutput;
        std::map<int, bool> m_reactUpMap;
        std::map<int, GLuint> m_reactIDMap;

		glm::ivec3 m_surfDistDims;
		std::vector<float> m_surfDistData;

        int m_timeDiffMs;
};

class RevealedLayer {
    public:
        RevealedLayer();
        virtual ~RevealedLayer(){}
        virtual bool update(const int& timeDiffMs, const int& depth, 
                            char* data, const int& width, const int& height)=0;
        virtual void setAnimPos(const float& pos){}
        virtual void setAnimPlay(const bool& play){m_playing=play;}
        virtual void setAnimSpeed(const float& speed){m_speed=speed;}
		virtual void releaseData(){}
    protected:
        bool m_playing;
        float m_speed;
        bool m_update;
        int m_accumTimeDiff;
};

class RevealedLayerVideo: public RevealedLayer {
    public:
        RevealedLayerVideo(const int& w, const int& h):RevealedLayer(){
            m_texWidth=w;
            m_texHeight=h;
            m_thread=NULL;
            m_data = new char[w*h*3];
            m_gotFrame=false;
			m_vidData = NULL;
        }
        virtual bool update(const int& timeDiffMs, const int& depth, 
                            char* data, const int& width, const int& height);
        virtual void setAnimPos(const float& pos);
		virtual void setVideo(cv::VideoCapture vid);
        static void* videoReadThreadFunction(void* pvParam) {
            RevealedLayerVideo *pThis=(RevealedLayerVideo*)pvParam;
            pThis->read();
            return NULL;
        }

        virtual void setAnimPlay(const bool& play) {
            if(play && !m_playing) {
                m_playing=play;
                gettimeofday(&m_prevTime,NULL);
				m_curFrame=0;
                m_thread = new std::thread(videoReadThreadFunction, this);
            }
            else if(!play && m_playing) {
                if(m_thread) {
                    m_playing=play;
                    m_thread->join();
                    delete m_thread;
                    m_thread=NULL;
                }
            }
        }
        void read();

        cv::VideoCapture m_video;
        cv::Mat m_frame;
		int m_texWidth, m_texHeight;
		int m_vidWidth, m_vidHeight;
		int m_vidFrames, m_vidFps;
		char* m_vidData;
        std::thread* m_thread;
        std::mutex m_lock;
        bool m_gotFrame;
        char* m_data;
        timeval m_prevTime;
		int m_curFrame;
};

class RevealedLayerImage : public RevealedLayer {
    public:
        RevealedLayerImage():RevealedLayer(){}
        virtual bool update(const int& timeDiffMs, const int& depth, 
                            char* data, const int& width, const int& height);
		virtual void releaseData(){m_image->release();}
        Fl_Shared_Image* m_image;
};

class RevealedLayerAnim : public RevealedLayer {
    public:
        RevealedLayerAnim();
        virtual bool update(const int& timeDiffMs, const int& depth, 
                            char* data, const int& width, const int& height);
        virtual void setAnimPos(const float& pos);
        std::vector<Fl_Shared_Image*> m_images;
        int m_current;
};

#endif

