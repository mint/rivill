/***************************************************************************
 *  OutputOscModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "OutputOscModule.hpp"
#include <iostream>

using namespace std;

OutputOscModule::OutputOscModule(): Module() {
    m_type="OutputOsc";
    m_name="osc";
    addNameAttribute();

    //string attribute for ip adress
    addAttribute(new Attribute("address",
                                Attribute::STRING_ATTRIBUTE,
                                addressCallback, this, 1, Attribute::LOCAL));
    m_attributesMap["address"]->setStrings(vector<string>(1, "127.0.0.1"));

    //int attribute for port
    addAttribute(new Attribute("port",
                                Attribute::INT_ATTRIBUTE,
                                portCallback, this, 1, Attribute::LOCAL));
    m_attributesMap["port"]->setInts(vector<int>(1, 7000));

}

OutputOscModule::~OutputOscModule() {

}

void OutputOscModule::setAddress(const string& add) {
    m_address=add;
}

void OutputOscModule::setPort(const int& port) {
    m_port=port;
}

