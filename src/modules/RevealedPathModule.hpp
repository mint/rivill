/***************************************************************************
 *  RevealedPathModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef RevealedPathModule_h
#define RevealedPathModule_h

#include "RevealedModule.hpp"

class PathPoint;
class PathPointModule;

class RevealedPathModule : public RevealedModule, public ModuleListObserver {
    public:
        RevealedPathModule();
        virtual ~RevealedPathModule();

        virtual void deleteModule();

        virtual void load(xmlNodePtr node);
        virtual xmlNodePtr save(xmlNodePtr parentNode);

        static void addPointCallback(Module* mod, 
                                     const std::vector<float>& vals) {
            dynamic_cast<RevealedPathModule*>(mod)->addPoint(vals[0], 
                                                             vals[1], 
                                                             vals[2]);
        }
        void addPoint(const float& x, const float& y, const float& z);
        static void addModulePointCallback(Module* mod,
                                    const std::vector<std::string>& vals) {
            dynamic_cast<RevealedPathModule*>(mod)->addModulePoint(vals[0]);
        }
        void addModulePoint(const std::string&);
        static void removePointCallback(Module* mod) {
            dynamic_cast<RevealedPathModule*>(mod)->removePoint();
        }
        void removePoint();

        static void clearPointsCallback(Module* mod) {
            dynamic_cast<RevealedPathModule*>(mod)->clearPoints();
        }
        void clearPoints();
        static void thicknessCallback(Module* mod, 
                                      const std::vector<float>& vals) {
            dynamic_cast<RevealedPathModule*>(mod)->setThickness(vals[0]);
        }
        void setThickness(const float& t);
        static void edgesCallback(Module* mod, 
                                  const std::vector<int>& vals) {
            dynamic_cast<RevealedPathModule*>(mod)->setEdges(vals[0]);
        }
        void setEdges(const int& e);

        void refreshGeometry();
        void updateModelMatrix();
        virtual void processReveal(const std::vector<std::string>&, 
                           const std::vector<float>&,const std::vector<float>&,
                           const std::vector<float>&,const std::vector<float>&,
                           const std::vector<float>&);

        virtual void updateModulesList(const std::map<std::string, Module*>&);

        void draw(const int& contextID,
                  const Reveal::REVIL_PROGRAM& prog, 
                  std::map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                  const unsigned int& comp=0);

    protected:
        std::vector<PathPoint*> m_points;
        std::map<unsigned int, std::vector<glm::vec4> > m_compoVertices;
        std::map<unsigned int, glm::mat4> m_compoRots;
        std::map<unsigned int, glm::vec4> m_compoLocalSizes;
        float m_thickness;
        int m_nbEdges;
        Geometry* m_pathGeom;
		int m_pathInitialised;
};

class PathPoint {
    public:
        PathPoint(){}
        PathPoint(glm::vec4 pnt):m_point(pnt){}
        virtual ~PathPoint(){}
        inline const glm::vec4& getPos(){return m_point;}
        inline glm::vec4& editPos(){return m_point;}
        virtual void loadPnt(xmlNodePtr node);
        virtual xmlNodePtr savePnt(xmlNodePtr parentNode);
        virtual void refreshPos(){}

    protected:
        glm::vec4 m_point;
        glm::vec4 m_absPoint;
};

class PathPointModule : public PathPoint, public Listener {
    public:
        PathPointModule(RevealedPathModule*);
        PathPointModule(RevealedPathModule*, Module*);
        virtual ~PathPointModule();
        virtual void update(std::vector<float>& floatValues, 
                            std::vector<int>& intValues,
                            std::vector<bool>& boolValues,
                            std::vector<std::string>& stringValues);
        virtual void loadPnt(xmlNodePtr node);
        virtual xmlNodePtr savePnt(xmlNodePtr parentNode);
        virtual void refreshPos();

    protected:
        RevealedPathModule* m_path;
        Module* m_module;
        std::string m_moduleName;
};


#endif

