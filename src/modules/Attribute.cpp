/***************************************************************************
 *  Attribute.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "Attribute.hpp"

#include <sstream>
#include <iostream>
#include <FL/fl_utf8.h>

#include "../Reveal.hpp"
#include "Module.hpp"

using namespace std;

Attribute::~Attribute() {
    vector<Listener*>::iterator itLis = m_listeners.begin();
    for(; itLis!=m_listeners.end(); ++itLis) {
        Reveal::getInstance()->deleteListener((*itLis));
    }
    m_listeners.clear();
    m_listenersMap.clear();
}

void Attribute::load(xmlNodePtr node) {
    vector<float> floatVals;
    vector<int> intVals;
    vector<bool> boolVals;
    vector<string> strVals;
    xmlNodePtr valNode;
    for(valNode=node->children; valNode; valNode=valNode->next){
        if(valNode->type == XML_ELEMENT_NODE) {
            if(!xmlStrcmp(valNode->name, (const xmlChar *)"Float")) {
                char* value = (char*)xmlGetProp(valNode,(xmlChar*)"value");
                if(value!=NULL) {
                    floatVals.push_back(atof(value));
                }
            }
            else if(!xmlStrcmp(valNode->name, (const xmlChar *)"Int")) {
                char* value = (char*)xmlGetProp(valNode,(xmlChar*)"value");
                if(value!=NULL) {
                    intVals.push_back(atoi(value));
                }
            }
            else if(!xmlStrcmp(valNode->name, (const xmlChar *)"Bool")) {
                char* value = (char*)xmlGetProp(valNode,(xmlChar*)"value");
                if(value!=NULL) {
                    boolVals.push_back(atoi(value));
                }
            }
            else if(!xmlStrcmp(valNode->name, (const xmlChar *)"String")) {
                char* value = (char*)xmlGetProp(valNode,(xmlChar*)"value");
                if(value!=NULL) {
                    strVals.push_back(string(value));
                }
            } //Listeners
            else if((!xmlStrcmp(valNode->name, (const xmlChar *)"Listeners"))) {
                xmlNodePtr lisNode;
                for(lisNode=valNode->children;lisNode;lisNode=lisNode->next) {
                    if(lisNode->type == XML_ELEMENT_NODE) {
                        string typeStr((char*) lisNode->name);
                        Listener* lis = Reveal::getInstance()
                                                    ->createListener(typeStr);
                        if(lis!=NULL) {
                            lis->load(lisNode);
                            addListener(lis);
                        }
                    }
                }
            }
        }
    }

    if(floatVals.size()>0 && m_type==FLOAT_ATTRIBUTE) {
        if(m_loadable) {
            setFloats(floatVals);
        }
        else {
            initFloats(floatVals);
        }
    }
    else if(intVals.size()>0 && m_type==INT_ATTRIBUTE) {
        if(m_loadable) {
            setInts(intVals);
        }
        else {
            initInts(intVals);
        }
    }
    else if(boolVals.size()>0 && m_type==BOOL_ATTRIBUTE) {
        if(m_loadable) {
            setBools(boolVals);
        }
        else {
            initBools(boolVals);
        }
    }
    else if(strVals.size()>0 && m_type==STRING_ATTRIBUTE) {
        if(m_loadable) {
            setStrings(strVals);
        }
        else {
            initStrings(strVals);
        }
    }
    else if(strVals.size()>0 && m_type==FILE_OPEN_ATTRIBUTE) {

        if(FILE *file = fopen(strVals[0].c_str(), "r")) {
            fclose(file);
        } else if(strVals.size()>1) {
            getAbsPathFromRev(strVals[1], strVals[0]);
        }  

        if(m_loadable) {
            setFile(strVals[0]);
        }
        else {
            initFile(strVals[0]);
        }
    }
    else if(strVals.size()>0 && m_type==FOLDER_OPEN_ATTRIBUTE) {
        if(FILE *file = fopen(strVals[0].c_str(), "r")) {
            fclose(file);
        } else if(strVals.size()>1) {
            getAbsPathFromRev(strVals[1], strVals[0]);
        }  

        if(m_loadable) {
            setFile(strVals[0]);
        }
        else {
            initFile(strVals[0]);
        }
    }
    else if(strVals.size()>0 && m_type==FILE_SAVE_ATTRIBUTE) {
        initFile(strVals[0]);
    }
}

void Attribute::save(xmlNodePtr parentNode) {
    xmlNodePtr newNode = xmlNewChild(parentNode, NULL, 
                                     BAD_CAST m_name.c_str(), NULL);
    if(m_accessibility!=OUTPUT) {
        vector<float>::iterator itFloat = m_floatValues.begin();
        for(; itFloat!=m_floatValues.end(); ++itFloat) {
            xmlNodePtr valNode = xmlNewChild(newNode, NULL, 
                                             BAD_CAST "Float", NULL);
            ostringstream oss;
            oss<<(*itFloat);
            xmlNewProp(valNode, BAD_CAST "value", BAD_CAST oss.str().c_str());
        }
        vector<int>::iterator itInt = m_intValues.begin();
        for(; itInt!=m_intValues.end(); ++itInt) {
            xmlNodePtr valNode = xmlNewChild(newNode, NULL, 
                                             BAD_CAST "Int", NULL);
            ostringstream oss;
            oss<<(*itInt);
            xmlNewProp(valNode, BAD_CAST "value", BAD_CAST oss.str().c_str());
        }
        vector<bool>::iterator itBool = m_boolValues.begin();
        for(; itBool!=m_boolValues.end(); ++itBool) {
            xmlNodePtr valNode = xmlNewChild(newNode, NULL, 
                                             BAD_CAST "Bool", NULL);
            ostringstream oss;
            oss<<(*itBool);
            xmlNewProp(valNode, BAD_CAST "value", BAD_CAST oss.str().c_str());
        }
        //if file or folder, save path relative to current rev file
        if(m_type==FILE_OPEN_ATTRIBUTE || m_type==FOLDER_OPEN_ATTRIBUTE) {
            if(m_stringValues.size()>0) {
                if(m_stringValues[0].compare("")!=0) {
                    std::string relPath;
                    getRelPathToRev(m_stringValues[0], relPath);
                    if(m_stringValues.size()<2) {
                        m_stringValues.push_back("");
                    }
                    m_stringValues[1]=relPath;
                }
            }
        }
        vector<string>::iterator itStr = m_stringValues.begin();
        for(; itStr!=m_stringValues.end(); ++itStr) {
            xmlNodePtr valNode = xmlNewChild(newNode, NULL, 
                                             BAD_CAST "String", NULL);
            if(fl_utf8test((*itStr).c_str(),(*itStr).length())>0) {
                xmlNewProp(valNode, BAD_CAST "value", 
                           xmlCharStrdup((*itStr).c_str()));
            }
        }
    }

    //save listeners
    if(m_listeners.size()>0) {
        xmlNodePtr lisNode = xmlNewChild(newNode,NULL,BAD_CAST"Listeners",NULL);
        vector<Listener*>::iterator itLis = m_listeners.begin();
        for(; itLis!=m_listeners.end(); ++itLis) {
            if((*itLis)->getListenerType().compare("GuiListener")!=0 &&
                    (*itLis)->getListenerType().compare("PathListener")!=0) {
                (*itLis)->save(lisNode);
            }
        }
    }
}

void Attribute::addListener(Listener* lis) {
    unsigned int newLisID = 0;
    while(m_listenersMap.find(newLisID)!=m_listenersMap.end()) {
        newLisID++;
    }
    m_listenersMap[newLisID] = lis;
    lis->setID(newLisID);
    lis->setAttribute(this);
    updateListenersVec();
}

void Attribute::removeListener(Listener* lis) {
    m_listenersMap.erase(lis->getID());
    updateListenersVec();
}

void Attribute::updateListenersVec() {
    m_listeners.clear();
    map<int, Listener*>::iterator itLis = m_listenersMap.begin();
    for(; itLis!=m_listenersMap.end(); ++itLis) {
        m_listeners.push_back(itLis->second);
    }
    m_module->gotAttributeListenersUpdate(this);
}

void Attribute::clearListeners() {
    m_listeners.clear();
    m_listenersMap.clear();
}

void Attribute::sendValuesToListeners(const int& lisID) {
    vector<Listener*>::iterator itLis = m_listeners.begin();
    for(; itLis!=m_listeners.end(); ++itLis) {
        if((*itLis)->getID()!=lisID) { //do not send to origin listener
            (*itLis)->update(m_floatValues, 
                             m_intValues,
                             m_boolValues,
                             m_stringValues);
        }
    }
}



void Attribute::getRelPathToRev(const string& abs, 
                                string& rel) {
    string revDir = Reveal::getInstance()->getCurrentFile();

    revDir = revDir.substr(0, revDir.find_last_of("/\\"));
    string absDir=abs.substr(0, abs.find_last_of("/\\"));
    string name=abs.substr(abs.find_last_of("/\\")+1);

    bool equal=true;
    while(equal) {
        size_t pos1,pos2;
        std::string subRev, subAbs;
        pos1 = revDir.find_first_of('/');
        subRev = revDir.substr(0, pos1);
        pos2 = absDir.find_first_of('/');
        subAbs = absDir.substr(0, pos2);
        if(subAbs.compare(subRev)!=0) {	//reached the different path
            equal=false;
        }
        else if(pos2==string::npos) {
            absDir="";
            equal=false;
        }
        else {
            revDir = revDir.substr(pos1+1);
            absDir = absDir.substr(pos2+1);
        }
    }

    //test if we need to go up (../)
    size_t pos3=revDir.find_first_of('/');
    while(pos3!=string::npos) {
        pos3 = revDir.find_first_of('/');
        if(pos3!=string::npos) {
            revDir=revDir.substr(pos3+1);
            absDir="../"+absDir;
        }
    }
    rel=absDir+"/"+name;
}


void Attribute::getAbsPathFromRev(const string& rel, 
                                  string& abs) {
    string revDir = Reveal::getInstance()->getCurrentFile();

    revDir = revDir.substr(0, revDir.find_last_of("/\\"));
    abs = revDir+"/"+rel;
}
