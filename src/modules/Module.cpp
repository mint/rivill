/***************************************************************************
 *  Module.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "Module.hpp"
#include <iostream>
#include <limits>
#include <sstream>

#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/norm.hpp>

#include "../Reveal.hpp"
#include "SpaceModule.hpp"
#include "DepthCamModule.hpp"

using namespace std;
using namespace glm;

Module::Module() {
    m_depth=0;
    m_type="Module";
    m_position=vec3(0.0, 0.0, 0.0);
    m_scale=vec3(1.0, 1.0, 1.0);
	m_modelScale = vec3(1.0, 1.0, 1.0);
    m_parent=NULL;
    m_parentMat = mat4(1.0);
    m_modelMat = mat4(1.0);
    m_rotMat = mat4(1.0);
    m_visible=true;
    m_parentVisible=true;
    m_upID=-1;
    m_visibilityMask=1;
    m_texWidth=640;
    m_texHeight=480;
}

void Module::deleteModule() {
    vector<Attribute*>::iterator itAtt = m_attributesVec.begin();
    for(; itAtt!=m_attributesVec.end(); ++itAtt) {
        (*itAtt)->clearListeners();
    }
    if(m_parent!=NULL) {
        m_parent->removeChild(this);
    }
    Reveal::getInstance()->deleteModule(this);
}

Module::~Module() { 
    vector<Attribute*>::iterator itAtt = m_attributesVec.begin();
    for(; itAtt!=m_attributesVec.end(); ++itAtt) {
        delete (*itAtt);
    }
    m_attributesVec.clear();
    m_attributesMap.clear();
}

void Module::load(xmlNodePtr node) {
    //call load on each attribute, from their names
    xmlNodePtr attsNode;
    for(attsNode=node->children; attsNode; attsNode=attsNode->next){
        if(attsNode->type == XML_ELEMENT_NODE &&
                !xmlStrcmp(attsNode->name, (const xmlChar *) "Attributes")) {
            xmlNodePtr attNode;
            for(attNode=attsNode->children; attNode; attNode=attNode->next){
                if(attNode->type == XML_ELEMENT_NODE) {
                    string nameStr((char*) attNode->name);
                    if(m_attributesMap.find(nameStr)!=m_attributesMap.end()) {
                        m_attributesMap[nameStr]->load(attNode);
                    }
                }
            }
        }
    }
}

xmlNodePtr Module::save(xmlNodePtr parentNode) {
    xmlNodePtr newNode;
    if(parentNode==NULL) {
        newNode = xmlNewNode(NULL, BAD_CAST m_type.c_str());
    }
    else {
         newNode = xmlNewChild(parentNode, NULL, 
                               BAD_CAST m_type.c_str(), NULL);
    }
    xmlNodePtr attsNode = xmlNewChild(newNode, NULL, 
                                      BAD_CAST "Attributes", NULL);
    vector<Attribute*>::iterator itAtt = m_attributesVec.begin();
    for(; itAtt!=m_attributesVec.end(); ++itAtt) {
        (*itAtt)->save(attsNode);
    }
    return newNode;
}

void Module::setName(const std::string& name) {
    m_name=name;
    Reveal::getInstance()->refreshModules();
}

void Module::setVisibleFrom(const std::string& visibleFrom) {
    //find space or proj, get visibility bit
    setVisibilityMask(Reveal::getInstance()->getVisibilityMask(visibleFrom));
}

void Module::setVisibleFromList(vector<string>& modVec) {
    map<string, Attribute*>::iterator itAtt 
        = m_attributesMap.find("visible_from");
    if(itAtt!=m_attributesMap.end()) {
        itAtt->second->editStringValuesChoices().assign(1, modVec);
    }
}

void Module::setRevealedBy(const std::string& by) {
    m_revealedBy=0;
    if(!by.compare("none") || !by.compare("0") || by.length()==0) {
        m_revealedBy=0;
    }
    else if(!by.compare("all")) {
        m_revealedBy=numeric_limits<int>::max();
    }
    else {
        //get all markers 
        string markStr=by;
        while(markStr.length()>0) {
            size_t pos=markStr.find_first_of('|');
            string subStr = markStr.substr(0,pos);
            istringstream iss(subStr);
            int mark;
            iss>>mark;
            m_revealedBy |= 1 << (mark);
            markStr = (pos==string::npos)?"":markStr.substr(pos+1);
        }
    }
    m_revealedBy=std::max(0,m_revealedBy);
}

void Module::setPosition(const float& posX, 
                         const float& posY, 
                         const float& posZ) {
    m_position = vec3(posX, posY, posZ);
    updateModelMatrix();
}

void Module::setScale(const float& scaX, 
                      const float& scaY, 
                      const float& scaZ) {
    m_scale= vec3(scaX, scaY, scaZ);
    updateModelMatrix();
}

void Module::setRotation(const float& rotX, 
                         const float& rotY, 
                         const float& rotZ) {
    m_rotEuler = vec3(rotX/180.0*M_PI, rotZ/180.0*M_PI, rotY/180.0*M_PI);
    m_rotQuat = toQuat(orientate3(m_rotEuler));
    m_rotMat = toMat4(m_rotQuat);
    vector<float> quatVec(4, 0.0);
    quatVec[0]=m_rotQuat[0];
    quatVec[1]=m_rotQuat[1];
    quatVec[2]=m_rotQuat[2];
    quatVec[3]=m_rotQuat[3];
    m_attributesMap["quaternion"]->initFloats(quatVec);
    updateModelMatrix();
}

void Module::setQuaternion(const float& rotX, 
                           const float& rotY, 
                           const float& rotZ,
                           const float& rotW) {
    m_rotQuat = quat(rotX, rotY, rotZ, rotW);
    m_rotEuler = eulerAngles(m_rotQuat);
    m_rotMat = toMat4(m_rotQuat);
    vector<float> rotVec(3, 0.0);
    rotVec[0]=m_rotEuler[0]/M_PI*180.0;
    rotVec[1]=m_rotEuler[1]/M_PI*180.0;
    rotVec[2]=m_rotEuler[2]/M_PI*180.0;
    m_attributesMap["rotation"]->initFloats(rotVec);
    updateModelMatrix();
}

void Module::setFollowMarker(const int& markID) {
	if(markID>0) { 
		//if not already registered
		//subscribe to marker update in all depthcams of all spaces
		if(m_markerListeners.size()==0) { 
			const std::vector<SpaceModule*>& spaces = Reveal::getInstance()
													->getSpaces()->getSpaces();
			for(SpaceModule* sp:spaces) {
				unsigned int nbcams = sp->getNbDepthCamModules();
				for(unsigned int i=0; i<nbcams; ++i) {
					Attribute* attr 
						= dynamic_cast<SpaceModule*>(sp)
										->getDepthCamModule(i)
											->getAttribute("marker_output");
					if(attr) {
						//create a listener for that camera
						m_markerListeners.push_back(MarkerListener(this));

						//register that listener
						attr->addListener(&m_markerListeners.back());
					}
				}
			}
		}
		//update marker number
		for(auto& mark:m_markerListeners) {
			mark.setMarkerID(markID);
		}
	}
	else { //unsubscribe all
		for(auto& mark:m_markerListeners) {
			mark.getAttribute()->removeListener(&mark);
		}
		m_markerListeners.clear();
	}
}

void Module::setModelMat(const glm::mat4& mat) { 
    m_modelMat=mat;
	m_invModelMat = inverse(m_modelMat);

    glm::vec3 sk;
    glm::vec4 pers;
    glm::decompose(m_modelMat, m_scale, m_rotQuat, m_position, sk, pers);
    m_rotMat = toMat4(m_rotQuat);
    m_rotEuler = eulerAngles(m_rotQuat);
	m_modelScale = m_scale;


    vector<float> posVec(3, 0.0);
    posVec[0]=m_position[0];posVec[1]=m_position[1];posVec[2]=m_position[2];
    m_attributesMap["position"]->initFloats(posVec);
    vector<float> rotVec(3, 0.0);
    rotVec[0]=m_rotEuler[0]/M_PI*180.0;
    rotVec[1]=m_rotEuler[1]/M_PI*180.0;
    rotVec[2]=m_rotEuler[2]/M_PI*180.0;
    m_attributesMap["rotation"]->initFloats(rotVec);
    vector<float> quatVec(4, 0.0);
    quatVec[0]=m_rotQuat[0];quatVec[1]=m_rotQuat[1];
    quatVec[2]=m_rotQuat[2];quatVec[3]=m_rotQuat[3];
    m_attributesMap["quaternion"]->initFloats(quatVec);
}

void Module::updateModelMatrix() {
    mat4 transMat = translate(m_position);
    mat4 scaleMat = scale(m_scale);
    m_modelMat = m_parentMat * transMat * m_rotMat * scaleMat;
	m_invModelMat = inverse(m_modelMat);
	for(int i=0; i<3; i++) {
		m_modelScale[i] = length(vec3(m_modelMat[i][0],
									  m_modelMat[i][1],
									  m_modelMat[i][2]));
	}

    if(m_attributesMap.find("absolute_position")!=m_attributesMap.end()) {
        vector<float> absP(3,0);
        absP[0]=m_modelMat[3][0];
        absP[1]=m_modelMat[3][1];
        absP[2]=m_modelMat[3][2];
        m_attributesMap["absolute_position"]->setFloats(absP);
    }
}

void Module::listModulesAndAttributes(const std::string& parentName, 
                                      std::map<std::string, Module*>& mods,
                                      std::map<std::string, Attribute*>& atts) {
    m_fullName = parentName+"/"+m_name;
    mods[m_fullName]=this;
    vector<Attribute*>::iterator itAtt = m_attributesVec.begin();
    for(; itAtt!=m_attributesVec.end(); ++itAtt) {
        std::string fullName = parentName+"/"+m_name+"/"+(*itAtt)->getName();
        atts[fullName] = (*itAtt);
        (*itAtt)->setFullName(fullName);
    }
}

void Module::addNameAttribute() {
    addAttribute(new Attribute("name", 
                                Attribute::STRING_ATTRIBUTE,
                                nameCallback, this, 1));
    m_attributesMap["name"]->setStrings(vector<string>(1, m_name));
}

void Module::addRemoveAttribute() {
    addAttribute(new Attribute("remove", 
                                Attribute::ACTION_ATTRIBUTE,
                                removeCallback, this));
}

void Module::addTransformationAttributes() {
    addAttribute(new Attribute("position", 
                                Attribute::FLOAT_ATTRIBUTE,
                                positionCallback, this, 3));
    addAttribute(new Attribute("scale", 
                                Attribute::FLOAT_ATTRIBUTE,
                                scaleCallback, this, 3));
    addAttribute(new Attribute("quaternion", 
                                Attribute::FLOAT_ATTRIBUTE,
                                quaternionCallback, this, 4));
    addAttribute(new Attribute("rotation", 
                                Attribute::FLOAT_ATTRIBUTE,
                                rotationCallback, this, 3));
    addAttribute(new Attribute("absolute_position", 
                                Attribute::FLOAT_ATTRIBUTE,
                                absPositionCallback, this, 3, 
                                Attribute::HIDDEN));
    m_attributesMap["scale"]->setFloats(vector<float>(3,1.0));
}

void Module::addVisibleAttribute() {
    addAttribute(new Attribute("visible", 
                                Attribute::BOOL_ATTRIBUTE,
                                visibleCallback, this, 1));
    m_attributesMap["visible"]->initBools(vector<bool>(1, true));
}

void Module::addVisibleFromAttribute() {
    addAttribute(new Attribute("visible_from", 
                                Attribute::STRING_ATTRIBUTE,
                                visibleFromCallback, this, 1));
    m_attributesMap["visible_from"]->initStrings(vector<string>(1, "all"));
}

void Module::addRevealedByAttribute() {
    addAttribute(new Attribute("revealed_by", 
                                Attribute::STRING_ATTRIBUTE,
                                revealedByCallback, this, 1));
/*
    m_revealedBys.push_back("all");
    m_revealedBys.push_back("1");
    m_revealedBys.push_back("2");
    m_revealedBys.push_back("3");
    m_revealedBy=0;
    m_attributesMap["revealed_by"]->editStringValuesChoices()
                                        .assign(1,m_revealedBys);
*/
    m_attributesMap["revealed_by"]->setStrings(vector<string>(1, "all"));
}

void Module::addFollowMarkerAttribute() {
    addAttribute(new Attribute("follow_marker", 
                                Attribute::INT_ATTRIBUTE,
                                followMarkerCallback, this, 1));

}

void Module::getQuatFromTwoVecs(glm::quat& q, glm::vec3& from, glm::vec3& to) {
    glm::vec3 start = normalize(from);
    glm::vec3 dest = normalize(to);

    float cosTheta = dot(start, dest);
    glm::vec3 rotationAxis;

    if(cosTheta < -1 + 0.001f) {
        rotationAxis = cross(glm::vec3(0.0f, 0.0f, 1.0f), start);
        if(glm::length2(rotationAxis) < 0.01) {
            rotationAxis = cross(glm::vec3(1.0f, 0.0f, 0.0f), start);
        }
        rotationAxis = normalize(rotationAxis);
        q=glm::angleAxis(glm::radians(180.0f), rotationAxis);
        return;
    }

    rotationAxis = cross(start, dest);

    float s = sqrt( (1+cosTheta)*2 );
    float invs = 1 / s;

    q[0]=s * 0.5f;
    q[1]=rotationAxis.x * invs;
    q[2]=rotationAxis.y * invs;
    q[3]=rotationAxis.z * invs;
    return;
}

//----------------------------Marker Listener---------------------------------

MarkerListener::MarkerListener(Module* mod) : Listener() {
	m_mod=mod;
	m_listenerType = "Marker";
}

MarkerListener::~MarkerListener() {

}

void MarkerListener::update(std::vector<float>& floatValues, 
							std::vector<int>& intValues,
							std::vector<bool>& boolValues,
							std::vector<std::string>& stringValues) {
	if(floatValues[0]==m_markerID && floatValues[1]>0) {
		m_mod->getAttribute("position")
			->setFloats(vector<float>{floatValues[2], 
										floatValues[3], 
										floatValues[4]});
		m_mod->getAttribute("quaternion")
			->setFloats(vector<float>{floatValues[5], 
										floatValues[6], 
										floatValues[7],
										floatValues[8]});
	}
}

