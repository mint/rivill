/***************************************************************************
 *  SpaceModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef SpaceModule_h
#define SpaceModule_h

#include "DepthGroupModule.hpp"
#include <iostream>

class Geometry;

class SpaceModule : public DepthGroupModule {
    public:
        SpaceModule();
        virtual ~SpaceModule();

        virtual void draw();

        void removeProj(ProjectorModule*);

        void removeCam(DepthCamModule*);
		inline const unsigned int getNbDepthCamModules() { 
			return m_depthCamModules.size();
		}
        inline DepthCamModule* getDepthCamModule(const unsigned int& mod) {
            if(mod<m_depthCamModules.size()) {
                return m_depthCamModules[mod];
            }
            return NULL;
        }
        DepthCamModule* getDepthCamModule(const std::string& name);

        void addSpaceModule(Module*);
        void removeDepthModule(DepthModule*);
        void updateDepthModules();
        DepthModule* getDepthModuleFromID(const unsigned int& id){ 
            if(m_depthIDsModules.find(id)!=m_depthIDsModules.end()) {
                return m_depthIDsModules[id];
            }
            return NULL;
        }
        unsigned int getNbDepthModulesFromID(){return m_depthIDsModules.size();}

        virtual void addChild(Module* mod);
        void refreshDepthCamList();
        void refreshProjList();

        const std::vector<Geometry*>& getGeoms(){return m_geoms;}
        Geometry* getGeom(const Reveal::GEOM_ID& geomStr) { 
            return m_geomsMap[geomStr];
        }
        void registerGeom(Geometry* geom);
        void unregisterGeom(Geometry* geom);
        virtual void getVisibleFromList(std::vector<std::string>& modVec, 
                                        std::map<std::string, int>& modMap);

    private:
        std::vector<DepthCamModule*> m_depthCamModules;
        std::vector<DepthShapeModule*> m_depthShapeModules;
        std::vector<DepthMeshModule*> m_depthMeshModules;
        std::vector<ProjectorModule*> m_projModules;
        std::vector<DepthModule*> m_depthModules;
        std::map<unsigned int, DepthModule*> m_depthIDsModules;
        std::map<Reveal::GEOM_ID, Geometry*> m_geomsMap;
        std::vector<Geometry*> m_geoms;
};

class SpacesModule: public GroupModule {
    public:
        SpacesModule(): GroupModule() {
            m_type="Spaces";
            m_name="spaces";
            addAttribute(new Attribute("add_space", 
                                        Attribute::ACTION_ATTRIBUTE,
                                        addSpaceCallback, this, 
                                        Attribute::LOCAL));
        }
        static void addSpaceCallback(Module* mod) {
            dynamic_cast<SpacesModule*>(mod)->addSpace();
        }
        void addSpace();
        void updateVisibleFrom(std::vector<std::string>& modVec, 
                               std::map<std::string, int>& modMap);
        virtual void load(xmlNodePtr node);
		const std::vector<SpaceModule*>& getSpaces(){return m_spaces;}
	private:
		std::vector<SpaceModule*> m_spaces;
};

#endif

