/***************************************************************************
 *  PreviewModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef PreviewModule_h
#define PreviewModule_h

#include "Module.hpp"
#include "../Reveal.hpp"

class Geometry;

class PreviewModule: public Module, public ContextHandler {
    public:
        PreviewModule();
        virtual ~PreviewModule();
        virtual void draw();

        virtual void load(xmlNodePtr node);

        static void previewMouseBtnCB(GLFWwindow* win, int btn, 
                                   int action, int mods) {
            static_cast<PreviewModule*>(glfwGetWindowUserPointer(win))
                ->mouseBtnCB(btn, action, mods);
        }
        void mouseBtnCB(const int& btn, const int& action, const int& mods);

        static void previewMousePosCB(GLFWwindow* win, int btn, 
                                      double x, double y) {
            static_cast<PreviewModule*>(glfwGetWindowUserPointer(win))
                ->mousePosCB(x, y);
        }
        void mousePosCB(const double& x, const double& y);

        static void previewMouseScrollCB(GLFWwindow* win, int btn, 
                                      double x, double y) {
            static_cast<PreviewModule*>(glfwGetWindowUserPointer(win))
                ->mouseScrollCB(x, y);
        }
        void mouseScrollCB(const double& x, const double& y);

        static void activeCallback(Module* mod, 
                                           const std::vector<bool>& vals) {
            static_cast<PreviewModule*>(mod)->setActive(vals[0]);
        }
        inline void setActive(bool active) {m_active=active;}
        static void eyePositionCallback(Module* mod, 
                                        const std::vector<float>& vals) {
            static_cast<PreviewModule*>(mod)->setEyePosition(vals[0], 
                                                             vals[1],
                                                             vals[2]);
        }
        static void lookAtCallback(Module* mod, 
                                   const std::vector<float>& vals) {
            static_cast<PreviewModule*>(mod)->setLookAt(vals[0],
                                                        vals[1],
                                                        vals[2]);
        }

        void setEyePosition(const float& x, const float& y, const float& z);
        void setLookAt(const float& x, const float& y, const float& z);
        void updateViewProjMatrix();
        inline GLFWwindow* getWindow(){return m_previewWindow;}

    private:
        void initGL();

    protected:
        bool m_active;
        GLFWwindow* m_previewWindow;
        glm::vec3 m_eyePos;
        glm::vec3 m_lookAt;
        float m_near;
        float m_far;
        glm::mat4 m_viewMat;
        glm::mat4 m_projMat;
        glm::mat4 m_viewProjMat;
        GLint m_viewProjUniform;
        bool m_initialized;
        GLint m_program;
        std::map<Reveal::REVIL_UNIFORM, GLint> m_uniforms;
        std::vector<Geometry*> m_geometries;
        enum {NONE, TURNING, ORBITING, MOVING} m_control;
        float m_mousePosX, m_mousePosY;
        float m_mouseStartPosX, m_mouseStartPosY;
};

#endif

