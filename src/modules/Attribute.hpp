/***************************************************************************
 *  Attribute.hpp
 *  Part of Revil
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef Attribute_h
#define Attribute_h

#include <string>
#include <vector>
#include <iostream>
#include <map>
#include <libxml/parser.h>
#include <libxml/tree.h>

#include "Listener.hpp"

class Module;

class Attribute {

    public:
        enum TYPE {FLOAT_ATTRIBUTE, 
                   INT_ATTRIBUTE, 
                   STRING_ATTRIBUTE, 
                   BOOL_ATTRIBUTE, 
                   FILE_OPEN_ATTRIBUTE, 
                   FOLDER_OPEN_ATTRIBUTE, 
                   FILE_SAVE_ATTRIBUTE, 
                   FOLDER_SAVE_ATTRIBUTE, 
                   ACTION_ATTRIBUTE, 
                   ACTION_INT_ATTRIBUTE,
                   ACTION_FLOAT_ATTRIBUTE,
                   ACTION_STRING_ATTRIBUTE,
                   BLOB_ATTRIBUTE};

        typedef void (ActionCallback) (Module*);
        typedef void (FileCallback) (Module*, const std::string&);
        typedef void (FloatCallback) (Module*, const std::vector<float>&);
        typedef void (IntCallback) (Module*, const std::vector<int>&);
        typedef void (StringCallback) (Module*,const std::vector<std::string>&);
        typedef void (BoolCallback) (Module*,const std::vector<bool>&);
        typedef void (BlobCallback) (Module*, const std::string&, void*);

        enum ACCESSIBILITY {HIDDEN, OUTPUT, LOCAL, GLOBAL};

    public:
        Attribute(const std::string& name, const TYPE& type, 
                  ActionCallback* callback, Module* mod, 
                  ACCESSIBILITY acc=GLOBAL, bool loadable=true): 
                                             m_name(name), 
                                             m_type(type),
                                             m_module(mod),
                                             m_nbValues(0),
                                             m_loadable(loadable),
                                             m_accessibility(acc),
                                             m_actionCallback(callback){
        }
        Attribute(const std::string& name, const TYPE& type, 
                  FileCallback* callback, Module* mod,
                  ACCESSIBILITY acc=GLOBAL, bool loadable=true): 
                                             m_name(name), 
                                             m_type(type),
                                             m_module(mod),
                                             m_nbValues(1),
                                             m_loadable(loadable),
                                             m_accessibility(acc),
                                             m_fileCallback(callback){
            m_stringValues.resize(1, "");
        }
        Attribute(const std::string& name, const TYPE& type, 
                  FloatCallback* callback, Module* mod, 
                  const unsigned int& nbValues, 
                  ACCESSIBILITY acc=GLOBAL, bool loadable=true): 
                                             m_name(name), 
                                             m_type(type),
                                             m_module(mod),
                                             m_nbValues(nbValues),
                                             m_loadable(loadable),
                                             m_accessibility(acc),
                                             m_floatCallback(callback){
            m_floatValues.resize(nbValues,0);
        }
        Attribute(const std::string& name, const TYPE& type, 
                  BoolCallback* callback, Module* mod, 
                  const unsigned int& nbValues, 
                  ACCESSIBILITY acc=GLOBAL, bool loadable=true): 
                                             m_name(name), 
                                             m_type(type),
                                             m_module(mod),
                                             m_nbValues(nbValues),
                                             m_loadable(loadable),
                                             m_accessibility(acc),
                                             m_boolCallback(callback){
            m_boolValues.resize(nbValues,false);
        }
        Attribute(const std::string& name, const TYPE& type, 
                  IntCallback* callback, Module* mod,
                  const unsigned int& nbValues, 
                  ACCESSIBILITY acc=GLOBAL, bool loadable=true): 
                                             m_name(name), 
                                             m_type(type),
                                             m_module(mod),
                                             m_nbValues(nbValues),
                                             m_loadable(loadable),
                                             m_accessibility(acc),
                                             m_intCallback(callback){
            m_intValues.resize(nbValues,0);
        }
        Attribute(const std::string& name, const TYPE& type, 
                  StringCallback* callback, Module* mod,
                  const unsigned int& nbValues, 
                  ACCESSIBILITY acc=GLOBAL, bool loadable=true): 
                                             m_name(name), 
                                             m_type(type),
                                             m_module(mod),
                                             m_nbValues(nbValues),
                                             m_loadable(loadable),
                                             m_accessibility(acc),
                                             m_stringCallback(callback){ 
            m_stringValues.resize(nbValues,"");
        }

        ~Attribute();

        inline const std::string& getName(){return m_name;}
        inline const TYPE& getType(){return m_type;}
        inline void setFullName(const std::string& fn){m_fullName=fn;}
        inline const std::string& getFullName(){return m_fullName;}

        void load(xmlNodePtr node);
        void save(xmlNodePtr parentNode);

        inline const unsigned int& getNbValues(){return m_nbValues;}

        void setStrings(const std::vector<std::string>& strings, 
                        const int& lisID=-1) {
            m_stringValues.assign(strings.begin(), 
                                  strings.begin()+m_stringValues.size());
            m_stringCallback(m_module, m_stringValues);
            sendValuesToListeners(lisID);
        }
        void setStringsIfChanged(const std::vector<std::string>& strings) {
            if(strings!=m_stringValues) {
                setStrings(strings);
            }
        }
        void initStrings(const std::vector<std::string>& strings) {
            m_stringValues.assign(strings.begin(), 
                                  strings.begin()+m_stringValues.size());
            sendValuesToListeners(-1);
        }
        const std::string& getString(const unsigned int& s) { 
            return m_stringValues[s];
        }

        void setFloats(const std::vector<float>& floats, 
                       const int& lisID=-1) {
            m_floatValues.assign(floats.begin(), 
                                 floats.begin()+m_floatValues.size());
            m_floatCallback(m_module, m_floatValues);
            sendValuesToListeners(lisID);
        }
        void setFloatsIfChanged(const std::vector<float>& floats) {
            if(floats!=m_floatValues) {
                setFloats(floats);
            }
        }
        void setFloatsIfChangedOrCount(const std::vector<float>& floats) {
			m_changeCounter++;
			if(m_changeCounter>m_count || floats!=m_floatValues) {
                setFloats(floats);
				m_changeCounter=0;
            }
        }
        void initFloats(const std::vector<float>& floats) {
            m_floatValues.assign(floats.begin(), 
                                 floats.begin()+m_floatValues.size());
            sendValuesToListeners(-1);
        }
        const float& getFloat(const unsigned int& f) {return m_floatValues[f];}
        const std::vector<float>& getFloats(){return m_floatValues;}

        void setInts(const std::vector<int>& ints,
                     const int& lisID=-1) {
            m_intValues.assign(ints.begin(), 
                               ints.begin()+m_intValues.size());
            m_intCallback(m_module, m_intValues);
            sendValuesToListeners(lisID);
        }
        void initInts(const std::vector<int>& ints) {
            m_intValues.assign(ints.begin(), 
                               ints.begin()+m_intValues.size());
            sendValuesToListeners(-1);
        }
        const int& getInt(const unsigned int& i) {return m_intValues[i];}

        void setBools(const std::vector<bool>& bools,
                      const int& lisID=-1) {
            m_boolValues.assign(bools.begin(), 
                                bools.begin()+m_boolValues.size());
            m_boolCallback(m_module, m_boolValues);
            sendValuesToListeners(lisID);
        }
        void initBools(const std::vector<bool>& bools) {
            m_boolValues.assign(bools.begin(), 
                                bools.begin()+m_boolValues.size());
            sendValuesToListeners(-1);
        }
        bool getBool(const unsigned int& i) {return m_boolValues[i];}

        void setFile(const std::string& f,
                     const int& lisID=-1) {
            m_stringValues[0]=f;
            m_fileCallback(m_module, f);
            sendValuesToListeners(lisID);
        }
        void initFile(const std::string& f) {m_stringValues[0]=f;}
        const std::string& getFile() {return m_stringValues[0];}

        void setAction() { m_actionCallback(m_module);}

        inline const std::vector<std::string>& getValueLabels() {
            return m_valuesLabels;
        }
        inline std::vector<std::string>& editValueLabels() {
            return m_valuesLabels;
        }

		inline void setChangeCount(const int& count) {m_count=count;}
        inline const std::vector<float>& getValuesMin() {return m_valuesMin;}
        inline std::vector<float>& editValuesMin() {return m_valuesMin;}
        inline const std::vector<float>& getValuesMax() {return m_valuesMax;}
        inline std::vector<float>& editValuesMax() {return m_valuesMax;}

        inline std::vector<std::vector<std::string> >& 
                                        editStringValuesChoices() {
            return m_stringValuesChoices;
        }
        inline const std::vector<std::vector<std::string> >& 
                                        getStringValuesChoices() {
            return m_stringValuesChoices;
        }

        inline std::vector<Listener*>& editListeners(){return m_listeners;}
        void clearListeners();
        void addListener(Listener*);
        void removeListener(Listener* lis);
        void updateListenersVec();
        void sendValuesToListeners(const int& lisID=-1);

        ACCESSIBILITY getAccessibility(){return m_accessibility;}
        void setAccessibility(ACCESSIBILITY acc){m_accessibility=acc;}
        void setLoadable(bool loadable){m_loadable=loadable;}

    private: 
        void getRelPathToRev(const std::string& abs, 
                             std::string& rel);
        void getAbsPathFromRev(const std::string& rel, 
                               std::string& abs);
    protected:
        std::string m_name; 
        std::string m_fullName;
        TYPE m_type;
        Module* m_module;
        unsigned int m_nbValues;
        bool m_loadable;
        ACCESSIBILITY m_accessibility;
        std::vector<float> m_floatValues;
        std::vector<int> m_intValues;
        std::vector<bool> m_boolValues;
        std::vector<std::string> m_stringValues;
		int m_changeCounter;
		int m_count;

        std::vector<std::string> m_valuesLabels;

        std::vector<std::vector<std::string> > m_stringValuesChoices;
        std::vector<float> m_valuesMin;
        std::vector<float> m_valuesMax;

        ActionCallback* m_actionCallback;
        FileCallback* m_fileCallback;
        FloatCallback* m_floatCallback;
        IntCallback* m_intCallback;
        BoolCallback* m_boolCallback;
        StringCallback* m_stringCallback;

        std::vector<Listener*> m_listeners;
        std::map<int, Listener*> m_listenersMap;
};

class AttributeValue {
    public: 
        enum {ACTION, FLOAT_VALUE, INT_VALUE, STRING_VALUE} m_type;




    private:
        std::string m_label;

        std::vector<std::string >m_values;

        float m_floatValue;
        int m_intValue;
        std::string m_strValue;
};


#endif

