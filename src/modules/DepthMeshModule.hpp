/***************************************************************************
 *  DepthMeshModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef DepthMeshModule_h
#define DepthMeshModule_h

#include "DepthModule.hpp"

class DepthMeshModule : public DepthModule {
    public:
        DepthMeshModule();
        virtual ~DepthMeshModule();
        virtual void updateModelMatrix();
        virtual void setSpace(SpaceModule* space);

        static void addVertexCallback(Module* mod,
                                   const std::vector<float>& vals) {
            dynamic_cast<DepthMeshModule*>(mod)->addVertex(vals[0], 
                                                           vals[1],
                                                           vals[2]);
        }
        void addVertex(const float& z, const float& y, const float& x);

        static void clearVerticesCallback(Module* mod) {
            dynamic_cast<DepthMeshModule*>(mod)->clearVertices();
        }
        void clearVertices();

    private:
        Geometry* m_meshGeom;
};


#endif

