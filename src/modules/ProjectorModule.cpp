/***************************************************************************
 *  ProjectorModule.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 130, Boston, MA 02111-1307, USA.
 */

/*////////////
  Calibration heavily relies on code from :
https://github.com/Kj1/ofxProjectorKinectCalibration
https://github.com/kylemcdonald/ofxCv
*/////////////////////

#include "ProjectorModule.hpp"

#include <iostream>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include "../Reveal.hpp"
#include "DepthCamModule.hpp"
#include "SpaceModule.hpp"
#include "RevealedModule.hpp"

#include "shaders.h.in"
#include "../audio/AudioManager.hpp"

using namespace std;
using namespace glm;
using namespace cv;

ProjectorModule::ProjectorModule(): Module() {
    m_type="Projector";
    m_name="projector";

    m_space=NULL;
    m_width=640;
    m_height=480;
    m_posX=0;
    m_posY=0;
    m_processOutput=true;
    m_audioProcessType=0;
    m_active=true;
    m_postFilter=0;
    m_transparentBackground=false;

    //create window
    m_projWindow = glfwCreateWindow(m_width, m_height,
            "Rivill - Projector", NULL, NULL);
    if(!m_projWindow) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    m_rttWidth=1920;
    m_rttHeight=1080;
    glfwSetWindowUserPointer(m_projWindow, this);
    glfwSetWindowSizeCallback(m_projWindow, windowSizeCallback);
    glfwSetWindowPosCallback(m_projWindow, windowPositionCallback);
    glfwSetFramebufferSizeCallback(m_projWindow, framebufferSizeCallback);
    glfwSetInputMode(m_projWindow, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    addNameAttribute();
    addRemoveAttribute();
    addAttribute(new Attribute("active",
                Attribute::BOOL_ATTRIBUTE,
                activeCallback, this, 1,
                Attribute::LOCAL));
    addAttribute(new Attribute("window_dimensions",
                Attribute::INT_ATTRIBUTE,
                windowDimensionsCallback, this, 4,
                Attribute::LOCAL));
    addAttribute(new Attribute("fullscreen_monitor",
                Attribute::STRING_ATTRIBUTE,
                fullscreenMonitorCallback, this, 1,
                Attribute::LOCAL));
    addAttribute(new Attribute("view_matrix",
                Attribute::FLOAT_ATTRIBUTE,
                viewMatrixCallback, this, 16,
                Attribute::HIDDEN));
    addAttribute(new Attribute("projection_matrix",
                Attribute::FLOAT_ATTRIBUTE,
                projectionMatrixCallback, this, 16,
                Attribute::HIDDEN));

    addAttribute(new Attribute("mirrored",
                Attribute::BOOL_ATTRIBUTE,
                mirroredCallback, this, 1,
                Attribute::LOCAL));

    addTransformationAttributes();
    m_attributesMap["scale"]->setAccessibility(Attribute::HIDDEN);
    vector<float> posVec(3,0);
    posVec[2]=-700;
    m_attributesMap["position"]->setFloats(posVec);

    addAttribute(new Attribute("attached_to_camera",
                Attribute::STRING_ATTRIBUTE,
                attachToCamCallback, this, 1,
                Attribute::LOCAL));
    addAttribute(new Attribute("calibrate_with_camera",
                Attribute::ACTION_STRING_ATTRIBUTE,
                calibrateCallback, this, 1,
                Attribute::LOCAL,
                false));
    addAttribute(new Attribute("output_revealed",
                Attribute::BOOL_ATTRIBUTE,
                outputRevealedCallback, this, 1,
                Attribute::LOCAL));
    addAttribute(new Attribute("post_filter",
                Attribute::INT_ATTRIBUTE,
                postFilterCallback, this, 1,
                Attribute::LOCAL));
    addAttribute(new Attribute("transparent_window",
                Attribute::BOOL_ATTRIBUTE,
                transparentWindowCallback, this, 1,
                Attribute::LOCAL));
    addAttribute(new Attribute("audio_process", 
                Attribute::STRING_ATTRIBUTE,
                audioProcessTypeCallback, this, 1,
                Attribute::LOCAL));
    m_audioProcessTypes.push_back("none");
    m_audioProcessTypes.push_back("additiveSynthesis");
    m_audioProcessTypes.push_back("pitchedAdditiveSynthesis");
    m_attributesMap["audio_process"]
        ->editStringValuesChoices().assign(1, m_audioProcessTypes);
    m_attributesMap["audio_process"]
        ->setStrings(vector<string>(1,m_audioProcessTypes[0]));

    //initialize attributes
    vector<string> labels;
    labels.push_back("none");
    m_attributesMap["attached_to_camera"]
        ->editStringValuesChoices().assign(1, labels);
    m_attributesMap["attached_to_camera"]
        ->initStrings(vector<string>(1, labels[0]));
    m_attributesMap["calibrate_with_camera"]
        ->editStringValuesChoices().assign(1, labels);
    m_attributesMap["calibrate_with_camera"]
        ->initStrings(vector<string>(1, labels[0]));
    refreshMonitors();
    m_attributesMap["fullscreen_monitor"]
        ->initStrings(vector<string>(1, "windowed"));
    m_attachedToCam=NULL;
    m_attributesMap["output_revealed"]->initBools(vector<bool>(1, true));
    m_attributesMap["post_filter"]->initInts(vector<int>(1, 0));
    m_attributesMap["active"]->initBools(vector<bool>(1, true));

    m_mirrored=false;

    //initialize matrices
    m_viewMat = lookAt(vec3(0.0, 0.0, 0.0),
            vec3(0.0, 0.0, 1.0),
            vec3(0.0, 1.0, 0.0));
    mat4 glCoordsMat(-1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);
    m_viewMat=glCoordsMat*m_viewMat;
    m_attributesMap["view_matrix"]->setFloats(vector<float>(
                value_ptr(m_viewMat),
                value_ptr(m_viewMat)+16));
    m_projMat = perspective(45.0, 4.0/3.0, 100.0, 10000.0);
    m_attributesMap["projection_matrix"]->setFloats(vector<float>(
                value_ptr(m_projMat),
                value_ptr(m_projMat)+16));

    vector<int> dims(4,0);
    dims[0]=m_posX;
    dims[1]=m_posY;
    dims[2]=m_width;
    dims[3]=m_height;
    m_attributesMap["window_dimensions"]->setInts(dims);

    //calibration
    m_calibrated=false;
    m_patternSquareWidth = 40;
    m_patternPosition = Size(100, 200);
    m_patternSize = Size(6,4);
    m_patternBrightness = 100;
    m_patternPosRange = Size(m_width, m_height);
    m_patternPosOffset = Size(0,0);


    //audio
    m_nbTracks=200;
    m_maxAudioValue = (std::numeric_limits<int>::max())/(m_width * m_height);
    m_audioMixSize = AudioManager::getInstance()->getMixSize();
    m_audioBufSize = AudioManager::getInstance()->getBufferSize();
    m_imgBufSize = m_audioBufSize+3;
    m_audioBuffer = new float[m_audioBufSize];
    memset(&m_audioBuffer[0], 0, m_audioBufSize*sizeof(float));
    m_audioNextBuf=1;


    Reveal::getInstance()->addContextHandler(this);

    m_initialized=false;
}

ProjectorModule::~ProjectorModule() {}

void ProjectorModule::draw() {
    if(!m_active) {
        return;
    }

    glfwMakeContextCurrent(m_projWindow);

    if(!m_initialized) {
        initGL();
        m_initialized=true;
    }

    //slice to texture
    glBindFramebuffer(GL_FRAMEBUFFER, m_sliceFBO);
    glViewport(0, 0, m_rttWidth, m_rttHeight);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glDisable(GL_CULL_FACE);
    glDisable(GL_BLEND);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(m_programs[Reveal::SLICEPROG]);
    glUniform1f(m_uniforms[Reveal::SLICEPROG][Reveal::MIRRORED],
            float(m_mirrored));
    glUniformMatrix4fv(m_uniforms[Reveal::SLICEPROG][Reveal::VIEWPROJMAT], 1,
            GL_FALSE, value_ptr(m_viewProjMat));
    glUniformMatrix4fv(m_uniforms[Reveal::SLICEPROG][Reveal::VIEWMAT], 1,
            GL_FALSE, value_ptr(m_transViewMat));
    const vector<Geometry*>& depthGeoms = m_space->getGeoms();

    vector<Geometry*>::const_iterator itDGeom=depthGeoms.begin();
    for(; itDGeom!=depthGeoms.end(); ++itDGeom) {
        (*itDGeom)->draw(m_contextHandlerID,
                Reveal::SLICEPROG,
                m_uniforms[Reveal::SLICEPROG],
                m_visibilityMask);
    }

    //select to texture for 3D models only
    glBindFramebuffer(GL_FRAMEBUFFER, m_selectFBO);
    glViewport(0, 0, m_rttWidth, m_rttHeight);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_ALWAYS);
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
    glBlendFuncSeparate(GL_ONE, GL_ONE, GL_ONE, GL_ONE);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(m_programs[Reveal::SELECTPROG]);
    glUniform1f(m_uniforms[Reveal::SELECTPROG][Reveal::MIRRORED],
            float(m_mirrored));
    glUniformMatrix4fv(m_uniforms[Reveal::SELECTPROG][Reveal::VIEWPROJMAT], 1,
            GL_FALSE, value_ptr(m_viewProjMat));
    glUniformMatrix4fv(m_uniforms[Reveal::SELECTPROG][Reveal::VIEWMAT], 1,
            GL_FALSE, value_ptr(m_transViewMat));
    glUniform1f(m_uniforms[Reveal::SELECTPROG][Reveal::VIEWPORTWIDTH],
            m_rttWidth);
    glUniform1f(m_uniforms[Reveal::SELECTPROG][Reveal::VIEWPORTHEIGHT],
            m_rttHeight);
    glUniform1i(m_uniforms[Reveal::SELECTPROG][Reveal::SLICETEX], 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_sliceRttTex);
    const vector<Geometry*>& sceneGeoms = Reveal::getInstance()->getGeoms();
    for(auto& geom : sceneGeoms) {
        if(geom->getIsModel()) {
            geom->draw(m_contextHandlerID,
                    Reveal::SELECTPROG,
                    m_uniforms[Reveal::SELECTPROG],
                    m_visibilityMask);
        }
    }


    //RENDER TO SCREEN
    if(m_postFilter>0) {
        glBindFramebuffer(GL_FRAMEBUFFER, m_renderFBO);
        glViewport(0, 0, m_rttWidth, m_rttHeight);
    }
    else {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, m_width, m_height);
    }
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_ALWAYS);
    glEnable(GL_CULL_FACE);
    if(m_mirrored) {
        glCullFace(GL_FRONT);
    }
    else {
        glCullFace(GL_BACK);
    }
    glEnable(GL_BLEND);
    glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);
    glBlendFuncSeparate(GL_ONE, GL_ONE, GL_ONE, GL_ONE);
    glClearColor(0.0, 0.0, 0.0, !m_transparentBackground);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glUseProgram(m_programs[Reveal::RENDERPROG]);
    glUniform1f(m_uniforms[Reveal::RENDERPROG][Reveal::MIRRORED],
            float(m_mirrored));
    glUniformMatrix4fv(m_uniforms[Reveal::RENDERPROG][Reveal::VIEWPROJMAT], 1,
            GL_FALSE, value_ptr(m_viewProjMat));
    glUniformMatrix4fv(m_uniforms[Reveal::RENDERPROG][Reveal::VIEWMAT], 1,
            GL_FALSE, value_ptr(m_transViewMat));
    if(m_postFilter>0) {
        glUniform1f(m_uniforms[Reveal::RENDERPROG][Reveal::VIEWPORTWIDTH],
                m_rttWidth);
        glUniform1f(m_uniforms[Reveal::RENDERPROG][Reveal::VIEWPORTHEIGHT],
                m_rttHeight);
    }
    else {
        glUniform1f(m_uniforms[Reveal::RENDERPROG][Reveal::VIEWPORTWIDTH],
                m_width);
        glUniform1f(m_uniforms[Reveal::RENDERPROG][Reveal::VIEWPORTHEIGHT],
                m_height);
    }
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::SLICETEX], 0);
    glBindTexture(GL_TEXTURE_2D, m_sliceRttTex);
    glActiveTexture(GL_TEXTURE1);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::SELECTTEX], 1);
    glBindTexture(GL_TEXTURE_2D, m_selectTex);
#ifdef GL43
    //glActiveTexture(GL_TEXTURE7);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::OUTPUTTEX], 0);
    glBindImageTexture(0, m_outputTex, 0, GL_FALSE, 0,
            GL_READ_WRITE, GL_R32UI);


    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::OUTSHAPESIZE], 
            m_outShapeSize);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::OUTDEPTHSIZE], 
            m_outDepthSize);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::OUTNBDEPTH], 
            m_outNbDepth);

    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::PROCESSAUDIO], 
            m_audioProcessType);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::AUDIOTEX], 1);
    glBindImageTexture(1, m_audioTex, 0, GL_FALSE, 0,
            GL_READ_WRITE, GL_R32I);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::AUDIORATE], 
            AudioManager::getInstance()->getRate());
    glUniform1f(m_uniforms[Reveal::RENDERPROG][Reveal::MAXAUDIOVALUE], 
            m_maxAudioValue);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::AUDIOBUFSIZE], 
            m_audioBufSize);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::AUDIOMIXSIZE], 
            m_audioMixSize);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::AUDIONEXTBUF], 
            m_audioNextBuf);
    glUniform1i(m_uniforms[Reveal::RENDERPROG][Reveal::AUDIONBTRACKS], 
            m_nbTracks);
#endif

    //Draw everything
    for(auto& geom : sceneGeoms) {
        geom->draw(m_contextHandlerID,
                Reveal::RENDERPROG,
                m_uniforms[Reveal::RENDERPROG],
                m_visibilityMask);
    }

    //POST PROCESSING
    if(m_postFilter>0) {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, m_width, m_height);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glDisable(GL_BLEND);
        glClearColor(0.0, 0.0, 0.0, !m_transparentBackground);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glUseProgram(m_programs[Reveal::POSTPROG]);
        glActiveTexture(GL_TEXTURE0);
        glUniform1i(m_uniforms[Reveal::POSTPROG][Reveal::RENDERTEX], 0);
        glBindTexture(GL_TEXTURE_2D, m_renderTex);
        glUniform1f(m_uniforms[Reveal::POSTPROG][Reveal::VIEWPORTWIDTH],
                m_width);
        glUniform1f(m_uniforms[Reveal::POSTPROG][Reveal::VIEWPORTHEIGHT],
                m_height);
        glUniform1f(m_uniforms[Reveal::POSTPROG][Reveal::POSTFILTER],
                m_postFilter);
        m_calibGeom->drawDirect(m_contextHandlerID);
    }

    //SWAP
    glfwSwapInterval(0);
    glfwSwapBuffers(m_projWindow);

    //PROCESS OUTPUT
#ifdef GL43
    if(m_processOutput) {
        processOutput();
    }
    if(m_audioProcessType>0) {
        processAudio();
    }
#endif
}

void ProjectorModule::initGL() {
#ifdef OSX
    glewExperimental = GL_TRUE;
#endif
    glewInit();


    //POSTPROCESSING
    {

#ifdef GL43
        m_programs[Reveal::POSTPROG]
            = Reveal::getInstance()->createProgram(post43VS.c_str(), 
                    post43FS.c_str());
#else
        m_programs[Reveal::POSTPROG]
            = Reveal::getInstance()->createProgram(post30VS.c_str(), 
                    post30FS.c_str());
#endif
        m_uniforms[Reveal::POSTPROG]=map<Reveal::REVIL_UNIFORM, GLint>();
        m_uniforms[Reveal::POSTPROG][Reveal::VIEWPORTWIDTH]
            = glGetUniformLocation(m_programs[Reveal::POSTPROG],"viewportWidth");
        m_uniforms[Reveal::POSTPROG][Reveal::VIEWPORTHEIGHT]
            = glGetUniformLocation(m_programs[Reveal::POSTPROG],"viewportHeight");
        m_uniforms[Reveal::POSTPROG][Reveal::POSTFILTER]
            = glGetUniformLocation(m_programs[Reveal::POSTPROG],"postFilter");
        m_uniforms[Reveal::POSTPROG][Reveal::RENDERTEX]
            = glGetUniformLocation(m_programs[Reveal::POSTPROG],"renderTex");
    }

    //RENDER
    {
#ifdef GL43
        m_programs[Reveal::RENDERPROG]
            = Reveal::getInstance()->createProgram(render43VS.c_str(), 
                    render43FS.c_str());
#else
        m_programs[Reveal::RENDERPROG]
            = Reveal::getInstance()->createProgram(render30VS.c_str(), 
                    render30FS.c_str());
#endif

        m_uniforms[Reveal::RENDERPROG] = map<Reveal::REVIL_UNIFORM, GLint>();
        m_uniforms[Reveal::RENDERPROG][Reveal::MIRRORED]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "mirrored");
        m_uniforms[Reveal::RENDERPROG][Reveal::VIEWPROJMAT]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "viewProjMat");
        m_uniforms[Reveal::RENDERPROG][Reveal::VIEWMAT]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "viewMat");
        m_uniforms[Reveal::RENDERPROG][Reveal::MODELMAT]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "modelMat");
        m_uniforms[Reveal::RENDERPROG][Reveal::INVMODELMAT]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "invModelMat");
        m_uniforms[Reveal::RENDERPROG][Reveal::MODELSCALE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "modelScale");
        m_uniforms[Reveal::RENDERPROG][Reveal::VIEWPORTWIDTH]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "viewportWidth");
        m_uniforms[Reveal::RENDERPROG][Reveal::VIEWPORTHEIGHT]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG],"viewportHeight");
        m_uniforms[Reveal::RENDERPROG][Reveal::SLICETEX]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "sliceTex");
        m_uniforms[Reveal::RENDERPROG][Reveal::SELECTTEX]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "selectTex");
        m_uniforms[Reveal::RENDERPROG][Reveal::INSIDETEX]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "insideTex");
        m_uniforms[Reveal::RENDERPROG][Reveal::COORDSTEX]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "coordsTex");
        m_uniforms[Reveal::RENDERPROG][Reveal::TEXGRAY]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "texGray");
        m_uniforms[Reveal::RENDERPROG][Reveal::REACTTEX]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "reactTex");
        m_uniforms[Reveal::RENDERPROG][Reveal::SURFDISTEX]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "surfDistTex");

        m_uniforms[Reveal::RENDERPROG][Reveal::SHAPEID]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "shapeID");
        m_uniforms[Reveal::RENDERPROG][Reveal::SHAPEIDBIT]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "shapeIDBit");
        m_uniforms[Reveal::RENDERPROG][Reveal::SHAPEGEOM]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "shapeGeom");
        m_uniforms[Reveal::RENDERPROG][Reveal::SHAPECOLOR]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "shapeColor");
        m_uniforms[Reveal::RENDERPROG][Reveal::SUBSHAPEID]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "subShapeID");
        m_uniforms[Reveal::RENDERPROG][Reveal::SUBSHAPESNB]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "subShapesNb");
        m_uniforms[Reveal::RENDERPROG][Reveal::SUBSHAPEINVMAT]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG],"subShapeInvMat");
        m_uniforms[Reveal::RENDERPROG][Reveal::REVEALEDBY]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "revealedBy");

        m_uniforms[Reveal::RENDERPROG][Reveal::SURFACE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "surface");
        m_uniforms[Reveal::RENDERPROG][Reveal::SURFACECOLOR]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "surfaceColor");
        m_uniforms[Reveal::RENDERPROG][Reveal::SURFACETHICKNESS]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "thickness");
        m_uniforms[Reveal::RENDERPROG][Reveal::SURFACETEX]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "surfaceTex");

        m_uniforms[Reveal::RENDERPROG][Reveal::INSIDEVISIBLE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "insideVisible");
        m_uniforms[Reveal::RENDERPROG][Reveal::INSIDESTRUCT]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "insideStructure");
        m_uniforms[Reveal::RENDERPROG][Reveal::STRUCTRATIO]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "structureRatio");

        m_uniforms[Reveal::RENDERPROG][Reveal::GRADIENTALPHA]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "gradientAlpha");
        m_uniforms[Reveal::RENDERPROG][Reveal::GRADIENTTYPE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "gradientType");
        m_uniforms[Reveal::RENDERPROG][Reveal::GRADIENTSTEPS]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "gradientSteps");
        m_uniforms[Reveal::RENDERPROG][Reveal::GRADIENTCURVERATIO]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "gradientCurveRatio");
        m_uniforms[Reveal::RENDERPROG][Reveal::GRADIENTTEXTURE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "gradientTex");

        m_uniforms[Reveal::RENDERPROG][Reveal::DENSITYALPHA]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "densityAlpha");
        m_uniforms[Reveal::RENDERPROG][Reveal::DENSITYTYPE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "densityType");
        m_uniforms[Reveal::RENDERPROG][Reveal::DENSITYRATIO]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "densityRatio");
        m_uniforms[Reveal::RENDERPROG][Reveal::DENSITYSIZE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "densitySize");
        m_uniforms[Reveal::RENDERPROG][Reveal::DENSITYCURVERATIO]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "densityCurveRatio");

        m_uniforms[Reveal::RENDERPROG][Reveal::TEXALPHA]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "texAlpha");
        m_uniforms[Reveal::RENDERPROG][Reveal::TEXOFFSET]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "texOffset");
        m_uniforms[Reveal::RENDERPROG][Reveal::TEXSCALE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "texScale");
        m_uniforms[Reveal::RENDERPROG][Reveal::REACTIVITY]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "reactivity");


        m_uniforms[Reveal::RENDERPROG][Reveal::BBOXMIN]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "bboxMin");
        m_uniforms[Reveal::RENDERPROG][Reveal::BBOXSIZE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "bboxSize");
        m_uniforms[Reveal::RENDERPROG][Reveal::BBOXLOCALSIZE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "bboxLocalSize");
        m_uniforms[Reveal::RENDERPROG][Reveal::BBOXROT]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "bboxRot");
#ifdef GL43
        //output texture
        m_outputImg = new unsigned int[m_outputImgSize*m_outputImgSize];
        m_outputImgInit = new unsigned int[m_outputImgSize*m_outputImgSize];
        memset(&m_outputImgInit[0], 0, m_outputImgSize*m_outputImgSize*4);
        glGenTextures(1, &m_outputTex);
        glBindTexture(GL_TEXTURE_2D, m_outputTex);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R32UI,
                m_outputImgSize, m_outputImgSize, 0,
                GL_RED_INTEGER, GL_UNSIGNED_INT, m_outputImgInit);

        //Audio texture
        m_audioImg = new int[m_imgBufSize*m_nbTracks];
        memset(&m_audioImg[0], 0, m_imgBufSize*m_nbTracks*sizeof(int));
        m_audioImgInit = new int[3*m_nbTracks];
        memset(&m_audioImgInit[0], 0, 3*m_nbTracks*sizeof(int));
        glGenTextures(1, &m_audioTex);
        glBindTexture(GL_TEXTURE_2D, m_audioTex);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I,
                m_imgBufSize, m_nbTracks, 0,
                GL_RED_INTEGER, GL_INT, m_audioImg);

        m_uniforms[Reveal::RENDERPROG][Reveal::PROCESSAUDIO]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "processAudio");
        m_uniforms[Reveal::RENDERPROG][Reveal::AUDIOTEX]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "audioTex");
        m_uniforms[Reveal::RENDERPROG][Reveal::AUDIOMIXSIZE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "audioMixSize");
        m_uniforms[Reveal::RENDERPROG][Reveal::AUDIOBUFSIZE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "audioBufSize");
        m_uniforms[Reveal::RENDERPROG][Reveal::MAXAUDIOVALUE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "maxAudioValue");
        m_uniforms[Reveal::RENDERPROG][Reveal::AUDIORATE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "audioRate");
        m_uniforms[Reveal::RENDERPROG][Reveal::AUDIONEXTBUF]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "audioNextBuf");
        m_uniforms[Reveal::RENDERPROG][Reveal::AUDIONBTRACKS]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "audioNbTracks");

        m_uniforms[Reveal::RENDERPROG][Reveal::OUTPUTTEX]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "outputTex");
        m_uniforms[Reveal::RENDERPROG][Reveal::REVSIZE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "revSize");
        m_uniforms[Reveal::RENDERPROG][Reveal::REVSURFACE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "revSurface");
        m_uniforms[Reveal::RENDERPROG][Reveal::REVINSIDE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "revInside");
        m_uniforms[Reveal::RENDERPROG][Reveal::REVCENTER]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "revCenter");
        m_uniforms[Reveal::RENDERPROG][Reveal::REVCURSOR]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "revCursor");
        m_uniforms[Reveal::RENDERPROG][Reveal::REVCOLOR]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "revColor");
        m_uniforms[Reveal::RENDERPROG][Reveal::REVHISTO]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "revHisto");
        m_uniforms[Reveal::RENDERPROG][Reveal::REVVOXELS]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "revVoxels");
        m_uniforms[Reveal::RENDERPROG][Reveal::OUTSHAPESIZE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "shapeOutSize");
        m_uniforms[Reveal::RENDERPROG][Reveal::OUTHISTOSIZE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "histoOutSize");
        m_uniforms[Reveal::RENDERPROG][Reveal::OUTVOXELSIZE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "voxelOutSize");
        m_uniforms[Reveal::RENDERPROG][Reveal::OUTDEPTHSIZE]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "depthOutSize");
        m_uniforms[Reveal::RENDERPROG][Reveal::OUTNBDEPTH]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "depthOutNb");
        m_uniforms[Reveal::RENDERPROG][Reveal::BRECT]
            = glGetUniformLocation(m_programs[Reveal::RENDERPROG], "boundingRect");
        createRTT(m_renderTex, m_renderFBO, m_rttWidth, m_rttHeight, false);
#endif
    }

    {
        //select prog
#ifdef GL43
        m_programs[Reveal::SELECTPROG]
            = Reveal::getInstance()->createProgram(select43VS.c_str(), 
                    select43FS.c_str());
#else
        m_programs[Reveal::SELECTPROG]
            = Reveal::getInstance()->createProgram(select30VS.c_str(), 
                    select30FS.c_str());
#endif
        m_uniforms[Reveal::SELECTPROG]=map<Reveal::REVIL_UNIFORM, GLint>();
        m_uniforms[Reveal::SELECTPROG][Reveal::MIRRORED]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG], "mirrored");
        m_uniforms[Reveal::SELECTPROG][Reveal::VIEWPROJMAT]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG], "viewProjMat");
        m_uniforms[Reveal::SELECTPROG][Reveal::VIEWMAT]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG], "viewMat");
        m_uniforms[Reveal::SELECTPROG][Reveal::MODELMAT]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG], "modelMat");
        m_uniforms[Reveal::SELECTPROG][Reveal::VIEWPORTWIDTH]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG], "viewportWidth");
        m_uniforms[Reveal::SELECTPROG][Reveal::VIEWPORTHEIGHT]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG],"viewportHeight");
        m_uniforms[Reveal::SELECTPROG][Reveal::SLICETEX]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG], "sliceTex");
        m_uniforms[Reveal::SELECTPROG][Reveal::SHAPEID]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG], "shapeID");
        m_uniforms[Reveal::SELECTPROG][Reveal::SHAPEIDBIT]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG], "shapeIDBit");
        m_uniforms[Reveal::SELECTPROG][Reveal::SURFACETHICKNESS]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG], "thickness");
        m_uniforms[Reveal::SELECTPROG][Reveal::SURFACE]
            = glGetUniformLocation(m_programs[Reveal::SELECTPROG], "surface");
        createRTT(m_selectTex, m_selectFBO, m_rttWidth, m_rttHeight, false);
    }

    //SLICE
    {
#ifdef GL43
        m_programs[Reveal::SLICEPROG]
            = Reveal::getInstance()->createProgram(slice43VS.c_str(), 
                    slice43FS.c_str());
#else
        m_programs[Reveal::SLICEPROG]
            = Reveal::getInstance()->createProgram(slice30VS.c_str(), 
                    slice30FS.c_str());
#endif

        m_uniforms[Reveal::SLICEPROG]=map<Reveal::REVIL_UNIFORM, GLint>();
        m_uniforms[Reveal::SLICEPROG][Reveal::BACKGROUND]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "background");
        m_uniforms[Reveal::SLICEPROG][Reveal::MIRRORED]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "mirrored");
        m_uniforms[Reveal::SLICEPROG][Reveal::VIEWPROJMAT]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "viewProjMat");
        m_uniforms[Reveal::SLICEPROG][Reveal::VIEWMAT]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "viewMat");
        m_uniforms[Reveal::SLICEPROG][Reveal::MODELMAT]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "modelMat");
        m_uniforms[Reveal::SLICEPROG][Reveal::DEPTHTYPE]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "depthType");
        m_uniforms[Reveal::SLICEPROG][Reveal::DEPTHID]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "depthID");
        m_uniforms[Reveal::SLICEPROG][Reveal::CAMXRESO]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "camXReso");
        m_uniforms[Reveal::SLICEPROG][Reveal::CAMYRESO]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "camYReso");
        m_uniforms[Reveal::SLICEPROG][Reveal::CAMXZFACTOR]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "camXZFactor");
        m_uniforms[Reveal::SLICEPROG][Reveal::CAMYZFACTOR]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "camYZFactor");
        m_uniforms[Reveal::SLICEPROG][Reveal::CAMTEX]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "depthCamTex");
        m_uniforms[Reveal::SLICEPROG][Reveal::CAMTEXFIL]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "depthCamTexFil");
        m_uniforms[Reveal::SLICEPROG][Reveal::MARKTEX]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "markersTex");
        m_uniforms[Reveal::SLICEPROG][Reveal::CAMFILTER]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "camFilter");
        m_uniforms[Reveal::SLICEPROG][Reveal::CAMCONTOURTHRESH]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "camContThresh");
        m_uniforms[Reveal::SLICEPROG][Reveal::CAMMARKERS]
            = glGetUniformLocation(m_programs[Reveal::SLICEPROG], "camMarkers");
        createRTT(m_sliceRttTex, m_sliceFBO, m_rttWidth, m_rttHeight, false);
    }

    //Calibration
    {
#ifdef GL43
        m_programs[Reveal::CALIBPROG]
            = Reveal::getInstance()->createProgram(calib43VS.c_str(), 
                    calib43FS.c_str());
#else
        m_programs[Reveal::CALIBPROG]
            = Reveal::getInstance()->createProgram(calib30VS.c_str(), 
                    calib30FS.c_str());
#endif
        m_uniforms[Reveal::CALIBPROG]=map<Reveal::REVIL_UNIFORM, GLint>();
        m_uniforms[Reveal::CALIBPROG][Reveal::VIEWPORTWIDTH]
            = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"viewportWidth");
        m_uniforms[Reveal::CALIBPROG][Reveal::VIEWPORTHEIGHT]
            = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"viewportHeight");

        /*
           m_uniforms[Reveal::CALIBPROG][Reveal::PATTERNTEX]
           = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"patternTex");
           */


        m_uniforms[Reveal::CALIBPROG][Reveal::PATW]
            = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"patW");
        m_uniforms[Reveal::CALIBPROG][Reveal::PATPOSX]
            = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"patPosX");
        m_uniforms[Reveal::CALIBPROG][Reveal::PATPOSY]
            = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"patPosY");
        m_uniforms[Reveal::CALIBPROG][Reveal::PATNBX]
            = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"patNbX");
        m_uniforms[Reveal::CALIBPROG][Reveal::PATNBY]
            = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"patNbY");
        m_uniforms[Reveal::CALIBPROG][Reveal::PATRANGE]
            = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"patRange");
        m_uniforms[Reveal::CALIBPROG][Reveal::PATOFFSET]
            = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"patOffset");
        m_uniforms[Reveal::CALIBPROG][Reveal::PATBRIGHT]
            = glGetUniformLocation(m_programs[Reveal::CALIBPROG],"patBright");


        m_calibGeom = new QuadGeometry();
    }
}

void ProjectorModule::createRTT(GLuint& tex, GLuint& fbo, 
        const int& width, const int& height,
        bool filter) {
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height,
            0, GL_RGBA, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, 
            filter?GL_LINEAR:GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
            filter?GL_LINEAR:GL_NEAREST);
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex, 0);
    GLenum drawBuffers[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, drawBuffers);
    GLuint idDepthrenderbuffer;
    glGenRenderbuffers(1, &idDepthrenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, idDepthrenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT,
            width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
            GL_RENDERBUFFER, idDepthrenderbuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ProjectorModule::deleteModule() {
    if(m_space) {
        m_space->removeProj(this);
    }
    glfwDestroyWindow(m_projWindow);
    Module::deleteModule();
}

void ProjectorModule::setSpace(SpaceModule* space) {
    m_space=space;
}

void ProjectorModule::refreshDepthCamList(const vector<DepthCamModule*> cams) {
    vector<string> labels;
    labels.push_back("none");
    vector<DepthCamModule*>::const_iterator itCam=cams.begin();
    for(; itCam!=cams.end(); ++itCam) {
        labels.push_back((*itCam)->getName());
    }
    m_attributesMap["attached_to_camera"]
        ->editStringValuesChoices().assign(1, labels);
    m_attributesMap["calibrate_with_camera"]
        ->editStringValuesChoices().assign(1, labels);
}

void ProjectorModule::attachToCam(const string& camName) {
    if(m_space) {
        if(m_attachedToCam) {
            m_attachedToCam->detachProj(this);
        }
        m_attachedToCam = m_space->getDepthCamModule(camName);
        if(m_attachedToCam) {
            m_attachedToCam->attachProj(this);
        }
    }
}

void ProjectorModule::setWindowDimensions(const int& x, const int& y,
        const int& w, const int& h) {
    glfwSetWindowPos(m_projWindow, x, y);
    glfwSetWindowSize(m_projWindow, w, h);
    m_width=w;
    m_height=h;
}

void ProjectorModule::setWindowDecoration(bool d) {
    //m_window->setWindowDecoration(d);
}

void ProjectorModule::fullscreenMonitor(const std::string& monitorName) {
    int count;
    GLFWmonitor** monitors = glfwGetMonitors(&count);

    GLFWmonitor* monitor  = NULL;
    for(int m=0; m<count; ++m) {
        if(monitorName.compare(glfwGetMonitorName(monitors[m]))==0) {
            monitor=monitors[m];
        }
    }

    int posX=0;
    int posY=0;
    m_width=640;
    m_height=480;
    if(monitor) {
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);
        m_width=mode->width;
        m_height=mode->height;
        glfwGetMonitorPos(monitor, &posX, &posY);
    }
    m_posX=posX;
    m_posY=posY;
    glfwSetWindowMonitor(m_projWindow, monitor, 0, 0,
            m_width, m_height, GLFW_DONT_CARE);
    updateWindowDims();
    refreshMonitors();
}

void ProjectorModule::refreshMonitors(){
    int count;
    GLFWmonitor** monitors = glfwGetMonitors(&count);
    vector<string> monitorNames;
    monitorNames.push_back("windowed");
    for(int m=0; m<count; ++m) {
        monitorNames.push_back(glfwGetMonitorName(monitors[m]));
    }
    m_attributesMap["fullscreen_monitor"]
        ->editStringValuesChoices().assign(count, monitorNames);
}

void ProjectorModule::setViewMatrix(const vector<float>& vals) {
    m_viewMat = mat4(vals[0], vals[1], vals[2], vals[3],
            vals[4], vals[5], vals[6], vals[7],
            vals[8], vals[9], vals[10], vals[11],
            vals[12], vals[13], vals[14], vals[15]);
    updateViewProjMatrix();
}

void ProjectorModule::setProjectionMatrix(const vector<float>& vals) {
    m_projMat = mat4(vals[0], vals[1], vals[2], vals[3],
            vals[4], vals[5], vals[6], vals[7],
            vals[8], vals[9], vals[10], vals[11],
            vals[12], vals[13], vals[14], vals[15]);
    updateViewProjMatrix();
}

void ProjectorModule::updateModelMatrix() {
    if(!m_attachedToCam) {
        Module::updateModelMatrix();
    }
    updateViewProjMatrix();
}

void ProjectorModule::setModelMat(const glm::mat4& mat) {
    Module::setModelMat(mat);
    updateViewProjMatrix();
}

void ProjectorModule::updateViewProjMatrix() {
    m_transViewMat = m_modelMat * inverse(m_viewMat);
    m_viewProjMat = m_projMat * inverse(m_transViewMat);
    //m_transViewMat = m_viewMat * m_modelMat;
    //m_viewProjMat = m_projMat * m_transViewMat;
}

void ProjectorModule::setModuleName(const string& name) {
    Module::setModuleName(name);
    glfwSetWindowTitle(m_projWindow, name.c_str());
    if(m_space) {
        m_space->refreshProjList();
    }
}

void ProjectorModule::outputRevealed(bool output) {
    m_processOutput=output;
}

void ProjectorModule::postFilter(const int& filt) {
    m_postFilter=filt;
}

void ProjectorModule::setTransparentWindow(const bool& transp) {
    m_transparentBackground=transp;
}

void ProjectorModule::processOutput() {
    bool outputProcessed=false;

    //retrieve active revealed modules
    vector<RevealedModule*>& revModules
        = Reveal::getInstance()->editRegisteredOutputRevealed();
    vector<DepthCamModule*>& camModules
        = Reveal::getInstance()->editRegisteredOutputDepthCams();

    if(revModules.size()>0 || camModules.size()>0) {
        //retrieve output image
        glBindTexture(GL_TEXTURE_2D, m_outputTex);
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, 
                GL_INT,
                m_outputImg);
    }


    //process shape revealing output
    if(revModules.size()>0) {
        m_outNbDepth = m_space->getNbDepthModulesFromID();
        //get output values for revealed modules with active output
        vector<RevealedModule*>::iterator itSh = revModules.begin();
        for(; itSh!=revModules.end(); ++itSh) {
            //test if shape is visible from this projector
            if((*itSh)->getVisibilityMask() & m_visibilityMask) {
                for(int d=1; d<m_outNbDepth+1; ++d) {
                    DepthModule* mod = m_space->getDepthModuleFromID(d);
                    if(mod) {
                        if(mod->isDepthVisible(d)) {
                            (*itSh)->processReveal(d, 
                                    mod->getName(), 
                                    m_outNbDepth,
                                    m_outputImg);
                            outputProcessed=true;
                        }
                    }
                }
            }
        }
    }

    /*
    //process cam
    if(camModules.size()>0) {
    unsigned int curs;
    vector<float> pos(3,0);
    vector<DepthCamModule*>::iterator itCa = camModules.begin();
    for(; itCa!=camModules.end(); ++itCa) {
    extractOutputValue((*itCa)->getDepthID()+500, 0, curs);
    pos[0] = (int(curs) - int(curs/1000)*1000)*10-5000;
    pos[1] = (int(curs/1000) - int(curs/1000000)*1000)*10-5000;
    pos[2] = 10000-int(curs/1000000)*10;
    //cout<<"got cursor "<<curs<<" from cam at pos "
    //    <<pos[0]<<" "<<pos[1]<<" "<<pos[2]<<endl;
    (*itCa)->processCursor(pos);
    }
    outputProcessed=true;
    }
    */

    if(outputProcessed) {
        //reset output image to 0
        glBindTexture(GL_TEXTURE_2D, m_outputTex);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 
                m_outputImgSize, m_outputImgSize,
                GL_RED_INTEGER, GL_UNSIGNED_INT, m_outputImgInit);
        /*
           glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8,
           m_outputImgSize, m_outputImgSize, 0,
           GL_RGBA, GL_UNSIGNED_BYTE, m_outputImgInit);
           */

    }
}

void ProjectorModule::setAudioProcessType(const std::string& type) {
    int t=0;
    for(auto &ptype : m_audioProcessTypes) {
        if(ptype == type) {
            m_audioProcessType = t;
        }
        ++t;
    }
}

void ProjectorModule::processAudio() {
    glBindTexture(GL_TEXTURE_2D, m_audioTex);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RED_INTEGER, 
            GL_INT,
            m_audioImg);

    //FIXME mix in a compute shader
    float totalGains = 0;
    for(int i = 0; i<m_nbTracks; i++) {
        //check if the note has to be played this frame
        float trackGain = m_audioImg[m_imgBufSize * i];
        if(trackGain>0) {
            totalGains += trackGain;
            //Mix all tracks with gain from number of voxels
            for(int j=0; j<m_audioBufSize; j++) {
                m_audioBuffer[j] += trackGain 
                    * (float(m_audioImg[(m_imgBufSize*i)+3+j])
                            / m_maxAudioValue);
            }

            //copy the old and new phases
            m_audioImgInit[3*i+1] = m_audioImg[m_imgBufSize*i+1];
            m_audioImgInit[3*i+2] = m_audioImg[m_imgBufSize*i+2];
        }
    }


    float gain = 0.4;
    if(totalGains > 0) {
        for(int i=0; i<m_audioBufSize; i++) {
            m_audioBuffer[i] = gain*m_audioBuffer[i]/totalGains;
        }
        m_audioNextBuf = AudioManager::getInstance()->changeBuf(m_audioBuffer, 
                m_maxAudioValue);
        memset(&m_audioBuffer[0], 0, (m_audioBufSize)*sizeof(float));
    }
    else{
        memset(&m_audioBuffer[0], 0, (m_audioBufSize)*sizeof(float));
        m_audioNextBuf = AudioManager::getInstance()->changeBuf(m_audioBuffer, 
                m_maxAudioValue);
    }

    //reset the gain and transfer the old and new phases
    glBindTexture(GL_TEXTURE_2D, m_audioTex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 3, m_nbTracks,
            GL_RED_INTEGER, GL_INT, &m_audioImgInit[0]);

}

void ProjectorModule::updateWindowSize(const int& w, const int& h) {
    m_height=h;
    m_width=w;
    updateWindowDims();
}

void ProjectorModule::updateWindowPos(const int& x, const int& y) {
    m_posX=x;
    m_posY=y;
    updateWindowDims();
}

void ProjectorModule::updateWindowDims() {
    vector<int> dims(4,0);
    dims[0]=m_posX;
    dims[1]=m_posY;
    dims[2]=m_width;
    dims[3]=m_height;
    m_attributesMap["window_dimensions"]->initInts(dims);
    m_patternPosRange = Size(m_width, m_height);
}

//-------------------------Calibration----------------------------------

/*
   static void cbPatternSquareWidth(int w, void* mod) {
   if(w>10) {
   ProjectorModule* proj = static_cast<ProjectorModule*>(mod);
   proj->setPatternSquareWidth(w);
   }
   }*/

void ProjectorModule::setPatternSquareWidth(const int& w) {
    m_patternSquareWidth=w;
    updateCalibrationPattern();
}

static void cbPatternPosOX(int x, void* mod) {
    ProjectorModule* proj = static_cast<ProjectorModule*>(mod);
    proj->setPatternPosOX(x);
}

void ProjectorModule::setPatternPosOX(const int& x) {
    m_patternPosOffset.width=x;
    updateCalibrationPattern();
}

static void cbPatternPosOY(int y, void* mod) {
    ProjectorModule* proj = static_cast<ProjectorModule*>(mod);
    proj->setPatternPosOY(y);
}

void ProjectorModule::setPatternPosOY(const int& y) {
    m_patternPosOffset.height=y;
    updateCalibrationPattern();
}

static void cbPatternPosRX(int x, void* mod) {
    ProjectorModule* proj = static_cast<ProjectorModule*>(mod);
    proj->setPatternPosRX(x);
}

void ProjectorModule::setPatternPosRX(const int& x) {
    m_patternPosRange.width=x;
    updateCalibrationPattern();
}

static void cbPatternPosRY(int y, void* mod) {
    ProjectorModule* proj = static_cast<ProjectorModule*>(mod);
    proj->setPatternPosRY(y);
}

void ProjectorModule::setPatternPosRY(const int& y) {
    m_patternPosRange.height=y;
    updateCalibrationPattern();
}

static void cbCalib(int c, void* mod) {
    ProjectorModule* proj = static_cast<ProjectorModule*>(mod);
    proj->setCalib(c);
}

void ProjectorModule::setCalib(const int& c) {
    if(m_calibrating==1 && c==0) {
        m_calibrationStopped=true;
    }
    m_calibrating=c;
}

static void cbPatternBrightness(int b, void* mod) {
    ProjectorModule* proj = static_cast<ProjectorModule*>(mod);
    proj->setPatternBrightness(b);
}

void ProjectorModule::setPatternBrightness(const int& b) {
    m_patternBrightness=b;
}

void ProjectorModule::updateCalibrationPattern() {
    m_generatedCorners.clear();
    for(int j=1; j<m_patternSize.height+1; ++j) {
        for(int i=1; i<m_patternSize.width+1; ++i) {
            int cornerX=m_patternSquareWidth*i+m_patternPosition.width;
            int cornerY=m_patternSquareWidth*j+m_patternPosition.height;
            m_generatedCorners.push_back(Point2f(cornerX, cornerY));
        }
    }
}

void ProjectorModule::calibrate(const std::string& camName) {
    //get depth camera module and activate color image
    DepthCamModule* cam = NULL;
    if(m_space) {
        cam = m_space->getDepthCamModule(camName);
    }
    if(cam==NULL) {
        cout<<"No camera attached to projector"<<endl;
        return;
    }

    //close cam and reopen in calibration mode
    cam->closeDevice();
    cam->openDevice(cam->getDeviceID(), true);

    //check cam is open
    if(!cam->isOpen()) {
        cout<<"Camera "<<cam->getName()<<"is not open"<<endl;
        return;
    }

    cout<<"Calibrating projector ..."<<endl;

    //create opencv windows, trackbars and initialize variables
    m_imageSize = cv::Size(640 ,480); //fixme, get from cam color image size
    m_calibrating=0;
    m_calibrationStopped=false;
    m_neededFramesNb=9;
    cv::Mat kinectRgbImg = Mat::zeros(m_imageSize, CV_8UC3);
    namedWindow("DepthCamRGB");
    namedWindow("Controls");
    createTrackbar("Pattern Offset X", "Controls",
            &m_patternPosOffset.width, 1000, cbPatternPosOX, this);
    createTrackbar("Pattern Offset Y", "Controls",
            &m_patternPosOffset.height, 500, cbPatternPosOY, this);
    createTrackbar("Pattern Range X", "Controls",
            &m_patternPosRange.width, 1920, cbPatternPosRX, this);
    createTrackbar("Pattern Range Y", "Controls",
            &m_patternPosRange.height, 1080, cbPatternPosRY, this);
    createTrackbar("Pattern Brightness", "Controls",
            &m_patternBrightness, 100, cbPatternBrightness, this);
    createTrackbar("Calibrate (1:Start, 0:Cancel)", "Controls",
            NULL, 1, cbCalib, this);
    vector<vector<Point3f> > objectPoints;
    vector<vector<Point2f> > imagePoints;
    Mat cameraMatrix = Mat::eye(3, 3, CV_64F);
    Mat distCoeffs = Mat::zeros(8, 1, CV_64F);
    vector<Mat> rvecs;
    vector<Mat> tvecs;

    cam->activateColor();
    for(int f=0; f<m_neededFramesNb && !m_calibrationStopped;) {
        //update pattern position
        m_patternSquareWidth = m_height/20;

        m_patternPosition.width = (m_patternPosRange.width-m_patternSquareWidth)
            / sqrt(m_neededFramesNb)
            *(fmod(f,sqrt(m_neededFramesNb)))
            +m_patternSquareWidth
            +m_patternPosOffset.width;

        m_patternPosition.height=(m_patternPosRange.height-m_patternSquareWidth)
            / sqrt(m_neededFramesNb)
            *(floor(f/sqrt(m_neededFramesNb)))
            +m_patternSquareWidth
            +m_patternPosOffset.height;
        updateCalibrationPattern();

        //display frame with calibration pattern
        glfwMakeContextCurrent(m_projWindow);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, m_width, m_height);
        glDisable(GL_CULL_FACE);
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glUseProgram(m_programs[Reveal::CALIBPROG]);
        glUniform1f(m_uniforms[Reveal::CALIBPROG][Reveal::VIEWPORTWIDTH],
                m_width);
        glUniform1f(m_uniforms[Reveal::CALIBPROG][Reveal::VIEWPORTHEIGHT],
                m_height);
        glUniform1f(m_uniforms[Reveal::CALIBPROG][Reveal::PATW],
                m_patternSquareWidth);
        glUniform1f(m_uniforms[Reveal::CALIBPROG][Reveal::PATPOSX],
                m_patternPosition.width);
        glUniform1f(m_uniforms[Reveal::CALIBPROG][Reveal::PATPOSY],
                m_patternPosition.height);
        glUniform2f(m_uniforms[Reveal::CALIBPROG][Reveal::PATOFFSET],
                m_patternPosOffset.width, m_patternPosOffset.height);
        glUniform2f(m_uniforms[Reveal::CALIBPROG][Reveal::PATRANGE],
                m_patternPosRange.width, m_patternPosRange.height);
        glUniform1f(m_uniforms[Reveal::CALIBPROG][Reveal::PATNBX],
                m_patternSize.width);
        glUniform1f(m_uniforms[Reveal::CALIBPROG][Reveal::PATNBY],
                m_patternSize.height);
        glUniform1f(m_uniforms[Reveal::CALIBPROG][Reveal::PATBRIGHT],
                float(m_patternBrightness)/100.0);
        m_calibGeom->drawDirect(m_contextHandlerID);
        glfwSwapBuffers(m_projWindow);

        waitKey(10);
        sleep(1);

        //grab frame
        if(cam->getFrames()) {
            openni::VideoFrameRef depthFrame = cam->getDepthFrame();
            openni::VideoFrameRef colorFrame = cam->getColorFrame();

            //copy to opencv mat
            for(int j=0; j<m_imageSize.height; ++j) {
                for(int i=0; i<m_imageSize.width; ++i) {
                    cv::Vec3b& pix = kinectRgbImg.at<cv::Vec3b>(j,i);
                    int coord=0;
                    coord=j*m_imageSize.width+i;
                    openni::RGB888Pixel colorPix =
                        ((openni::RGB888Pixel*) colorFrame.getData())[coord];
                    pix[0]=colorPix.r;
                    pix[1]=colorPix.g;
                    pix[2]=colorPix.b;
                }
            }


            imshow("DepthCamRGB", kinectRgbImg);
            if(m_calibrating) {
                m_foundCorners.clear();
                //find chessboard in image
                bool patternFound = findChessboardCorners(
                        kinectRgbImg,
                        m_patternSize,
                        m_foundCorners,
                        CALIB_CB_ADAPTIVE_THRESH);
                //if we found it
                if(patternFound) {
                    //refine
                    Mat gray;
                    cvtColor(kinectRgbImg, gray, COLOR_RGB2GRAY);
                    cornerSubPix(gray, m_foundCorners,
                            cv::Size(1, 1), cv::Size(-1, -1),
                            TermCriteria(TermCriteria::EPS,
                                30, 0.1));


                    bool validDepths=true;
                    //retrieve the corresponding position and depth
                    vector<Point3f> worldCorners;
                    vector<Point2f>::iterator itPnt=m_foundCorners.begin();
                    for(; itPnt!=m_foundCorners.end(); ++itPnt) {
                        float posX, posY, posZ;
                        Point2f coord = Point2f((*itPnt).x,(*itPnt).y);
                        Point2f depthCoord = coord;

                        openni::DepthPixel depthPix =
                            ((openni::DepthPixel*)depthFrame.getData())[int(
                            depthCoord.y*m_imageSize.width +
                            depthCoord.x)];
                        posZ=depthPix;

                        //filter the depth
                        int size=6;
                        vector<double> vals;
                        for(int x=depthCoord.x-size/2;
                                x<depthCoord.x+size/2; ++x) {
                            for(int y=depthCoord.y-size/2;
                                    y<depthCoord.y+size/2; ++y) {
                                if(x>=0 && x<m_imageSize.width &&
                                        y>=0 && y<m_imageSize.height) {
                                    vals.push_back(
                                            ((openni::DepthPixel*)depthFrame.getData())
                                            [int(y*m_imageSize.width + x)]);
                                }
                            }
                        }
                        sort(vals.begin(), vals.end());
                        posZ=vals[vals.size()/2];
                        depthPix=posZ;

                        openni::CoordinateConverter::convertDepthToWorld(
                                cam->getDepthStream(),
                                int(depthCoord.x),
                                int(depthCoord.y),
                                depthPix,
                                &posX, &posY, &posZ);
                        //if(reversed) {
                        //    posX=-posX;
                        //    posY=-posY;
                        //}
                        if(posZ<=500.0 || posZ>4000.0) {
                            validDepths=false;
                        }
                        else {
                            worldCorners.push_back(Point3f(posX, posY, posZ));
                        }
                    }

                    //if everything is valid
                    if(validDepths) {
                        cout<<"Pattern found "<<f<<"/"<<m_neededFramesNb<<endl;
                        //add the generated corners
                        imagePoints.push_back(m_generatedCorners);
                        cout<<m_generatedCorners[0]<<endl;
                        //add the detected world corners
                        objectPoints.push_back(worldCorners);
                        cout<<worldCorners[0]<<endl;

                        //cout<<m_generatedCorners<<endl;
                        //cout<<worldCorners<<endl;

                        //draw corners
                        drawChessboardCorners(kinectRgbImg, m_patternSize,
                                m_foundCorners, patternFound);


                        imshow("DepthCamRGB", kinectRgbImg);
                        waitKey(10);

                        //wait
#ifdef POSIX
                        sleep(1);
#else
                        Sleep(1000);
#endif

                        //increase frame count
                        ++f;
                    }
                }
            }
            waitKey(10);
        }
    }

    double rms=0;

    if(objectPoints.size()>0) {

        //put the image and object points in the right order for opencv
        vector<vector<Point3f> > vvo(1); //object points
        vector<vector<Point2f> > vvi(1); //image points
        for (unsigned int i=0; i<objectPoints.size(); ++i) {
            for (unsigned int j = 0; j<objectPoints[i].size(); j++) {
                vvo[0].push_back(objectPoints[i][j]);
                vvi[0].push_back(imagePoints[i][j]);
            }
        }

        //when enough points found, get the extrinsics/intrisics parameter
        float w = m_width;
        float h = m_height;
        cameraMatrix = (Mat1d(3, 3) <<  w, 0, w/2.,
                0, h, h / 2.,
                0, 0, 1);
        distCoeffs = Mat::zeros(8, 1, CV_64F);
        rms=calibrateCamera(vvo, vvi,
                Size(w, h), cameraMatrix,
                distCoeffs, rvecs, tvecs,
                CALIB_FIX_K1+
                CALIB_FIX_K2+
                CALIB_FIX_K3+
                CALIB_FIX_K4+
                CALIB_FIX_K5+
                CALIB_FIX_K6+
                CALIB_ZERO_TANGENT_DIST+
                CALIB_USE_INTRINSIC_GUESS);

        cout<<"... done , RMS="<<rms<<endl;

        //Build the opengl view matrix
        Mat rot;
        rvecs[0].copyTo(rot);
        Mat rotMat = Mat::zeros(3, 3, CV_64F);
        Rodrigues(rot, rotMat);

        mat4 viewMatrix(rotMat.at<double>(0,0), rotMat.at<double>(1,0),
                rotMat.at<double>(2,0), 0,
                rotMat.at<double>(0,1), rotMat.at<double>(1,1),
                rotMat.at<double>(2,1), 0,
                rotMat.at<double>(0,2), rotMat.at<double>(1,2),
                rotMat.at<double>(2,2), 0,
                tvecs[0].at<double>(0,0), tvecs[0].at<double>(1,0),
                tvecs[0].at<double>(2,0), 1);

        mat4 glCoordsMat(-1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);
        m_viewMat=glCoordsMat*viewMatrix;

        m_attributesMap["view_matrix"]->setFloats(
                vector<float>(
                    value_ptr(m_viewMat),
                    value_ptr(m_viewMat)+16));
        //build the opengl projection matrix
        double fx=cameraMatrix.at<double>(0,0);
        double cx=cameraMatrix.at<double>(0,2);
        double fy=cameraMatrix.at<double>(1,1);
        double cy=cameraMatrix.at<double>(1,2);
        double l=0;
        double r=w;
        double b=h;
        double t=0;
        double n=100;
        double f=10000;
        mat4 projMatrix(
                2.0*fx/w,       0,                      0,              0,
                0,              2.0*fy/h,               0,              0,
                1.0-2.0*(cx)/w,-1.0+(2.0*(cy)+2.0)/h,   (f+n)/(n-f),    -1,
                0,              0,                      (2.0*f*n)/(n-f), 0);
        m_attributesMap["projection_matrix"]->setFloats(
                vector<float>(value_ptr(projMatrix),
                    value_ptr(projMatrix)+16));
        m_attributesMap["attached_to_camera"]
            ->setStrings(vector<string>(1, cam->getName()));
    }

    ostringstream oss;
    oss<<rms;

    cv::Mat reconstructionImg = Mat::zeros(Size(m_width, m_height), CV_8UC3);

    //project reconstructed kinect
    while(!m_calibrationStopped) {
        if(cam->getFrames()) {
            openni::VideoFrameRef depthFrame = cam->getDepthFrame();
            openni::VideoFrameRef colorFrame = cam->getColorFrame();

            //get points from the kinect and their actual 3D positions
            reconstructionImg.setTo(cv::Vec3b(0,0,0));
            vector<Point3f> objPnts;
            vector<cv::Vec3b> kinPnts;
            vector<Point2f> imgPnts;
            for(int j=0; j<m_imageSize.height; ++j) {
                for(int i=0; i<m_imageSize.width; ++i) {
                    float posX, posY, posZ;
                    int coord = m_imageSize.width*j+i;
                    openni::DepthPixel depthPix =
                        ((openni::DepthPixel*)depthFrame.getData())[coord];
                    openni::CoordinateConverter::convertDepthToWorld(
                            cam->getDepthStream(),
                            i, j, depthPix,
                            &posX, &posY, &posZ);
                    objPnts.push_back(Point3f(posX,posY,posZ));
                    const openni::RGB888Pixel& colorPix =
                        ((openni::RGB888Pixel*) colorFrame.getData())[coord];
                    kinPnts.push_back(cv::Vec3b(colorPix.r,
                                colorPix.g,
                                colorPix.b));
                }
            }
            projectPoints(objPnts, rvecs[0], tvecs[0],
                    cameraMatrix, distCoeffs, imgPnts);

            //for each of the image points
            vector<Point2f>::iterator itPnt=imgPnts.begin();
            vector<cv::Vec3b>::iterator itKin=kinPnts.begin();
            vector<Point3f>::iterator itObj=objPnts.begin();
            for(; itPnt!=imgPnts.end() && itKin!=kinPnts.end();
                    ++itPnt, ++itKin, ++itObj) {
                if((*itPnt).x<reconstructionImg.cols &&
                        (*itPnt).y<reconstructionImg.rows &&
                        (*itPnt).x>0 &&
                        (*itPnt).y>0) {
                    reconstructionImg.at<cv::Vec3b>(
                            (*itPnt).y,
                            (*itPnt).x)
                        =(*itKin);
                }
            }

            putText(reconstructionImg, "RMS="+oss.str(), Point(0,50), 
                    cv::FONT_HERSHEY_SIMPLEX, 2, 
                    Scalar(255, 255, 255));
            imshow( "DepthCamRGB", reconstructionImg);
        }
        waitKey(10);
    }

    //close opencv windows
    destroyWindow("DepthCamRGB");
    destroyWindow("Controls");
    waitKey(10);

    //reopen cam in normal mode
    cam->closeDevice();
    cam->openDevice(cam->getDeviceID());
}

void ProjectorModule::calibrateCamWithMarker(const std::string& camName) {



}

