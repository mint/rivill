/***************************************************************************
 *  SpaceModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/*////////////
Calibration heavily relies on code from : 
https://github.com/Kj1/ofxSpaceKinectCalibration
*/////////////////////

#include "SpaceModule.hpp" 

#include <iostream>

#include "../Reveal.hpp"
#include "DepthCamModule.hpp"
#include "ProjectorModule.hpp"
#include "DepthShapeModule.hpp"
#include "DepthMeshModule.hpp"

#include "../geoms/BoxGeometry.hpp"
#include "../geoms/CamGeometry.hpp"
#include "../geoms/QuadGeometry.hpp"
#include "../geoms/SphereGeometry.hpp"
#include "../geoms/TubeGeometry.hpp"
#include "../geoms/ConeGeometry.hpp"

using namespace std;

SpaceModule::SpaceModule(): DepthGroupModule() {
    m_type="Space";
    m_name="space";
    m_space=this;

    /*
    addAttribute(new Attribute("name", 
                                Attribute::STRING_ATTRIBUTE,
                                nameCallback, this, 1, Attribute::LOCAL));
    m_attributesMap["name"]->setStrings(vector<string>(1, m_name));
    addAttribute(new Attribute("add_projector", 
                                Attribute::ACTION_ATTRIBUTE,
                                addProjCallback, this, Attribute::LOCAL));
    addAttribute(new Attribute("add_depth_group", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addDepthGroupCallback, this, 1));
    addAttribute(new Attribute("add_depth_camera", 
                                Attribute::ACTION_ATTRIBUTE,
                                addCamCallback, this, Attribute::LOCAL));
    addAttribute(new Attribute("add_depth_shape", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addDepthShapeCallback, this, 1));
    m_attributesMap["add_depth_shape"]->initStrings(vector<string>(1,"shape"));
    addAttribute(new Attribute("add_depth_mesh", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addDepthMeshCallback, this, 1));
    m_attributesMap["add_depth_mesh"]->initStrings(vector<string>(1,"mesh"));
*/
    //create all depth geometries 
    m_geomsMap[Reveal::GEOM_BOX] = new BoxGeometry();
    m_geomsMap[Reveal::GEOM_QUAD] = new QuadGeometry();
    m_geomsMap[Reveal::GEOM_CAM1280] = new CamGeometry(1280, 960);
    m_geomsMap[Reveal::GEOM_CAM640] = new CamGeometry(640, 480);
    m_geomsMap[Reveal::GEOM_CAM320] = new CamGeometry(320, 240);
    m_geomsMap[Reveal::GEOM_SPHERE] = new SphereGeometry();
    m_geomsMap[Reveal::GEOM_TUBE] = new TubeGeometry();
    m_geomsMap[Reveal::GEOM_CONE] = new ConeGeometry();
    map<Reveal::GEOM_ID, Geometry*>::iterator itGeom = m_geomsMap.begin();
    for(; itGeom!=m_geomsMap.end(); ++itGeom) {
        itGeom->second->setGeomID(itGeom->first);
        m_geoms.push_back(itGeom->second);
    }
    m_attributesMap["name"]->setStrings(vector<string>(1, m_name));
}

SpaceModule::~SpaceModule() {}

void SpaceModule::draw() {
    vector<ProjectorModule*>::iterator itProj=m_projModules.begin();
    for(; itProj!=m_projModules.end(); ++itProj) {
        (*itProj)->draw();
    }
}

void SpaceModule::removeProj(ProjectorModule* proj) {
    vector<ProjectorModule*>::iterator itProj=m_projModules.begin();
    for(; itProj!=m_projModules.end();) {
        if((*itProj)->getFullName().compare(proj->getFullName())==0) {
            itProj = m_projModules.erase(itProj);
        }
        else {
            itProj++;   
        }
    }
    removeChild(proj);
    refreshDepthCamList();
    refreshProjList();
}

void SpaceModule::removeCam(DepthCamModule* cam) {
    vector<DepthCamModule*>::iterator itCam=m_depthCamModules.begin();
    for(; itCam!=m_depthCamModules.end();) {
        if((*itCam)->getFullName().compare(cam->getFullName())==0) {
            itCam = m_depthCamModules.erase(itCam);
        }
        else {
            itCam++;   
        }
    }
    removeDepthModule(cam);
    removeChild(cam);
    refreshDepthCamList();
    refreshProjList();
}

void SpaceModule::refreshDepthCamList() {
    vector<ProjectorModule*>::iterator itProj=m_projModules.begin();
    for(; itProj!=m_projModules.end(); ++itProj) {
        (*itProj)->refreshDepthCamList(m_depthCamModules);
    }
}

void SpaceModule::refreshProjList() {
    vector<DepthCamModule*>::iterator itCam=m_depthCamModules.begin();
    for(; itCam!=m_depthCamModules.end(); ++itCam) {
        (*itCam)->refreshProjList(m_projModules);
    }
}

void SpaceModule::addChild(Module* mod) {
    if(mod->getType().compare("Projector")==0) {
        ProjectorModule* proj = static_cast<ProjectorModule*>(mod);
        proj->setSpace(this);
    }
    else {
        DepthModule* dep = dynamic_cast<DepthModule*>(mod);
        dep->setSpace(this);
    }
    DepthGroupModule::addChild(mod);
}


DepthCamModule* SpaceModule::getDepthCamModule(const std::string& name) {
    DepthCamModule* cam=NULL;
    vector<DepthCamModule*>::const_iterator itCam=m_depthCamModules.begin();
    for(; itCam!=m_depthCamModules.end(); ++itCam) {
        if((*itCam)->getName().compare(name)==0) {
            cam=(*itCam);
        }
    }
    return cam;
}

void SpaceModule::addSpaceModule(Module* mod) {
    if(mod->getType().compare("Projector")==0) {
        ProjectorModule* proj = static_cast<ProjectorModule*>(mod);
        m_projModules.push_back(proj);
        proj->setSpace(this);
    }
    else if(mod->getType().compare("DepthCam")==0) {
        DepthCamModule* cam = dynamic_cast<DepthCamModule*>(mod);
        m_depthCamModules.push_back(cam);
        m_depthModules.push_back(cam);
        cam->setSpace(this);
    }
    else if(mod->getType().compare("DepthShape")==0) {
        DepthShapeModule* sha = dynamic_cast<DepthShapeModule*>(mod);
        m_depthShapeModules.push_back(sha);
        m_depthModules.push_back(sha);
        sha->setSpace(this);
    }
    else if(mod->getType().compare("DepthMesh")==0) {
        DepthMeshModule* mes = dynamic_cast<DepthMeshModule*>(mod);
        m_depthMeshModules.push_back(mes);
        m_depthModules.push_back(mes);
        mes->setSpace(this);
    }
    else if(mod->getType().compare("DepthGroup")==0) {
        DepthGroupModule* gro = dynamic_cast<DepthGroupModule*>(mod);
        gro->setSpace(this);
    }


    updateDepthModules();
    refreshDepthCamList();
    refreshProjList();
}

void SpaceModule::removeDepthModule(DepthModule* mod) {
    vector<DepthModule*>::iterator itMod = m_depthModules.begin();
    for(; itMod!=m_depthModules.end();) {
        if((*itMod)->getDepthID()==mod->getDepthID()) {
            itMod=m_depthModules.erase(itMod);
        }
        else {
            ++itMod;
        }
    }
}

void SpaceModule::updateDepthModules() {
    unsigned int id=1;
    //assign depth ids 
    m_depthIDsModules.clear();
    vector<DepthModule*>::iterator itMod = m_depthModules.begin();
    for(; itMod!=m_depthModules.end(); ++itMod) {
        (*itMod)->setDepthID(id);
        for(int s=0; s<(*itMod)->getNbSubDepthIDs(); ++s, ++id) {
            m_depthIDsModules[id]=*itMod;
        }
    }
    //reorder things : depths then projs 
    m_children.clear();
    vector<DepthModule*>::iterator itDe = m_depthModules.begin();
    for(; itDe!=m_depthModules.end(); ++itDe) {
        m_children.push_back(*itDe);
    }
    vector<ProjectorModule*>::iterator itProj = m_projModules.begin();
    for(; itProj!=m_projModules.end(); ++itProj) {
        m_children.push_back(*itProj);
    }
}

void SpaceModule::getVisibleFromList(vector<string>& modVec, 
                                     map<string, int>& modMap) {
    int spaceMask = 1 << modVec.size();
    modVec.push_back(m_name);
    modMap[modVec.back()] = spaceMask;

    vector<ProjectorModule*>::iterator itProj=m_projModules.begin();
    for(; itProj!=m_projModules.end(); ++itProj) {
        int projMask = (1 << modVec.size());
        modVec.push_back((*itProj)->getName());
        modMap[modVec.back()] = projMask;
        (*itProj)->setVisibilityMask(projMask | spaceMask | 1);
    }
}

void SpaceModule::registerGeom(Geometry* geom) {
    if(geom->getGeomID()<0) {
        Reveal::GEOM_ID newID = Reveal::GEOM_MODEL;
        while(m_geomsMap.find(newID)!=m_geomsMap.end()) {
            newID = Reveal::GEOM_ID(int(newID)+1);
        }
        geom->setGeomID(newID);
        m_geomsMap[newID]=geom;
        m_geoms.push_back(geom);
    }
}

void SpaceModule::unregisterGeom(Geometry* geom) {
    m_geomsMap.erase(geom->getGeomID());
    vector<Geometry*>::iterator itGeom = m_geoms.begin();
    for(; itGeom!=m_geoms.end(); ) {
        if((*itGeom)->getGeomID()==geom->getGeomID()) {
            itGeom = m_geoms.erase(itGeom);
        }
        else {
            ++itGeom;
        }
    }
}

//SPACES
void SpacesModule::addSpace() {
    SpaceModule* proj = new SpaceModule();
    addChild(proj);
	m_spaces.push_back(proj);
    Reveal::getInstance()->refreshModules();
}

void SpacesModule::load(xmlNodePtr node) {
    Module::load(node);
    xmlNodePtr modsNode;
    for(modsNode=node->children; modsNode; modsNode=modsNode->next){
        if(modsNode->type == XML_ELEMENT_NODE &&
                !xmlStrcmp(modsNode->name, (const xmlChar *) "Modules")) {
            xmlNodePtr modNode;
            for(modNode=modsNode->children; modNode; modNode=modNode->next){
                if(modNode->type == XML_ELEMENT_NODE) {
                    if(!xmlStrcmp(modNode->name, (const xmlChar *)"Preview")) {
                        m_children[0]->load(modNode);
                    }
                    else {
                        string typeStr((char*) modNode->name);
                        Module* mod = 
                            Reveal::getInstance()->createModule(typeStr);
                        if(mod) {
                            addChild(mod);
                            mod->load(modNode);
                        }
                    }
                }
            }
        }
    }
}


