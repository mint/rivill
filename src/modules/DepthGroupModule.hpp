/***************************************************************************
 *  DepthGroupModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef DepthGroupModule_h
#define DepthGroupModule_h

#include "GroupModule.hpp"
#include "DepthModule.hpp"

class DepthCamModule;
class DepthMeshModule;
class ProjectorModule;
class DepthShapeModule;

class DepthGroupModule : public GroupModule, public DepthModule {
    public:
        DepthGroupModule();
        virtual ~DepthGroupModule();
        void deleteModule();

        virtual void addChild(Module*);

        virtual void updateModelMatrix();

        static void nameCallback(Module* mod, 
                                 const std::vector<std::string>& vals) {
            dynamic_cast<DepthGroupModule*>(mod)->setName(vals[0]);
        }

        static void addProjCallback(Module* mod) {
            dynamic_cast<DepthGroupModule*>(mod)->addProj();
        }
        void addProj();

        static void addCamCallback(Module* mod) {
            dynamic_cast<DepthGroupModule*>(mod)->addCam();
        }
        void addCam();

        static void addDepthShapeCallback(Module* mod, 
                                      const std::vector<std::string>& vals) {
            dynamic_cast<DepthGroupModule*>(mod)->addDepthShape(vals[0]);
        }
        void addDepthShape(const std::string& name);

        static void addDepthMeshCallback(Module* mod,
                                      const std::vector<std::string>& vals) {
            dynamic_cast<DepthGroupModule*>(mod)->addDepthMesh(vals[0]);
        }
        void addDepthMesh(const std::string& name);

        static void addDepthGroupCallback(Module* mod,
                                      const std::vector<std::string>& vals) {
            dynamic_cast<DepthGroupModule*>(mod)->addDepthGroup(vals[0]);
        }
        void addDepthGroup(const std::string& name);

    private:
};


#endif

