/***************************************************************************
 *  RevealedCutModule.cpp
 *  Part of Reveal 
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include "RevealedCutModule.hpp" 

#include <iostream>
#include <glm/gtx/quaternion.hpp>

#include "../Reveal.hpp"

#include "../geoms/Geometry.hpp"

using namespace std;

RevealedCutModule::RevealedCutModule(): ShapeModule(), RevealedModule() {
    m_type="Cut";
    m_name="cut";
    m_cutWin=NULL;
    m_updateOnce=false;
	m_shapeGeom = Reveal::GEOM_BOX;

    Reveal::getInstance()->getGeom(Reveal::GEOM_BOX)->registerModule(this);

    m_layersNb=1;
    int nbBytes = m_texWidth*m_texHeight*m_layersNb*3;
    m_layersData = new char[nbBytes];
    m_coordImage = new float[m_texWidth*m_texHeight*2];

    addNameAttribute();
    addRemoveAttribute();
    addVisibleAttribute();
    addVisibleFromAttribute();
    addRevealedByAttribute();

    //add cut attributes
    addAttribute(new Attribute("refresh_windows", 
                                Attribute::ACTION_ATTRIBUTE,
                                refreshWindowsCallback, this));
    addAttribute(new Attribute("window", 
                               Attribute::STRING_ATTRIBUTE,
                               windowCallback, this, 1));

    addAttribute(new Attribute("selection_position", 
                               Attribute::INT_ATTRIBUTE,
                               selPosCallback, this, 2));
    addAttribute(new Attribute("selection_size", 
                               Attribute::INT_ATTRIBUTE,
                               selSizeCallback, this, 2));
    addAttribute(new Attribute("update_once", 
                               Attribute::BOOL_ATTRIBUTE,
                               updateOnceCallback, this, 1));
    refreshWindows();

    addTransformationAttributes();
    addRevealedAttributes();
    addRevealedOutputAttributes();
    m_localInsideAxes=true;
	m_insideDensityAlpha=0;
	m_insideGradientAlpha=0;
	m_insideTextureAlpha=1.0;
	m_insideVisible=true;
    m_insideTextureBlend=false;

    Reveal::getInstance()->addUpdateObserver(this);
}

RevealedCutModule::~RevealedCutModule() {
    Reveal::getInstance()->getGeom(Reveal::GEOM_BOX)->unregisterModule(this);
    Reveal::getInstance()->removeUpdateObserver(this);
}

void RevealedCutModule::refreshWindows() {
    CutWindowsManager* winsMan = CutWindowsManager::getInstance();
    winsMan->updateWindowsList();
	m_windowNames.clear();
	m_windowNames.push_back("none");
	vector<CutWindow*>& wins = winsMan->editWindowList();
	vector<CutWindow*>::iterator itWin = wins.begin();
	for(; itWin!=wins.end(); ++itWin) {
		m_windowNames.push_back(((*itWin)->getName()).c_str());
	}
	m_attributesMap["window"]->editStringValuesChoices().assign(1,m_windowNames);
	m_attributesMap["window"]->initStrings(vector<string>(1, m_windowNames[0]));
}

void RevealedCutModule::setWindow(const std::string& winName) {
    CutWindow* win = CutWindowsManager::getInstance()->getWindow(winName);
    m_cutWin=win;
    if(m_cutWin) {
        m_nbPixPerRow = m_cutWin->computeNbPixPerRow(200, 100);
        vector<int> coords(2,0);
        m_attributesMap["selection_position"]->initInts(coords);
        coords[0]=m_cutWin->getWidth();
        coords[1]=m_cutWin->getHeight();
        m_attributesMap["selection_size"]->initInts(coords);
        m_winX=0;
        m_winY=0;
        m_winW=m_cutWin->getWidth();
        m_winH=m_cutWin->getHeight();
        recomputePixels();
		m_initTex=false;
    }
}

void RevealedCutModule::selPos(const int& posX, const int& posY) {
    m_winX=posX;
    m_winY=posY;
    recomputePixels();
}

void RevealedCutModule::selSize(const int& sizeX, const int& sizeY) {
    m_winW=sizeX;
    m_winH=sizeY;
    recomputePixels();
}

void RevealedCutModule::updateOnce(bool upOn) {
    m_updateOnce=upOn;
    m_updateOnceDone=false;
}

void RevealedCutModule::recomputePixels() {
    //if window attached, recompute correspondance between pixel coordinates 
    if(m_cutWin) {
        //get new pixperrow in window
        m_nbPixPerRow = m_cutWin->computeNbPixPerRow(m_winW, 
                                                     m_winH);
        //clear existing coordinates 
        vector<float> coord(2,0);
        vector<float> start(2,0);
        vector<bool> restart(2,false);
        vector<float> stepY(2,0);
        vector<float> stepX(2,0);

        //compute the pixels offset and steps
        start[0]=m_winX;
        start[1]=m_winY;
        restart[0]=true;
        stepX[0]=float(m_winW)/float(m_texWidth);
        stepY[1]=float(m_winH)/float(m_texHeight);

        vector<int> intCoord(2, 0);
        coord[0]=start[0];
        coord[1]=start[1];

        //compute coordinates according to shape
        for(int py = 0; py < m_texHeight; py++) {
            coord[0]=restart[0]?start[0]:coord[0];
            coord[1]=restart[1]?start[1]:coord[1];
            for(int px = 0; px < m_texWidth; px++) {
                intCoord[0]=coord[0]; 
                intCoord[1]=coord[1];
                coord[0]+=stepX[0];
                coord[1]+=stepX[1];
                m_coordImage[(m_texWidth*py+px)*2]=float(intCoord[0]) 
                                 / float(m_cutWin->getWidth());
                m_coordImage[(m_texWidth*py+px)*2+1]=float(intCoord[1]) 
                                 / float(m_cutWin->getHeight());
            }
            coord[0]+=stepY[0];
            coord[1]+=stepY[1];
        }
        //ask for reinitializing texture uniform
        map<int, bool>::iterator itUp = m_coordsUpMap.begin();
        for(; itUp!=m_coordsUpMap.end(); ++itUp) {
            itUp->second=true;
        }
    }
    m_updateOnceDone=false;
}

void RevealedCutModule::draw(const int& contextID, 
                             const Reveal::REVIL_PROGRAM& prog, 
                             map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                             const unsigned int& comp){

    RevealedModule::draw(contextID, prog, uniforms, comp);
    if(m_cutWin && prog==Reveal::RENDERPROG) {
        glUniform1i(uniforms[Reveal::INSIDETEX], 2);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_3D, m_insideIDMap[contextID]);
        if(!m_updateOnce || (m_updateOnce && !m_updateOnceDone)) {
            glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
			if(!m_initTex) {
				glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA8, 
							 m_nbPixPerRow, m_cutWin->getHeight(), 
							 m_layersNb, 0,
							 GL_BGRA, GL_UNSIGNED_BYTE,
							 m_cutWin->grabImage());
				m_initTex=true;
			}
			else {
				glTexSubImage3D(GL_TEXTURE_3D, 0, 0, 0, 0,
							 m_nbPixPerRow, m_cutWin->getHeight(), m_layersNb, 
							 GL_BGRA, GL_UNSIGNED_BYTE,
							 m_cutWin->grabImage());
			}
            m_updateOnceDone=true;
        }

		/*
        glUniform1i(uniforms[Reveal::COORDSTEX], 4);
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, m_coordsIDMap[contextID]);
        if(m_coordsUpMap[contextID]) {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F,
                         m_texWidth, m_texHeight,
                         0, GL_RG, GL_FLOAT,
                         m_coordImage);
            m_coordsUpMap[contextID]=false;
        }*/
    }
}

void RevealedCutModule::update(const int& timeDiffMs) {
    if(m_cutWin) {
        m_cutWin->releaseImage();
    }
}

//Window manager
#ifdef LINUX
CutWindow::CutWindow(CutWindowsManager* man, 
                     Display* disp, 
                     const Window& win): m_winsMan(man), 
                                         m_disp(disp), 
                                         m_win(win) { 
    Window root;
    unsigned int bw, d, w, h;
    XGetGeometry(m_disp, m_win, &root, &m_posX, &m_posY, 
                 &w, &h, &bw, &d);
    m_width=w;
    m_height=h;
    m_offsetX=0;
    m_offsetY=0;
    m_pixPerRow=m_width;
    m_grabbed=false;
    m_init=false;
    m_defaultImgData = new uchar[m_width*m_height*3];
}
#endif

void CutWindow::releaseImage() {
#ifdef LINUX
    if(m_grabbed) {
        if(m_ximg) {
            //XDestroyImage(m_ximg);
        }
        m_grabbed=false;
    }
#endif
}

uchar* CutWindow::grabImage() {
#ifdef LINUX
    if(!m_grabbed) {
        //XImage* newImg = XGetImage(m_disp, m_win, 
        //                           0, 0, 
        //                           m_width, m_height, 
        //                           AllPlanes, ZPixmap);
        XImage* newImg=NULL;
        if(!m_init) {
            newImg = XGetImage(m_disp, m_win, 
                               0, 0, 
                               m_width, m_height, 
                               AllPlanes, ZPixmap);
            if(newImg) {
                m_ximg=newImg;
                m_init=true;
            }
        }
        else {
            newImg = XGetSubImage(m_disp, m_win, 
                                  0, 0, 
                                  m_width, m_height, 
                                  AllPlanes, ZPixmap,
                                  m_ximg, 0, 0);
        }
        if(newImg) {
            m_imgData=(uchar*)m_ximg->data;
        }
        else {
            m_imgData=m_defaultImgData;
        }
        m_grabbed=true;
    }
#endif
    return m_imgData;
}

int CutWindow::computeNbPixPerRow(const int& srcW, const int& srcH) {
#ifdef LINUX
    XWindowAttributes attrs;
    XGetWindowAttributes(m_disp, m_win, &attrs);
    m_width=attrs.width;
    m_height=attrs.height;
    m_pixPerRow=m_width;
    return m_width;
#else
    return 100;
#endif
}

CutWindowsManager* CutWindowsManager::getInstance() {
    static CutWindowsManager winsMan;
    return &winsMan;
}

CutWindowsManager::CutWindowsManager() {
#ifdef LINUX
    m_disp = XOpenDisplay(NULL);
#endif
}

CutWindowsManager::~CutWindowsManager() {
#ifdef LINUX
    XCloseDisplay(m_disp);
#endif
}

void CutWindowsManager::updateWindowsList() {
    map<string, CutWindow*> prevWins;
    vector<CutWindow*>::iterator itWin=m_windowList.begin();
    for(; itWin!=m_windowList.end(); ++itWin) {
        prevWins[(*itWin)->getName()]=(*itWin);
    }
    m_windowList.clear();
    unsigned long len;

#ifdef LINUX
    Window *list = getWinList(m_disp, &len);
    for(int i=0; i<(int)len; i++) {
        char* name = getWinName(m_disp, list[i]);
        CutWindow* newWin=NULL;
        if(prevWins.find(string(name))!=prevWins.end()) {
            newWin = prevWins[string(name)];
        }
        else {
            newWin = new CutWindow(this, m_disp, list[i]);
        }
        newWin->setName(string(name));
        m_windowList.push_back(newWin);
        free(name);
    }
    XFree(list);
#endif

}

#ifdef LINUX
char* CutWindowsManager::getWinName(Display* disp, Window win) {
    Atom prop = XInternAtom(disp,"_NET_WM_NAME",false);
	Atom utf8Atom = XInternAtom(disp,"UTF8_STRING",false);
    Atom type;
    int form;
    unsigned long remain, len;
    unsigned char *list;

    if (XGetWindowProperty(disp,win,prop,0,65536,false,utf8Atom,
            &type,&form,&len,&remain,&list) != Success) {
        cout<<"Error getting window name"<<endl;
        return NULL;
    }
    return (char*)list;
}

Window* CutWindowsManager::getWinList(Display* disp, unsigned long* len) {
    Atom prop = XInternAtom(disp,"_NET_CLIENT_LIST",false);
    Atom type;
    int form;
    unsigned long remain;
    unsigned char *list;
    if(XGetWindowProperty(disp,XDefaultRootWindow(disp), 
                          prop,0,1024,false,XA_WINDOW,
                          &type,&form,len,&remain,&list) != Success) {
        cout<<"Error getting windows list"<<endl;
        return 0;
    }
    return (Window*)list;
}
#endif

