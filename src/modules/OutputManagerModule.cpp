/***************************************************************************
 *  OutputManagerModule.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "../Reveal.hpp"
#include "OutputManagerModule.hpp"

using namespace std;

OutputManagerModule::OutputManagerModule():GroupModule() {
    m_type="OutputManager";
    m_name="outputs";
    addAttribute(new Attribute("add_osc_output", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addOscOutputCallback, this, 1,
                                Attribute::LOCAL));
    m_attributesMap["add_osc_output"]->initStrings(vector<string>(1, "osc"));
    addAttribute(new Attribute("add_pd_output", 
                                Attribute::ACTION_STRING_ATTRIBUTE,
                                addPdOutputCallback, this, 1,
                                Attribute::LOCAL));
    m_attributesMap["add_pd_output"]->initStrings(vector<string>(1, "patch"));
}

OutputManagerModule::~OutputManagerModule() {
    
}

void OutputManagerModule::addOscOutput(const string& name) {
    OutputOscModule *chi = new OutputOscModule();
    addChild(chi);
    chi->getAttribute("name")->setStrings(vector<string>(1,name));
    Reveal::getInstance()->refreshModules();
}

void OutputManagerModule::addPdOutput(const string& name) {
    OutputPdModule *chi = new OutputPdModule();
    addChild(chi);
    chi->getAttribute("name")->setStrings(vector<string>(1,name));
    Reveal::getInstance()->refreshModules();
}

