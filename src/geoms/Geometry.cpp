/***************************************************************************
 *  Geometry.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "Geometry.hpp"
#include <iostream>

using namespace std;

Geometry::Geometry():m_hasTexCoords(false), m_points(false), m_isModel(false) {
    m_geomID=(Reveal::GEOM_ID)-1;
}

Geometry::~Geometry() {}

void Geometry::draw(const int& contextID,
                    const Reveal::REVIL_PROGRAM& prog, 
                    map<Reveal::REVIL_UNIFORM, GLint>& uniforms,
                    const int& visibilityMask) {

    if(m_modules.size()==0) {
        return;
    }

    if(m_contexts.find(contextID)==m_contexts.end()) {
        ContextGeometry& cont = m_contexts[contextID];
        cont.m_refresh=true;
        glGenVertexArrays(1, &cont.m_vertexArrayID);
        glBindVertexArray(cont.m_vertexArrayID); 
        glGenBuffers(1, &cont.m_vertexBuffer);
        glGenBuffers(1, &cont.m_indexBuffer);
    }
    if(m_contexts[contextID].m_refresh) {
        ContextGeometry& cont = m_contexts[contextID];
        glBindBuffer(GL_ARRAY_BUFFER, cont.m_vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, 
                     m_vertexBufferData.size()*sizeof(GLfloat),
                     &m_vertexBufferData[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cont.m_indexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 
                     m_indexBufferData.size()*sizeof(GLuint), 
                     &m_indexBufferData[0], GL_STATIC_DRAW);
        cont.m_refresh=false;
    }

    ContextGeometry& cont = m_contexts[contextID];

    //load vertices
    GLintptr posOff = 0;
    GLintptr texOff = 3*sizeof(float);
    GLsizei stride = 0;
    glBindVertexArray(cont.m_vertexArrayID); 
    glBindBuffer(GL_ARRAY_BUFFER, cont.m_vertexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cont.m_indexBuffer);

    glEnableVertexAttribArray(0);
	stride= 5*sizeof(float);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride,(GLvoid*)texOff);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (GLvoid*)posOff);

    //draw geomodules attached to that geometry 
    vector<GeomModule*>::const_iterator itGeom=m_modules.begin();
    for(; itGeom!=m_modules.end(); ++itGeom) {
        if((*itGeom)->isVisible() 
                && (((*itGeom)->getVisibilityMask() & visibilityMask)
                    || prog==Reveal::PREVIEWPROG )) {
            //for each component
            unsigned int start=0;
            for(unsigned int c=0; c<m_compoSizes.size(); ++c) {
                //load uniforms and model mat
                (*itGeom)->draw(contextID, prog, uniforms, c);
                //draw triangles
                if(m_points) {
                    glDrawElements(GL_POINTS, m_compoSizes[c], 
                           GL_UNSIGNED_INT,(GLvoid*)(start*sizeof(GLuint)));
                }
                else {
                    glDrawElements(GL_TRIANGLES, m_compoSizes[c], 
                           GL_UNSIGNED_INT,(GLvoid*)(start*sizeof(GLuint)));
                }
                start+=m_compoSizes[c];
            }
        }
    }
    glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

void Geometry::drawDirect(const int& contextID) {
    if(m_contexts.find(contextID)==m_contexts.end()) {
        ContextGeometry& cont = m_contexts[contextID];
        glGenVertexArrays(1, &cont.m_vertexArrayID);
        glBindVertexArray(cont.m_vertexArrayID); 
        glGenBuffers(1, &cont.m_vertexBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, cont.m_vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, 
                     m_vertexBufferData.size()*sizeof(GLfloat),
                     &m_vertexBufferData[0], GL_STATIC_DRAW);
        glGenBuffers(1, &cont.m_indexBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cont.m_indexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 
                     m_indexBufferData.size()*sizeof(GLuint), 
                     &m_indexBufferData[0], GL_STATIC_DRAW);
    }

    ContextGeometry& cont = m_contexts[contextID];

    //load vertices
    glBindBuffer(GL_ARRAY_BUFFER, cont.m_vertexBuffer);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float),(void*)(3*sizeof(float)));
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cont.m_indexBuffer);

    //draw triangles
    glDrawElements(GL_TRIANGLES, m_indexBufferData.size(), 
                   GL_UNSIGNED_INT, 0);

    glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

void Geometry::registerModule(GeomModule* geom) {
    unsigned int id=0;
    while(m_modulesMap.find(id)!=m_modulesMap.end()) {
        id++;
    }
    geom->setGeomID(id);
    m_modulesMap[id]=geom;
    m_modules.push_back(geom);
}

void Geometry::unregisterModule(GeomModule* geom) {
    m_modulesMap.erase(geom->getGeomID());
    vector<GeomModule*>::iterator itGeom=m_modules.begin();
    for(; itGeom!=m_modules.end();) {
        if((*itGeom)->getGeomID()==geom->getGeomID()) {
            itGeom=m_modules.erase(itGeom);
        }
        else {
             ++itGeom;
        }
    }
}

void Geometry::refresh() {
    map<int, ContextGeometry>::iterator itCo=m_contexts.begin();
    for(; itCo!=m_contexts.end(); ++itCo) {
        itCo->second.m_refresh=true;
    }
}

