/***************************************************************************
 *  SphereGeometry.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "SphereGeometry.hpp"

using namespace std;

SphereGeometry::SphereGeometry(): Geometry() {
    int nbHor=20;
    int nbVert=10;
    float rad=0.5;
    float stepH = (M_PI*2.0/float(nbHor));
    float stepV = (M_PI/float(nbVert));

    //top vertex
    m_vertexBufferData.push_back(0);
        m_vertexBufferData.push_back(rad);
            m_vertexBufferData.push_back(0);
				m_vertexBufferData.push_back(0);
					m_vertexBufferData.push_back(0);
    //bottom vertex
    m_vertexBufferData.push_back(0);
        m_vertexBufferData.push_back(-rad);
            m_vertexBufferData.push_back(0);
				m_vertexBufferData.push_back(0);
					m_vertexBufferData.push_back(0);

    //other vertices and indices
    for(int v=1; v<nbVert+1; ++v) {
        float posY = rad*cos(float(v)*stepV);
        float out = abs(rad * sin(float(v)*stepV));
        for(int h=0; h<nbHor; ++h) {
            float angleH = float(h)*stepH;
            m_vertexBufferData.push_back(out*cos(angleH));
            m_vertexBufferData.push_back(posY);
            m_vertexBufferData.push_back(out*sin(angleH));
				m_vertexBufferData.push_back(0);
					m_vertexBufferData.push_back(0);

            if(v==nbVert) { //bottom row
                m_indexBufferData.push_back(2+(v-1)*nbHor+h);
                m_indexBufferData.push_back(2+(v-1)*nbHor+(h+1)%nbHor);
                m_indexBufferData.push_back(1);
            }
            else {//other rows (2 triangles)
                if(v==1) { //top row
                    m_indexBufferData.push_back(0);
                    m_indexBufferData.push_back(2+(v-1)*nbHor+(h+1)%nbHor);
                    m_indexBufferData.push_back(2+(v-1)*nbHor+h);
                }
                m_indexBufferData.push_back(2+(v-1)*nbHor+h);
                m_indexBufferData.push_back(2+(v-1)*nbHor+(h+1)%nbHor);
                m_indexBufferData.push_back(2+(v)*nbHor+(h+1)%nbHor);

                m_indexBufferData.push_back(2+(v-1)*nbHor+h);
                m_indexBufferData.push_back(2+(v)*nbHor+(h+1)%nbHor);
                m_indexBufferData.push_back(2+(v)*nbHor+h);
            }
        }
    }
    m_compoSizes.push_back(m_indexBufferData.size());
}

SphereGeometry::~SphereGeometry() {}

