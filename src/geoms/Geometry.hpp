/***************************************************************************
 *  Geometry.hpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef Geometry_h
#define Geometry_h

#include <glm/vec3.hpp>
#include <map>
#include <vector>

#include "../modules/GeomModule.hpp"
#include "../Reveal.hpp"

struct ContextGeometry {
    GLuint m_vertexBuffer;
    GLuint m_vertexArrayID;
    GLuint m_indexBuffer;
    bool m_refresh;
};

class Geometry {
    public:
        Geometry();
        virtual ~Geometry();
        virtual void draw(const int& contextID,
                          const Reveal::REVIL_PROGRAM&, 
                          std::map<Reveal::REVIL_UNIFORM, GLint>&,
                          const int& visibilityMask);
        virtual void drawDirect(const int& contextID);
        
        void registerModule(GeomModule*);
        void unregisterModule(GeomModule*);
        inline const std::vector<GeomModule*>& getModules(){return m_modules;}

        inline void setGeomID(const Reveal::GEOM_ID& id) {m_geomID=id;}
        inline const Reveal::GEOM_ID& getGeomID() {return m_geomID;}

        std::vector<GLfloat>& editVertexBufferData(){return m_vertexBufferData;}
        std::vector<GLuint>& editIndexBufferData(){return m_indexBufferData;}
        std::vector<int>& editCompoSizes(){return m_compoSizes;}
        void refresh();

        inline void hasTexCoords(bool tc){m_hasTexCoords=tc;}

        inline void setIsModel(bool m){m_isModel=m;}
        inline bool getIsModel(){return m_isModel;}

    protected:
        Reveal::GEOM_ID m_geomID;
        bool m_hasTexCoords;
        bool m_points;
        bool m_isModel;

        std::vector<GeomModule*> m_modules;
        std::map<unsigned int, GeomModule*> m_modulesMap;

        std::vector<GLfloat> m_vertexBufferData;
        std::vector<GLuint> m_indexBufferData;
        std::vector<int> m_compoSizes;

        std::map<int, ContextGeometry> m_contexts;
};

#endif
