/***************************************************************************
 *  ConeGeometry.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "ConeGeometry.hpp"

using namespace std;

ConeGeometry::ConeGeometry(): Geometry() {

    int nbEdges=10;
    int length=1;

    //add the vertices 
    m_vertexBufferData.push_back(0);
    m_vertexBufferData.push_back(0.5);
    m_vertexBufferData.push_back(0);
				m_vertexBufferData.push_back(0);
					m_vertexBufferData.push_back(0);
    float angle=0;
    float angleStep=M_PI*2.0/float(nbEdges);
    for(int v=0; v<nbEdges; ++v, angle+=angleStep) {
        glm::vec4 pnt = glm::vec4(cos(angle)*length/2.0, 
                                  -0.5,
                                  sin(angle)*length/2.0, 
                                  1);
        m_vertexBufferData.push_back(pnt[0]);
        m_vertexBufferData.push_back(pnt[1]);
        m_vertexBufferData.push_back(pnt[2]);
				m_vertexBufferData.push_back(0);
					m_vertexBufferData.push_back(0);
    }

    //indices
    //  sides
    for(int v=1; v<nbEdges+1; ++v) {
        m_indexBufferData.push_back(0);
        m_indexBufferData.push_back((v+1<nbEdges+1)?v+1:1);
        m_indexBufferData.push_back(v);
    }
    //  bottom
    for(int v=2; v<nbEdges; ++v) {
        m_indexBufferData.push_back(1);
        m_indexBufferData.push_back(v);
        m_indexBufferData.push_back(v+1);
    }


    m_compoSizes.push_back(m_indexBufferData.size());
}

ConeGeometry::~ConeGeometry() {}

