/***************************************************************************
 *  CamGeometry.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "CamGeometry.hpp"

using namespace std;

CamGeometry::CamGeometry(const int& width, const int& height): Geometry() {
    m_points=false;
    setSize(width, height);
}

CamGeometry::~CamGeometry() {}

void CamGeometry::setSize(const int& width, const int& height) {
    m_vertexBufferData.clear();
    m_indexBufferData.clear();
    m_compoSizes.clear();

    for(int r=0; r<height; r++) {
        for(int c=0; c<width; c++) {
            m_vertexBufferData.push_back(GLfloat(c));
            m_vertexBufferData.push_back(GLfloat(r));
            m_vertexBufferData.push_back(10.0);
				m_vertexBufferData.push_back(0);
					m_vertexBufferData.push_back(0);
        }
    }
    for(int r=0; r<height-1; r++) {
        for(int c=0; c<width-1; c++) {
            if(m_points) {
                m_indexBufferData.push_back(width*r+c);
            }
            else {
                m_indexBufferData.push_back(width*r+c);
                m_indexBufferData.push_back(width*(r)+c+1);
                m_indexBufferData.push_back(width*(r+1)+c);

                m_indexBufferData.push_back(width*(r+1)+c+1);
                m_indexBufferData.push_back(width*(r+1)+c);
                m_indexBufferData.push_back(width*(r)+c+1);
            }
        }
    }

    m_compoSizes.push_back(m_indexBufferData.size());
	for(auto& cont : m_contexts) {
		cont.second.m_refresh=true;
	}
}

