/***************************************************************************
 *  FrameGeometry.cpp
 *  Part of Reveal
 *  2015-  Florent Berthaut
 *  hitmuri.net
 ****************************************************************************/
/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "FrameGeometry.hpp"

using namespace std;

FrameGeometry::FrameGeometry(): Geometry() {

    float inter=0.4;
    for(float x=0; x<2; ++x) {
        m_vertexBufferData.push_back(0.1*(x-0.5));
            m_vertexBufferData.push_back(0);
                m_vertexBufferData.push_back(0.5);
					m_vertexBufferData.push_back(0);
						m_vertexBufferData.push_back(0);
        m_vertexBufferData.push_back(inter*2*(x-0.5));
            m_vertexBufferData.push_back(inter);
                m_vertexBufferData.push_back(0.45);
					m_vertexBufferData.push_back(0);
						m_vertexBufferData.push_back(0);
        m_vertexBufferData.push_back(-0.5+x);
            m_vertexBufferData.push_back(0.5);
                m_vertexBufferData.push_back(0);
					m_vertexBufferData.push_back(0);
						m_vertexBufferData.push_back(0);
        m_vertexBufferData.push_back(inter*2*(x-0.5));
            m_vertexBufferData.push_back(inter);
                m_vertexBufferData.push_back(-0.45);
					m_vertexBufferData.push_back(0);
						m_vertexBufferData.push_back(0);

        m_vertexBufferData.push_back(0.1*(x-0.5));
            m_vertexBufferData.push_back(0);
                m_vertexBufferData.push_back(-0.5);
					m_vertexBufferData.push_back(0);
						m_vertexBufferData.push_back(0);
        m_vertexBufferData.push_back(inter*2*(x-0.5));
            m_vertexBufferData.push_back(-inter);
                m_vertexBufferData.push_back(-0.45);
					m_vertexBufferData.push_back(0);
						m_vertexBufferData.push_back(0);
        m_vertexBufferData.push_back(-0.5+x);
            m_vertexBufferData.push_back(-0.5);
                m_vertexBufferData.push_back(0);
					m_vertexBufferData.push_back(0);
						m_vertexBufferData.push_back(0);
        m_vertexBufferData.push_back(inter*2*(x-0.5));
            m_vertexBufferData.push_back(-inter);
                m_vertexBufferData.push_back(0.45);
					m_vertexBufferData.push_back(0);
						m_vertexBufferData.push_back(0);
    }

    //around faces
    for(int v=0; v<8; v++) {
        if(v<7) {
            m_indexBufferData.push_back(v%16);
                m_indexBufferData.push_back((v+9)%16);
                    m_indexBufferData.push_back((v+1)%16);
        }
        m_indexBufferData.push_back(v%16);
            m_indexBufferData.push_back((v+8)%16);
                m_indexBufferData.push_back((v+9)%16);
    }

    m_indexBufferData.push_back(15);
            m_indexBufferData.push_back(8);
        m_indexBufferData.push_back(0);


    //extremities
    m_indexBufferData.push_back(0);
        m_indexBufferData.push_back(1);
            m_indexBufferData.push_back(7);
    m_indexBufferData.push_back(8);
        m_indexBufferData.push_back(15);
            m_indexBufferData.push_back(9);

    m_indexBufferData.push_back(12);
        m_indexBufferData.push_back(11);
            m_indexBufferData.push_back(13);
    m_indexBufferData.push_back(4);
        m_indexBufferData.push_back(5);
            m_indexBufferData.push_back(3);


    //sides faces
    for(int x=0; x<2; x++) {
        m_indexBufferData.push_back(2+x*8);
            m_indexBufferData.push_back((x==0)?7:1+x*8);
                m_indexBufferData.push_back((x==0)?1:7+x*8);

        m_indexBufferData.push_back(2+x*8);
            m_indexBufferData.push_back((x==0)?6:7+x*8);
                m_indexBufferData.push_back((x==0)?7:6+x*8);

        m_indexBufferData.push_back(2+x*8);
            m_indexBufferData.push_back((x==0)?5:6+x*8);
                m_indexBufferData.push_back((x==0)?6:5+x*8);

        m_indexBufferData.push_back(2+x*8);
            m_indexBufferData.push_back((x==0)?3:5+x*8);
                m_indexBufferData.push_back((x==0)?5:3+x*8);
    }


    m_compoSizes.push_back(m_indexBufferData.size());
}

FrameGeometry::~FrameGeometry() {}

