#include "Ringbuffer.hpp"
#include <iostream>
ringbuffer* ringbuffer_create(size_t sz){
    int power_of_two;
    ringbuffer* rb;
    rb = (ringbuffer*) malloc(sizeof(ringbuffer));

    if(rb == NULL){
        return NULL;
    }

    //for(power_of_two = 1; 1 << power_of_two < sz; power_of_two++);
    rb->size = sz;//1 << power_of_two;
    rb->size_mask = rb->size;
    rb->write_ptr = 0;
    rb->read_ptr = 0;
    rb->buf = (float *) malloc (rb->size * sizeof(float));
	if(rb->buf == NULL) {
	    free (rb);
	    return NULL;
    }
    memset(rb->buf, 0, rb->size * sizeof(float));

	rb->mlocked = 0;
	rb->writed = 0;
	
	return rb;
}

void ringbuffer_free(ringbuffer *rb){
    free(rb->buf);
    free(rb);
}

int ringbuffer_mlock(ringbuffer *rb){
    rb->mlocked = 1;
    return 0;
}

void ringbuffer_reset(ringbuffer *rb){
    rb->read_ptr = 0;
    rb->write_ptr = 0;
    memset(rb->buf, 0, rb->size);
}

void ringbuffer_reset_size(ringbuffer *rb, size_t sz){
    rb->size = sz;
    rb->size_mask = rb->size;
    //rb->size_mask -= 1;
    rb->read_ptr = 0;
    rb->write_ptr = 0;
}

size_t ringbuffer_read_space(const ringbuffer *rb){
    size_t w,r;

    w = rb->write_ptr;
    r = rb->read_ptr;

    if(w>r){
        return w - r;
    }
    else if(w==r){
		if(rb->writed){
			return rb->size;
		}
		else{
			return 0;
		}
        //return (w - r + rb->size) & rb->size_mask;
    }
    else{
		return rb->size - (r - w);
	}
}

size_t ringbuffer_get_write_vector(const ringbuffer *rb){
    size_t w, r;

	w = rb->write_ptr;
	r = rb->read_ptr;

	if (w > r) {
		return ((r - w + rb->size) & rb->size_mask) - 1;
	} else if (w < r) {
		return (r - w) - 1;
	} else {
		return rb->size - 1;
	}
}

size_t ringbuffer_read(ringbuffer *rb, float *dest, size_t cnt){
    size_t free_cnt;
    size_t cnt2;
    size_t to_read;
    size_t n1,n2;

    free_cnt = ringbuffer_read_space(rb);
    //std::cout<<free_cnt<<std::endl;
    if(free_cnt == 0){
        return 0;
    }
    
    if(free_cnt < cnt){
		to_read = free_cnt;
	}
	else{
		to_read = cnt;
	}
	
    //to_read = cnt > free_cnt ? free_cnt : cnt;
    cnt2 = rb->read_ptr + to_read;

    if(cnt2 > rb->size){
        n1 = rb->size - rb->read_ptr;
        n2 = to_read - n1;//cnt2 & rb->size_mask;
    }
    else{
        n1 = to_read;
        n2 = 0;
    }
    
    memcpy(dest, &(rb->buf[rb->read_ptr]), n1*sizeof(float));

    //rb->read_ptr = (rb->read_ptr + n1) & rb->size_mask;
    rb->read_ptr = rb->read_ptr + n1;
    if(rb->read_ptr >= rb->size){
		rb->read_ptr = rb->read_ptr - rb->size;
	} 

    if(n2){
        memcpy(dest + n1, &(rb->buf[rb->read_ptr]), n2*sizeof(float));
        //rb->read_ptr = (rb->read_ptr + n2) & rb->size_mask;
        rb->read_ptr = rb->read_ptr + n2;
		if(rb->read_ptr >= rb->size){
			rb->read_ptr = rb->read_ptr - rb->size;
		}
    }
    
    if(rb->read_ptr == rb->write_ptr){
		rb->writed = 0;
	}

    return to_read;
}

size_t ringbuffer_write_space (const ringbuffer * rb)
{
	size_t w, r;

	w = rb->write_ptr;
	r = rb->read_ptr;

	if (w > r) {
		return rb->size -(w - r);
		//return ((r - w + rb->size) & rb->size_mask) - 1;
	} else if (w < r) {
		return (r - w);// - 1;
	} else {
		if(!(rb->writed)){
			return rb->size;
		}
		else{
			return 0;//rb->size - 1;
		}
	}
}

size_t ringbuffer_peek(ringbuffer *rb, float *dest, size_t cnt){
    size_t free_cnt;
	size_t cnt2;
	size_t to_read;
	size_t n1, n2;
	size_t tmp_read_ptr;

	tmp_read_ptr = rb->read_ptr;
    free_cnt = ringbuffer_read_space(rb);
	if (free_cnt == 0) {
		return 0;
	}

	to_read = cnt > free_cnt ? free_cnt : cnt;

	cnt2 = tmp_read_ptr + to_read;

	if (cnt2 > rb->size) {
		n1 = rb->size - tmp_read_ptr;
		n2 = cnt2 & rb->size_mask;
	} else {
		n1 = to_read;
		n2 = 0;
	}

	memcpy(dest, &(rb->buf[tmp_read_ptr]), n1);
	tmp_read_ptr = (tmp_read_ptr + n1) & rb->size_mask;

	if (n2) {
		memcpy(dest + n1, &(rb->buf[tmp_read_ptr]), n2);
	}

	return to_read;
}

size_t ringbuffer_write(ringbuffer *rb, const float *src, size_t cnt){
    size_t free_cnt;
	size_t cnt2;
	size_t to_write;
	size_t n1, n2;

    free_cnt = ringbuffer_write_space(rb);
	if (free_cnt == 0) {
		return 0;
	}
	
	if(free_cnt < cnt){
		to_write = free_cnt;
	}
	else{
		to_write = cnt;
	}
	//to_write = cnt > free_cnt ? free_cnt : cnt;

	cnt2 = rb->write_ptr + to_write;

	if (cnt2 > rb->size) {
		n1 = rb->size - rb->write_ptr;
		n2 = to_write - n1;//cnt2 & rb->size_mask;
	} else {
		n1 = to_write;
		n2 = 0;
	}

	memcpy(&(rb->buf[rb->write_ptr]), src, n1*sizeof(float));
	//rb->write_ptr = (rb->write_ptr + n1) & rb->size_mask;
	rb->write_ptr = rb->write_ptr + n1;

    if(rb->write_ptr >= rb->size){
		rb->write_ptr = rb->write_ptr - rb->size;
	}


	if(n2){
		memcpy(&(rb->buf[rb->write_ptr]), src + n1, n2*sizeof(float));
		//rb->write_ptr = (rb->write_ptr + n2) & rb->size_mask;
		rb->write_ptr = rb->write_ptr + n2;
		if(rb->write_ptr >= rb->size){
			rb->write_ptr = rb->write_ptr - rb->size;
		}
	}

	rb->writed = 1;
	return to_write;
}

void ringbuffer_read_advance(ringbuffer *rb, size_t cnt){
    size_t temp = (rb->read_ptr + cnt) & rb->size_mask;
    rb->read_ptr = temp;
}

void ringbuffer_write_advance(ringbuffer *rb, size_t cnt){
    size_t temp = (rb->write_ptr + cnt) & rb->size_mask;
    rb->write_ptr = temp;
}

