/***************************************************************************
 *	AudioManager.hpp
 *	Part of Rivill
 *	2015-  Florent Berthaut / Luka Claeys
 *	https://gitlab.univ-lille.fr/mint/rivill
 ****************************************************************************/
/*
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef AudioManager_h
#define AudioManager_h

#include <rtaudio/RtAudio.h>
#include <cstring>
#include <math.h>
#include <unistd.h>
#include <mutex>
#include <thread>
#include <queue>

#include <iostream>
#include <fstream>

#include <sys/time.h>
#include "Ringbuffer.hpp"

using namespace std;


//#define POWER32 pow(2,31)


class AudioManager {
	public:

		struct CallbackData {
			unsigned int	nRate;
			unsigned int nFrames;
			unsigned int	nChannel;
			float ratioSampleFreq;
			float ratioFreqSample;
			ringbuffer *ringBuffer;

			float *buffer; 
			int bufSize;

			float *mixBuffer;
			int mixSize;

			int bufCounter;
			int nbBufs;
		};

		static AudioManager* getInstance();
		~AudioManager();
		void init();
		bool changeBuf(float* outputBuf, float maxSinValue);
		int getRate();
		int getBufferSize();
		int getMixSize();

		inline void startStream(){m_rtAudio->startStream();}


	private:
		std::thread* m_thread;
		AudioManager();
		float * audioValue;
		const float *constAudioValue;
		RtAudio::StreamOptions *options;
		RtAudio *m_rtAudio;
		RtAudio::StreamParameters *param;
		unsigned int m_rtBufSize; 
		unsigned int m_bufSize; 
		CallbackData data;

		std::mutex mtx_audio;
		bool m_init;


		static int rtaudio_callback(void *outbuf, void *inbuf, 
				unsigned int nFrames, double streamtime, 
				RtAudioStreamStatus	status, void *userdata);

};

#endif

