
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

typedef struct
{
    float *buf;
    size_t len;
} ringbuffer_data;

typedef struct
{
    float* buf;
    volatile size_t write_ptr;
    volatile size_t read_ptr;
    size_t size;
    size_t size_mask;
    int mlocked;
    int writed;
}ringbuffer;

ringbuffer *ringbuffer_create(size_t size);
void ringbuffer_free(ringbuffer *rb);
size_t ringbuffer_read(ringbuffer *rb, float *dest, size_t cnt);
size_t ringbuffer_peek(ringbuffer *rb, float *dest, size_t cnt);
void ringbuffer_read_advance(ringbuffer *rb, size_t cnt);
size_t ringbuffer_read_space(const ringbuffer *rb);
int ringbuffer_mlock(ringbuffer *rb);
void ringbuffer_reset(ringbuffer *rb);
void ringbuffer_reset_size (ringbuffer * rb, size_t sz);
size_t ringbuffer_write(ringbuffer *rb, const float *src, size_t cnt);
void ringbuffer_write_advance(ringbuffer *rb, size_t cnt);
size_t ringbuffer_write_space(const ringbuffer *rb);

