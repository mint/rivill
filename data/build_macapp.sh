#install all dependencies through macports

#compile revil
./waf configure
./waf

#create app
cd build
dylibbundler -od -b -x ./revil.app/Contents/MacOS/revil -d ./revil.app/Contents/libs/ 
cp -R /opt/local/lib/OpenNI2 revil.app/Contents/libs/

