# Rivill

## Description
Rivill is an application for building and manipulating 3D SceneGraphs for 
Mixed-Reality Artistic Performances and Digital Mediation. 
It is based on OpenGL (glfw, glm), OpenNI2 
and is entirely controllable via OpenSoundControl messages. It relies on the
approach of revealing virtual content in the physical space by intersecting 
it with performers and spectator's bodies and props. This content can be 
used for visualisation, e.g. to augment digital performances or museum
cabinets, or for interaction, e.g. controlling sound with 3D widgets.

The principle is as follows :


![Rivill principle](doc/rivill_principle.png)


Rivill (previously named Revil) has been / is being 
used in the following projects :
* https://mint.univ-lille.fr/revealable-volume-displays.html
* https://mint.univ-lille.fr/revealed-interfaces.html
* https://terrev.univ-lille.fr/
* https://vibrating-shapes.univ-lille.fr/
* https://vimeo.com/126753974

## Software requirements:
* OpenGL >= 4.3 (or >=3 but there will be no revealing output)

## Hardware Requirements: 
* Depth Camera (one handled by OpenNI2)
* Projector 

## Compiling / Installing on GNU/Linux
* Install the following packages :
	* glm 
	* glfw >= 3.3
	* glew
	* FLTK (http://fltk.org)  >= 1.3.X
	* OpenNI2 (http://structure.io/openni)
	* OpenCV >=3 (http://opencv.org/) 
		with OpenCV Contrib aruco
	* xml++2
	* assimp
* ./waf configure
* ./waf 
* sudo ./waf install


## Compiling on Mac OSX
* Install command-line tools
* Install macports https://www.macports.org/install.php
* Open a terminal
* Install the following ports (sudo port install "port_name")
	* glfw
	* glm
	* glew
	* opencv3 +contrib
	* fltk
	* assimp
	* libusb
* Install OpenNI2
	* Download https://github.com/occipital/OpenNI2/archive/refs/heads/master.zip
	* Compile and install it :
		* make
		* make install
* Clone and compile the rivill source code
	* git clone https://gitlab.cristal.univ-lille.fr/fberthau/rivill.git
	* cd rivill
	* ./waf configure
	* ./waf 
* Start rivill
	* ./build/rivill
* To create a .app 
	* Install https://github.com/auriamg/macdylibbundler
	* In the rivill/build folder, type : 
		* dylibbundler -od -b -x ./rivill.app/Contents/MacOS/rivill -d ./rivill.app/Contents/libs/ 


## Uses
* OscPack (http://www.rossbencina.com/code/oscpack)
* Calibration code from https://github.com/Kj1/ofxProjectorKinectCalibration
* 1€ filter by Casiez et al. http://cristal.univ-lille.fr/~casiez/1euro/
* Signed distance functions from https://iquilezles.org/www/articles/distfunctions/distfunctions.htm

## Authors:
* Florent Berthaut, Assistant Professor, Université de Lille, CRIStAL/MINT
	(https://pro.univ-lille.fr/florent-berthaut)
* Alan Menit
* Boris Legal
* Anthony Beuchey
* Cagan Arslan
* Luka Claeys


